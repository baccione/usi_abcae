#
DOTRAIN 0 # necessary to disable this explicitly if this is not a training run
ADD2PYPATH 1
PYPATH /home/mbacci/sfw/abcae /home/mbacci/sfw/spux # first path will be at the very top, others will follow in order
#
FATAL_HYPER_PARAM_MISM 0
LEN_TIMESERIES  100
#
PRIORINFL prior_synth.in
DOSYN 1
