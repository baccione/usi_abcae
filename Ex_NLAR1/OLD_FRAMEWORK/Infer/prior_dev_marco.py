# specify prior distributions using stats.scipy for each parameter (independently)
# available options (univariate): https://docs.scipy.org/doc/scipy/reference/stats.html

from scipy import stats

from spux.distributions.tensor import Tensor
from spux.distributions.utils import logmeanstd

distributions = {}

# model parameters
distributions['alpha'] = stats.uniform(loc=4.2, scale=1.6)
distributions['sigma'] = stats.uniform(loc=0.005, scale=0.020)

# observational error model parameters
# distributions['error'] = stats.lognorm (**logmeanstd (logm=1, logs=1))

# construct a joint distribution for a vector of independent parameters by tensorization
prior = Tensor(distributions)
