#
import numpy as np
from main_abc_dev_marco import sampler
#
def ae_stats_new(observation):
    if type(observation) is not np.ndarray:
        x = observation.to_numpy().reshape(-1)
    else:
        x = observation.reshape(-1)

    # x.shape = (N, )

    x_for_ae = np.reshape(x, (1, len(x), 1))

    #####################################################
    # Auto-Encoder
    #####################################################
    z_latent_temp = sampler.encode(x_for_ae)
    z_latent = z_latent_temp.numpy()
    stats = z_latent.reshape(-1, 1)

    return stats


def ae_decode_new(z, noise_vector):

    z_for_decoder = np.reshape(z, (1, -1))
    noise_for_decoder = np.reshape(noise_vector,(1,len(noise_vector), 1))

    #####################################################
    # Auto-Encoder
    #####################################################
    x_reconst = sampler.decode((z_for_decoder, noise_for_decoder))[:, :, 0]

    return x_reconst
#
#
### Any attempt to use the class below fail due to a pickle errorr: TypeError: cannot pickle 'weakref' object, go figure ###
#
#class statistic_abc():
#
#    def __init__(self, sampler):
#        self.sampler = sampler
#
#    def __call__(self, observation):
#        self.ae_stats(observation)
#
#    def ae_stats(self, observation):
#        if type(observation) is not np.ndarray:
#            x = observation.to_numpy().reshape(-1)
#        else:
#            x = observation.reshape(-1)
#        x_for_ae = np.reshape(x, (1, len(x), 1))
#    
#        #####################################################
#        # Auto-Encoder
#        #####################################################
#        z_latent_temp = self.sampler.encode(x_for_ae)
#        z_latent = z_latent_temp.numpy()
#        stats = z_latent.reshape(-1, 1)
#    
#        return stats
#    
#    
#    def ae_decode(self, z, noise_vector):
#    
#        z_for_decoder = np.reshape(z, (1, -1))
#        noise_for_decoder = np.reshape(noise_vector,(1,len(noise_vector), 1))
#    
#        #####################################################
#        # Auto-Encoder
#        #####################################################
#        x_reconst = sampler.decode((z_for_decoder, noise_for_decoder))[:, :, 0]
#    
#        return x_reconst
