# # # # # # # # # # # # # # # # # # # # # # # # # #
# NLAR1 Model class
#
# Simone Ulzega
# ZHAW, Wädenswil
# simone.ulzega@zhaw.ch
# # # # # # # # # # # # # # # # # # # # # # # # # #

import pandas, numpy
from scipy import stats
#from spux.utils.annotate import annotate
from spux.utils import transforms
import time as tmprfl
import importlib

from spux.models.model import Model

class NLAR1(Model):
    """Class for NLAR1 model."""

    # construct model
    def __init__ (self, dt=1.0, verbosity=0, initial=None, constants=None, user_input=None):

        # set the specified time step size
        self.dt = dt
        self.verbosity = verbosity
        self.initial = initial
        self.constants = constants

        assert user_input is not None, ":: Fatal: I need to know."
        self.user_input = user_input
        try:
            self.user_module = importlib.import_module("src.modls."+self.user_input.umodlfl)
            self.user_model = getattr(self.user_module, self.user_input.umodlcl)
            print("User model: ",self.user_model)
        except:
            assert False, ":: Fatal: could not load the specifies user module. Hack me if you are know what you are doing, like, your model is superflex, it is not picklable, and you passing None here, and want to continue sfw dev."

    # initialize model using specified 'initial' and 'parameters'
    def init (self, initial, parameters):
        """Initialize model using specified 'initial' and 'parameters'."""

        # OPTIONAL: base class 'init (...)' method
        # Model.init(self, initial, parameters) # useless

        # store parameters required by the model
        self.alpha = parameters["alpha"]
        self.sigma = parameters["sigma"]

        # uset the initial time of the walk
        self.time = initial['time']

        # set the initial position of the walk
        self.position = initial['position']

    def initialize (self, parameters = None):
        """Automatically perform model state initialization using either 'laod (...)' or 'init (...)' methods."""

        if parameters is None: # to synthesize without overhead
            return

        # draw initial model state
        if self.initial is not None:
            # if specified, merge constants into parameters
            if self.constants is not None:
                initial = self.initial (parameters.append (self.constants))
            else:
                initial = self.initial (parameters)
            if hasattr (initial, 'draw'):
                initial = initial.draw (rng = self.rng)
        else:
            initial = None

        # if model 'state' is provided, load model directly
        if type (initial) == dict and 'state' in initial and type (initial ['state']) == self.state ():
            self.load (initial ['state'])
            if 'directory' in initial:
                source = os.path.join (initial ['directory'], 'statefiles')
                if os.path.exists (source):
                    # TODO: .update (...) functionality might be insufficient here..
                    self.sandbox.update (self.statefiles, source)

        # else, init model as usual (i.e. from 'initial' and 'parameters')
        else:
            self.init (initial, parameters)

    def __call__ (self, parameters, times):
        """Perform init and multiple consecutive runs (for each time in 'times') and return the results in a dictionary of outputs."""

        #start = tmprfl.time()

        self.print ('__call__', extras = {'parameters' : parameters, 'times' : times})

        # container for all seeds
        # seeds = {}

        # root seed
        # seed = self.seed
        # plant the model
        #print('spawning in model seed: ',seed(),seed.cumulative())
        #self.plant (seed.spawn (0, name='model stage'))
        #print('done spawning in model')
        # set initial model state
        self.initialize (parameters)

        # initialize data structs needed in loop and intial values
        ks = times
        vs = [None]*len(times)
        predictions = {k:v for k,v in zip(ks,vs)}
        #vs = [i for i in range(0,len(times))]
        #stages = {k:v for k,v in zip(ks,vs)}
        #vs = [seed.spawn (stages[time], name='model stage') for time in times]
        #seeds = {k:v for k,v in zip(ks,vs)} # I really do not think that this is required as there is no filtering

        # initialize initial values and check
        predictions [times [0]] = self.position
        assert ( times[-1] % self.dt == 0.0 ) # may not be strictly necessary, but then approx times open up...
        nsteps = int( (times[-1] - self.time) / self.dt ) # truncate, it's ok as long as assert above does not fail
        mdltms = numpy.linspace(self.time+self.dt,times[-1],nsteps)
        assert all(mdltms == times[1:]), ":: Fatal: mismatch model and obs times. Hack me if you know."
        rndvals = stats.norm.rvs(loc=0.0, scale=1.0, random_state=self.rng, size=nsteps) # pre-computing is not the same as one by one, amen... 29Mar21

        # initialize info container with success assumption
        info = {'successful' : True}

        #end = tmprfl.time()
        #print('first part of __call__ took: ',end-start) # less than 1 %of __call__time is spent in first part

        #start = tmprfl.time()
        for step in range(nsteps):

            #start = tmprfl.time()
            #if time is not None and self.verbosity:
            #    print ("Time", time)

            time = mdltms [step]

            # increment stage, set stage seed, and plant model stage - I do not deem we need it at each timestep in abc
            # self.plant (seeds [time])

            #end = tmprfl.time()
            #print('first part of __call__ loop took: ',end-start)

            #start = tmprfl.time()
            # run model to the specified time
            predictions [time] = self.run(rndvals [step])
            #end = tmprfl.time()
            #print('self.run part of __call__ loop took: ',end-start)

            #start = tmprfl.time()
            # check if the model prediction is valid
            if predictions [time] is None:
                info ['successful'] = False
                break
            # compute diagnostics (if specified) and append to info
            if hasattr (self, 'diagnostics') and self.diagnostics is not None:
                if self.verbosity:
                    print ("Computing diagnostics...")
                diagnostics = self.diagnostics (time, predictions [time], parameters, self.rng)
                transforms.append (info, time, diagnostics)

            # drop auxiliary predictions
            # predictions [time] = predictions [time] .values
            # check if the diagnostics are valid
            if not info ['successful']:
                if self.verbosity:
                    print (" :: WARNING: unsuccessful diagnostics in model.run (...).")
                    print ("  : -> stopping here and returning infos as they are.")
                break
            #end = tmprfl.time()
            #print('last part of __call__ loop took: ',end-start)

        #end = tmprfl.time()
        #print('loop part of __call__ took: ',end-start) # about 85% of the time of __call__ time is spent in the loop
        #start = tmprfl.time()
        self.time = time # withou this, it will never stop, 29Mar21

        # transform predictions to a dataframe
        frstvals = predictions[list(predictions.keys())[0]] #next(iter(predictions.values()))
        assert numpy.isscalar(frstvals), ":: Fatal: If you are not using a 1D model, you need to hack me."
        predictions = pandas.DataFrame(predictions.values(),index=predictions.keys(),columns=['position'])
        predictions.index.rename ('time', inplace = True)
        info ['predictions'] = predictions
        profile = None # legacy

        # save final model state (for future forecasting or sequential updating)
        #if info ['successful']:
        #    self.hibernate ()
        #info ["seeds"] = assemble.seeds (seeds)
        # no timing information
        #end = tmprfl.time()
        #print('last part of __call__ took: ',end-start) # about 85% of the time of __call__ time is spent in the loop
        # return predictions and info
        #return predictions, info, profile

        return info, profile

    # run model up to specified 'time' and return the annotated prediction
    def run(self, rndval):
        """Run model up to specified 'time' and return the annotated prediction."""

        #self.position = self.alpha * pow(self.position, 2) * (1 - self.position) +\
        #                self.sigma * rndval
        #                # self.sigma * stats.norm.rvs(loc=0.0, scale=1.0, random_state=self.rng)

        self.position = self.user_model(self.alpha,self.position,self.sigma,rndval)

        return self.position
        #return annotate([self.position],['position'],time)
