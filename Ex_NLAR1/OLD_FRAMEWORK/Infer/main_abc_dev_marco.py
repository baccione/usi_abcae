#############################################################
#                                                           #
# This is the reference script for inference part.          #
# The rest of the files are modules (in the fortran sense). #
# Still, this is script is to be imported and not run.      #
#                                                           #
#############################################################

#############################################################
if __name__ == '__main__':
    assert False, ":: Fatal: this script is not be to be run, only to be imported"
#############################################################

### imports - imported modules do not have global scope, ie, are not seen in the script that imports this script
import importlib
import numpy as np
import sys
import os
import tensorflow as tf
import logging
from src.tools.utils import ExpSetup
from dataset import dataset

### get input from command line
args = ExpSetup(0) # 0 means that it will read from command line or take the default

### some settings 
os.environ['TF_CPP_MIN_LOG_LEVEL'] = args.tf_cpp_min_log_level
os.environ['NUMEXPR_MAX_THREADS'] = args.numexpr_max_threads
tf.config.set_visible_devices([], 'GPU')
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
physical_devices = tf.config.list_physical_devices('GPU')
try:
    # Disable GPU(s)
    tf.config.set_visible_devices([], 'GPU')
    print('GPUs: ', tf.config.list_physical_devices('GPU'))
except:
    # Invalid device or cannot modify virtual devices once initialized.
    pass

varstats_file = args.varstats_file
varstats = np.loadtxt(varstats_file, dtype=float)
weights = np.reshape(1/np.sqrt(varstats), (-1, 1))
sampler_module = importlib.import_module("src.samplers."+args.samplrfl)
p_model_weights = args.logdir
basename = args.load_file_basename

### print some info
print('TF version: %s' % tf.__version__)
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
print(" ")
print("================================================================================")
print("Loading weights for Euclidean distance")
print("================================================================================")
print("Summary stats variance file: ", varstats_file)
print("Summary stats variance: ", varstats)
print("Weights for Euclidean distance: ", weights)
print("Sampler: ", args.samplrfl+'.'+args.samplrcl)
print('Model weights: ', p_model_weights)
print('Architecture: ',args.aem2load)
print("================================================================================")
print(" ")

### load and init what need to be loaded inited for spux main script
from weighted_euclidean_dev_marco import Weighted_Euclidean
weighted_euclidean = Weighted_Euclidean (order=2, diagnostics=['statistic', 'observations'], weights=weights)
sampler_class = getattr(sampler_module, args.samplrcl) 
sampler = sampler_class(logdir=p_model_weights, basename=basename, batch_size=None, args=args) # batch_size None? 23 Jun 2021
# Load dataset and test sampler
from dataset import dataset
data = dataset.iloc[:, 0].values
if len(data) < args.len_timeseries:
    print(":: Warning: data are shorter than timeseries. You are on a very narrow path likely to fail.")
    newdata = np.repeat(None,args.len_timeseries)
    for i in range(args.len_timeseries):
        j = i % len(data)
        newdata[i] = data[j]
elif len(data) > args.len_timeseries:
    print(":: Warning: data are longer than timeseries. You are on a very narrow path likely to fail.")
    newdata = data[0:args.len_timeseries]
else:
    newdata = data
assert sum([x == None for x in newdata]) == 0, ":: Fatal: None in values."
data_for_ae = np.reshape(newdata, (1, len(newdata), 1))
z_latent_test = sampler.encode(np.asarray(data_for_ae).astype('float32'))
print("*****************************************")
print("Test sum stats compression (AE) with dataset: ")
print(z_latent_test)
print("*****************************************")
#
#from statistic_new_ae_dev_marco import statistic_abc
#ae_statistics = statistic_abc(sampler) # create statistics class for distance - fails due to pickle error, go figure
