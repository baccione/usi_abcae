# specify distribution for the initial model state using stats.scipy
# available options (univariate): https://docs.scipy.org/doc/scipy/reference/stats.html
# for deterministic variables, use Deterministic from spux.library.distributions.deterministic

from scipy import stats
from spux.distributions.tensor import Tensor
from spux.distributions.deterministic import Deterministic

distributions = {}
distributions['time'] = Deterministic(0)
distributions['position'] = Deterministic(0.25)
# distributions['position'] = stats.norm(loc = 10, scale = 2)

distribution = Tensor(distributions)


def initial(parameters):

    return distribution
