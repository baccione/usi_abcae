#############################################################
if __name__ == '__main__':
    assert False, ":: Fatal: this script is not be to be run, only to be imported"
#############################################################

#############################################################
import numpy as np
from spux.distances.distance import Distance
from spux.utils import transforms
#############################################################

#############################################################
class Weighted_Euclidean(Distance):
    """A distance measure based on a specified norm (custom or from np.linalg.norm (...)) of the statistic vector."""

    def __init__(self, order=2, norm=None, diagnostics = [], weights=1):
        """If *norm* method is provided, it is applied on the difference of the statistics.
        Otherwise, if only *order* is provided, the np.linalg.norm (..., ord = order) is used instead.
        An optional *diagnostics* list of diagnostics to be included in info (irrespective of informative level) can be provided."""

        self.order = order
        self.norm = norm
        self.diagnostics = diagnostics
        self.weights = weights

    def __call__(self, parameters):
        """Evaluate Euclidean distance to the specified parameters."""

        if self.verbosity >= 2:
            print("Distance computation for parameters:")
            print(parameters)

        #timing = Timing(self.informative >= informative.levels ['timestamps'])
        #timing.start('evaluate')

        # setup, isolate, and plant the model (mirror the distance)
        # TODO: here, only 'setup' part is needed!
        #self.prepare ([self.model], mirror = True)
        self.setup(verbosity=0)

        # get times and snapshot flags
        times, snapshots = transforms.times (self.dataset, self.timeset)

        # contruct diagnostics function
        diagnostics = lambda time, prediction, parameters, rng : self.diagnose (time, prediction, parameters, rng, self.verbosity, snapshots, self.error)
        self.model.diagnostics = diagnostics

        # run model for specified times and obtain results
        #start = tmprfl.time()
        results, timings = self.executor.map ([self.model], args = [parameters, times], sandboxes = [self.sandbox], seeds = [self.seed])
        #end = tmprfl.time()
        #print(end - start)
        assert len(results)==1,":: Fatal: len of results is not 1. Hack me if you know that's ok."

        # extract predictions, infos (e.g. including observations) and profile from results
        #predictions, info, profile = results[0]
        info, profile = results[0]

        #info ['extime'] = end - start # you need to reactivate this to have performance data from run_dev_marco.sh

        # append executor timing
        #timing += self.executor.report ()

        # construct observations
        if self.error is not None:
            if 'observations' not in info:
                for time in times:
                    #_diagnostics = diagnostics (time, predictions.loc [time], parameters, self.rng)
                    _diagnostics = diagnostics (time, info ['predictions'].loc [time], parameters, self.rng)
                transforms.append (info, time, _diagnostics)
            observations = transforms.pandify (info ['observations'])
        else:
            observations = info ['predictions'] # predictions, plain predictions, no obervable operator

        info["statistic"] = self.statistic(observations)

        if self.norm is not None:
            distance = self.norm (info ["statistic"] - self.observed)
        else:
            distance = np.linalg.norm(np.multiply(self.weights, (info["statistic"] - self.observed)),
                                         ord=self.order)

        #timing.time('evaluate')

        if self.verbosity:
            print("Distance: {}".format(distance))

        # TODO: think again about the 'auxset' incorporation for the Distance classes

        #if self.informative < informative.levels ['infos']:
        if "statistic" not in self.diagnostics:
            assert False, "statistic not in self.diagnostics? And why so?"
            del info ["statistic"]
        if "observations" not in self.diagnostics:
            assert False, "observations not in self.diagnostics? And why so?"
            del info ["observations"]

        # assemble profile
        # profile = assemble.profile (self.informative, timing, timings, [profile])

        #return distance, predictions, info , profile
        return distance, info
