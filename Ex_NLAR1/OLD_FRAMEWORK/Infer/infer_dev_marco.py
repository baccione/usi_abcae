import os, sys
print("######")
print(sys.path)
print("######")
#if "AE_dev_marco" not in sys.path:
#    sys.path.append("/content/drive/MyDrive/rclone/USI/AE_dev_marco")
#if "SPUX_dev_marco" not in sys.path:
#    sys.path.append("/content/drive/MyDrive/rclone/USI/SPUX_dev_marco")
from initial_dev_marco import initial
from nlar1_dev_marco import NLAR1
from prior_dev_marco import prior
from spux import framework
from spux.samplers.sabc import SABC # this differs quite substantially between our branches
from dataset import dataset

# === Do all the needs to be done for AE by importing its main
from main_abc_dev_marco import weighted_euclidean as distance
from main_abc_dev_marco import args as user_input
#from statistic_ae import ae_stats
#from main_abc_dev_marco import ae_statistics # fails due to pickle error
from statistic_new_ae_dev_marco import ae_stats_new

# === BARRIER
from spux import framework
from spux.executors.mpi4py.connectors import utils
connector = utils.select ('legacy', verbosity = 1)
framework.barrier (connector = connector)

# === INIT FRAMEWORK
# MODEL
model = NLAR1 (dt=1.0,initial=initial,user_input=user_input)

# DISTANCE
#distance.assign (model, ae_statistics, dataset)           # fails due to pickle error, go figure
#distance.assign (model, ae_statistics.ae_stats, dataset)  # fails due to pickle error, go figure
distance.assign (model, ae_stats_new, dataset)

# SAMPLER
sampler = SABC (batchsize=500, chains=50, epsilon_init=1e3)
sampler.assign (distance, prior)

# FRAMEWORK
framework.assign (sampler)

# attach the specified number of parallel workers
distance.attach (workers=1)
sampler.attach (workers=1)

# setup SPUX framework
sandbox = None
from spux.utils.seed import Seed
seed = Seed (8)
outputdir='output_dev_marco'
framework.setup (sandbox= sandbox, seed=seed, verbosity=1, outputdir=outputdir, trace=0, informative=None)

# init SPUX framework
framework.init ()

# === SAMPLING
sampler.configure (lock = 1)
sampler.init ()

# generate samples from posterior distribution
sampler (1000) # number of batches = to this number divided by batchsize in configure

# === EXIT FRAMEWORK

framework.exit ()
