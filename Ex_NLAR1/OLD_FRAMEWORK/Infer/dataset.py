# # # # # # # # # # # # # # # # # # # # # # # # # #
# Automatically generated script for SPUX framework
# Owner: spux.utils.generate.synthesize(...)
# Timestamp: 2020-11-06 09:28:48
# # # # # # # # # # # # # # # # # # # # # # # # # #

import pandas, os
filename = os.path.join ("datasets", 'dataset.dat')
if os.path.exists (filename):
    dataset = pandas.read_csv (filename, sep=",", usecols=None, index_col=0)
else:
    dataset = None
