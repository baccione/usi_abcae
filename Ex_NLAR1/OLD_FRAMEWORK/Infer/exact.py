# # # # # # # # # # # # # # # # # # # # # # # # # #
# Automatically generated script for SPUX framework
# Owner: spux.utils.generate.synthesize(...)
# Timestamp: 2020-11-06 09:28:48
# # # # # # # # # # # # # # # # # # # # # # # # # #

import os
from spux.utils.io import loader

# dictionary for exact parameters and predictions
exact = {}

# exact parameters
exact['parameters'] = loader.txt("parameters.txt")

# exact predictions
exact['predictions'] = loader.csv(os.path.join ("datasets", 'predictions.dat'))

# exact auxiliary predictions
def auxloader (time):
    timestr = ('-' + str(time)) if time is not None else ''
    filename = 'predictions-auxiliary' + timestr + '.dat'
    if os.path.exists (os.path.join ("datasets", filename)):
        return loader.load (filename, "datasets", verbosity = 0)
    else:
        return None
exact['auxiliary'] = auxloader