# === LOADING

from spux.io import loader
samples, infos = loader.reconstruct (timingsfiles = None, directory="output_dev_marco")

# === DIAGNOSTICS

# burnin sample batch
burnin = 0

# plotting class
from spux.plot.mpl import MatPlotLib
plot = MatPlotLib (samples, infos, burnin = burnin)

# plot samples
plot.parameters ()

# plot evolution of acceptances
plot.acceptances ()

# === RESULTS (with removed burnin)

samples, infos = loader.tail (samples, infos, batch = burnin)
plot = MatPlotLib (samples, infos, burnin = burnin, tail = burnin)

# compute metrics
plot.metrics ()

# plot autocorrelations
plot.autocorrelations ()

# plot marginal posterior distributions
plot.posteriors ()

# TODO: plot marginal posterior distributions for the initial model values (see plot_config.py for priors)

# plot pairwise joint posterior distributions
plot.posteriors2d ()

plot.status ()
