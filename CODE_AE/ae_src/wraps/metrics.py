# Author: Firat Ozdemir, October 2019, firat.ozdemir@datascience.ch
# This file contains custom metrics: losses or scores

import tensorflow as tf


def r_squared_score(labels, predictions):
    '''Function computes the R^2 score'''
    l_flat = tf.reshape(labels, [-1])
    p_flat = tf.reshape(predictions, [-1])
    avg_label = tf.math.reduce_mean(l_flat, keepdims=True)
    ss_tot = tf.math.reduce_sum(tf.math.square(l_flat - avg_label), keepdims=False)
    ss_res = tf.math.reduce_sum(tf.math.squared_difference(l_flat, p_flat), keepdims=False)
    return 1.-(ss_res / ss_tot)
