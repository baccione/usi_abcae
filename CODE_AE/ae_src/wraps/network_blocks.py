#Author: Firat Ozdemir, October 2019, firat.ozdemir@datascience.ch
from __future__ import absolute_import, division, print_function, unicode_literals
import tensorflow as tf
import numpy as np
import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')

class Encoder_FC:
    '''Class for the encoder of the AE architecture.
    In BISTOM solar forecasting project, input is expected to be of shape: [batch_size, N]'''

    def __init__(self, input, l_units=None, scope='encoder', **kwargs):
        # TODO: put check for input tensor dim.
        self.scope = scope
        self.input_tmp = input
        self.fc_act = lambda x: tf.keras.activations.elu(x, alpha=1.0)
        self.ndims_latent = kwargs.get('ndims_latent', 3)
        self.l_units = l_units
        if l_units is None:
            self.l_units = [1000, 100, 10]
        if not (isinstance(self.l_units, list) or isinstance(self.l_units, tuple)):
            raise AssertionError('l_units needs to be a list or a tuple.')
        if len(self.l_units) == 0:
            raise AssertionError('l_units cannot be an empty array.')
        with tf.variable_scope(name_or_scope=self.scope):
            self.latent_space = self.build_model()
        self.model = tf.keras.Model(inputs=self.input, outputs=self.latent_space, name='Encoder')

    def dense(self, units, act=None, use_bias=True, kernel_init='glorot_uniform', name=None):
        return tf.keras.layers.Dense(units=units, activation=act, use_bias=use_bias, kernel_initializer=kernel_init,
                                     name=name)

    def build_model(self):
        self.input = tf.keras.Input(tensor=self.input_tmp, dtype=tf.float32, name='input')
        fc_ = self.dense(units=self.l_units[0], act=self.fc_act, name='fc1')(self.input)
        for i_ in range(1, len(self.l_units)):
            name = 'fc'+str(i_+1)
            fc_ = self.dense(units=self.l_units[i_], act=self.fc_act, name=name)(fc_)
        latent_space = self.dense(units=self.ndims_latent, act=None, name='latent_space')(fc_)
        return latent_space

class Encoder_LSTM:
    '''Class for the encoder of the AE architecture.
        In BISTOM solar forecasting project, input is expected to be of shape: [batch_size, N]'''

    def __init__(self, input, num_cells=1, scope='encoder', **kwargs):
        self.scope = scope
        self.input_tmp = input
        self.input = None
        self.num_rnn_cells = num_cells
        self.fc_act = lambda x: tf.keras.activations.elu(x, alpha=1.0)
        self.ndims_latent = kwargs.get('ndims_latent', 3)
        with tf.variable_scope(name_or_scope=self.scope):
            self.latent_space = self.build_model()
        self.model = tf.keras.Model(inputs=self.input, outputs=self.latent_space, name='Encoder')

    def build_model(self):
        extend_dims = tf.keras.layers.Reshape(target_shape=(-1, 1), name='extend_dims')
        # flatten_dims = tf.keras.layers.Reshape(target_shape=[-1], name='flatten_dims')
        self.input = tf.keras.Input(tensor=self.input_tmp, dtype=tf.float32, name='input')
        rnn = extend_dims(self.input)  # [batch_size, len_timeseries, 1]

        for i in range(self.num_rnn_cells):
            num_units = 16
            if i != (self.num_rnn_cells-1): # For cells before the last
                rnn = tf.keras.layers.LSTM(units=num_units, return_sequences=True, dtype=tf.float32, name='lstm_cell_%d' % (i + 1))(rnn)
            else: # For final cell
                rnn = tf.keras.layers.LSTM(units=num_units, return_sequences=False, dtype=tf.float32, name='lstm_cell_%d' % (i + 1))(rnn)

        latent_space = tf.keras.layers.Dense(units=self.ndims_latent, activation=None, name='latent_space')(rnn)
        return latent_space

class Encoder_Conv:
    '''Class for the encoder of the AE architecture.
    In BISTOM solar forecasting project, input is expected to be of shape: [batch_size, N]'''

    def __init__(self, input, scope='encoder', **kwargs):
        # TODO: put check for input tensor dim.
        self.scope = scope
        self.input_tmp = input
        # self.fc_act = lambda x: tf.keras.activations.elu(x, alpha=1.0)
        self.num_conv_filters = [[16, 16],[32, 32]]
        self.size_conv_kernel = 3
        self.strides_conv_layer = 1
        self.conv_act = tf.keras.activations.relu
        # self.conv_act = lambda x: tf.keras.activations.elu(x, alpha=1.0)
        self.ndims_latent = kwargs.get('ndims_latent', 3)
        with tf.variable_scope(name_or_scope=self.scope):
            self.latent_space = self.build_model()
        self.model = tf.keras.Model(inputs=self.input, outputs=self.latent_space, name='Encoder')

    def dense(self, units, act=None, use_bias=True, kernel_init='glorot_uniform', name=None):
        return tf.keras.layers.Dense(units=units, activation=act, use_bias=use_bias, kernel_initializer=kernel_init,
                                     name=name)
    def conv(self, filters, act=None, padding='valid', use_bias=True, kernel_init='glorot_uniform', strides=1, name=None):
        return tf.keras.layers.Conv1D(filters=filters, kernel_size=self.size_conv_kernel, activation=act,
                                      strides=strides, padding=padding, use_bias=use_bias,
                                      kernel_initializer=kernel_init, name=name)

    def build_model(self):
        extend_dims = tf.keras.layers.Reshape(target_shape=(-1, 1), name='extend_dims')
        flatten_dims = tf.keras.layers.Reshape(target_shape=[-1], name='flatten_dims')
        self.input = tf.keras.Input(tensor=self.input_tmp, dtype=tf.float32, name='input')
        conv = extend_dims(self.input) #[batch_size, len_timeseries, 1]
        for i in range(len(self.num_conv_filters)):
            if i != 0:
                conv = tf.keras.layers.MaxPool1D(pool_size=2, name='maxpool%d'%(i+1))(conv)
            for j in range(len(self.num_conv_filters[i])):
                conv = self.conv(filters=self.num_conv_filters[i][j], act=self.conv_act, strides=self.strides_conv_layer,
                                 name='conv%d_%d'%((i+1), (j+1)))(conv) #[batch_size, len_timeseries, num_conv_filters[-1]]

        conv = self.conv(filters=self.ndims_latent, act=None, name='final_conv')(conv)  # [batch_size, len_timeseries, ndims_latent]
        latent_space = tf.keras.layers.GlobalAveragePooling1D(name='global_avg_pool')(conv)
        return latent_space

class Encoder_Conv_FC_v2:
    '''Class for the encoder of the AE architecture.
    In BISTOM solar forecasting project, input is expected to be of shape: [batch_size, N]'''

    def __init__(self, input, scope='encoder', **kwargs):
        # TODO: put check for input tensor dim.
        self.scope = scope
        self.input_tmp = input
        self.fc_act = lambda x: tf.keras.activations.elu(x, alpha=1.0)
        self.num_conv_filters = [[16, 16],[32, 32], [64, 64], [16]]
        self.size_conv_kernel = 5
        self.strides_conv_layer = 1
        self.conv_act = lambda x: tf.keras.activations.elu(x, alpha=1.0)
        self.ndims_latent = kwargs.get('ndims_latent', 3)
        with tf.variable_scope(name_or_scope=self.scope):
            self.latent_space = self.build_model()
        self.model = tf.keras.Model(inputs=self.input, outputs=self.latent_space, name='Encoder')

    def dense(self, units, act=None, use_bias=True, kernel_init='glorot_uniform', name=None):
        return tf.keras.layers.Dense(units=units, activation=act, use_bias=use_bias, kernel_initializer=kernel_init,
                                     name=name)
    def conv(self, filters, act=None, padding='valid', use_bias=True, kernel_init='glorot_uniform', strides=1, name=None):
        return tf.keras.layers.Conv1D(filters=filters, kernel_size=self.size_conv_kernel, activation=act,
                                      strides=strides, padding=padding, use_bias=use_bias,
                                      kernel_initializer=kernel_init, name=name)

    def build_model(self):
        extend_dims = tf.keras.layers.Reshape(target_shape=(-1, 1), name='extend_dims')
        # flatten_dims = tf.keras.layers.Reshape(target_shape=[-1], name='flatten_dims')#somehow loading weights not happy with this. Need to hardcode..
        len_flat_tensor = self.input_tmp.shape.as_list()[1]
        for i_ in range(len(self.num_conv_filters)): # compute the length of the tensor after convolutions and maxpooling.
            len_flat_tensor -= (self.size_conv_kernel-1)*len(self.num_conv_filters[i_])
            if i_ != (len(self.num_conv_filters)-1): # no maxpooling after last conv block
                len_flat_tensor = len_flat_tensor//2
        len_flat_tensor = len_flat_tensor*self.num_conv_filters[-1][-1]
        flatten_dims = tf.keras.layers.Reshape(target_shape=[len_flat_tensor], name='flatten_dims')
        self.input = tf.keras.Input(tensor=self.input_tmp, dtype=tf.float32, name='input')
        conv = extend_dims(self.input) #[batch_size, len_timeseries, 1]
        for i in range(len(self.num_conv_filters)):
            if i != 0:
                conv = tf.keras.layers.MaxPool1D(pool_size=2, name='maxpool%d'%(i+1))(conv)
            for j in range(len(self.num_conv_filters[i])):
                conv = self.conv(filters=self.num_conv_filters[i][j], act=self.conv_act, strides=self.strides_conv_layer,
                                 name='conv%d_%d'%((i+1), (j+1)))(conv) #[batch_size, len_timeseries, num_conv_filters[-1]]

        self.fc1 = self.dense(units=100, act=self.fc_act, name='fc1')(flatten_dims(conv)) # size: [batch_size, 224] -> [batch_size, 100]
        self.fc2 = self.dense(units=10, act=self.fc_act, name='fc2')(self.fc1)
        latent_space = self.dense(units=self.ndims_latent, act=None, name='latent_space')(self.fc2)
        return latent_space

class Encoder_Conv_FC:
    '''Class for the encoder of the AE architecture.
    In BISTOM solar forecasting project, input is expected to be of shape: [batch_size, N]'''

    def __init__(self, input, scope='encoder', **kwargs):
        # TODO: put check for input tensor dim.
        self.scope = scope
        self.input_tmp = input
        self.fc_act = lambda x: tf.keras.activations.elu(x, alpha=1.0)
        self.num_conv_filters = 3
        self.size_conv_kernel = 1000
        self.strides_conv_layer = 10
        self.conv_act = lambda x: tf.keras.activations.elu(x, alpha=1.0)
        self.ndims_latent = kwargs.get('ndims_latent', 3)
        with tf.variable_scope(name_or_scope=self.scope):
            self.latent_space = self.build_model()
        self.model = tf.keras.Model(inputs=self.input, outputs=self.latent_space, name='Encoder')

    def dense(self, units, act=None, use_bias=True, kernel_init='glorot_uniform', name=None):
        return tf.keras.layers.Dense(units=units, activation=act, use_bias=use_bias, kernel_initializer=kernel_init,
                                     name=name)
    def conv(self, filters, act=None, padding='valid', use_bias=True, kernel_init='glorot_uniform', strides=1, name=None):
        return tf.keras.layers.Conv1D(filters=filters, kernel_size=self.size_conv_kernel, activation=act,
                                      strides=strides, padding=padding, use_bias=use_bias,
                                      kernel_initializer=kernel_init, name=name)

    def build_model(self):
        extend_dims = tf.keras.layers.Reshape(target_shape=(-1, 1), name='extend_dims')
        flatten_dims = tf.keras.layers.Reshape(target_shape=(-1, 1), name='flatten_dims')
        self.input = tf.keras.Input(tensor=self.input_tmp, dtype=tf.float32, name='input')
        self.conv1 = self.conv(filters=self.num_conv_filters, act=self.conv_act, strides=self.strides_conv_layer,
                               name='conv1')(extend_dims(self.input))
        self.fc1 = self.dense(units=1000, act=self.fc_act, name='fc1')(flatten_dims(self.conv1))
        self.fc2 = self.dense(units=100, act=self.fc_act, name='fc2')(self.fc1)
        self.fc3 = self.dense(units=10, act=self.fc_act, name='fc3')(self.fc2)
        latent_space = self.dense(units=self.ndims_latent, act=self.fc_act, name='latent_space')(self.fc3)
        return latent_space


class Decoder_Alpha_Epsilon:
    '''Class for the decoder of the AE architecture'''

    def __init__(self, latent_alpha, latent_epsilon, input, len_timeseries, scope='decoder', **kwargs):
        self.scope = scope
        # self.latent_input = latent_input
        # self.output_shape = output_shape
        self.len_timeseries = len_timeseries
        self.num_conv_filters = 3
        self.size_conv_kernel = 3
        self.conv_act = None
        self.fc_act = lambda x: tf.keras.activations.elu(x, alpha=1.0)
        with tf.variable_scope(name_or_scope=self.scope):
            self.input = tf.keras.Input(tensor=input, dtype=tf.float32, name='input')
            self.latent_alpha = tf.keras.Input(tensor=latent_alpha, dtype=tf.float32, name='latent_alpha')
            self.latent_epsilon = tf.keras.Input(tensor=latent_epsilon, dtype=tf.float32, name='latent_epsilon')
            self.output = self.build_model()
        self.model = tf.keras.Model(inputs=[self.latent_alpha, self.latent_epsilon, self.input], outputs=self.output, name='Decoder')

    def dense(self, units, act=None, use_bias=True, kernel_init='glorot_uniform', name=None):
        # TODO: look into typical decoder structure in FC autoencoders
        return tf.keras.layers.Dense(units=units, activation=act, use_bias=use_bias, kernel_initializer=kernel_init,
                                     name=name)

    def conv(self, filters, act=None, padding='valid', use_bias=True, kernel_init='glorot_uniform', name=None):
        return tf.keras.layers.Conv1D(filters=filters, kernel_size=self.size_conv_kernel, activation=act,
                                      padding=padding, use_bias=use_bias, kernel_initializer=kernel_init, name=name)

    def build_model(self):

        extend_dims = tf.keras.layers.Reshape(target_shape=(-1, 1), name='extend_dims')

        self.fc1_alpha = self.dense(units=100, act=self.fc_act, name='fc1_alpha')
        self.fc2_alpha = self.dense(units=1000, act=self.fc_act, name='fc2_alpha')
        self.pred_alpha = self.dense(units=self.len_timeseries, act=None, name='pred_alpha')
        self.fc1_epsilon = self.dense(units=100, act=self.fc_act, name='fc1_epsilon')
        self.fc2_epsilon = self.dense(units=1000, act=self.fc_act, name='fc2_epsilon')
        self.pred_epsilon = self.dense(units=self.len_timeseries, act=None, name='pred_epsilon')

        pre_pn_alpha = self.pred_alpha(self.fc2_alpha(self.fc1_alpha(self.latent_alpha)))
        pre_pn_epsilon = self.pred_epsilon(self.fc2_epsilon(self.fc1_epsilon(self.latent_epsilon)))  # size: [bs, N]

        ## Run a few blocks of conv(ks=3) on input, then fuse with alpha and epsilon predictions
        # pad the input to account for first and last items disappearing after conv(ks=3)
        input_concat_op = tf.keras.layers.Concatenate(axis=1, name='pad_pn_for_conv_filter')
        self.input_concat = input_concat_op([self.input[:, 0:1], self.input, self.input[:, -1:]])  # size: [bs, N+2]
        self.input_concat_conv_fr = extend_dims(self.input_concat)  # size [bs, N+2, 1]
        self.conv1 = self.conv(filters=self.num_conv_filters, act=self.conv_act, name='conv1')(self.input_concat_conv_fr)  # size: [bs, N, 3]
        rshp_conv_map = tf.keras.layers.Reshape(target_shape=[self.num_conv_filters], name='reshape_conv')

        #         slicer_op = tf.keras.layers.Lambda(lambda x: x[0][:,x[1]:x[1]+1,...], name='slice_op')

        self.fuse_alpha_with_p_fc1 = self.dense(units=10, act=self.fc_act, name='fuse_alpha_p_fc1')
        self.fuse_alpha_with_p_fc2 = self.dense(units=10, act=self.fc_act, name='fuse_alpha_p_fc2')
        self.fuse_alpha_with_p_pred = self.dense(units=1, act=None, name='fuse_alpha_p_pred')
        fuse_alpha = lambda x: self.fuse_alpha_with_p_pred(self.fuse_alpha_with_p_fc2(self.fuse_alpha_with_p_fc1(x)))
        concat_a_p_tmp = tf.keras.layers.Concatenate(axis=1, name='concat_latent_alpha_and_pn_input')
        #         slicer_a = tf.keras.layers.Lambda(lambda x: concat_a_p_tmp([slicer_op((x[0], x[2])), rshp_conv_map(slicer_op((x[1],x[2])))]), name='concat_pre_alpha_and_p') #x = (t1, t2, ind)
        slicer_a = tf.keras.layers.Lambda(
            lambda x: concat_a_p_tmp([x[0][:, x[2]:x[2] + 1], rshp_conv_map(x[1][:, x[2]:x[2] + 1, :])]),
            name='concat_pre_alpha_and_p')  # x = (t1, t2, ind)

        l_pred_alpha = []
        with tf.variable_scope(name_or_scope='concat_pn'):
            for ind_p in range(self.len_timeseries):
                concat_a_p = slicer_a((pre_pn_alpha, self.conv1, ind_p))
                #             concat_a_p = concat_a_p_op([pre_pn_alpha[:,ind_p:ind_p+1], rshp_conv_map(self.conv1[:,ind_p:ind_p+1,:])])
                a_tmp = fuse_alpha(concat_a_p)
                l_pred_alpha.append(a_tmp)
            pred_alpha_timeseries = tf.keras.layers.Concatenate(axis=1, name='stack_fused_a_p')(l_pred_alpha)

        self.fuse_epsilon_with_p_fc1 = self.dense(units=10, act=self.fc_act, name='fuse_epsilon_p_fc1')
        self.fuse_epsilon_with_p_fc2 = self.dense(units=10, act=self.fc_act, name='fuse_epsilon_p_fc2')
        self.fuse_epsilon_with_p_pred = self.dense(units=1, act=None, name='fuse_epsilon_p_pred')
        fuse_epsilon = lambda x: self.fuse_epsilon_with_p_pred(self.fuse_epsilon_with_p_fc2(self.fuse_epsilon_with_p_fc1(x)))
        #         concat_e_p_op = tf.keras.layers.Concatenate(axis=1, name='concat_latent_epsilon_and_pn_input')
        concat_e_p_tmp = tf.keras.layers.Concatenate(axis=1, name='concat_latent_epsilon_and_pn_input')
        #         slicer_e = tf.keras.layers.Lambda(lambda x: concat_e_p_tmp([slicer_op((x[0], x[2])), rshp_conv_map(slicer_op((x[1],x[2])))]), name='concat_pre_epsilon_and_p') #x = (t1, t2, ind)
        slicer_e = tf.keras.layers.Lambda(
            lambda x: concat_e_p_tmp([x[0][:, x[2]:x[2] + 1], rshp_conv_map(x[1][:, x[2]:x[2] + 1, :])]),
            name='concat_pre_epsilon_and_p')  # x = (t1, t2, ind)

        l_pred_epsilon = []
        with tf.variable_scope(name_or_scope='concat_pn'):
            for ind_p in range(self.len_timeseries):
                concat_e_p = slicer_e((pre_pn_epsilon, self.conv1, ind_p))
                #             concat_e_p = concat_e_p_op([pre_pn_epsilon[:, ind_p:ind_p + 1], rshp_conv_map(self.conv1[:,ind_p:ind_p+1,:])])
                e_tmp = fuse_epsilon(concat_e_p)
                l_pred_epsilon.append(e_tmp)
            pred_epsilon_timeseries = tf.keras.layers.Concatenate(axis=1, name='stack_fused_e_p')(l_pred_epsilon)

        pred_final = tf.keras.layers.Concatenate(axis=-1)(
            [extend_dims(pred_alpha_timeseries), extend_dims(pred_epsilon_timeseries)])
        return pred_final

        # rshp_op = tf.keras.layers.Reshape(target_shape=(self.num_conv_filters, 1))

        # l_pred_alpha, l_pred_epsilon = [], []
        # concat_a_p = tf.keras.layers.Concatenate(axis=1, name='concat_latent_alpha_and_pn_input')
        # concat_e_p = tf.keras.layers.Concatenate(axis=1, name='concat_latent_epsilon_and_pn_input') #TODO: not sure if using the same concat can have any adverse affect (e.g., less parallelization)
        # #TODO: double check layers are being reused for different indices.
        # for ind_p in range(self.len_timeseries):
        #     self.concat_alpha_and_pn = concat_a_p([self.latent_alpha, rshp_op(self.conv1[:,ind_p:ind_p+1,...])])
        #     self.concat_epsilon_and_pn = concat_e_p([self.latent_epsilon, rshp_op(self.conv1[:, ind_p:ind_p + 1, ...])])
        #     alpha_p_tmp = self.pred_alpha(self.fc2_alpha(self.fc1_alpha(self.concat_alpha_and_pn)))
        #     epsilon_p_tmp = self.pred_epsilon(self.fc2_epsilon(self.fc1_epsilon(self.concat_epsilon_and_pn)))
        #     l_pred_alpha.append(alpha_p_tmp)
        #     l_pred_epsilon.append(epsilon_p_tmp)
        # pred_alpha_timeseries = tf.keras.layers.Concatenate(axis=)


class Decoder_FC:
    '''Class for the decoder of the AE architecture'''

    def __init__(self, latent_input, input, len_timeseries, scope='decoder', **kwargs):
        self.scope = scope
        # assert len(out_shape) == 2 #expect out_shape to have [len_timeseries , num_channel_outputs] format
        self.len_timeseries = len_timeseries
        self.num_input_channels = kwargs.get('num_input_channels', 1)  # #input_channels
        self.num_channels = kwargs.get('num_channels', 2) # #output_channels
        self.name_channels = kwargs.get('name_channels', None) # Output channels can be named
        if self.name_channels is not None:
            if not isinstance(self.name_channels, list):
                raise AssertionError('argument name_channels needs to be a list.')
            if len(self.name_channels) != self.num_channels:
                raise AssertionError('#name_channels and num_channels must be the same.')
        self.num_conv_filters = 3
        self.size_conv_kernel = 3
        self.conv_act = None
        self.fc_act = lambda x: tf.keras.activations.elu(x, alpha=1.0)
        with tf.variable_scope(name_or_scope=self.scope):
            self.input = tf.keras.Input(tensor=input, dtype=tf.float32, name='input')
            #mean, var = tf.nn.moments(x=input, axes=[1], keepdims=True)
            #input_whitened = (input - mean) / var # whitening of timeseries data since we use it as a source of noise.
            #self.input = tf.keras.Input(tensor=input_whitened, dtype=tf.float32, name='input')
            self.latent_input = tf.keras.Input(tensor=latent_input, dtype=tf.float32, name='latent_input')
            # self.latent_alpha = tf.keras.Input(tensor=latent_alpha, dtype=tf.float32, name='latent_alpha')
            # self.latent_epsilon = tf.keras.Input(tensor=latent_epsilon, dtype=tf.float32, name='latent_epsilon')
            self.output = self.build_model()
        self.model = tf.keras.Model(inputs=[self.latent_input, self.input], outputs=self.output, name='Decoder')

    def dense(self, units, act=None, use_bias=True, kernel_init='glorot_uniform', name=None):
        # TODO: look into typical decoder structure in FC autoencoders
        return tf.keras.layers.Dense(units=units, activation=act, use_bias=use_bias, kernel_initializer=kernel_init,
                                     name=name)

    def conv(self, filters, act=None, padding='valid', use_bias=True, kernel_init='glorot_uniform', name=None):
        return tf.keras.layers.Conv1D(filters=filters, kernel_size=self.size_conv_kernel, activation=act,
                                      padding=padding, use_bias=use_bias, kernel_initializer=kernel_init, name=name)
    def conv1(self, filters, act=None, padding='valid', use_bias=True, kernel_init='glorot_uniform', name=None):
        return tf.keras.layers.Conv1D(filters=filters, kernel_size=(1), activation=act,
                                      padding=padding, use_bias=use_bias, kernel_initializer=kernel_init, name=name)

    def build_model(self):

        extend_dims_to_2 = tf.keras.layers.Reshape(target_shape=(-1, self.num_input_channels), name='extend_dims') #note bs dim is kept as is

        self.fc1 = self.dense(units=100, act=self.fc_act, name='fc1')
        self.fc2 = self.dense(units=1000, act=self.fc_act, name='fc2')
        self.pred_tmp = self.dense(units=self.len_timeseries * self.num_channels, act=None, name='pred')

        pre_preds = self.pred_tmp(self.fc2(self.fc1(self.latent_input))) # size: [bs, N * #channels]

        ## Run a few blocks of conv(ks=3) on input, then fuse with alpha and epsilon predictions
        # pad the input to account for first and last items disappearing after conv(ks=3)
        input_concat_op = tf.keras.layers.Concatenate(axis=1, name='pad_pn_for_conv_filter')
        self.input_concat = input_concat_op([self.input[:, 0:1, ...], self.input, self.input[:, -1:, ...]])  # size: [bs, N+2] #pad the f_n input by one element at both ends.
        self.input_concat_conv_fr = extend_dims_to_2(self.input_concat)  # size [bs, N+2, self.num_input_channels]

        # Merge conv output with pre_preds
        concat_channel_dim = tf.keras.layers.Concatenate(axis=2, name='concat_in_channel_dim')

        # Now the conv kernels that learn to combine (p_i-1, p_i, p_i+1) for conditioning for alpha_i and epsilon_i predictions
        l_pred_ch_timeseries = []
        for i_ in range(self.num_channels):
            if self.name_channels is not None:
                name = 'conv_'+self.name_channels[i_]
            else:
                name = 'conv_ch'+str(i_)
            conv_ = self.conv(filters=self.num_conv_filters, act=self.conv_act, name=name)(self.input_concat_conv_fr)  # size: [bs, N, 3] # learn conv kernels for alpha

            _pre_pred = extend_dims_to_2(pre_preds[:, :self.len_timeseries])  # size[ bs, N, self.num_input_channels]
            _merged_pre_pred = concat_channel_dim([_pre_pred, conv_])  # size [bs, N, 4]

            if self.name_channels is not None:
                name = 'fuse_'+self.name_channels[i_]
            else:
                name = 'fuse_ch'+str(i_)
            _fuse_ch_i_with_x_fc1 = self.conv1(filters=10, act=self.fc_act, name=name + '_x_fc1') #use conv1d(ksize=1) instead of dense blocks for each cropped section.
            _fuse_ch_i_with_x_fc2 = self.conv1(filters=10, act=self.fc_act, name=name + '_x_fc2')
            _fuse_ch_i_with_x_pred = self.conv1(filters=1, act=None, name=name + '_x_pred')
            fuse_ch_i = lambda x: _fuse_ch_i_with_x_pred(_fuse_ch_i_with_x_fc2(_fuse_ch_i_with_x_fc1(x)))
            pred_ch_i_timeseries = fuse_ch_i(_merged_pre_pred)  # shape: [bs, N, 1]
            l_pred_ch_timeseries.append(pred_ch_i_timeseries)

        pred_final = concat_channel_dim(l_pred_ch_timeseries)
        pred_final = tf.keras.layers.Reshape(target_shape=[self.len_timeseries, self.num_channels], name='Output_Shape')(pred_final)

        return pred_final

class Decoder_LSTM:
    '''Class for the decoder of the AE architecture. Uses RNN to reconstruct timeseries signal.'''

    def __init__(self, latent_input, input, len_timeseries, num_cells=1, scope='decoder', **kwargs):
        self.scope = scope
        self.len_timeseries = len_timeseries
        self.ndims_latent = latent_input.shape.as_list()[-1]
        self.num_channels = kwargs.get('num_channels', 2)
        self.name_channels = kwargs.get('name_channels', None)  # Output channels can be named
        self.num_rnn_cells = num_cells
        self.is_on_GPU = kwargs.get('is_on_GPU', False)
        if self.name_channels is not None:
            if not isinstance(self.name_channels, list):
                raise AssertionError('argument name_channels needs to be a list.')
            if len(self.name_channels) != self.num_channels:
                raise AssertionError('#name_channels and num_channels must be the same.')
        self.fc_act = lambda x: tf.keras.activations.elu(x, alpha=1.0)
        with tf.variable_scope(name_or_scope=self.scope):
            # Input will be a timeseries. We reshape and tile latent_dims to have the form [batch_size, len_timeseries, ndims_latent + noise]
            if len(input.shape.as_list()) == 2: #if it's a timeseries element (size [batch_size, len_timeseries]
                tseries_input = tf.expand_dims(input, axis=-1)
            elif len(input.shape.as_list()) == 3: #it's a multidimensional timeseries.
                tseries_input = input #
            else:
                raise AssertionError('Unexpected input timeseries shape: %s' % str(input.shape.as_list()))
            l_input = tf.tile(tf.expand_dims(latent_input, axis=1), multiples=[1, self.len_timeseries, 1])
            input = tf.concat([l_input, tseries_input], axis=-1, name='merge_tseries_and_ldims')
            self.input = tf.keras.Input(tensor=input, dtype=tf.float32, name='broadcasted_fused_input')
            # self.latent_input = tf.keras.Input(tensor=latent_input, dtype=tf.float32, name='latent_input')
            self.output = self.build_model()
        self.model = tf.keras.Model(inputs=self.input, outputs=self.output, name='Decoder')

    def dense(self, units, act=None, use_bias=True, kernel_init='glorot_uniform', name=None):
        # TODO: look into typical decoder structure in FC autoencoders
        return tf.keras.layers.Dense(units=units, activation=act, use_bias=use_bias, kernel_initializer=kernel_init,
                                     name=name)
    def build_model(self):
        rnn = self.input
        for i in range(self.num_rnn_cells):
            num_units = 16
            if self.is_on_GPU:
                rnn = tf.compat.v1.keras.layers.CuDNNLSTM(units=num_units, return_sequences=True, dtype=tf.float32, name='lstm_cell_%d' % (i + 1))(rnn)
            else:
                rnn = tf.keras.layers.LSTM(units=num_units, return_sequences=True, dtype=tf.float32, name='lstm_cell_%d'%(i+1))(rnn)
        pre_preds = tf.keras.layers.Dense(units=self.num_channels, activation=None, name='pred')(rnn)
        pred_final = tf.keras.layers.Reshape(target_shape=[self.len_timeseries, self.num_channels], name='Output_Shape')(pre_preds)

        return pred_final

class Decoder_Concat_FC:
    '''Class for the decoder of the AE architecture'''

    def __init__(self, latent_input, input, len_timeseries, scope='decoder', **kwargs):
        self.scope = scope
        self.len_timeseries = len_timeseries
        self.num_channels = kwargs.get('num_channels', 2)
        self.name_channels = kwargs.get('name_channels', None) # Output channels can be named
        if self.name_channels is not None:
            if not isinstance(self.name_channels, list):
                raise AssertionError('argument name_channels needs to be a list.')
            if len(self.name_channels) != self.num_channels:
                raise AssertionError('#name_channels and num_channels must be the same.')
        if self.len_timeseries > 1000:
            logging.warning('\n\n\n!!!!!!!!!!\nWarning: len_timeseries is rather large -> #parameters will be huge!! \n!!!!!!!!!!!!\n\n\n\n')
        self.fc_act = lambda x: tf.keras.activations.elu(x, alpha=1.0)
        with tf.variable_scope(name_or_scope=self.scope):
            self.input = tf.keras.Input(tensor=input, dtype=tf.float32, name='input')
            self.latent_input = tf.keras.Input(tensor=latent_input, dtype=tf.float32, name='latent_input')
            self.output = self.build_model()
        self.model = tf.keras.Model(inputs=[self.latent_input, self.input], outputs=self.output, name='Decoder')

    def dense(self, units, act=None, use_bias=True, kernel_init='glorot_uniform', name=None):
        # TODO: look into typical decoder structure in FC autoencoders
        return tf.keras.layers.Dense(units=units, activation=act, use_bias=use_bias, kernel_initializer=kernel_init,
                                     name=name)

    def build_model(self):

        concat_Z_and_X_op = tf.keras.layers.Concatenate(axis=1, name='concat_Z_and_X_op')
        self.concat_Z_and_X = concat_Z_and_X_op([self.latent_input, self.input])

        self.fc1 = self.dense(units=self.len_timeseries, act=self.fc_act, name='fc1')
        self.fc2 = self.dense(units=self.len_timeseries, act=self.fc_act, name='fc2')
        self.pred_tmp = self.dense(units=self.len_timeseries * self.num_channels, act=None, name='pred')

        pre_preds = self.pred_tmp(self.fc2(self.fc1(self.concat_Z_and_X))) # size: [bs, N * #channels]

        pred_final = tf.keras.layers.Reshape(target_shape=[self.len_timeseries, self.num_channels], name='Output_Shape')(pre_preds)

        return pred_final

class Decoder_FC_Conv:
    '''Class for the decoder of the AE architecture'''

    def __init__(self, latent_input, input, len_timeseries, scope='decoder', **kwargs):
        self.scope = scope
        # self.latent_input = latent_input
        # self.output_shape = output_shape
        # assert len(out_shape) == 2 #expect out_shape to have [len_timeseries , num_channel_outputs] format
        self.len_timeseries = len_timeseries
        self.num_channels = kwargs.get('num_channels', 2)
        if self.num_channels != 2:
            raise NotImplementedError('Decoder network is built strictly for alpha and epsilon outputs. Source code needs to be modified for different #output_channels')
        self.num_conv_filters = 3
        self.size_conv_kernel = 3
        self.conv_act = None
        self.deconv_act = lambda x: tf.keras.activations.elu(x, alpha=1.0)
        self.fc_act = lambda x: tf.keras.activations.elu(x, alpha=1.0)
        with tf.variable_scope(name_or_scope=self.scope):
            self.input = tf.keras.Input(tensor=input, dtype=tf.float32, name='input')
            self.latent_input = tf.keras.Input(tensor=latent_input, dtype=tf.float32, name='latent_input')
            # self.latent_alpha = tf.keras.Input(tensor=latent_alpha, dtype=tf.float32, name='latent_alpha')
            # self.latent_epsilon = tf.keras.Input(tensor=latent_epsilon, dtype=tf.float32, name='latent_epsilon')
            self.output = self.build_model()
        self.model = tf.keras.Model(inputs=[self.latent_input, self.input], outputs=self.output, name='Decoder')

    def dense(self, units, act=None, use_bias=True, kernel_init='glorot_uniform', name=None):
        # TODO: look into typical decoder structure in FC autoencoders
        return tf.keras.layers.Dense(units=units, activation=act, use_bias=use_bias, kernel_initializer=kernel_init,
                                     name=name)

    def conv(self, filters, act=None, padding='valid', use_bias=True, kernel_init='glorot_uniform', name=None):
        return tf.keras.layers.Conv1D(filters=filters, kernel_size=self.size_conv_kernel, activation=act,
                                      padding=padding, use_bias=use_bias, kernel_initializer=kernel_init, name=name)
    def conv1(self, filters, act=None, padding='valid', use_bias=True, kernel_init='glorot_uniform', name=None):
        return tf.keras.layers.Conv1D(filters=filters, kernel_size=(1), activation=act,
                                      padding=padding, use_bias=use_bias, kernel_initializer=kernel_init, name=name)

    def deconv(self, input, filters, upsample_factor=2, act=None, padding='same', use_bias=True, kernel_init='glorot_uniform', name=None):
        upsampled = tf.keras.layers.UpSampling1D(size=upsample_factor, name=name+'_upsample')(input)
        dcf = self.conv(filters=filters, act=act, padding=padding, use_bias=use_bias, kernel_init=kernel_init, name=name+'_conv')(upsampled)
        return tf.keras.layers.Lambda(lambda x: dcf(upsampled(x)), name=name)(input)

    def build_model(self):

        extend_dims_to_2 = tf.keras.layers.Reshape(target_shape=(-1, 1), name='extend_dims') #note bs dim is kept as is

        self.fc1 = self.dense(units=100, act=self.fc_act, name='fc1')(self.latent_input)
        self.fc2 = self.dense(units=1000 * self.num_channels, act=self.fc_act, name='fc2')(self.fc1)
        self.prepare_for_conv = tf.keras.layers.Reshape(target_shape=(-1, self.num_channels), name='split_to_channels')(self.fc2)
        upsampling_factor = int(np.ceil(float(self.len_timeseries) / 1000))
        residual_ = (upsampling_factor * 1000) % self.len_timeseries
        if residual_ > 0: #if upsampling do not exactly match desired time series length, crop the corresponding mid section from the tensor.
            self.pred_tmp_ = self.deconv(input=self.prepare_for_conv, filters=self.num_channels, upsample_factor=upsampling_factor,
                                        act=self.deconv_act, padding='same', name='pred_not_cropped')
            pre_crop = np.floor(residual_.astype(np.float)/2)
            post_crop = np.ceil(residual_.astype(np.float) / 2)
            self.pred_tmp = tf.keras.layers.Lambda(lambda x: x[:,pre_crop:-1-post_crop,...], name='pred')(self.pred_tmp_)
        else:
            self.pred_tmp = self.deconv(input=self.prepare_for_conv, filters=self.num_channels, upsample_factor=upsampling_factor,
                                        act=self.deconv_act, padding='same', name='pred')
        # self.pred_tmp = self.dense(units=self.len_timeseries * self.num_channels, act=None, name='pred')

        # pre_preds = self.pred_tmp # size: [bs, N * #channels]
        # alpha_pre_pred = extend_dims_to_2(pre_preds[:, :self.len_timeseries]) #size[ bs, N, 1]
        # epsilon_pre_pred = extend_dims_to_2(pre_preds[:, self.len_timeseries:]) #size [bs, N, 1]
        alpha_pre_pred = self.pred_tmp[...,0:1]
        epsilon_pre_pred = self.pred_tmp[...,1:]

        ## Run a few blocks of conv(ks=3) on input, then fuse with alpha and epsilon predictions
        # pad the input to account for first and last items disappearing after conv(ks=3)
        input_concat_op = tf.keras.layers.Concatenate(axis=1, name='pad_pn_for_conv_filter')
        self.input_concat = input_concat_op([self.input[:, 0:1], self.input, self.input[:, -1:]])  # size: [bs, N+2] #pad the f_n input by one element at both ends.
        self.input_concat_conv_fr = extend_dims_to_2(self.input_concat)  # size [bs, N+2, 1]
        # Now the conv kernels that learn to combine (p_i-1, p_i, p_i+1) for conditioning for alpha_i and epsilon_i predictions
        self.conv_alpha = self.conv(filters=self.num_conv_filters, act=self.conv_act, name='conv_alpha')(self.input_concat_conv_fr)  # size: [bs, N, 3] # learn conv kernels for alpha
        self.conv_epsilon = self.conv(filters=self.num_conv_filters, act=self.conv_act, name='conv_epsilon')(self.input_concat_conv_fr)  # size: [bs, N, 3] # learn conv kernels for epsilon

        # Merge conv output with pre_preds (self.pred_tmp)
        concat_channel_dim = tf.keras.layers.Concatenate(axis=2, name='concat_in_channel_dim')
        alpha_merged_pre_pred = concat_channel_dim([alpha_pre_pred, self.conv_alpha]) #size [bs, N, 4]
        epsilon_merged_pre_pred = concat_channel_dim([epsilon_pre_pred, self.conv_epsilon])

        # dense layers estimations with conv kernels to go from 4 channels to 1 channel (4: [conv_alpha, alpha_pre_pred] -> 1: [alpha_pred])
        self.fuse_alpha_with_p_fc1 = self.conv1(filters=10, act=self.fc_act, name='fuse_alpha_p_fc1') #use conv1d(ksize=1) instead of dense blocks for each cropped section.
        self.fuse_alpha_with_p_fc2 = self.conv1(filters=10, act=self.fc_act, name='fuse_alpha_p_fc2')
        self.fuse_alpha_with_p_pred = self.conv1(filters=1, act=None, name='fuse_alpha_p_pred')
        # self.fuse_alpha_with_p_fc1 = self.dense(units=10, act=self.fc_act, name='fuse_alpha_p_fc1')
        # self.fuse_alpha_with_p_fc2 = self.dense(units=10, act=self.fc_act, name='fuse_alpha_p_fc2')
        # self.fuse_alpha_with_p_pred = self.dense(units=1, act=None, name='fuse_alpha_p_pred')
        fuse_alpha = lambda x: self.fuse_alpha_with_p_pred(self.fuse_alpha_with_p_fc2(self.fuse_alpha_with_p_fc1(x)))
        #
        pred_alpha_timeseries = fuse_alpha(alpha_merged_pre_pred) #shape: [bs, N, 1]

        self.fuse_epsilon_with_p_fc1 = self.conv1(filters=10, act=self.fc_act, name='fuse_epsilon_p_fc1')  # use conv1d(ksize=1) instead of dense blocks for each cropped section.
        self.fuse_epsilon_with_p_fc2 = self.conv1(filters=10, act=self.fc_act, name='fuse_epsilon_p_fc2')
        self.fuse_epsilon_with_p_pred = self.conv1(filters=1, act=None, name='fuse_epsilon_p_pred')
        fuse_epsilon = lambda x: self.fuse_epsilon_with_p_pred(self.fuse_epsilon_with_p_fc2(self.fuse_epsilon_with_p_fc1(x)))
        pred_epsilon_timeseries = fuse_epsilon(epsilon_merged_pre_pred) #shape: [bs, N, 1]

        pred_final = concat_channel_dim([pred_alpha_timeseries, pred_epsilon_timeseries])
        return pred_final


class CNN:
    '''Class for the CNN architecture with conv blocks followed by dense blocks'''

    def __init__(self, input, scope='decoder', **kwargs):

        self.scope = scope
        self.input_tmp = input
        self.input = None
        self.num_conv_filters = kwargs.get('num_conv_filters', 5)
        self.size_conv_kernel = kwargs.get('size_conv_kernels', 3)
        self.conv_act = tf.keras.activations.relu
        self.fc_act = lambda x: tf.keras.activations.elu(x, alpha=1.0)
        self.ndims_latent = kwargs.get('ndims_latent', 6)
        self.ndims_parameters = kwargs.get('ndims_parameters', 3) # alpha_1, alpha_2, epsilon_max
        self.latent_space = None
        with tf.variable_scope(name_or_scope=self.scope):
            self.parameter_space = self.build_model()
        self.model = tf.keras.Model(inputs=self.input, outputs=self.parameter_space, name='CNN')

    def dense(self, units, act=None, use_bias=True, kernel_init='glorot_uniform', name=None):
        return tf.keras.layers.Dense(units=units, activation=act, use_bias=use_bias, kernel_initializer=kernel_init,
                                     name=name)

    def conv(self, filters=None, act=None, padding='valid', use_bias=True, kernel_init='glorot_uniform', kernel_size=None, name=None):
        if filters is None:
            filters = self.num_conv_filters
        if kernel_size is None:
            kernel_size = self.size_conv_kernel
        return tf.keras.layers.Conv1D(filters=filters, kernel_size=kernel_size, activation=act,
                                      padding=padding, use_bias=use_bias, kernel_initializer=kernel_init, name=name)

    def build_model(self):
        self.input = tf.keras.Input(tensor=self.input_tmp, dtype=tf.float32, name='input') #shape [BS, seq_length, 1]

        self.conv1 = self.conv(act=self.conv_act, name='conv1')(self.input)
        self.conv2 = self.conv(act=self.conv_act, name='conv2')(self.conv1)
        self.conv3 = self.conv(filters=1, kernel_size=1, act=self.conv_act, name='conv3')(self.conv2) #we want to recover a single vector (merge all channels created by conv filters)
        self.flattened = tf.keras.layers.Flatten(name='flatten')(self.conv3) #shape [BS, seq_length-4]

        #TODO: consider introducing one layer of nonlinearity prior to the latent space
        self.fc1 = self.dense(units=self.ndims_latent, act=None, name='fc1')(self.flattened)
        self.latent_space = self.fc1
        self.fc2 = self.dense(units=self.ndims_latent, act=self.fc_act, name='fc2')(self.fc1)
        self.fc3 = self.dense(units=self.ndims_parameters, act=self.fc_act, name='fc3')(self.fc2) # parameter space
        return self.fc3


class CNN_v2:
    '''Class for the CNN architecture with conv blocks followed by dense blocks'''

    def __init__(self, input, scope='decoder', **kwargs): #default scope as 'decoder' is a bug, but to keep old models running, need to keep it.

        self.scope = scope
        self.input_tmp = input
        self.input = None
        self.ndims_latent = kwargs.get('ndims_latent', 6)
        self.num_conv_filters = kwargs.get('num_conv_filters', self.ndims_latent)
        self.size_conv_kernel = kwargs.get('size_conv_kernels', 3)
        self.conv_act = tf.keras.activations.relu
        self.fc_act = lambda x: tf.keras.activations.elu(x, alpha=1.0)
        self.ndims_parameters = kwargs.get('ndims_parameters', 3) # alpha_1, alpha_2, epsilon_max
        self.latent_space = None
        with tf.variable_scope(name_or_scope=self.scope):
            self.parameter_space = self.build_model()
        self.model = tf.keras.Model(inputs=self.input, outputs=self.parameter_space, name='CNN')

    def dense(self, units, act=None, use_bias=True, kernel_init='glorot_uniform', name=None):
        return tf.keras.layers.Dense(units=units, activation=act, use_bias=use_bias, kernel_initializer=kernel_init,
                                     name=name)

    def conv(self, filters=None, act=None, padding='valid', use_bias=True, kernel_init='glorot_uniform', kernel_size=None, name=None):
        if filters is None:
            filters = self.num_conv_filters
        if kernel_size is None:
            kernel_size = self.size_conv_kernel
        return tf.keras.layers.Conv1D(filters=filters, kernel_size=kernel_size, activation=act,
                                      padding=padding, use_bias=use_bias, kernel_initializer=kernel_init, name=name)

    def build_model(self):
        self.input = tf.keras.Input(tensor=self.input_tmp, dtype=tf.float32, name='input') #shape [BS, seq_length, 1]

        self.conv1 = self.conv(act=self.conv_act, name='conv1')(self.input)
        self.conv2 = self.conv(act=self.conv_act, name='conv2')(self.conv1)
        self.conv3 = self.conv(act=self.conv_act, name='conv3')(self.conv2)
        # self.conv3 = self.conv(filters=1, kernel_size=1, act=self.conv_act, name='conv3')(self.conv2) #we want to recover a single vector (merge all channels created by conv filters)
        self.flattened = tf.keras.layers.Flatten(name='flatten')(self.conv3) #shape [BS, (seq_length-4) * ndims_latent]

        #TODO: consider introducing one layer of nonlinearity prior to the latent space
        self.fc1 = self.dense(units=self.ndims_latent, act=None, name='fc1')(self.flattened) # its histogram looks like a uniformly weighted sum.
        self.latent_space = self.fc1
        self.fc2 = self.dense(units=self.ndims_latent, act=self.fc_act, name='fc2')(self.fc1)
        self.fc3 = self.dense(units=self.ndims_parameters, act=self.fc_act, name='fc3')(self.fc2) # parameter space
        return self.fc3

class CNN_v3:
    '''Class for the CNN architecture with conv blocks followed by dense blocks'''

    def __init__(self, input, scope='CNN', **kwargs):

        self.scope = scope
        self.input_tmp = input
        self.input = None
        self.ndims_latent = kwargs.get('ndims_latent', 6)
        self.num_conv_filters = kwargs.get('num_conv_filters', self.ndims_latent)
        self.size_conv_kernel = kwargs.get('size_conv_kernels', 3)
        self.conv_act = tf.keras.activations.relu
        self.fc_act = lambda x: tf.keras.activations.elu(x, alpha=1.0)
        self.ndims_parameters = kwargs.get('ndims_parameters', 3) # alpha_1, alpha_2, epsilon_max
        self.latent_space = None
        with tf.variable_scope(name_or_scope=self.scope):
            self.parameter_space = self.build_model()
        self.model = tf.keras.Model(inputs=self.input, outputs=self.parameter_space, name='CNN')

    def dense(self, units, act=None, use_bias=True, kernel_init='glorot_uniform', name=None):
        return tf.keras.layers.Dense(units=units, activation=act, use_bias=use_bias, kernel_initializer=kernel_init,
                                     name=name)

    def conv(self, filters=None, act=None, padding='valid', use_bias=True, kernel_init='glorot_uniform', kernel_size=None, name=None):
        if filters is None:
            filters = self.num_conv_filters
        if kernel_size is None:
            kernel_size = self.size_conv_kernel
        return tf.keras.layers.Conv1D(filters=filters, kernel_size=kernel_size, activation=act,
                                      padding=padding, use_bias=use_bias, kernel_initializer=kernel_init, name=name)

    def build_model(self):
        self.input = tf.keras.Input(tensor=self.input_tmp, dtype=tf.float32, name='input') #shape [BS, seq_length, 1]

        self.conv1 = self.conv(act=self.conv_act, name='conv1')(self.input)
        self.conv2 = self.conv(act=self.conv_act, name='conv2')(self.conv1)
        self.conv3 = self.conv(act=self.conv_act, name='conv3')(self.conv2)

        # Add a few dense layers to merge channels of the timeseries.
        self.conv_ch_fc1 = self.conv(filters=100, kernel_size=1, act=self.fc_act, name='mix_conv_ch_1')(self.conv3)
        self.conv_ch_fc2 = self.conv(filters=100, kernel_size=1, act=self.fc_act, name='mix_conv_ch_2')(self.conv_ch_fc1)
        self.conv_ch_fc3 = self.conv(filters=100, kernel_size=1, act=self.fc_act, name='mix_conv_ch_3')(self.conv_ch_fc2)
        self.conv_ch_fc4 = self.conv(filters=1, kernel_size=1, act=None, name='mix_conv_ch_4')(self.conv_ch_fc3)

        self.flattened = tf.keras.layers.Flatten(name='flatten')(self.conv_ch_fc4) #shape [BS, (seq_length-4)]

        #TODO: consider introducing one layer of nonlinearity prior to the latent space
        self.fc1 = self.dense(units=self.ndims_latent*20, act=self.fc_act, name='fc1')(self.flattened)
        self.fc2 = self.dense(units=self.ndims_latent*10, act=self.fc_act, name='fc2')(self.fc1)
        self.fc3 = self.dense(units=self.ndims_latent, act=None, name='fc3')(self.fc1)
        self.latent_space = self.fc3
        self.fc4 = self.dense(units=self.ndims_latent, act=self.fc_act, name='fc4')(self.fc3)
        self.fc5 = self.dense(units=self.ndims_parameters, act=self.fc_act, name='fc5')(self.fc4) # parameter space
        return self.fc5

class CNN_v4:
    '''Class for the CNN architecture with conv blocks followed by dense blocks'''

    def __init__(self, input, scope='CNN', **kwargs):

        self.scope = scope
        self.input_tmp = input
        self.input = None
        self.ndims_latent = kwargs.get('ndims_latent', 6)
        self.num_conv_filters = kwargs.get('num_conv_filters', 1000)
        self.size_conv_kernel = kwargs.get('size_conv_kernels', 3)
        self.conv_act = tf.keras.activations.relu
        self.fc_act = lambda x: tf.keras.activations.elu(x, alpha=1.0)
        self.ndims_parameters = kwargs.get('ndims_parameters', 3) # alpha_1, alpha_2, epsilon_max
        self.latent_space = None
        with tf.variable_scope(name_or_scope=self.scope):
            self.parameter_space = self.build_model()
        self.model = tf.keras.Model(inputs=self.input, outputs=self.parameter_space, name='CNN')

    def dense(self, units, act=None, use_bias=True, kernel_init='glorot_uniform', name=None):
        return tf.keras.layers.Dense(units=units, activation=act, use_bias=use_bias, kernel_initializer=kernel_init,
                                     name=name)

    def conv(self, filters=None, act=None, padding='valid', use_bias=True, kernel_init='glorot_uniform', kernel_size=None, name=None):
        if filters is None:
            filters = self.num_conv_filters
        if kernel_size is None:
            kernel_size = self.size_conv_kernel
        return tf.keras.layers.Conv1D(filters=filters, kernel_size=kernel_size, activation=act,
                                      padding=padding, use_bias=use_bias, kernel_initializer=kernel_init, name=name)



    def build_model(self):
        self.input = tf.keras.Input(tensor=self.input_tmp, dtype=tf.float32, name='input') #shape [BS, seq_length, 1]

        self.conv1 = self.conv(act=self.conv_act, name='conv1')(self.input)
        self.conv2 = self.conv(act=self.conv_act, name='conv2')(self.conv1)
        self.conv3 = self.conv(act=self.conv_act, name='conv3')(self.conv2)

        # Add a few dense layers to merge channels of the timeseries.
        self.conv_ch_fc1 = self.conv(filters=100, kernel_size=1, act=self.fc_act, name='mix_conv_ch_1')(self.conv3)
        self.conv_ch_fc2 = self.conv(filters=100, kernel_size=1, act=self.fc_act, name='mix_conv_ch_2')(self.conv_ch_fc1)
        self.conv_ch_fc3 = self.conv(filters=100, kernel_size=1, act=self.fc_act, name='mix_conv_ch_3')(self.conv_ch_fc2)
        self.conv_ch_fc4 = self.conv(filters=1, kernel_size=1, act=None, name='mix_conv_ch_4')(self.conv_ch_fc3)

        self.flattened = tf.keras.layers.Flatten(name='flatten')(self.conv_ch_fc4) #shape [BS, (seq_length-4)]

        #TODO: consider introducing one layer of nonlinearity prior to the latent space
        self.fc1 = self.dense(units=self.ndims_latent*20, act=self.fc_act, name='fc1')(self.flattened)
        self.fc2 = self.dense(units=self.ndims_latent*10, act=self.fc_act, name='fc2')(self.fc1)
        self.fc3 = self.dense(units=self.ndims_latent, act=None, name='fc3')(self.fc1)
        self.latent_space = self.fc3
        self.fc4 = self.dense(units=self.ndims_latent, act=self.fc_act, name='fc4')(self.fc3)
        self.fc5 = self.dense(units=self.ndims_parameters, act=self.fc_act, name='fc5')(self.fc4) # parameter space
        return self.fc5

class FC_Simple():
    def __init__(self, input, scope='MLP', **kwargs):
        # TODO: put check for input tensor dim.
        self.scope = scope
        self.input_tmp = tf.squeeze(input, name='change_input_ndim_to_2')
        self.fc_act = lambda x: tf.keras.activations.elu(x, alpha=1.0)
        self.ndims_latent = kwargs.get('ndims_latent', 6)
        self.ndims_parameters = kwargs.get('ndims_parameters', 3)
        self.latent_space = None
        with tf.variable_scope(name_or_scope=self.scope):
            self.parameter_space = self.build_model()
        self.model = tf.keras.Model(inputs=self.input, outputs=self.parameter_space, name='Encoder')

    def dense(self, units, act=None, use_bias=True, kernel_init='glorot_uniform', name=None):
        return tf.keras.layers.Dense(units=units, activation=act, use_bias=use_bias, kernel_initializer=kernel_init,
                                     name=name)

    def build_model(self):
        self.input = tf.keras.Input(tensor=self.input_tmp, dtype=tf.float32, name='input')
        self.fc1 = self.dense(units=1000, act=self.fc_act, name='fc1')(self.input)
        self.fc2 = self.dense(units=100, act=self.fc_act, name='fc2')(self.fc1)
        self.fc3 = self.dense(units=10, act=self.fc_act, name='fc3')(self.fc2)
        self.latent_space = self.dense(units=self.ndims_latent, act=self.fc_act, name='latent_space')(self.fc3)
        param_space = self.dense(units=self.ndims_parameters, act=self.fc_act, name='parameter_space')(self.latent_space)
        return param_space