#
# quick and dirt script to generate variance-based weights
#
import sys
import numpy as np
import tensorflow as tf
import importlib
from ae_src.imports import make_instance
from ae_src.imports import make_inpdic
from ae_src.imports import add_ae_args
from ae_src.tools.utils import set_gpus
from ae_src.tools.wraps import wrap_setattr
from ae_src.modls.archs import data2ss
#
def calc_ss_varwghts(args):
    #
    gpus = set_gpus()
    nparams_samples = args["nparams_samples"]
    nmodel_samples = args["nmodel_samples"]
    write_params = args["write_params"]
    write_output = args["write_output"]
    write_noise = args["write_noise"]
    write_ss_obs = args["write_ss_obs"]
    write_sstats = args["write_sstats"]
    #user_module = importlib.import_module(args["umodlfl"])
    #user_class = getattr(user_module, args["umodlcl"])
    #print("User class: ",user_class)
    #
    # Make specialized input dictionary and prior for user model and user model
    inpdic = make_inpdic(args)
    prior = make_instance(mdnm=args["makepriorfl"], objnm=args["makepriorfn"], **args)
    model = make_instance(mdnm=args["umodlfl"], objnm=args["umodlcl"], prior=prior, **args, **inpdic)
    #
    nmp = args["num_model_parameters"]
    nnc = args["num_noise_channels"]
    nstats = args["ndims_latent"]
    len_ts = args["len_timeseries"]
    #
    # add AE args and make AE
    args = add_ae_args(args)
    ae = make_instance(mdnm=args["archsfl"], objnm=args["aem2load"], **args)
    #
    if write_ss_obs:
        module = importlib.import_module(args["readcsvfl"])
        readcsv = getattr(module, args["readcsvfn"])
        dataset = readcsv(args["obsinfl"])
        data = dataset.iloc[:, 0].values
        ssobs = data2ss(data, ae)
        np.savetxt('stats_obs.dat',ssobs)
    #
    sumstats = np.full((nparams_samples, nmodel_samples, nstats), np.nan)
    o_params = np.full((nparams_samples, nmp), np.nan) # drawn parameters
    if write_noise:
        noise = np.full((nparams_samples, nmodel_samples, nnc, len_ts), np.nan)
    if write_output:
        output = np.full((nparams_samples, nmodel_samples, len_ts), np.nan) # it will fail in case of multidim output
    #
    rng = np.random.RandomState(args["seed_infer"])
    for i in range(0, nparams_samples):
        o_params [i,:] = prior.draw(rng).values.copy()
        d = next(model.__iter__())
        ss = data2ss(d[0], ae)
        sumstats [i,0,:] = ss.numpy().copy()
        if write_noise:
            if not ae.is_aggregator:
                assert d[2].shape[1]==nnc, ":: Fatal: more testing on write noise needed."
                for j in range(0,d[2].shape[1]):
                    noise [i,0,j,:] = d[2][:,j]
            else:
                assert d[2].shape[-1]==nnc, ":: Fatal: more testing on write noise needed."
                for j in range(0,d[2].shape[-1]):
                    noise [i,0,j,:] = d[2][0,:,j] # take only one not to have to change function...
        if write_output:
            if not ae.is_aggregator:
              assert d[0].shape[1]==1, ":: Fatal: more testing on write output needed."
              output [i,0,:] = d[0][:,0]
            else:
              assert d[0].shape[-1]==1, ":: Fatal: more testing on write output needed."
              output [i,0,:] = d[0][0,:,0] # take only one not to have to change function...
    #
    for i in range(0, nparams_samples):
        params = dict(zip(prior.labels,o_params[i,:]))
        for j in range(1, nmodel_samples):
            d = next(model.__iter__(inparams=params))
            ss = data2ss(d[0], ae)
            sumstats [i,j,:] = ss.numpy().copy()
            if write_noise:
                if not ae.is_aggregator:
                    for k in range(0,d[2].shape[1]):
                        noise [i,j,k,:] = d[2][:,k]
                else:
                    for k in range(0,d[2].shape[-1]):
                        noise [i,j,k,:] = d[2][0,:,k] # take only one not to have to change function...

            if write_output:
                if not ae.is_aggregator:
                    output [i,j,:] = d[0][:,0]
                else:
                    output [i,j,:] = d[0][0,:,0]
    #
    varstats = np.full( (nparams_samples, nstats), np.nan)
    varstats2 = np.full( nstats, np.nan)
    for i in range(0,nstats):
        varstats2[i] = np.var(sumstats [...,i]) # one shot across samples and parameters
        for j in range(0,nparams_samples):
            varstats[j,i] = np.var(sumstats [j,:,i])
    #
    avevarstats = np.full(nstats, np.nan)
    for i in range(0,nstats):
        avevarstats [i] = np.mean(varstats[:,i])
    #
    if all(varstats2==0.0):
        print("::Warning: all one-shot weights are 0s, converting to 1s")
        varstats2 = np.ones(len(avevarstats))
    if all(avevarstats==0.0):
        print("::Warning: all average weights are 0s, converting to 1s")
        avevarstats = np.ones(len(avevarstats))
    #
    if not ae.is_aggregator:
        np.savetxt('stats_varw_enca.dat',avevarstats)
        np.savetxt('stats_varw_1shot_enca.dat',varstats2)
    else:
        np.savetxt('stats_varw_inca.dat',avevarstats)
        np.savetxt('stats_varw_1shot_inca.dat',varstats2)
    #
    if write_sstats:
        for i in range(0,nparams_samples):
            fn = "sumstats_"+str(i).zfill(5)+".dat"
            open(fn, 'w').close() # erase content
            with open(fn, "ab") as f:
                for j in range(nmodel_samples):
                    np.savetxt(f, np.atleast_2d(sumstats[i,j,:]))
    if write_params:
        head = ', '.join([p for p in prior.labels])
        np.savetxt('sampled_parameters.dat',o_params,header=head)
    if write_noise:
        for i in range(nparams_samples):
            for k in range(nnc):
                fn = "model_noise_"+str(i).zfill(5)+"_"+str(k).zfill(2)+".dat"
                open(fn, 'w').close() # erase content
                with open(fn, "ab") as f:
                    for j in range(nmodel_samples):
                        np.savetxt(f, np.atleast_2d(noise[i,j,k,:])) # write by row
    if write_output:
        for i in range(0,nparams_samples):
            fn = "model_output_"+str(i).zfill(5)+".dat"
            open(fn, 'w').close() # erase content
            with open(fn, "ab") as f:
                for j in range(nmodel_samples):
                    np.savetxt(f, np.atleast_2d(output[i,j,:]))
    #
    print("DONE")
