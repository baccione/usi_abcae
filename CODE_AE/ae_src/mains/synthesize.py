#
# quick and dirt script to create synthetic data
#
import sys
import numpy as np
import importlib
import pandas
from ae_src.imports import make_instance
from ae_src.imports import make_inpdic
from ae_src.imports import add_ae_args
from ae_src.tools.utils import set_gpus
from ae_src.tools.wraps import wrap_setattr
#
def synrun(args):
    gpus = set_gpus()
    inpdic = make_inpdic(args)
    prior = make_instance(mdnm=args["makepriorfl"], objnm=args["makepriorfn"], **args)
    model = make_instance(mdnm=args["umodlfl"], objnm=args["umodlcl"], prior=prior, **args, **inpdic)
    rng = np.random.RandomState(args["seed_infer"])
    params = prior.draw(rng)
    d = next(model.__iter__(inparams=params))
    df = pandas.DataFrame(d[0],columns=args['obsvarnms'])
    df.index += 1
    df.index.name = 'Time'
    df.to_csv("predictions.synth")
    with open('parameters.synth', 'w') as f:
        for key in params.keys():
            f.write("%s %s\n"%(key, params[key]))
