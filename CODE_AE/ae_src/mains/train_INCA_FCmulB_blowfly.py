#
################
# This training pipeline has an encoder-aggregator-like architecture.

import tensorflow as tf
assert tf.__version__[0] == '2' # this script is intended for tf v2 (2.2.0 to be precise.)
import os
import datetime
import numpy as np
import sys
import importlib
import logging
root = logging.getLogger()
root.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)

from ae_src.wraps.utils import LearningRateScheduleExponentialDecayWithLinearWarmup

from ae_src.tools.utils import Manage_Hyper_Parameters

from ae_src.tools.utils import ExpSetup

from ae_src.tools.utils import export_summary_scalars
from ae_src.tools.utils import export_summary_histograms

from ae_src.tools.utils import set_gpus
from ae_src.tools.utils import activate_gpus

from ae_src.modls.trains import train_step_INCA_FCmulB as train_step

##################################################################################################
gpus = set_gpus()
# tf.config.set_visible_devices([], 'GPU') # force not using any GPU
##################################################################################################

##################################################################################################
def main(laf=None, lrp=None):

    args = ExpSetup()

    if not os.path.isdir(args.logdir):
        os.makedirs(args.logdir)

    physical_devices = activate_gpus()

    # all that is loaded should come with full path and loaded with spec_from_file_location so to forget about PYTHONPATH
    arch_module = importlib.import_module("ae_src.modls.archs")
    print('Architecture:',args.aem2load)
    assert "FC_multibatch" == str(args.aem2load), ":: Fatal: this file is intended to be used only with FC_multibatch arch."
    Architecture = getattr(arch_module,args.aem2load)

    # Define a generator function for observations, parameters, and noise vectors
    user_module = importlib.import_module("ae_src.modls."+args.umodlfl)
    user_class = getattr(user_module, args.umodlcl)
    print("User class: ",user_class)
    assert "DataGenerator_Blowfly_Simplified_BatchSampler" in str(user_class), ":: Fatal: this main is inteded for DataGenerator_Blowfly_Simplified_BatchSampler"

    gen_train = user_class(len_timeseries=args.len_timeseries, batch_size=args.batch_size)

    dataset_train = tf.data.Dataset.from_generator(lambda: gen_train, output_types=(tf.float32, tf.float32, tf.float32))
    dataset_train = dataset_train.repeat(count=1)
    dataset_train = dataset_train.batch(args.num_different_mb, drop_remainder=True) # shape: [num_different_mb, batch_size, ...]
    dataset_train = dataset_train.prefetch(buffer_size=10) # #number of minibatches to pre-fetch.

    # Define the architecture
    model_obj = Architecture(args=args)
    model_obj.encoder.summary()
    model_obj.aggregator.summary()

    # Define optimizer
    #lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(initial_learning_rate=1.e-3, decay_steps=int(6*1e3), decay_rate=0.92, staircase=True)
    lr_schedule = LearningRateScheduleExponentialDecayWithLinearWarmup(steps_warmup=args.linear_warmup_steps,
                                                                       initial_learning_rate=1.e-3,
                                                                       decay_steps=int(6*1e3), decay_rate=0.92,
                                                                       staircase=True)
    optimizer = tf.keras.optimizers.Adam(learning_rate=lr_schedule)

    if getattr(args,'global_gradient_clipnorm',None) is None:
        setattr(args,'global_gradient_clipnorm',None) # temporary legacy - should come from keyfile
    if getattr(args,'gradient_clip_value',None) is None:
        setattr(args,'gradient_clip_value',None) # temporary legacy - should come from keyfile
    if getattr(args,'gradient_clip_norm',None) is None:
        setattr(args,'gradient_clip_norm',5) # temporary legacy - should come from keyfile

    # Define ckpt managers to save model weights throughout optimization
    ckpt = tf.train.Checkpoint(optimizer=optimizer, encoder=model_obj.encoder, aggregator=model_obj.aggregator)
    save_manager = tf.train.CheckpointManager(checkpoint=ckpt, directory=args.logdir, max_to_keep=3, checkpoint_name='model_ckpt')
    save_manager_best = tf.train.CheckpointManager(checkpoint=ckpt, directory=args.logdir, max_to_keep=3, checkpoint_name='model_best_ckpt') # manager for early stopping.
    save = lambda save_manager, ckpt_number=None: save_manager.save(checkpoint_number=ckpt_number)
    # Define a manager object for training hyper-parameters
    hp_manager = Manage_Hyper_Parameters(logdir=args.logdir)

    # Restore a previously interrupted training session (if exists)
    ckpt.restore(save_manager.latest_checkpoint)
    if save_manager.latest_checkpoint:
        print(f"Restored from {save_manager.latest_checkpoint}.")
        # Double check if experiment setup matches the one in the saved chkpt dir
        hp_manager.check_args_maybe_append(args)
    else:
        print("Initializing training from scratch. \nLogdir: %s" % args.logdir)
        # Dump hyper-parameters to ckpt dir for future reference.
        hp_manager.save_parameters(args)

    # Setup summaries for tensorboard
    summary_writer = tf.summary.create_file_writer(logdir=os.path.join(args.logdir, 'train'))

    # Define a few metrics to export to tensorboard
    avg_loss_total = tf.keras.metrics.Mean(name='loss_total', dtype=tf.float32) # variable will keep track of average of loss value since last freq_log
    avg_loss_reg_theta = tf.keras.metrics.Mean(name='loss_regress_theta', dtype=tf.float32)
    avg_loss_reg_z = tf.keras.metrics.Mean(name='loss_regress_z', dtype=tf.float32)
    dict_avg_loss_recon_x_items = {} #will create mean metric on the fly since we don't know names in advance.
    dict_avg_loss_reg_theta_items = {}
    dict_avg_loss_reg_z_items = {}
    dict_avg_rmse_recon_x_items = {}
    dict_avg_rmse_reg_theta_items = {}
    dict_avg_rmse_reg_z_items = {}

    # Define a metric to keep track of best reconstruction over a longer window
    avg_loss_long_term = tf.keras.metrics.Mean(name='loss_long_term', dtype=tf.float32)
    curr_best_loss = np.inf

    user_module = importlib.import_module("ae_src.modls.losses")
    user_class = getattr(user_module, args.ulosscl)
    name = "unkwown"
    if 'ChiSquare' in str(user_class):
        name = 'ChiSquare'
    loss = user_class(name=name)

    # Execute training loop
    for x, params, noise in dataset_train:
        num_step = optimizer.iterations
        if num_step >= args.max_training_steps:
            logging.info('Training is completed.')
            break

        loss_tuple, x_theta_z, dict_mse = train_step(model=model_obj, x=x, params=params, noise=noise,
                                                     optimizer=optimizer, args=args,
                                                     summary_writer=summary_writer,
                                                     loss_aggregator_fn=laf,
                                                     loss_regress_params_fn=lrp,
                                                     loss=loss)

        theta_reconst, z_latent = x_theta_z
        loss_regress_theta, loss_regress_z = loss_tuple
        loss_total = loss_regress_theta + loss_regress_z
        dict_regress_theta_mse, dict_regress_z_mse = dict_mse

        # Update loggers for tensorboard
        avg_loss_reg_theta.update_state(loss_regress_theta)
        avg_loss_reg_z.update_state(loss_regress_z)
        avg_loss_total.update_state(loss_total) # aggregate values since last flush
        avg_loss_long_term.update_state(loss_total)
        # A little patchy way to keep track of each reconstructed signal and noise channel
        for k in dict_regress_theta_mse:
            if k not in dict_avg_loss_reg_theta_items:
                dict_avg_loss_reg_theta_items[k] = tf.keras.metrics.Mean(name=k, dtype=tf.float32)
            dict_avg_loss_reg_theta_items[k].update_state(dict_regress_theta_mse[k])
        for k in dict_regress_z_mse:
            if k not in dict_avg_loss_reg_z_items:
                dict_avg_loss_reg_z_items[k] = tf.keras.metrics.Mean(name=k, dtype=tf.float32)
            dict_avg_loss_reg_z_items[k].update_state(dict_regress_z_mse[k])

        # Record RMSE for reconstruction and regressed parameters regardless of the loss
        for i_ in range(params.shape[-1]):
            k = 'RMSE_theta_ch_%d'%(i_+1)
            if k not in dict_avg_rmse_reg_theta_items:
                dict_avg_rmse_reg_theta_items[k] = tf.keras.metrics.RootMeanSquaredError(name='rmse_theta', dtype=tf.float32)
            dict_avg_rmse_reg_theta_items[k].update_state(y_true=params[:,i_], y_pred=theta_reconst[:,i_]) # params.shape: [num_different_mb, num_model_params], theta_reconst.shape: [num_different_mb, num_model_params]
        for i_ in range(params.shape[-1]):
            k = 'RMSE_z_ch_%d'%(i_+1)
            if k not in dict_avg_rmse_reg_z_items:
                dict_avg_rmse_reg_z_items[k] = tf.keras.metrics.RootMeanSquaredError(name='rmse_z', dtype=tf.float32)
            dict_avg_rmse_reg_z_items[k].update_state(y_true=params[...,i_:i_+1], y_pred=z_latent[...,i_]) # params.shape: [num_different_mb, num_model_params], z_latent.shape: [num_different_mb, args.batch_size, ndims_latent]
        # Export status to tensorboard
        if tf.equal(optimizer.iterations % args.freq_log, 0):
            d_scalars = {'loss_total': avg_loss_total.result(), 'loss_regress_theta': avg_loss_reg_theta.result(), 'loss_regress_z': avg_loss_reg_z.result()}
            for k in dict_avg_loss_reg_theta_items:
                d_scalars[k] = dict_avg_loss_reg_theta_items[k].result()
                dict_avg_loss_reg_theta_items[k].reset_states()
            for k in dict_avg_loss_reg_z_items:
                d_scalars[k] = dict_avg_loss_reg_z_items[k].result()
                dict_avg_loss_reg_z_items[k].reset_states()
            for k in dict_avg_rmse_reg_theta_items:
                d_scalars[k] = dict_avg_rmse_reg_theta_items[k].result()
                dict_avg_rmse_reg_theta_items[k].reset_states()
            for k in dict_avg_rmse_reg_z_items:
                d_scalars[k] = dict_avg_rmse_reg_z_items[k].result()
                dict_avg_rmse_reg_z_items[k].reset_states()
            d_scalars['lr_schedule'] = lr_schedule(step=num_step)
            export_summary_scalars(dict_name_and_val=d_scalars, step=optimizer.iterations, writer=summary_writer)
            # Print current loss status to terminal
            logging.info('Step %d: avg loss: %.3f, parameter (theta) regression loss: %.3f, latent_z regression loss: %.3f.' % \
                    (num_step, d_scalars['loss_total'], d_scalars['loss_regress_theta'], d_scalars['loss_regress_z']))
            avg_loss_total.reset_states() #reset kept history of loss
            # avg_loss_recon_x.reset_states()
            avg_loss_reg_theta.reset_states()
            # Export trainable variables to tboard histogram
            # TODO: consider speeding this up.
            l_enc = list(zip(*[['enc_'+v.name, v.value()] for v in model_obj.encoder.trainable_variables]))
            l_agg = list(zip(*[['agg_'+v.name, v.value()] for v in model_obj.aggregator.trainable_variables]))
            d_histograms = {**dict(zip(l_enc[0], l_enc[1])), **dict(zip(l_agg[0], l_agg[1]))}
            export_summary_histograms(dict_name_and_val=d_histograms, step=optimizer.iterations, writer=summary_writer)
            # Save the current state of the model weights on disk.
            save(save_manager, ckpt_number=optimizer.iterations)
        # Check loss for long term best reconstruction
        if tf.equal(optimizer.iterations % (10 * args.freq_log), 0):
            if avg_loss_long_term.result() < curr_best_loss:
                curr_best_loss = avg_loss_long_term.result()
                avg_loss_long_term.reset_states()
                logging.info('New long term best loss found: %.3f. Saving.' % curr_best_loss)
                save(save_manager_best, ckpt_number=optimizer.iterations)

    ##################################################################################################
    # Save model once again on the exit
    save(save_manager, ckpt_number=optimizer.iterations)
