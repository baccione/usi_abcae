#
################
# This training pipeline has an encoder-decoder-like architecture.

import tensorflow as tf
assert tf.__version__[0] == '2'
import os
import datetime
import numpy as np
import sys

from ae_src.tools.wraps import LearningRateScheduleExponentialDecayWithLinearWarmup

from ae_src.tools.utils import Manage_Hyper_Parameters

from ae_src.tools.utils import export_summary_scalars
from ae_src.tools.utils import export_summary_histograms

from ae_src.tools.utils import set_gpus
from ae_src.tools.utils import activate_gpus
from ae_src.imports import add_ae_args

from ae_src.modls.trains import train_step_ENCA_BiLSTM as train_step

from ae_src.imports import make_inpdic
from ae_src.imports import get_function
from ae_src.imports import make_instance

##################################################################################################
gpus = set_gpus()
# tf.config.set_visible_devices([], 'GPU') # force not using any GPU
##################################################################################################

##################################################################################################
def main(args):

    assert "BiLSTM" == args["aem2load"], ":: Fatal: this file is intended to be used only with BiLSTM arch."

    if not os.path.isdir(args['logdir']):
        os.makedirs(args['logdir'])

    physical_devices = activate_gpus()

    # add AE args and make AE
    args = add_ae_args(args)
    ae = make_instance(mdnm=args["archsfl"], objnm=args["aem2load"], **args)
    ae.encoder.summary()
    ae.decoder.summary()

    # Make specialized input dictionary and prior for user model
    inpdic = make_inpdic(args)
    prior = make_instance(mdnm=args["makepriorfl"], objnm=args["makepriorfn"], **args)

    # Define user model (namely a generator function for observations, parameters, and noise vectors)
    gen_train = make_instance(mdnm=args["umodlfl"], objnm=args["umodlcl"], prior=prior, **args, **inpdic)
    dataset_train = tf.data.Dataset.from_generator(lambda: gen_train, output_types=(tf.float32, tf.float32, tf.float32))
    dataset_train = dataset_train.repeat(count=1)
    dataset_train = dataset_train.batch(args["batch_size"], drop_remainder=True)
    dataset_train = dataset_train.prefetch(buffer_size=10) # #number of minibatches to pre-fetch.

    # Define optimizer
    lws = args["linear_warmup_steps"]
    lr_schedule = LearningRateScheduleExponentialDecayWithLinearWarmup(steps_warmup=lws,
                                                                       initial_learning_rate=1.e-3,
                                                                       decay_steps=int(6*1e3), decay_rate=0.92,
                                                                       staircase=True)
    optimizer = tf.keras.optimizers.Adam(learning_rate=lr_schedule) # clipvalue to prevent exploding., #clipnorm: preventive measure for divergence. Unfortunately both are clipping individually for each gradient, which can change the direction of the gradients..

    # Define ckpt managers to save model weights throughout optimization
    logdir = args["logdir"]
    ckpt = tf.train.Checkpoint(optimizer=optimizer, encoder=ae.encoder, decoder=ae.decoder)
    save_manager = tf.train.CheckpointManager(checkpoint=ckpt, directory=logdir, max_to_keep=3, checkpoint_name='model_ckpt')
    save_manager_best = tf.train.CheckpointManager(checkpoint=ckpt, directory=logdir, max_to_keep=3, checkpoint_name='model_best_ckpt') # manager for early stopping.
    save = lambda save_manager, ckpt_number=None: save_manager.save(checkpoint_number=ckpt_number)

    # Define a manager object for training hyper-parameters
    hp_manager = Manage_Hyper_Parameters(logdir=logdir)

    # Restore a previously interrupted training session (if exists)
    ckpt.restore(save_manager.latest_checkpoint)
    if save_manager.latest_checkpoint:
        print(f"Restored from {save_manager.latest_checkpoint}.")
        # Double check if experiment setup matches the one in the saved chkpt dir
        hp_manager.check_args_maybe_append(args)
    else:
        print("Initializing training from scratch. \nLogdir: %s" % logdir)
        # Dump hyper-parameters to ckpt dir for future reference.
        hp_manager.save_parameters(args)

    # Setup summaries for tensorboard
    summary_writer = tf.summary.create_file_writer(logdir=os.path.join(logdir, 'train'))

    # Define a few metrics to export to tensorboard
    avg_loss_total = tf.keras.metrics.Mean(name='loss_total', dtype=tf.float32) # tracks average loss value since last freq_log
    avg_loss_recon = tf.keras.metrics.Mean(name='loss_reconstruction', dtype=tf.float32)
    avg_loss_reg_p = tf.keras.metrics.Mean(name='loss_regress_params', dtype=tf.float32)
    dict_avg_loss_recon_items = {} #will create mean metric on the fly since we don't know names in advance.
    dict_avg_loss_reg_p_items = {}
    dict_avg_rmse_recon_items = {}
    dict_avg_rmse_reg_p_items = {}

    # Define a metric to keep track of best reconstruction over a longer window
    avg_loss_long_term = tf.keras.metrics.Mean(name='loss_long_term', dtype=tf.float32)
    curr_best_loss = np.inf

    # Get loss functions instances
    lrf = get_function(mdnm=args["loss_reconstruction_fl"], fnnm=args["loss_reconstruction_fn"])
    lrp = get_function(mdnm=args["loss_regress_params_fl"], fnnm=args["loss_regress_params_fn"])

    # Get loss type
    name = "unkwown"
    if 'ChiSquare' in args["ulosscl"]:
        name = 'ChiSquare'
    elif 'MSE' in args["ulosscl"]:
        name = 'MSE'
    elif 'MAE' in args["ulosscl"]:
        name = 'MAE'
    loss = make_instance (mdnm=args["ulossfl"], objnm=args["ulosscl"], name=name)

    # Execute training loop
    mts = args['max_training_steps']
    freq_log = args['freq_log']
    for x, params, noise in dataset_train:
        num_step = optimizer.iterations
        if num_step >= mts:
            print('Training is completed.')
            break

        loss_tuple, z_and_x, dict_mse = train_step(model=ae, x=x, params=params, noise=noise,
                                                   optimizer=optimizer, args=args,
                                                   summary_writer=summary_writer,
                                                   loss_reconstruction_fn = lrf,
                                                   loss_regress_params_fn = lrp,
                                                   loss=loss)

        z_latent, x_reconst = z_and_x
        loss_reconstruction, loss_regress_params = loss_tuple
        loss_total = loss_reconstruction + loss_regress_params
        dict_rec, dict_reg = dict_mse

        # Update loggers for tensorboard
        avg_loss_recon.update_state(loss_reconstruction)
        avg_loss_reg_p.update_state(loss_regress_params)
        avg_loss_total.update_state(loss_total) # aggregate values since last flush
        avg_loss_long_term.update_state(loss_total)
        # A little patchy way to keep track of each reconstructed signal and noise channel
        for k in dict_rec:
            if k not in dict_avg_loss_recon_items:
                dict_avg_loss_recon_items[k] = tf.keras.metrics.Mean(name=k, dtype=tf.float32)
            dict_avg_loss_recon_items[k].update_state(dict_rec[k])
        for k in dict_reg:
            if k not in dict_avg_loss_reg_p_items:
                dict_avg_loss_reg_p_items[k] = tf.keras.metrics.Mean(name=k, dtype=tf.float32)
            dict_avg_loss_reg_p_items[k].update_state(dict_reg[k])

        # Record RMSE for reconstruction and regressed parameters regardless of the loss
        for i_ in range(x.shape[-1]):
            k = 'RMSE_x_ch_%d'%(i_+1)
            if k not in dict_avg_rmse_recon_items:
                dict_avg_rmse_recon_items[k] = tf.keras.metrics.RootMeanSquaredError(name='rmse_reconstruction', dtype=tf.float32)
            dict_avg_rmse_recon_items[k].update_state(y_true=x[...,i_], y_pred=x_reconst[...,i_])
        for i_ in range(params.shape[-1]):
            k = 'RMSE_z_ch_%d'%(i_+1)
            if k not in dict_avg_rmse_reg_p_items:
                dict_avg_rmse_reg_p_items[k] = tf.keras.metrics.RootMeanSquaredError(name='rmse_regularization', dtype=tf.float32)
            dict_avg_rmse_reg_p_items[k].update_state(y_true=params[...,i_], y_pred=z_latent[...,i_])
        # Export status to tensorboard
        if tf.equal(optimizer.iterations % freq_log, 0):
            d_scalars = {'loss_total': avg_loss_total.result(), 'loss_reconstruction': avg_loss_recon.result(), 'loss_regress_params': avg_loss_reg_p.result()}
            for k in dict_avg_loss_recon_items:
                d_scalars[k] = dict_avg_loss_recon_items[k].result()
                dict_avg_loss_recon_items[k].reset_states()
            for k in dict_avg_loss_reg_p_items:
                d_scalars[k] = dict_avg_loss_reg_p_items[k].result()
                dict_avg_loss_reg_p_items[k].reset_states()
            for k in dict_avg_rmse_recon_items:
                d_scalars[k] = dict_avg_rmse_recon_items[k].result()
                dict_avg_rmse_recon_items[k].reset_states()
            for k in dict_avg_rmse_reg_p_items:
                d_scalars[k] = dict_avg_rmse_reg_p_items[k].result()
                dict_avg_rmse_reg_p_items[k].reset_states()
            d_scalars['lr_schedule'] = lr_schedule(step=num_step)
            export_summary_scalars(dict_name_and_val=d_scalars, step=optimizer.iterations, writer=summary_writer)
            # Print current loss status to terminal
            print('Step %d: avg loss: %.3f, reconstruction loss: %.3f, parameter regression loss: %.3f.' % \
                    (num_step, d_scalars['loss_total'], d_scalars['loss_reconstruction'], d_scalars['loss_regress_params']))
            avg_loss_total.reset_states() #reset kept history of loss
            avg_loss_recon.reset_states()
            avg_loss_reg_p.reset_states()
            # Export trainable variables to tboard histogram
            # TODO: consider speeding this up.
            l_enc = list(zip(*[['enc_'+v.name, v.value()] for v in ae.encoder.trainable_variables]))
            l_dec = list(zip(*[['dec_'+v.name, v.value()] for v in ae.decoder.trainable_variables]))
            d_histograms = {**dict(zip(l_enc[0], l_enc[1])), **dict(zip(l_dec[0], l_dec[1]))}
            export_summary_histograms(dict_name_and_val=d_histograms, step=optimizer.iterations, writer=summary_writer)
            # Save the current state of the model weights on disk.
            save(save_manager, ckpt_number=optimizer.iterations)
        # Check loss for long term best reconstruction
        if tf.equal(optimizer.iterations % (10 * freq_log), 0):
            if avg_loss_long_term.result() < curr_best_loss:
                curr_best_loss = avg_loss_long_term.result()
                avg_loss_long_term.reset_states()
                print('New long term best loss found: %.3f. Saving.' % curr_best_loss)
                save(save_manager_best, ckpt_number=optimizer.iterations)

    ##################################################################################################
    # Save model once again on the exit
    save(save_manager, ckpt_number=optimizer.iterations)
