# === Parallel region seen by all procs but before MPI_init

import os
import importlib
import numpy as np
from ae_src.tools.wraps import wrap_setattr
from ae_src.imports import get_function
from ae_src.imports import make_instance
from ae_src.imports import make_ae_for_spux
from ae_src.imports import add_ae_args

from spux import framework
from spux.samplers.sabc import SABC
from spux.executors.mpi4py.connectors import utils

def infer(args):
    print("Start") # gets printed as many times as there are mpi procs.

    user_funct = get_function(mdnm=args["umodlfl"], fnnm=args["umodlfn"])
    spux_class = get_function(mdnm=args["spuxmodlfl"], fnnm=args["spuxmodlcl"]) #get function should be named get obj, but...
    #model = spux_class (len_timeseries=args["len_timeseries"], fn=user_funct) # would be seen by all procs: good design, but...

    calc_ss = get_function(mdnm=args["aewrapfl"], fnnm=args["summstatfn"])

    readcsv = get_function(mdnm=args["readcsvfl"], fnnm=args["readcsvfn"])
    dataset = readcsv(args["obsinfl"])

    prior = make_instance(mdnm=args["makepriorfl"], objnm=args["makepriorfn"], **args)
    obsvars = args["obsvarnms"]

    varstats = np.loadtxt(args["varstatsfl"], dtype=float)
    weights = np.reshape(1/np.sqrt(varstats), (-1, 1))
    distclass = get_function(mdnm=args["abcdistfl"], fnnm=args["abcdistcl"])
    distance = distclass (diagnostics=['statistic', 'observations'], weights=weights, obsvars=obsvars, args=args)

    # add AE args and make AE for spux
    args = add_ae_args(args)
    ae = make_ae_for_spux(args=args) # executed by all process: storing for calc_ss works

    # === BARRIER - split of processes (crazy design)
    connector = utils.select ('legacy', verbosity = args["verb_infer"])
    framework.barrier (connector = connector)

    # === INIT FRAMEWORK
    # MODEL
    model = spux_class (len_timeseries=args["len_timeseries"], fn=user_funct)

    # DISTANCE
    distance.assign (model, calc_ss, dataset)

    # SAMPLER
    assert isinstance(args["nchains_infer"],int), ":: Fatal: nchains_infer must be int."
    assert (args["nchains_infer"] % 2) == 0, ":: Fatal: nchains_infer must be even."
    sampler = SABC (batchsize=args["nchains_infer"], chains=int(args["nchains_infer"]/2),
                    epsilon_init=args["epsilon_ini_infer"])
    sampler.assign (distance, prior)

    # FRAMEWORK
    framework.assign (sampler)

    # attach the specified number of parallel workers
    distance.attach (workers=args["likelik_workers"])
    sampler.attach (workers=args["sampler_workers"])

    # setup SPUX framework
    sandbox = args["sandbox_infer"]
    from spux.utils.seed import Seed
    seed = Seed (args["seed_infer"])
    outputdir=args["infero"]
    framework.setup (sandbox=sandbox, seed=seed, verbosity=args["verb_infer"],
                     outputdir=outputdir, trace=args["trace_infer"], informative=args["informative_infer"])

    # init SPUX framework
    framework.init ()

    # === SAMPLING
    sampler.configure (lock = args["lock_infer"])
    sampler.init ()

    # generate samples from posterior distribution
    sampler (args["nrejuv_steps"]*args["nchains_infer"])

    # === EXIT FRAMEWORK

    framework.exit ()
