# baccione - 2021
#
# Collection of routines that import modularly and/or deal with args
#
import importlib
import glob, re, os
from pathlib import Path
from ae_src.tools.wraps import wrap_getattr, wrap_setattr
from ae_src.tools.utils import get_AEspecs_from_infls
#
def add_ae_args(args):

    val = args['ndims_latent'] - args['num_model_parameters']
    assert val >= 0, ":: Fatal: more parameters than latent dimensions?!?"
    wrap_setattr(args,'ndims_free_latent_parameters',val) # ditto
    num_conv_filters = get_AEspecs_from_infls (flnm=args['num_conv_filters_fl'], incomm="for num_conv_filters")
    wrap_setattr(args,'num_conv_filters',num_conv_filters)

    return args
#
def check_aepath(mdnm):
    if 'ae_src' not in mdnm: # add it
        path, thismd = os.path.split(os.path.abspath(__file__))
        pt = glob.glob(path + "/**/"+mdnm+".py", recursive = True)
        assert len(pt)==1, ":: Fatal: two files with same name in src code. This is a bug."
        st = re.search('ae_src(.*)'+mdnm, pt[0]).group(1)
        st = st.replace("\\","/")
        st = st.replace("/",".")
        mdnm = "ae_src" + st + mdnm
    return mdnm
#
def make_ae_for_spux(args=None, load_weights=False):
    """Returns the specified arch to the inference script in the parallel region so that each
    worker can count on its local copy and we do not have to passe the AE around with MPI and
    we also do not have to reconstruct it all the time if a stored copy is available (what a
    much better design of spux this would have been if all workers had seen everything and
    avoided to destroy all they see...)"""

    if args is not None:
        arch_module = importlib.import_module(wrap_getattr(args,'archsfl'))
        arch_spec = wrap_getattr(args,'aem2load')
        Architecture = getattr(arch_module, arch_spec)
        make_ae_for_spux.ae = Architecture(**args)
        #print("storing and returning: ", make_ae.ae)
        return make_ae_for_spux.ae
    else:
        #print("returning available: ", make_ae.ae)
        return make_ae_for_spux.ae # was already set-up
    assert False

def make_inpdic(args):
        """For now this is specialized, prolly will never need a wrapper to make any dictionary"""

        # Define inputs to generator function
        modfl = wrap_getattr(args, "user_model_inargs_fl")
        if modfl is None:
            return {'inpdic':None} # yes a dic
        inpdicnm = wrap_getattr(args, "user_model_inargs_dic")
        infl = Path(modfl)
        if infl.is_file():
            #print("Using external input file {} with dict for user model named {}".format(modfl,inpdicnm))
            spec = importlib.util.spec_from_file_location(inpdicnm,modfl)
            module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(module)
            inpdic = getattr(module,inpdicnm)
        else:
            assert False, ":: Fatal: dict input file {} not found (.py extension required).".format(modfl)
        print("Making {} directly available as specialized input to user model from {} in {}.".format(inpdic,inpdicnm,modfl))

        return inpdic

def get_function(mdnm=None, fnnm=None):
    """Would work also for getting a non-instatiated object (back in the day was called get_instance indeed)"""

    assert mdnm is not None, ":: Fatal: file name is None."
    assert fnnm is not None, ":: function name is None."

    if mdnm [-3:] != '.py' and mdnm [-4:] != '.pyc':
        mdnm = check_aepath(mdnm)
        module = importlib.import_module(mdnm)
        if hasattr(module, fnnm):
            fun = getattr(module, fnnm)
        else:
            print(":: Warning: specified module {} has no function {}.") # this might still be okay for inference
            fun = None
    else:
        assert mdnm [-3:] == '.py', ":: Fatal: I have no idea if .pyc files can be dealt with."
        print("Importing {} from {}".format(fnnm, mdnm))
        spec = importlib.util.spec_from_file_location('forza_viola', mdnm)
        module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module)
        fun = getattr(module, fnnm)

    return fun

def make_instance(*args, mdnm=None, objnm=None, **kwargs):
    """Makes an instance of a general object"""

    assert mdnm is not None, ":: Fatal: file name is None."
    assert objnm is not None, ":: objnm is None."

    if mdnm [-3:] != '.py' and mdnm [-4:] != '.pyc':
        mdnm = check_aepath(mdnm)
        print("Importing {} from {}".format(objnm, mdnm))
        module = importlib.import_module(mdnm)
        obj = getattr(module, objnm)
        instance = obj(*args, **kwargs)
    else:
        assert mdnm [-3:] == '.py', ":: Fatal: I have no idea if .pyc files can be dealt with."
        print("importing {} from {}".format(objnm, mdnm))
        spec = importlib.util.spec_from_file_location('choose_vegan', mdnm)
        module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module)
        obj = getattr(module, objnm)
        instance = obj(*args, **kwargs)

    return instance
