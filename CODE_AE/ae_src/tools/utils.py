import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
import numpy as np
from collections import namedtuple
import os
import glob
import json
import shutil
import tensorflow as tf
import sys
import datetime
#from ae_src.mpi.mpis import get_rank
from ae_src.inputs.infiles import get_file
from ae_src.tools.read_files import read_cols
from ae_src.tools.wraps import wrap_dir, wrap_getattr, wrap_hasattr

def set_gpus():
    """Primarily found in BiLSTM scripts, name of func is just tentative"""
    gpus = tf.config.list_physical_devices('GPU')
    if (len(gpus)>=1):
        tf.config.set_visible_devices(gpus[0], 'GPU') # use 1 GPU
    else:
        tf.config.set_visible_devices([], 'GPU') # do not use any GPU
    return gpus

def activate_gpus():
    """Primarily found in BiLSTM scripts, name of func is just tentative"""
    tf.keras.backend.clear_session()
    physical_devices = tf.config.list_physical_devices('GPU')
    for gpu_instance in physical_devices:
        tf.config.experimental.set_memory_growth(gpu_instance, True)
    return physical_devices

def activate_debug(DB = False):
    if DB:
        # AttributeError: module 'tensorflow._api.v2.config' has no attribute 'run_functions_eagerly'
        tf.config.run_functions_eagerly(True)

def check_tf_ckpt(path, pattern):
    '''Function checks path for TF model checkpoints that match the pattern and return the latest, if any.'''
    l_candidates = glob.glob(os.path.join(path, '*%s*.index' % (pattern)))
    if len(l_candidates) == 0:
        return None, None
    else:
        curr_latest = 0
        full_path_name = ''
        ckpt_num = None
        for full_path in l_candidates:
            fname = os.path.basename(full_path)
            _, ckpt, _ = fname.split('.')
            ckpt_num = int(ckpt.split('-')[-1])
            if ckpt_num > curr_latest:
                full_path_name = full_path[:-1*len('.index')]
                curr_latest = ckpt_num
        return full_path_name, curr_latest

def get_latest_ckpt(path, id_model='model', return_ckpt_num=False):
    assert (os.path.isdir(path)) #provided path should be a directory, not a file.
    fl = glob.glob(os.path.join(path, id_model+'.ckpt*meta*'))
    # print('fl is:\n%s' % (fl))
    if len(fl) == 0:
        raise AssertionError('Directory does not contain valid checkpoint files.')
    highest_ckpt = 0
    for fname in fl:
        ckpt_num = int(os.path.basename(fname).split('.')[1].split('-')[1])
        if ckpt_num > highest_ckpt:
            highest_ckpt = ckpt_num
    fname_tmp = glob.glob(os.path.join(path, '*-'+str(highest_ckpt)+'.meta'))[0]
    ckpt_name = fname_tmp[:-1*(len(fname_tmp.split('.')[-1])+1)]
    if return_ckpt_num:
        return ckpt_name, highest_ckpt
    else:
        return ckpt_name

def get_tf_optmzr(pkg=None):
    """Depending on the pkg package you have, the same thing is called in different ways"""

    assert pkg is not None, ":: Fatal: tensorflow not passed?"

    if hasattr(pkg.train,'AdamOptimizer'):
        res = pkg.train.AdamOptimizer

    elif hasattr(pkg.optimizers,'Adam'):
        res = pkg.optimizers.Adam
    else:
        assert False, ":: Fatal: Adam optimizer not found in provided tensorflow version"

    return res

def get_mk_initlz_iter (pkg=None):
    """Depending on the pkg package you have, the same thing is called different ways"""

    assert pkg is not None, ":: Fatal: dataset_train not passed?"

    if hasattr(pkg,'make_initializable_iterator'):
        res = pkg.make_initializable_iterator()
    elif hasattr(pkg,'data.make_initializable_iterator'):
        res = pkg.data.make_initializable_iterator()
    else:
        assert False, ":: Fatal: make_initializable_iterator not found"

class Args_():
    "convert dict to a class with attributes (to match code similar with training)"
    def __init__(self, d):
        for k in d.keys():
            setattr(self, k, d[k])

class Manage_Hyper_Parameters:
    '''A simple class to manage everything regarding hyper parameter setup of an experiment.
    Warn for modifications in the experiment setup if training was interrupted.'''

    def __init__(self, logdir):
        self.param_config_fn = os.path.join(logdir, 'hyper_parameters.json')
        if not os.path.isfile(self.param_config_fn):
            self.args = None
        else:
            with open(self.param_config_fn) as fh:
                args = json.load(fh)
            self.args = args #Args_(args) # convert dict to a class with attributes (to match code similar with training)
        self.logdir = logdir

    def check_args_maybe_append(self, args):
        if self.args is None:
            return None
        save_args = False
        tmplst = []
        for k in wrap_dir(args):
            if k.startswith('__') or k == 'max_training_steps': # you can change max_training_steps
                continue
            if not wrap_hasattr(self.args, k):
                print('WARNING: key "%s" was missing in the new hyper-parameter configuration; will renew file.' % k)
                save_args = True
            else:
                if wrap_getattr(self.args, k) != wrap_getattr(args, k):
                    tmplst = tmplst + [k]
        if len(tmplst) > 0:
            if not wrap_hasattr(self.args, 'unsafe'):
                assert False, ":: Fatal: Mismatch in hyper-parameter settings for: {}".format(tmplst) # could be acceptable is some case
            else:
                print(":: Warning: mismatch on these hyper-parameters: ",tmplst)
        else:
            print("No mismatch on hyper-parameters found.")
        if save_args:
            self.save_parameters(args)

    def save_parameters(self, args):
        d_args = {}
        for attr in wrap_dir(args):
            if not attr.startswith('__'):  # don't get methods
                d_args[attr] = wrap_getattr(args, attr)
        d = d_args
        keys = d.keys()
        k_drop = []
        for k in keys:
            json.dumps(d[k])
        with open(self.param_config_fn, 'w') as fh:
            json.dump(d, fh, sort_keys=True, indent=4)

def get_path2__file__(fl=None):
    """Intended for __file__ of calling routine. Make a few checks and gives back the best guess
    (finally in python3.9 they seem to have got that abs path is best, better late than never)"""

    assert fl is not None, ":: Fatal: fl in get_path2__file__ not passed?"

    res = os.path.join(os.getcwd(),fl) #getcwd usually means where the script runs from, not where utils.py is

    if not os.path.isfile(res): # may not work if we run with abs path to script
        res = fl

    assert os.path.isfile(res),":: Fatal: get__filepath__ failed to find the running script."

    return res

def pdbtr():
    """To set a pdb-debugger stop"""
    import pdb; pdb.set_trace()

@tf.function
def export_summary_scalars(dict_name_and_val, step, writer):
    '''dict_name_and_val = {'name1': value1, 'name2': value2, ...}'''
    if not isinstance(dict_name_and_val, dict):
        raise AssertionError('dict_name_and_val must be a dictionary.')
    with writer.as_default():
        for k in dict_name_and_val.keys():
            tf.summary.scalar(k, dict_name_and_val[k], step=step)

@tf.function
def export_summary_histograms(dict_name_and_val, step, writer):
    if not isinstance(dict_name_and_val, dict):
        raise AssertionError('dict_name_and_val must be a dictionary.')
    with writer.as_default():
        for k in dict_name_and_val.keys():
            tf.summary.histogram(k, dict_name_and_val[k], step=step)

def get_ckptname(logdir, id):
    """get the checkpoint file to load I guess"""
    fl = glob.glob(os.path.join(logdir, '*')) # full paths of all files
    fn = [it for it in fl if id in os.path.basename(it)] #keep only files that match id in the basenames
    flb = [os.path.basename(it) for it in fn] #keep only basenames of matching abs path filenames
    ind = np.argmax([int(it[len(id)+1:].split('.')[0]) for it in flb]) #get highest ckpt ind of matching filenames
    fname = fn[ind] #get abs filename of the matching index file
    fname = '.'.join(fname.split('.')[:-1]) # truncate extension of the file: e.g., /path/to/file/{id}-{ind}.{extension} -> /path/to/file/{id}-{ind}
    return fname

def sanity_checks(have_keys=False,kwrds=None):
    """to be developed further"""
    if not have_keys:
        pass
        #my_rank = get_rank(doprints=True) # unimportant for now, disabled for Daint 
    else:
        pass

def setup_prelims(kwrds):
    """to be developed further"""
    pass

def convert_lstr2lint(x):
    """Convert a multilevel list of strings to the corresponding multivel list of ints"""

    if isinstance(x, list):
        return list(map(convert_lstr2lint, x))
    else:
        return int(x)

def get_AEspecs_from_infls(flnm, incomm=""):
    """Get arch-specific user settable attributes"""
    fl = get_file(flnm, incomm=incomm)
    lst = read_cols(fl)
    ae_attribute = convert_lstr2lint(lst)

    return ae_attribute
