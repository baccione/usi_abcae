#
import importlib
import sys
from scipy import stats
from ae_src.tools.read_files import read_cols
from ae_src.easy.build_distributions import Tensor
#
def uniform(*args):
    """wrapper to stats.unifor"""

    assert len(args[0]) == 2, ":: Fatal, wrong number of args for uniform distribution {}".format(len(args))
    loc = float(args[0][0])
    scl = float(args[0][1])

    distr = stats.uniform(loc=loc, scale=scl)
    return distr
#
def prior_by_stats(*args, **kwargs):
    """make prior by using stats"""
    fl = kwargs.get("priorinfl")

    entries = read_cols(fl)
    assert len(entries) > 0, ":: Fatal: no distributions specified for prior?!?."
    distributions = {}
    #this_module = sys.modules[__name__] # pipeline with within-this-module wrapper worked fine
    stats_module = importlib.import_module("scipy.stats")

    assert all([int(x[0])==1 for x in entries]), ":: Fatal: multivariate distributions not yet supported."

    for i in range(0, len(entries)):
        if int(entries[i][0]) == 1:
            k = entries[i][1]
            d = entries[i][2]
            #distributions [k] = getattr(this_module, d)(entries[i][3:]) # pipeline was working well
            #args = [ float(x) for x in entries[i][3:] ] # ValueError: could not convert string to float: '2.0**2'
            args = [ float(eval(x)) for x in entries[i][3:] ] # note that without float() 1 would eval to int, could be useful
                                                              # but we'd have to stipulate with the users that 1 and 1.0 are !=
            distributions [k] = getattr(stats_module, d)(*args)
        else:
            assert False, ":: Fatal: code here something smart to support multivariate distributions."

    prior = Tensor(distributions) # distributions + combo rule to go from marginals to joint should suffice

    return prior
