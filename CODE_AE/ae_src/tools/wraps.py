#
import tensorflow as tf
from tensorflow.python.ops import math_ops

def wrap_hasattr(caller, k):
    if isinstance(caller,dict):
        val = k in caller
        return val
    else:
        val = hasattr(caller, k)
        return val

def wrap_getattr(caller, k):
    if isinstance(caller,dict):
        if k in caller:
            val = caller [k]
        else:
            val = None
        return val
    else:
        val = getattr(caller,k,None)
        return val

def wrap_setattr(caller, k, val):
    if isinstance(caller,dict):
        caller [k] = val
        return caller
    else:
        setattr(caller, k, val)
        return caller

def wrap_dir(caller):
    if isinstance(caller,dict):
        return caller.keys()
    else:
        return dir(caller)

class LearningRateScheduleExponentialDecayWithLinearWarmup(tf.keras.optimizers.schedules.LearningRateSchedule):

    @tf.function
    def __init__(self, steps_warmup, initial_learning_rate, decay_steps, decay_rate, staircase):
        super(LearningRateScheduleExponentialDecayWithLinearWarmup, self).__init__()
        self.steps_warmup = steps_warmup
        self.initial_learning_rate = initial_learning_rate
        self.warmup_start_lr = 1e-9
        self.slope = (self.initial_learning_rate - self.warmup_start_lr) / max(self.steps_warmup,1)
        self.lr_expdecay = tf.keras.optimizers.schedules.ExponentialDecay(initial_learning_rate=initial_learning_rate, decay_steps=decay_steps, decay_rate=decay_rate, staircase=staircase)
        self.counter_linear = 0
        self.counter_exps = 0

    @tf.function
    def linear_warmup(self, step):
        return self.slope * (math_ops.cast(step, tf.float32)+1.) + self.warmup_start_lr

    @tf.function
    def __call__(self, step):
        if step < self.steps_warmup:
            return self.linear_warmup(step)
        else:
            return self.lr_expdecay(step-self.steps_warmup)
