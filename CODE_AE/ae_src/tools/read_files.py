import argparse
import pandas as pd
from pathlib import Path
#from ae_src.mpi.mpis import get_rank
from ae_src.inputs.infiles import get_default
from ae_src.tools.wraps import wrap_hasattr
from ae_src.tools.wraps import wrap_getattr
from ae_src.tools.wraps import wrap_setattr

def read_cols(infl):
    """read a files organized in columns, whatever it means"""

    fl = Path(infl)
    if fl.is_file():
        entries = []
        with open(fl) as fo:
            for line in fo:
                lnspl = line.split()
                end = len(lnspl)
                for i, s in enumerate(lnspl):
                    ss = list(s)
                    if ss[0]=="#":
                        end = i
                        break
                lnspl = lnspl[0:end]
                if len(lnspl) > 0:
                    entries.append(lnspl)
    else:
        assert False, ":: Fatal: file {} not found.".format(fl)

    return entries

def read_pdcsv(infl, usecols=None, names=None, index_col=0):
    """typically used to read input observations"""

    fl = Path(infl)
    if fl.is_file():
        if names is None:
            dataset = pd.read_csv (fl, sep=",", usecols=usecols, index_col=index_col)
        else:
            dataset = pd.read_csv (fl, sep=",", names=['time','pos'], usecols=usecols, index_col=index_col)
    else:
        assert False, ":: Fatal: file {} not found.".format(fl)

    return dataset

def parse_args_kfl():
    """to get the path to the keyfile"""

    parser = argparse.ArgumentParser()
    parser.add_argument("-k","--keyfile",default="./key_train.key",
                        help="path to keyfile. Default to ./key_train.key")

    args, unknown = parser.parse_known_args()

    return args

def set_defaults_keys(caller=None):

    kdefaults = { # main limitation seems that we are constrained to just 1 output feature for now
        "dotrain":1,
        "doinference":0,
        "logdir":'./tmplogdir',
        "ndims_latent":3,
        "len_timeseries":200,
        "num_noise_channels":1,
        "batch_size":300,
        "max_training_steps":int(1*1e6), #doesn't get default to type
        "freq_log":100,
        "linear_warmup_steps":int(0),
        "num_model_parameters":2,
        "reconstruction_loss_start_step":-1,
        "num_different_mb":60,
        "x_recon_precision":None,
        "loss_scale_optimizer":0,
        "step_num_activate_sharp_embedding":int(3*1e5),
        "num_input_channels":1,
        "load_fl_basename":'model_ckpt',
        "tf_cpp_min_log_level":1,
        "numexpr_max_threads":8,
        "varstatsfl":'stats_varw_enca.dat', # no default input file and there should not be one
        "aem2load":'BiLSTM',
        "archsfl":'ae_src.modls.archs',
        "umodlfl":'ae_src.modls.NLARs',  # default is module spec and not a file to circumvent intricacies
        "umodlcl":'DataGenerator_NLAR1_Simplified',
        "umodlfn":'NLAR1_SquaredExp',
        "fatal_hyper_param_mism":0,
        "spuxmodlfl":'ae_src.easy.nlar1_spux',
        "spuxmodlcl":'NLAR1_SPUX',
        "aewrapfl":'ae_src.easy.ae_wraps',
        "summstatfn":'calc_summary_stats_ENCA_BiLSTM',
        "obsinfl":'observations.csv',
        "readcsvfl":'ae_src.tools.read_files',
        "readcsvfn":'read_pdcsv',
        "priorinfl":'prior.in',  # no default input file and there should not be one
        "makepriorfl":'ae_src.tools.make_prior', # default is module spec and not a file
        "makepriorfn":'prior_by_stats',
        "abcdistfl":'ae_src.easy.abc_distances',
        "abcdistcl":'distance_a',
        "use_spux":1,
        "add2pypath":0,
        "pypath":['mypypath'],
        "loss_reconstruction_fl":'ae_src.modls.losses',
        "loss_reconstruction_fn":'loss_reconstruction_fn_a',
        "loss_regress_params_fl":'ae_src.modls.losses',
        "loss_regress_params_fn":'loss_regress_params_fn_a',
        "ulossfl":'ae_src.modls.losses',
        "ulosscl":'ChiSquareStatistic_a',
        "maintrainfl":'train_ENCA_BiLSTM',
        "mainfnnm":'main',
        "inferfl":'ae_src.mains.infer_dev_marco',
        "inferfnnm":'infer',
        "user_model_inargs_fl":None,
        "user_model_inargs_dic":'inpdic',
        "obsvarnms":['x'],
        "dossvarw":0,
        "ssvarwfl":'ae_src.mains.est_ss_var',
        "ssvarwfnnm":'calc_ss_varwghts',
        "unsafe":0,
        "nparams_samples":100,
        "nmodel_samples":25,
        "write_params":0,
        "write_output":0,
        "write_noise":0,
        "write_ss_obs":1,
        "write_sstats":0,
        "ae_load_weights":0,
        "global_grad_clipnorm":1e5,
        "ae_num_units":16,
        "num_conv_filters_fl":'num_conv_filters.in',
        "dopostproc":0,
        "get_samples_sabc":1, # default if dopostproc is True
        "infero":'output_dev_marco', # set the dir for the inference
        "getnsmpls":1,     # if 0 should return the min number to have complete preds (set big enough to get all)
        "startfromlast":1, # if 1 starts from last
        "extrpreds":[None], # if not None, will extract the given names; TODO: if all should extract all available preds
        "extrstats":1, # usually you want them
        "verb_infer":1, # to control spux verbosity
        "nchains_infer":100, # to control the number of chains (that is also particles I believe)
        "epsilon_ini_infer":1e3,
        "seed_infer":8,
        "trace_infer":0, # I don't remember what important spux parameter this is
        "informative_infer":None,
        "sandbox_infer":None, # I am not even sure this is an integer or a str
        "lock_infer":1,
        "nrejuv_steps":2000,
        "sampler_workers":1,
        "likelik_workers":1,
        "dosyn":0,
        "synfl":'ae_src.mains.synthesize',
        "synfnnm":'synrun',
        "global_gradient_clipnorm":None,
        "gradient_clip_value":None,
    }

    ktypes = {
        "dotrain":int,
        "doinference":int,
        "logdir":str,
        "ndims_latent":int,
        "len_timeseries":int,
        "num_noise_channels":int,
        "batch_size":int,
        "max_training_steps":int, #doesn't get default to type
        "freq_log":int,
        "linear_warmup_steps":int,
        "num_model_parameters":int,
        "reconstruction_loss_start_step":int,
        "num_different_mb":int,
        "x_recon_precision":float, # yes a string
        "loss_scale_optimizer":int, # False
        "step_num_activate_sharp_embedding":int,
        "num_input_channels":int,
        "load_fl_basename":str,
        "tf_cpp_min_log_level":int,
        "numexpr_max_threads":int,
        "varstatsfl":str,
        "aem2load":str,
        "archsfl": str,
        "umodlfl":str,
        "umodlcl":str,
        "umodlfn":str,
        "fatal_hyper_param_mism":int, # False
        "spuxmodlfl":str,
        "spuxmodlcl":str,
        "aewrapfl":str,
        "summstatfn":str,
        "obsinfl":str,
        "priorinfl":str,
        "readcsvfl":str,
        "readcsvfn":str,
        "makepriorfl":str,
        "makepriorfn":str,
        "abcdistfl":str,
        "abcdistcl":str,
        "use_spux":int,
        "add2pypath":int,
        "pypath":[str],
        "loss_reconstruction_fl":str,
        "loss_reconstruction_fn":str,
        "loss_regress_params_fl":str,
        "loss_regress_params_fn":str,
        "ulossfl":str,
        "ulosscl":str,
        "maintrainfl":str,
        "mainfnnm":str,
        "inferfl":str,
        "inferfnnm":str,
        "user_model_inargs_fl":str,
        "user_model_inargs_dic":str,
        "obsvarnms":[str],
        "dossvarw":int,
        "ssvarwfl":str,
        "ssvarwfnnm":str,
        "unsafe":int,
        "nparams_samples":int,
        "nmodel_samples":int,
        "write_params":int,
        "write_output":int,
        "write_noise":int,
        "write_ss_obs":int,
        "write_sstats":int,
        "ae_load_weights":int,
        "global_grad_clipnorm":float,
        "ae_num_units": int,
        "num_conv_filters_fl": str,
        "dopostproc":int,
        "get_samples_sabc":int,
        "infero":str,
        "getnsmpls":int,
        "startfromlast":int, # if 1 starts from last
        "extrpreds":[str],
        "extrstats":int,
        "verb_infer":int,
        "nchains_infer":int,
        "epsilon_ini_infer":float,
        "seed_infer":int,
        "trace_infer":int, # I don't remember what important spux parameter this is
        "informative_infer":int,
        "sandbox_infer":int, # I am not even sure this is an integer or a str
        "lock_infer":int,
        "nrejuv_steps":int,
        "sampler_workers":int,
        "likelik_workers":int,
        "dosyn":int,
        "synfl":str,
        "synfnnm":str,
        "global_gradient_clipnorm":int, # I am not even sure this is an integer (or bool) or a float
        "gradient_clip_value":int, # I am not even sure this is an integer (or bool) or a float
    }
    #'nlar1_spux' in kbuiltins['spuxmodlfl'][0]
    kbuiltins = { # built in values, some (not all) are checked if unsafe is not 1, could have used those as defaults
        "dotrain":[0,1],
        "doinference":[0,1],
        "load_fl_basename":['model_ckpt','model_best_ckpt'],
        "aem2load":['BiLSTM','FC_multibatch'],
        "archsfl":['ae_src.modls.archs'],
        "umodlfl":['ae_src.modls.NLARs','ae_src.modls.solar_dyns','ae_src.modls.blowfly'],
        "umodlcl":['DataGenerator_NLAR1_Simplified','DataGenerator_NLAR1_Simplified_BatchSampler',
                   'DataGenerator_SolarDynamo_Simplified','DataGenerator_SolarDynamo_Simplified_BatchSampler',
                   'DataGenerator_Blowfly_Simplified','DataGenerator_Blowfly_Simplified_BatchSampler'],
        "umodlfn":['NLAR1_SquaredExp','NLAR1_SquaredCub'],
        "fatal_hyper_param_mism":[0,1],
        "spuxmodlfl":['ae_src.easy.nlar1_spux','ae_src.easy.solarDynamo_spux','ae_src.easy.blowfly_spux'],
        "spuxmodlcl":['NLAR1_SPUX','solarDynamo_SPUX','Blowfly_SPUX'],
        "aewrapfl":['ae_src.easy.ae_wraps'],
        "summstatfn":['calc_summary_stats_ENCA_BiLSTM','calc_summary_stats_INCA_FCmulB'],
        "readcsvfl":['ae_src.tools.read_files'],
        "readcsvfn":['read_pdcsv'],
        "makepriorfl":['ae_src.tools.make_prior'],
        "makepriorfn":['prior_by_stats'],
        "abcdistfl":['ae_src.easy.abc_distances'],
        "abcdistcl":['distance_a'],
        "use_spux":[1], # no alternative for now
        "add2pypath":[0,1],
        "loss_reconstruction_fl":['ae_src.modls.losses'],
        "loss_reconstruction_fn":['loss_reconstruction_fn_a'],
        "loss_regress_params_fl":['ae_src.modls.losses'],
        "loss_regress_params_fn":['loss_regress_params_fn_a'],
        "ulossfl":['ae_src.modls.losses'],
        "ulosscl":['ChiSquareStatistic_a','MSE_withMedian_Loss','MAE_withMedian_Loss'],
        "maintrainfl":['train_ENCA_BiLSTM','train_INCA_FCmulB'],
        "mainfnnm":['main'],
        "inferfl":['ae_src.mains.infer_dev_marco'],
        "inferfnnm":['infer'],
        "dossvarw":[0,1],
        "ssvarwfl":['ae_src.mains.est_ss_var'],
        "ssvarwfnnm":['calc_ss_varwghts'],
        "unsafe":[0,1],
        "write_params":[0,1],
        "write_output":[0,1],
        "write_noise":[0,1],
        "write_ss_obs":[0,1],
        "write_sstats":[0,1],
        "ae_load_weights":[0,1],
        "dopostproc":[0,1],
        "get_samples_sabc":[0,1],
        "startfromlast":[1], # must be 1 for now
        "extrstats":[0,1],
        "dosyn":[0,1],
        "synfl":['ae_src.mains.synthesize'],
        "synfnnm":['synrun'],
    }

    checkbuiltins = [
        "dotrain",
        "doinference",
        "load_fl_basename",
        "aem2load",
        "archsfl",
        "umodlfl",
        "umodlcl",
        "umodlfn",
        "fatal_hyper_param_mism",
        "spuxmodlfl",
        "spuxmodlcl",
        "aewrapfl",
        "summstatfn",
        "readcsvfl",
        "readcsvfn",
        "makepriorfl",
        "makepriorfn",
        "abcdistfl",
        "abcdistcl",
        "use_spux",
        "add2pypath",
        "loss_reconstruction_fl",
        "loss_reconstruction_fn",
        "loss_regress_params_fl",
        "loss_regress_params_fn",
        "ulossfl",
        "ulosscl",
        "maintrainfl",
        "mainfnnm",
        "inferfl",
        "inferfnnm",
        "dossvarw",
        "ssvarwfl",
        "ssvarwfnnm",
        "unsafe",
        "write_params",
        "write_output",
        "write_noise",
        "write_ss_obs",
        "write_sstats",
        "ae_load_weights",
        "dopostproc",
        "get_samples_sabc",
        "startfromlast",
        "extrstats",
        "dosyn",
        "synfl",
        "synfnnm",
    ]

    assert len(checkbuiltins) == len(kbuiltins), ":: Fatal: checkbuiltins and kbuiltins lens differ. This is a bug."

    for k,v in kdefaults.items():
        assert k in ktypes, ":: Fatal: key {} has no associated type. This is a bug.".format(k)
        if v is None: # vars with a type but no default value
            continue
        if not isinstance(v,list):
            assert type(v) == ktypes [k], ":: Fatal: key {} has wrong type.".format(k)
        else:
            if v [0] is None:
                continue
            for i in range(len(v)):
                assert type(v[i]) == ktypes [k][i], ":: Fatal: key {} has wrong type.".format(k)

    if caller is not None:
        for key, value in kdefaults.items():
            print("setting key {} to {} (default)".format(key, value))
            setattr(caller, key, value)

    return kdefaults, ktypes, kbuiltins, checkbuiltins

def read_keyfl():
    """does the actual default assigment and reading of keyworkds"""

    parser = argparse.ArgumentParser()
    parser.add_argument("-k","--keyfile",default="./key_train.key",
                        help="path to keyfile. Default to ./key_train.key")

    args, unknown = parser.parse_known_args()
    kfl = args.keyfile

    #rnk = get_rank() # for daint, should not call MPI if not needed
    #if rnk==0:
    #    print("Got key file:",kfl)

    entries = read_cols(kfl)
    kwrds = {}
    for e in entries:
        kwrds [e[0]] = e[1:]

    return entries

def set_keys(dkws=None, ukws=None, kty=None):

    assert dkws is not None, ":: Fatal: calling set_keys with no default keys?!?"
    assert ukws is not None, ":: Fatal: calling set_keys with no user keys?!?"
    assert kty is not None, ":: Fatal: calling set_keys with no type keys?!?"

    inflsk = ['num_conv_filters_fl'] # list of input files, for now updated manually

    for k in inflsk:
        if k not in ukws:
            print (dkws [k])
            dkws [k] = get_default(k, dkws [k]) # search for the right default file in inputs folder

    for i in range(0,len(ukws)):
        k = ukws [i][0].lower()
        assert wrap_hasattr(dkws, k), ":: Fatal: specifying unknown keyword {} (or default vals not set)".format(k)
        v = ukws [i][1:]
        assert len(v) > 0, ":: Fatal: empty keyword {} ?!?".format(k)
        if len(v)==1 and not isinstance(wrap_getattr(dkws,k),list): # rm list if len 1 and not ref not list
            tp = kty[k]  # type(wrap_getattr(caller,k)) does not work anymore as I have Nones
            v = tp(v[0]) # cast as per default
            dkws = wrap_setattr(dkws, k, v)
            print('Keyword ',k,' gets value: ',v)
        else:
            assert isinstance(wrap_getattr(dkws,k),list), ":: Fatal: key {} with len > 1 not list?!?".format(k)
            tps = []
            for tp in kty[k]: # this is still ref value(s)
                tps = tps + [tp] # these are ref types of keyword k
            if len(tps)==1:
                vv = [tps[0](x) for x in v] # assumed all of the same type as ref key has only 1 entry
            else:
                assert len(tps)==len(v), ":: Fatal: mismatch on len of keyword {}.".format(k)
                vv = []
                for j in range(0,len(tps)):
                    print(v)
                    vv = vv + [tps[j](v[j])] # apply type cast one by one
            print('Keyword ',k,' gets values: ',vv)
            dkws = wrap_setattr(dkws, k, vv)

    return dkws

def keys_sanity_checks(keys, kbuilt, cbuilt):
    """A begin"""

    assert isinstance(keys["pypath"],list), ":: Fatal: pypath is not a list. This is a bug."
    howmanytrues = 0
    for k in ["dotrain", "dosyn", "doinference", "dossvarw", "dopostproc"]:
        howmanytrues += keys [k]
    assert howmanytrues <= 1, ":: Fatal: you can only have one request at a time at most"

    if keys ["dossvarw"] or keys ["doinference"]:
        assert keys ["ae_load_weights"], ":: Fatal: it makes little sense to compute ss var weights or perform inference without loading a trained AE."
    elif keys ["dotrain"]:
        assert not keys ["ae_load_weights"], ":: Fatal: in training available AE weights are loaded by default. Please, explicly disable keyworkd AE_LOAD_WEIGHTS."

    assert keys ["unsafe"] in [0,1], ":: Fatal: UNSAFE keyword not 0 nor 1"
    if not keys ["unsafe"]:
        for k in keys:
            if k in cbuilt:
                if len(kbuilt [k])==1:
                    test =  keys [k] in kbuilt [k]
                else:
                    test = [False]
                    for kb in kbuilt [k]:
                        print(k, keys[k], kb, keys[k] in [kb])
                        test += [keys[k] in [kb]]
                    if not any(test):
                        test = [False]
                        for kb in kbuilt [k]:
                            print(k, keys[k], kb, keys[k] in [kb][0])
                            test += [keys[k] in [kb][0]]
                    assert any(test), ":: Fatal: keyword {} has an unrecognized value {} rather than {} and UNSAFE is False.".format(k, keys [k], kbuilt [k])

def do_keys():
    defkeys, ktypes, kbuilt, cbuilt = set_defaults_keys()
    usrkeys = read_keyfl()
    keys = set_keys(defkeys, usrkeys, ktypes)
    keys_sanity_checks(keys, kbuilt, cbuilt)

    return keys
