#
def get_rank(comm=None,doprints=False):
    """get mpi rank in communicator comm, or get 0"""
    rnk = 0
    if comm is None:
        try:
            from mpi4py import MPI # not sure if this will ever work
            comm = MPI.COMM_WORLD
            rnk = comm.Get_rank()
            if doprints:
                print('I am rank:',rnk,'(mpi4py worked (this could still be serial))')
        except:
            if doprints:
                print(':: Warning: mpi failed, assigning rank 0')
            rnk = 0
    else:
        rnk = comm.Get_rank()
        if doprints:
            print('I am rank: ',rnk)

    return rnk
