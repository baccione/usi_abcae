# ABCAE - Approximate Bayesian Computations with Auto-Encoders


## Preamble
This is a pilot branch based on the original project that aims to make the 
software more accessible to practitioners, and simpler to develop further.
To these aims, `branch dev_marco_loadae` is implementing the following changes:

1. Reorganization of the code layout by provinding a cetralized structure, 
   which is collected in folder `ae_src` and relevant subfolders. 
   Application-specific files are organized in folder `exps` (stands for experiments), 
   e.g., `exps/NLAR1`.
2. Refactoring of the code to remove redundant parts (parts that were pragmatically 
   copied from on file to another for quick testing) and parts that do no appear 
   useful anymore, e.g.,  `sampler` class in model files. This is reducing the number 
   of files and the number of lines of code that we maintain and develop further. 

   The folder and file stucture in `ae_src` should give an idea of how the code is being 
   reorganized. In particular, it is important to note that we are attempting to unify 
   all the characteristics of a model in one single file. This is going to be fully 
   accomplished only for model `superflex`. For the other models we are still pragmatically 
   opting for having two different files, one for training and one for inference. However, 
   they rely on the same routine for model propagation (aka sampling of model states), which 
   is, for instance for NLAR1, `sample_nlar1_series` (note indeed at the top of file `ae_src/easy/nlar1_spux.py` 
   the import statement `from ae_src.modls.NLARs import sample_nlar1_series`). 
   
   The code is still relying on the SPUX framework for inference. However, there is an ongoing 
   separate attempt to remove this depency to create an internal SABC algorithm, called EASY, 
   which might be expanded further in the future given the experience collected with SPUX and 
   known limitations (see in particular files in `ae_src/easy`). However, this could be fully 
   accomplished only with a specific effort outside the ABCAE project.

3. Implementation of a *keyfile* (and all relevant functions) to operate the code. The keyfile is filled 
   by the practitioner according to a `key value` paradigm in order to operate the code. In other words, 
   the input keyfile is written by the user, read by our code at start, and instructs the code to 
   perform the operations that the user desires. This way, we avoid hardcoding of parameters, paths, 
   etc., and we avoid long command lines input, which are currently characteristic of temporary 
   development branches such as `dev_marco`. We also gives this way a precise standard to our code, 
   and a precise and controlled set of tasks that it can perform.

4. The user just runs a single file, a *main script* really, to operate the code. This file is always 
   the same, regardless of what the user wants to obtain (that is controlled by the keyfile, not by the 
   script to be run). This emulates how compiled codes work, that is, by running a single executable, 
   which does not change from the user perspective (only the input changes, at least in the great majority 
   (and only sane) set of cases). Such a main script for us is `ae_src/simple.py`.

5. Point 3 and 4 together means that to run the code we just need to 
   `python3 /your/path/to/ae_src/simple.py -k /your/path/to/keyfile` for training and 
   `mpiexec -n N python3 -m mpi4py /your/path/to/ae_src/simple.py -k /your/path/to/keyfile` for inference 
   (with N the number of parallel processes). While this is simple, having a working implementation of python3, 
   mpi, and all the requirements from the present software and from SPUX, might not be that easy. We assume for 
   now that those installations have been made and proceed to illustrate how to operate the code.

As a side note, we are focusing on the reimplementation of ENCA, leaving INCA a little bit behind. This is 
because of time constraints, but also because INCA would need more changes to be really useful, as propagating 
multiple model runs serially quickly becomes an unaffordable bottleneck (this means that INCA would very much 
benefit from a parallelization approach implemented in ABCAE, but that's quite demanding for a short term perspective).

## Keywords

Below the list of keywords that the user can specify to change the behavior of the software. Keywords take values 
either as integers, floats or strings. Keywords that imply a boolean value, are still expressed as integers (*0* 
means *False*, all other integers, specifically *1*, mean *True*). A few keywords default to *None* to comply with 
some specific internal functioning of out code

  **DOTRAIN**: default to 1 (True). Whether this run is a training run or not.

  **DOINFERENCE**: default to 0 (False). Whether this run is an inference run (mandates having a SPUX instance 
                   available for now).

  **LOGDIR**: default to *./tmplogdir*. Path to traing weights dir (corresponds to logdir autoencoder variable).

  **NDIMS_LATENT**: default to 3. Number of latent dimensions for the autoencoder

  **LEN_TIMESERIES**: default to 200. Length of timeseries.

  **NUM_NOISE_CHANNELS**: default to 1. Number of noise channels for the autoencoder.

  **BATCH_SIZE**: default to 300. Value of *batch_size* variable for the autoencoder, that is, how many data 
                  batches (aka, timeseries each one generated with a different set of model parameters values) 
                  are passed to the autoencoder for training as a single group of data, at least for ENCA. 
                  For INCA, *batch_size* seems to map to the number of model runs that are generated per set 
                  of parameters.

  **MAX_TRAINING_STEPS**: default to 1000000. Maximum number of training steps.

  **FREQ_LOG**: default to 100. Interval between subsequent checks of training performances and relevant 
                writing of checkpoint files. *TODO: Add a keyword to request writing down parameters, statistics 
                and predictions at each checkpoint as I have hardcoded in superflex.*

  **LINEAR_WARMUP_STEPS**: default to 0. How many steps of training should follow a linear warmup at the beginning 
                           of training.

  **NUM_MODEL_PARAMETERS**: default to 2. Number of model parameters. Primarily used by the autoencoder to normalize 
                            the relevant loss function.

  **RECONSTRUCTION_LOSS_START_STEP**: default to -1. A parameter for the functioning of the autoencoder. *TODO: check 
                                      what it really does and if it is still in use somewhere.*

  **NUM_DIFFERENT_MB**: default to 60: For INCA, this is the number of parameters draws that are concurrently seen by 
                        the autoencoder.

  **X_RECON_PRECISION**: default to None. A parameter for the functioning of the autoencoder. *TODO: check 
                         what it really does and if it is still in use somewhere.*

  **LOSS_SCALE_OPTIMIZER**: default to 0. A parameter for the functioning of the autoencoder. *TODO: check 
                            what it really does and if it is still in use somewhere.*

  **STEP_NUM_ACTIVATE_SHARP_EMBEDDING**: default to 300000. A parameter for the functioning of the autoencoder. *TODO: 
                                         check what it really does and if it is still in use somewhere.*

  **NUM_INPUT_CHANNELS**: default to 1. How many features are used as input to the autoencoder, which also likely means 
                          how many timeseries tha decoder has to learn to reconstruct. I think no values different from 
                          1 have ever been used, and hence the pipeline is likely to fail for any any value != 1. *TODO: 
                          double check usage of this paramter.*

  **LOAD_FL_BASENAME**: default to 'model_ckpt'. What is the basename for the file that contains the traing weights of the 
                        autoencoder and that we want to load for any task. *TODO: check if it also controls what checkpoint 
                        file is chosen at re-starting of training, I think so, and this is likely why it defaults to model_ckpt 
                        and not to model_best_ckpt.*

  **TF_CPP_MIN_LOG_LEVEL**: default to 1. A parameter for the functioning of the autoencoder. *TODO: 
                            check what it really does and if it is still in use somewhere.*

  **NUMEXPR_MAX_THREADS**: default to 8. A parameter that likely controls shared-memory parallelism in Tensorflwo. *TODO: 
                           check what it really does, if it is still in use somewhere, and what performace gain it delivers.*

  **VARSTATSFL**: default to 'stats_varw_enca.dat'. (path to) file that is contains the weights (e.g., variance based weighs 
                  written down with keyword dossvarw) for the statistics at distance computation in SABC

  **ARCHSFL**: default to 'ae_src.modls.archs'. Which built-in module or user-provided (path to) *.py* file contains the autoencoder 
               architechure specified by AEM2LOAD.

  **AEM2LOAD**: default to 'BiLSTM'. What type of autoencoder architechture to use. For now only BiLSTM (ENCA) has been ported 
                to this version of the code. *TODO: port also FC_multibatch (INCA).*

  **UMODLFL**: default to 'ae_src.modls.NLARs'. Which built-in module or user-provided *.py* file to look into to find the user 
               model specified with UMODLCL.

  **UMODLCL**: default to 'DataGenerator_NLAR1_Simplified'. Which user-model class to load among the ones possibly contained in 
               built-in module or user-provided python file UMODLFL.

  **UMODLFN**: default to 'NLAR1_SquaredExp'. Possible name of a function that is used passed to the *__init__* method of the user 
              model for later use. Useful for now only for NLAR1 to possibly switch from NLAR1_SquaredExp to NLAR1_SquaredCub.

  **FATAL_HYPER_PARAM_MISM**: default to 0. Whether mismatch on hyperparams at load of AE weights should be fatal.

  **SPUXMODLFL**: default to 'ae_src.easy.nlar1_spux'. Which built-in module or user-provided *.py* file to look into during
                  an inference run to find the user model class specified with SPUXMODLCL. *TODO: if EASY becomes a reality, 
                  SPUX -> EASY in names*.

  **SPUXMODLCL**: default to 'NLAR1_SPUX'. What user model calss to use for inference among the ones contained in built-in 
                  module or user-specified python file SPUXMODLFL.

  **AEWRAPFL**: default to 'ae_src.easy.ae_wraps'. Which built-in module or user-provided *.py* file to look into during an 
                inference run to find the distance function to be used by SPUX instances as specified by SUMMSTATFN.

  **SUMMSTATFN**: default to 'calc_summary_stats_ENCA_BiLSTM'. Which function is used by SPUX to compute the SABC distance.
                  Has to be contained in the module or python file specified with AEWRAPFL.
  
  **OBSINFL**: default to 'observations.csv'. (path to) file were reference observations are provided. It has to be a csv file 
               or has to be in compliance with the choice made with keyword READCSVFL and READCSVFN.

  **READCSVFL**: default to 'ae_src.tools.read_files'. Which built-in module or user-provided *.py* file to look into to find the 
                 method specified with READCSVFN to read-in the observations.                

  **READCSVFN**: default to 'read_pdcsv'. Which function to use to read-in the observations specified with OBSINFL. Has to be 
                 contained in built-in module or user-specified python file READCSVFL.

  **PRIORINFL**: default to 'prior.in'. (path to) input file that specifies how to construct the prior.

  **MAKEPRIORFL**: default to 'ae_src.tools.make_prior'. Which built-in module or user-provided (path to) *.py file 
                   should be used to make the prior through exectuting the function specified with MAKEPRIORFN.

  **MAKEPRIORFN**: default to 'prior_by_stats'. name of function within the provided MAKEPRIORFL file that the code should use 
                   to build the prior (subs built-in method prior_by_stats).

  **ABCDISTFL**: default to 'ae_src.easy.abc_distances'. (path to) *.py* file that contains the class to compute the distances 
                 for the inference framework. It has to contain the class specified by ABCDISTCL. 
                 For the moment a SPUX installation must be available to use ABC. It can be pointed to by using keyworkds ADD2PYPATH and PYPATH.
  
  **ABCDISTCL**: default to 'distance_a'. Python class that defines the method used by the inference framework to compute 
                 the distance in ABC. It has to be contained in built-in module or user-specified python file ABCDISTFL.
                 For the moment a SPUX installation must be available to use ABC. It can be pointed to by using keyworkds ADD2PYPATH and PYPATH

  **USE_SPUX**: default to 1. Whether or not to use SPUX as inference framework. For the moment this is the only available option. 
                For the moment a SPUX installation must be available to make inference. It can be pointed to by using keyworkds ADD2PYPATH and PYPATH

  **ADD2PYPATH**: default to 0. Whether or not to add to your pythonpath the paths specified with PYPATH.

  **PYPATH**: default to 'mypypath'. Path(s) to useful python modules, such as this module and spux. First string (path) after the keyword 
              will be at the very top of where python looks for modules, others will follow in order.

  **LOSS_RECONSTRUCTION_FL**: default to 'ae_src.modls.losses'. Which built-in module or user-provided (path to) *.py file 
                              that contains the function to compute the loss relevant to the reconstruction of the model output. 
                              It has to contain the funation specified with LOSS_RECONSTRUCTION_FN.

  **LOSS_RECONSTRUCTION_FN**: default to 'loss_reconstruction_fn_a'. Which function to use to compute the loss relevant to the reconstruction 
                              of the model output.

  **LOSS_REGRESS_PARAMS_FL**: default to 'ae_src.modls.losses'. (path to) *.py* file that contains the function to compute the loss relevant to 
                              the regression of the model parameters. It has to contain the function specified with LOSS_REGRESS_PARAMS_FN.

  **LOSS_REGRESS_PARAMS_FN**: default to 'loss_regress_params_fn_a'.

  **ULOSSFL**: default to 'ae_src.modls.losses'. Which built-in module or user-provided *.py* file to look into to find the loss 
               class specified with UMODLCL.

  **ULOSSCL**: default to 'ChiSquareStatistic_a'. Which loss class to be used by functions LOSS_RECONSTRUCTION_FN and LOSS_REGRESS_PARAMS_FN 
               among the ones possibly contained in built-in module or user-provided *.py* file ULOSSFL. For the time being separate losses 
               for output and parameters are not offered.

  **MAINTRAINFL**: default to 'train_ENCA_BiLSTM'. Which built-in module or user-provided (path to) *.py* file contains the main function to 
                   exectute the training, which is itself specified by MAINFNNM. *Built in option: train_ENCA_BiLSTM ...(to be completed)*

  **MAINFNNM**: default to 'main'. Name of function within the provided MAINTRAINFL file that the code should use as main program for training.

  **INFERFL**: default to 'ae_src.mains.infer_dev_marco'. Which built-in module or user-provided (path to) *.py* file contains the  
               function INFERFNNM, the execution of which specifies the steps of the inference.

  **INFERFNNM**: default to 'infer'. Name of function contained in built-in module or user-provided (path to) *.py* INFERFNNM that is exectuted 
                 to perform the inference (that is when DOINFER is enabled).

  **USER_MODEL_INARGS_FL**: default to None. (path to) file that contains the user-specified dictionary USER_MODEL_INARGS_DIC that is passed
                            to the user model UMODLCL during traing for any possible custom necessity.

  **USER_MODEL_INARGS_DIC**: default to 'inpdic'. Name of the dictionary that is passed to the user model UMODLCL and is defined in 
                             file USER_MODEL_INARGS_FL.

  **OBSVARNMS**: default to ['x']. Name of the variable used to compute the summary statistics during SABC inference with SPUX (only one 
                 variable for now, which is a limitation; name is given in user model UMODLCL during training).

  **DOSSVARW**: default to 0. Whether the run is a run to calculate the variance-based weights for the statistics or not.

  **SSVARWFL**: default to 'ae_src.mains.est_ss_var'. Which built-in module or user-provided (path to) *.py* file contains the main function to
                compute the variance-based weights for the statistics. It has to contain the function specified by SSVARWFNNM.

  **SSVARWFNNM**: default to 'calc_ss_varwghts'. Name of the function contained in built-in module or user-provided (path to) *.py* file SSVARWFL
                  that is used to compute the variance-based weights for the statistics.

  **UNSAFE**: default to 0. Whether a mismatch in hyper-parameter settings during training (as defined in function check_args_maybe_append) is fatal 
              or not. Could be used more widespread in the future.

  **NPARAMS_SAMPLES**: default to 100. How many parameters samples to draw for the computation of the variance-based weights for the statistics. 

  **NMODEL_SAMPLES**: default to 25. How many model exectutions to run per parameter sample for the computation of the variance-based weights for 
                      the statistics.

  **WRITE_PARAMS**: default to 0. Whether or not to save the parameters sampled for the computation of the variance-based weights for the statistics
                    in text file *sampled_parameters.dat*.

  **WRITE_OUTPUT**: default to 0. Whether or not to save the model output sampled for the computation of the variance-based weights for the statistics
                    in text files *model_output_?????.dat*, with ???? ranging from 00000 to 99999 and each corresponding to a different parameter sample.

  **WRITE_NOISE**: default to 0. Whether or not to save the noise processes sampled for the computation of the variance-based weights for the statistics
                   in text files *model_noise_?????_??.dat*, with ???? ranging from 00000 to 99999 and each corresponding to a different parameter sample, 
                   and ?? ranging in 00-99 corresponding to noise channels.

  **WRITE_SS_OBS**: default to 1. Whether or not to load observations (see READCSVFL, READCSVFN, and OBSINFL) to compute the summary statistics calculated
                    by the loaded autonecoder (see ARCHSFL, AEM2LOAD, AE_LOAD_WEIGHTS and LOGDIR) on those data, and write them to file *stats_obs.dat*.

  **WRITE_SSTATS**: default to 0. Whether or not to save the computed summary statistics for the computation of the variance-based weights for the statistics
                    in text files *model_output_?????.dat*, with ???? ranging from 00000 to 99999 and each corresponding to a different parameter sample. 
                    Each file contains the summary statistics for all the NMODEL_SAMPLES model trajectories.

  **AE_LOAD_WEIGHTS**: default to 0. Whether or not to load the weights for the specified autoencoder (see ARCHSFL and AEM2LOAD) and contained in folder LOGDIR.
                       This keywords is effective only when either DOSSVARW or DOINFER are set to 1, and must be set to 0 for training as the restore of checkpoints
                       happens automatically. This could be improved in the future.

  **GLOBAL_GRAD_CLIPNORM**: default to 100000. Parameter to control gradient clipping. It is currently not used as far as I can tell.

  **AE_NUM_UNITS**: default to 16. Control the number of units in the two Bidirectional LSTM layers of the built-in decoder function *decoder_fn* of class *BiLSTM* 
                    in module ae_src.modls.archs.

  **NUM_CONV_FILTERS_FL**: default to 'num_conv_filters.in'. Input file that specifies the number of convolutional filters in the implemented encoder function
                           for architecture *BiLSTM* in module ae_src.modls.archs. The default files is hosted in ae_src/inputs/num_conv_filters and is detected 
                           automatically within the source-code folders if no alternative is specified. It reads as:\ 
                           16 16 \
                           32 32 \
                           This files specifies a fist layer with 16 filters, then a second layer with 16 filters. 
                           Then, a MaxPool1D layer is placed (this happens at an newline instance in the input file file). 
                           Finally two layes with 32 filters each are added.

  **DOPOSTPROC**: default to 0. Whether or not to this run is deputed to the analysis of the inference results present in folder INFERO.

  **GET_SAMPLES_SABC**: default to 1. If DOPOSTPROC is activated this keywords instruct the code to extract the posterior distribution 
                        of parameters to file *dists_params_sabcae.csv*.

  **INFERO**: default to 'output_dev_marco'. Folder where to write to or read from the inference results.

  **GETNSMPLS**: default to 1. How many samples should be written to files if GET_SAMPLES_SABC is 1. The only allowed value if 1 for 
                 the moment as extraction of burn-in information is not offered yet.

  **STARTFROMLAST**: default to 1. Whether GETNSMPLS should be extracted from the end or the start of the inference. For now the only 
                     allowed values is 1 as extraction of burn-in information is not offered yet.

  **EXTRPREDS**: default to [None]. If specified, named variables EXTRPREDS will be extracted from posterior prediction dataset and written 
                 to file *preds_sabcae.csv* if DOPOSTPROC is activated and GET_SAMPLES_SABC is 1.

  **EXTRSTATS**: default to 1. If DOPOSTPROC is activated (set to 1) and GET_SAMPLES_SABC is also 1, whether or not to write posterior summary 
                 statistics to file  *dists_params_stats_sabcae.csv*.

  **VERB_INFER**: default to 1. Spux verbosity level.

  **NCHAINS_INFER**: default to 100. Number of chains, that is also particles, for SABC inference with SPUX.

  **EPSILON_INI_INFER**: default to 1000. SABC rate parameter for annealing temperature.

  **SEED_INFER**: default to 8. Seed for random number generator. It is used for inference with SPUX, but also for other tasks, primarily when 
                  a draw from the prior is needed as in the generation of a synthetic run (DOSYN) or for the computation of variance-based weights 
                  for statistics (DOSSVARW). It is also used by currently implemented models (so **_INFER** in keyword is for historical reasons).

  **TRACE_INFER**: default to 0. SPUX trace parameter for framework setup.

  **INFORMATIVE_INFER**: default to None. Informative level in SPUX framework setup.

  **SANDBOX_INFER**: default to None. Whether or not to use sandbox in SPUX. 

  **LOCK_INFER**: default to 1. Lock value for SPUX framework setup.

  **NREJUV_STEPS**: default to 2000. Number of rejuvenation steps in SABC, that is, how many times EPSILON_INI_INFER gets diminished.

  **SAMPLER_WORKERS**: default to 1. Number of workers to be used for SPUX sampler. Note that our code does not do anything to ensure that 
                       this number is in compliance with the number of summoned MPI processes.

  **LIKELIK_WORKERS**: default to 1. Number of workers to be used for SPUX distance computation, which should amount to particles per chain, 
                       which is likely meaningful only if it is 1.
  **DOSYN**: default to 0. Whether the run is a run to generate synthetic data from the specified user model.
  **SYNFL**: default to  *_src.mains.synthesize*. Which built-in module or user-provided (path to) *.py* file contains the  
             function SYNFNNM, the execution of which specifies the steps for the generation of synthetic data from the user model.
  **SYNFNNM**: default to *synrun*. Name of function contained in built-in module or user-provided (path to) *.py* SYNFL that is 
               exectuted to generate synthetic output from the user module (that is when DOSYN is enabled).
  **global_gradient_clipnorm**: default to *None*. Autoencoder parameter used in *tensorflow.clip_by_global_norm* function by the FC_multibatch 
                                architechture (or for anything that uses *train_step_INCA_FCmulB*, but modular *train step* is not yet offered).

  **gradient_clip_value**: default ot *None*. If not *None*, gradients are clipped to +/- this value by function *tf.clip_by_value* for architecture
                           FC_multibatch (or for anything that uses *train_step_INCA_FCmulB*, but modular *train step* is not yet offered).

