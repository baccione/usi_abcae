#
##########################################
# baccione - Oct 2021                    #
# can work only if user has reacheable   #
# (e.g. in PYTHONPATH) SPUX installation #
##########################################
#
import numpy as np
import pandas as pd
from ae_src.modls.blowfly import sample_blowfly_timeseries
from spux.models.model import Model
#
class Blowfly_SPUX(Model):

    def __init__(self, len_timeseries=180, initial=None, verbosity=0, **kwargs):

        self.len_timeseries = len_timeseries
        self.initial = initial
        self.sandboxing = False
        self.verbosity = 0

    def init (self, initial, parameters):

        self.time = 0
        if 'inipos' not in parameters:
            self.position = 0
        else:
            self.position = parameters ['inipos']

        for k, v in parameters.items():
            if 'log' in k:
                v = np.exp(v)
                k = k.replace('log.', '')
            setattr(self,k,v)

        for k in ['P', 'n0', 'sigmad', 'sigmap' , 'tau', 'delta']:
            assert hasattr(self, k), ":: Fatal: missing param {}".format(k)

    # run model all the way with SABC
    def __call__(self, parameters, times):

        assert self.len_timeseries == len(times), ":: Fatal: mismatch on len_timeseries {} \
                                                   and times {}.".format(self.len_timeseries,len(times))
        assert hasattr(self,'rng'),  ":: Fatal: doing spux inference without a pre-assigned rng?!?"
        assert self.rng is not None, ":: Fatal: doing spux inference with None pre-assigned rng?!?"

        self.init (None, parameters)

        info = {'successful' : True}

        predictions = sample_blowfly_timeseries(self.P, self.n0, self.sigmad,
                                                self.sigmap, self.tau, self.delta,
                                                prng=self.rng, len_timeseries=self.len_timeseries)

        self.time = times[-1]
        predictions = pd.DataFrame.from_dict(predictions)
        predictions.reindex(times)

        info ['predictions'] = predictions
        profile = None # legacy

        return info, profile
