#
import numpy as np
import importlib
from ae_src.imports import make_ae_for_spux
from ae_src.modls.archs import data2ss

def calc_summary_stats_ENCA_BiLSTM(observation):

    #if type(observation) is not np.ndarray:
    #    x = observation.to_numpy().reshape(-1)
    #else:
    #    x = observation.reshape(-1)

    #x_for_ae = np.reshape(x, (1, len(x), 1))

    #####################################################
    # Auto-Encoder
    #####################################################
    ae = make_ae_for_spux()
    #z_latent_temp = ae.encoder(x_for_ae)
    #z_latent = z_latent_temp.numpy()
    #stats = z_latent.reshape(-1, 1) # same as stats new
    stats_new = data2ss(observation.to_numpy(), ae).numpy().reshape(-1, 1)

    return stats_new

def calc_summary_stats_INCA_FCmulB(observation):

    ae = make_ae_for_spux()
    #dup = ae.aggregator.input_shape[1]
    #x_for_ae = np.stack([observation.values]*dup)
    #x_for_ae = np.expand_dims(x_for_ae, axis=0)

    #####################################################
    # Auto-Encoder
    #####################################################
    #z_latent_temp = ae.encoder(x_for_ae)
    #z_latent = z_latent_temp[0,0,:].numpy()
    #print(observation.shape)
    #print(ae.encoder.input_shape)
    #z_latent_new = ae.encoder(observation)
    #stats = z_latent.reshape(-1, 1)
    #stats_new = z_latent_new.numpy()
    stats_new2 = data2ss(observation.to_numpy(), ae).numpy().reshape(-1, 1)
    #print(stats) # they are all the same, 21 Dec 2021
    #print(stats_new)
    #print(stats_new2)

    return stats_new2
