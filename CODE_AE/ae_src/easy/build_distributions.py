import pandas
import numpy

# Suspended before deciding which is the best way to handle pmf, prolly by trial and error.
#def rounding (method):
#    '''A decorator to map method arguments from float to integer by rounding.'''
#
#    def mapped (value):
#        #print("value {} in rounding that will become: {}".format( value, (numpy.round (value)).astype(int)) ) #ok
#        return method ( (numpy.round (value)).astype(int) )
#    return mapped

class Tensor (object):

    def __init__ (self, distributions, units = None, types = None):

        self.distributions = distributions
        self.labels = list (distributions.keys ())
        self.configure (units, types)

    def configure (self, units = None, types = None):
        """Configure distribution units and variable types."""

        self.units = units
        self.types = {}

        for key in self.distributions:
            self.types [key] = 'float'
            # Suspended before deciding which is the best way to handle pmf, prolly by trial and error.
            #if types is not None:
            #    if key not in types:
            #        raise ValueError("Fatal. Specified parameter is not in the list.")
            #    self.types [key] = types [key]
            #    if types[key][0:3].lower() == 'int' or types[key].lower() == 'binary':
            #        self.distributions [key].pdf = rounding (self.distributions [key].pmf)
            #        self.distributions [key].logpdf = rounding (self.distributions [key].logpmf)

    # evaluate a joint PDF of the tensorized random variables
    # 'parameters' is assumed to be of a pandas.Series type
    def pdf (self, parameters):
        """Evaluate the (joint) prob. distr. function of the tensorized, i.e. assuming independence, random variables 'parameters'.

        'parameters' are assumed to be of a pandas.Series type
        """

        pdfs = [ distribution.pdf (parameters [label]) for label, distribution in self.distributions.items () if not numpy.isnan (parameters [label]) ]

        return numpy.prod (pdfs) if len (pdfs) > 0 else 0

    # evaluate a joint log-PDF of the tensorized random variables
    # 'parameters' is assumed to be of a pandas.Series type
    def logpdf (self, parameters):
        """Evaluate the logarithm of the (joint) prob. distr. function of the tensorized, i.e. assuming independence, random variables 'parameters'.

        'parameters' are assumed to be of a pandas.Series type
        """

        logpdfs = [ distribution.logpdf (parameters [label]) for label, distribution in self.distributions.items () if not numpy.isnan (parameters [label]) ]

        return numpy.sum (logpdfs) if len (logpdfs) > 0 else float ('-inf')

    # return marginal PDF for the specified parameter
    def mpdf (self, label, parameter):
        """Return marginal PDF for the specified parameter."""

        return self.distributions [label] .pdf (parameter)

    # return marginal log-PDF for the specified parameter
    def logmpdf (self, label, parameter):
        """Return marginal log-PDF for the specified parameter."""

        return self.distributions [label] .logpdf (parameter)

    # return intervals for the specified centered probability mass
    def intervals (self, alpha=0.99):
        """Return intervals for the specified centered probability mass."""

        intervals = { label : list (distribution.interval (alpha)) for label, distribution in self.distributions.items () }
        return intervals

    # draw a random vector using the provided random state 'rng'
    def draw (self, rng):
        """Draw a random vector using the provided random state 'rng'.
        """

        parameters = { label : distribution.rvs (random_state = rng) for label, distribution in self.distributions.items () }
        return pandas.Series (parameters)
