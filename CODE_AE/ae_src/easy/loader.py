import cloudpickle
import os
import glob
import numpy
import pandas
import re

def get_samples_sabc (kwrds):

    print("Writing down samples and co.")

    inferodir = kwrds.get("infero")
    nsmpls = kwrds.get("getnsmpls")
    stfrlast = kwrds.get("startfromlast")

    assert nsmpls==1, ":: Fatal: for now only nspls==1 is implemented for now"
    assert stfrlast, ":: Fatal: for now only starting from last is implemented"

    aval_smplfls = glob.glob(inferodir+"/"+"samples-*.dat")
    aval_infofls = glob.glob(inferodir+"/"+"infos-*.dat")
    aval_csvfls = glob.glob(inferodir+"/"+"samples-*.csv")
    assert len(aval_smplfls) > 0, ":: Fatal: no available samples?!?"
    assert len(aval_infofls) > 0, ":: Fatal: no available infos?!?"
    assert len(aval_csvfls) > 0, ":: Fatal: no available csvfls?!?"

    nfls = len(aval_smplfls)
    assert nfls == len(aval_infofls), ":: Fatal: different num of files."
    assert nfls == len(aval_csvfls), ":: Fatal: different num of files."

    sffxs = [None]*len(aval_smplfls)
    for i,fl in enumerate(aval_smplfls):
        sffxs [i] = int(re.findall('samples-(.+)-',fl)[-1])
    if stfrlast:
        indx = numpy.argsort([-x for x in sffxs])
    else:
        indx = numpy.argsort(sffx)
    sffxs = [ sffxs [i] for i in indx ]
    aval_smplfls = [ aval_smplfls [i] for i in indx ]
    sffxsold = sffxs.copy()

    sffxs = [None]*len(aval_infofls)
    for i,fl in enumerate(aval_infofls):
        sffxs [i] = int(re.findall('infos-(.+)-',fl)[-1])
    if stfrlast:
        indx = numpy.argsort([-x for x in sffxs])
    else:
        indx = numpy.argsort(sffx)
    sffxs = [ sffxs [i] for i in indx ]
    assert sffxs == sffxsold, ":: Fatal: mism on sffxs after sort."
    aval_infofls = [ aval_infofls [i] for i in indx ]

    sffxs = [None]*len(aval_csvfls)
    for i,fl in enumerate(aval_csvfls):
        sffxs [i] = int(re.findall('samples-(.+)-',fl)[-1])
    if stfrlast:
        indx = numpy.argsort([-x for x in sffxs])
    else:
        indx = numpy.argsort(sffx)
    sffxs = [ sffxs [i] for i in indx ]
    assert sffxs == sffxsold, ":: Fatal: mism on sffxs after sort."
    aval_csvfls = [ aval_csvfls [i] for i in indx ]

    for i,fl in enumerate(aval_infofls):
        tmp = int(re.findall('infos-(.+)-',fl)[-1])
        assert sffxs [i] == tmp, ":: Fatal: mismatch suffix for infos and samples"
    for i,fl in enumerate(aval_csvfls):
        tmp = int(re.findall('samples-(.+)-',fl)[-1])
        assert sffxs [i] == tmp, ":: Fatal: mismatch suffix for csvs and samples"

    if nsmpls == 1:
        aparams = pandas.read_csv(aval_csvfls [0])
        if aparams.columns[0] == 'Unnamed: 0':
            aparams.rename ({'Unnamed: 0':'pindex'}, axis=1, inplace=True)
            aparams.set_index (aparams.columns [0], inplace=True)
        with open (aval_infofls [0], "rb") as fl:
            info = cloudpickle.load (fl)
        with open (aval_smplfls [0], "rb") as fl:
            samples = cloudpickle.load (fl)
        assert all (samples.subtract(aparams).max() < 1E-12), ":: Fatal: mismatch from csv and dat sample files."
        nb = len(info)
        i = nb - 1
        nparams = info [i]['proposes'].shape[1]
        nchains = len(info [i]['infos'])
        parameters = info [i]['parameters'].copy()
        dists = pandas.DataFrame(info [i]['distances'].copy(), columns=['dists'])
        batch = info [i]['index']
        if kwrds ['extrpreds'][0] is not None or kwrds ["extrstats"]: # request to print out also preds and/or stats
            preds = [None]*nchains
            stats = [None]*nchains
            accepts = info [i]['accepts']
            assert len(accepts) == nchains, ":: Fatal: mism on len of accepts and nchains."
            notdone = False in accepts
            totaccepts = None
            if notdone:
                for i in range(nb-1,-1,-1): # go back and accept if preds is None is the right thing to do
                    accepts = info [i]['accepts']
                    if totaccepts is None:
                        totaccepts = accepts
                    else:
                        totaccepts += accepts
                    for j, bol in enumerate(accepts):
                        if not bol:
                            continue
                        assert info [i]['infos'][j]['successful'], ":: Fatal: accepted unsucess?!?"
                        if preds [j] is None and kwrds ['extrpreds'][0] is not None:
                            preds [j] = info [i]['infos'][j]['predictions'][kwrds['extrpreds']]
                            #a = parameters.iloc [j]['c0']
                            #b = preds [j].iloc[0]['c_0']
                            #assert a == b, ":: Fatal: mism pred {} par {}.".format(a,b) # tested once manually: OK
                            assert kwrds ['len_timeseries'] == preds [j].shape[0]
                        if stats [j] is None and kwrds ['extrstats']:
                            stats [j] = info [i]['infos'][j]['statistic']
            else:
                totaccepts = accepts
            notdone = any([x is None for x in preds])
            n = 1
            while notdone: # load subsequent files
                assert False in totaccepts, ":: Fatal: mismatch between notdone and totaccepts"
                with open (aval_infofls [n], "rb") as fl:
                    info = cloudpickle.load (fl)
                nb = len(info)
                for i in range (nb-1,-1,-1):
                    accepts = info [i]['accepts']
                    totaccepts += accepts
                    for j, bol in enumerate(accepts):
                        if not bol:
                            continue
                        if preds [j] is None and kwrds ['extrpreds'][0] is not None:
                            assert info [i]['infos'][j]['successful'], ":: Fatal: accepted unsucess?!?"
                            preds [j] = info [i]['infos'][j]['predictions'][kwrds['extrpreds']]
                            #a = parameters.iloc [j]['c0']
                            #b = preds [j].iloc[0]['c_0']
                            #assert a == b, ":: Fatal: mism pred {} par {}.".format(a,b) # tested once manually: OK
                        if stats [j] is None and kwrds ['extrstats']:
                            stats [j] = info [i]['infos'][j]['statistic']
                n = n + 1
                notdone = any([x is None for x in preds]) and not n == nfls

            assert len(preds) == parameters.shape [0], ":: Fatal: mism on number of preds and params."
            assert len(stats) == parameters.shape [0], ":: Fatal: mism on number of preds and params."

            if kwrds ['extrpreds'][0] is not None:
                assert all([x is not None for x in preds]), ":: Fatal: can not find some preds. Did you hide data? Or a bug"
                #zio = [ preds[i].iloc[0]['c_0'] for i in range(len(preds)) ]
                #for i in range(len(zio)):
                #    print(zio [i] - parameters.iloc[i]['c0']) # tested once manually: all 0: OK
                i = 0
                nms = ['batch', 'chain', 'time'] + [nm for nm in preds [0].columns]
                df = pandas.concat([ pandas.DataFrame([batch]*kwrds ['len_timeseries']),
                                     pandas.DataFrame([i]*kwrds ['len_timeseries']),
                                     pandas.DataFrame([t for t in range(0, kwrds ['len_timeseries'])]),
                                     preds [i] ], axis = 1)
                for i in range(1,len(preds)):
                    tmpdf = pandas.concat([ pandas.DataFrame([batch]*kwrds ['len_timeseries']),
                                            pandas.DataFrame([i]*kwrds ['len_timeseries']),
                                            pandas.DataFrame([t for t in range(0, kwrds ['len_timeseries'])]),
                                            preds [i] ], axis = 1)
                    df = df.append(tmpdf, ignore_index=True)

                df.columns = nms
                df.to_csv('preds_sabcae.csv', index=False)

    if kwrds ['extrstats']:
        dfstats = pandas.DataFrame(index=parameters.index,columns=['stat'+str(i) for i in range(0,len(stats[0]))] )
        for i in range(len(stats)):
            dfstats.iloc[i] = stats [i].flatten()
        df = pandas.concat([ dists, parameters, dfstats], axis=1)
        df.to_csv('dists_params_stats_sabcae.csv', index=False)
    else:
        df = pandas.concat([ dists, parameters], axis=1)
        df.to_csv('dists_params_sabcae.csv', index=False)

    print("Done writing down samples and co.")


def check_interval (name=None, directory='output'):
    """ Adjust intervals to feasible ones """

    assert (name is not None),":: Fatal: Please, specify a valid interval of batches. Got None."
    assert isinstance(name,list),":: Fatal: Please, specify a valid interval of batches. Did not get a list."
    assert all([ inm.split('-')[0] in ['samples','infos'] for inm in name ]), ":: Fatal: unknown rootname in check_interval. This is a bug."
    assert all([ inm.split('-')[2] in ['*.dat','*.csv'] for inm in name ]), ":: Fatal: unknown extention in check_interval. This is a bug."
    assert all([ len(inm.split('-'))==3 for inm in name ]), ":: Fatal: unknown length after split in check_interval. This is a bug."

    isinfo = False
    if name[0].split('-')[0] in ['infos']:
        isinfo = True

    avail_batches = list()
    avail_names = list()
    for inm in name:
        tmpstr = ''.join(glob.glob(os.path.join (directory, inm))) # intuitive as usual
        if tmpstr == '':
            print("batch {} not found".format(inm))
            continue
        if isinfo == True:
            actlb = [int(re.search('-(.+?)-', inm).group(1))]
            result = load (tmpstr, directory=None, verbosity=0)
            initb = [ actlb[0] - l + 1 for l in range(len(result),1,-1) ]
            actlb = initb + [int(re.search('-(.+?)-', inm).group(1))]
        else:
            actlb = [int(re.search('-(.+?)-', inm).group(1))]
        #print("actlb: {}".format(actlb))
        avail_batches = avail_batches + actlb  #.append(int(re.search('-(.+?)-', inm).group(1))) # very intuitive as always
        avail_names.append(inm)

    if tmpstr == '': # just don't guess anymore
         print(":: Warning: I could not include the last required batch: {}. This may be a bug or a wrong specification.".format(inm))
         assert len(avail_batches)>0, ":: Fatal. No eligible batches were found. This may be a bug or a wrong specification."

    avail_batches = sorted (avail_batches)
    avail_names = sorted (avail_names)

    return avail_names, avail_batches


def loadall (name="samples-*.dat", directory="output", verbosity=1, interactive=True, burnin=0, legacy=False, chains=None, last=False):
    """Load SPUX binary output files as generator."""

    if isinstance(name,str) == True:
        assert name.split('-')[0] in ['samples','infos'], ":: Fatal: unknown rootname in loadall. This is a bug."
        assert name.split('-')[1] in ['*.dat'], ":: Fatal: unknown extention in loadall. This is a bug."
    else:
        assert all([ inm.split('-')[0] in ['samples','infos'] for inm in name ]), ":: Fatal: unknown rootname in loadall. This is a bug."
        assert all([ inm.split('-')[2] in ['*.dat'] for inm in name ]), ":: Fatal: unknown extention in loadall. This is a bug."
        assert all([ len(inm.split('-'))==3 for inm in name ]), ":: Fatal: unknown length after split in loadall. This is a bug."

    if isinstance(name,str):
        path = os.path.join (directory, name)
        paths = sorted (glob.glob (path))
    elif isinstance(name,list):
        print("Going for interval of files:")
        path = list()
        for inm in name:
            tmpstr = ''.join(glob.glob(os.path.join (directory, inm))) # intuitive as usual
            if tmpstr == '': # should fatal now thanks to check_intervals
                assert False, ":: Fatal: got non-existing name in loadall. This is a bug."
                #continue
            path.append(tmpstr)
        paths = sorted (path)
    else:
        assert False, ":: Fatal: wrong type of name in loadall. This is likely a bug."

    previous = -1
    for checkpoint, p in enumerate (paths):
        lastindex = int (p.split ('-') [-2])
        count = lastindex - previous
        previous = lastindex
        if lastindex >= burnin:
            if burnin > 0:
                paths = paths [checkpoint:]
            break
    if last:
        paths = [paths [-1]]

    sizes = [ os.path.getsize (p) / (1024 ** 3) for p in paths ]
    size = numpy.sum (sizes)
    print ('  : -> Size of all \'%s\' files to be loaded (minimum requirement for RAM): %.1f GB' % (path, size))

    if size > 4 and interactive:
        print ('  : -> Proceed? [press ENTER if yes, and enter \'n\' if not]')
        reply = input ('  : -> ')
        if reply == 'n':
            print ('  : -> Canceling.' % size)
            yield None
        else:
            print ('  : -> Proceeding.' % size)

    previous = -1

    for checkpoint, path in enumerate (paths):
        lastindex = int (p.split ('-') [-2])
        count = lastindex - previous
        previous = lastindex
        result = load (path, directory=None, verbosity=verbosity)

        if chains is None:
            chains = len (result) // count
        #if verbosity:
        #    print ('  : -> Loaded %s with length %d%s and size: %.1f GB' % (path, len (result), (' (%d chains)' % chains) if burnin > 0 else '', sizes [checkpoint]))
        if burnin > 0 and checkpoint == 0:
            tail = - 1 - (lastindex - burnin)
            if hasattr (result, 'iloc'):
                result = result.iloc [tail * chains:]
            else:
                result = result [tail:]
        if legacy and burnin > 0 and checkpoint == 0:
            result = result.reindex (index = range (burnin * chains, burnin * chains + len (result)))
        yield result

def pickup (name="pickup-*.dat", directory="output"):
    """Loads the most recent pickup file."""

    path = os.path.join (directory, name)
    paths = sorted (glob.glob (path))
    assert os.path.isfile(paths [-1]), ":: Fatal: pick-up file {} not found. This may be a bug. Hack me if you know what you are doing.".format(paths[-1])
    if len (paths) >= 1:
        return load (os.path.basename (paths [-1]), directory)
    else:
        return None

def read_types (infl=None):
    """Read file with keys and types"""

    if infl is None or not os.path.exists (infl):
        return None

    if infl is not None:
        types = {}
        with open(infl) as fl:
            for line in fl:
                (key, val) = line.split()
                types[key] = val

    return types

def load_dict (filename = 'datasets/exact.py'):
    """Read text file into a dictionary."""

    if os.path.exists (filename):
        return pandas.read_csv (filename, sep="\s+", index_col=0, header=None, squeeze=True)
    else:
        return None

def load_csv (filename, verbosity = 0, directory='./output'):
    """Load csv file to a pandas.DataFrame."""

    if directory is not None:
        path = os.path.join (directory, filename)
    else:
        path = filename

    if os.path.exists ( path  ):
        return pandas.read_csv (path, sep=",", index_col=0)
    else:
        path = os.popen( 'ls '+ os.path.join (directory, filename) ).read().rstrip()
        if ( os.path.isfile(path) ):
            return pandas.read_csv (path, sep=",", index_col=0)
        else:
            return None

def load_dat(filename, directory='./output'):
    """ independent load of dat file more similar to load_csv """

    if directory is not None:
        path = os.path.join (directory, filename)
    else:
        path = filename

    if os.path.exists ( path ):
        with open (path, "rb") as f:
            obj = cloudpickle.load (f)
            return obj
    else:
        path = os.popen( 'ls '+ os.path.join (directory, filename) ).read().rstrip()
        if ( os.path.isfile(path) ):
            with open (path, "rb") as f:
                obj = cloudpickle.load (f)
                return obj
        else:
            return
