#############################################################
if __name__ == '__main__':
    assert False, ":: Fatal: this script is not be to be run, only to be imported"
#############################################################

#############################################################
import numpy as np
import pandas as pd
from spux.distances.distance import Distance
from spux.utils import transforms
#from ae_src.tools.imports import make_ae
#############################################################

#############################################################
class distance_a(Distance):
    """A distance measure based on a specified norm (custom or from np.linalg.norm (...)) of the statistic vector."""

    def __init__(self, order=2, norm=None, diagnostics = [], weights=1, obsvars=None, args=None):
        """If *norm* method is provided, it is applied on the difference of the statistics.
        Otherwise, if only *order* is provided, the np.linalg.norm (..., ord = order) is used instead.
        An optional *diagnostics* list of diagnostics to be included in info (irrespective of informative level) can be provided."""

        assert obsvars is not None, ":: Fatal: obsvars should contain the name of the observed output variable."
        assert isinstance(obsvars,list), ":: Fatal: obsvars should always be a list (have weakly typed languages)"
        assert len(obsvars)==1, ":: Fatal: we had no test case of more than one observations. Need to test/code."
        assert args is not None, ":: Fatal: to workaround spux, each call have to reassing statistics"

        self.order = order
        self.norm = norm
        self.diagnostics = diagnostics
        self.weights = weights
        self.prnt = True
        self.obsvars = obsvars
        self.args = args
        #self.ae = make_ae(args=self.args, load_weights=True) # TypeError: cannot pickle 'weakref' object - spux crazy design fault
        #self.ae = None # self assigning in call does not work either, this is going to kill performance

    def __call__(self, parameters):
        """Evaluate Euclidean distance to the specified parameters."""

        #if self.ae is None:
        #    self.ae = make_ae(args=self.args, load_weights=True) # TypeError: cannot pickle 'weakref' object
        #ae = make_ae(args=self.args, load_weights=True) # performance killer, mem killer, spux is a killer

        if self.verbosity >= 2:
            print("Distance computation for parameters:")
            print(parameters)

        self.setup(verbosity=0)

        # get times and snapshot flags
        times, snapshots = transforms.times (self.dataset, self.timeset)

        # contruct diagnostics function - I have no idea what it does and what it is useful for
        diagnostics = lambda time, prediction, parameters, rng : self.diagnose (time, prediction, parameters, rng, self.verbosity, snapshots, self.error)
        self.model.diagnostics = diagnostics

        # run model for specified times and obtain results
        results, timings = self.executor.map ([self.model], args = [parameters, times], sandboxes = [self.sandbox], seeds = [self.seed])
        assert len(results)==1,":: Fatal: len of results is not 1. Hack me if you know that's ok."

        # extract predictions, infos (e.g. including observations) and profile from results
        info, profile = results[0]

        # construct observations
        if self.error is not None:
            assert False, ":: Fatal: I believe you need to think harder."
            #if 'observations' not in info:
            #    for time in times:
            #        _diagnostics = diagnostics (time, info ['predictions'].loc [time], parameters, self.rng)
            #    transforms.append (info, time, _diagnostics)
            #observations = transforms.pandify (info ['observations'])
        else:
            indx = info ['predictions'][self.obsvars[0]].index
            ncols = len(self.obsvars)
            preds = pd.DataFrame(index=indx,columns=range(ncols))
            cnms = [preds.columns]
            #predsae = np.full((len(indx),ncols),float('nan'))
            for i in range(ncols):
                preds [i] = info ['predictions'][self.obsvars[i]]
                cnms [i] = info ['predictions'][self.obsvars[i]].name # it is almost impossible to rename one single column w/o hard-coding, crazy
                #predsae [:,i] = preds [i].values
            preds.columns = cnms

        #if ae.is_aggregator:
        #    data_for_ae = np.reshape(predsae, (len(predsae), 1))
        #else:
        #    data_for_ae = np.reshape(predsae, (1, len(predsae), 1))

        info["statistic"] = self.statistic(preds) #ae.encoder(data_for_ae)

        if self.norm is not None:
            distance = self.norm (info ["statistic"] - self.observed)
        else:
            distance = np.linalg.norm(np.multiply(self.weights, (info["statistic"] - self.observed)),
                                      ord=self.order)

        if (distance==0) and self.prnt:
            print(":: Warning: distance between stats is 0. Likely high-degeneracy in low dim proj (further warnings omitted till next init)")
            self.prnt = False

        if self.verbosity:
            print("Distance: {}".format(distance))

        if "statistic" not in self.diagnostics:
            assert False, "statistic not in self.diagnostics? And why so?"
            del info ["statistic"]
        if "observations" not in self.diagnostics:
            assert False, "observations not in self.diagnostics? And why so?"
            del info ["observations"]

        return distance, info
#############################################################
