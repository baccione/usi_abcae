# if you move this file from this folder, you'll get what you deserves
import os

def get_default(k, ofl):
    """set input file keyword to right default path"""

    path, thismd = os.path.split(os.path.abspath(__file__))
    fl = os.path.join(path, ofl)

    return fl

def get_file(fl, incomm="", take_def=True):
    """Get input file"""

    assert os.path.isfile(fl), ":: Fatal: user input file {} not found.".format(fl)
    if os.path.isfile(fl):
        print("Using user file {} {}".format(fl, incomm))

    return fl
