#############################################################
if __name__ != '__main__':
    assert False, ":: Fatal: this is main, not to be imported"
#############################################################

### check if we need to add to PYTHONPATH ###################
# cannot use anything from framework, as it may not be known
import argparse, sys
parser = argparse.ArgumentParser()
parser.add_argument("-k","--keyfile",default="./key_train.key",
                    help="path to keyfile. Default to ./key_train.key")
args, unknown = parser.parse_known_args()
kfl = args.keyfile
add2pypath = False
pypath = None
with open(kfl) as fo:
    for line in fo:
        lnspl = line.split()
        if lnspl [0].lower() == 'add2pypath':
            add2pypath = bool(lnspl[1])
        if lnspl [0].lower() == 'pypath':
            end = len(lnspl)
            for i, s in enumerate(lnspl):
                ss = list(s)
                if ss[0]=="#":
                    end = i
                    break
            lnspl = lnspl[0:end]
            if len(lnspl[1:]) > 0:
                pypath = lnspl[1:]
            else:
                assert ":: Fatal: empty pypath keyword. Please, clean your keyfile."
if add2pypath:
    assert pypath is not None, ":: Fatal: add2pypath True but no pypath found?!? This is inconsistent."
    #assert isinstance(pypath,list), ":: Fatal: pypath is not a list. This is a bug."
    sypath = sys.path
    doit = False
    if len(pypath) > len(sypath):
        doit = True
    else:
        for i,p in enumerate(pypath):
            if sypath [i] != pypath [i]:
                doit = True
                break
    if doit:
        for s in reversed(pypath):
            #print("Adding {} at beginning of sys.path".format(s))
            sys.path.insert(0, s)
        tmpp = []
        for s in sys.path:
            if s not in tmpp:
                tmpp += [s]
        sys.path = tmpp.copy()
#from ae_src.tools.utils import get_rank # disabled for daint for now
#cwrank = get_rank()
#if cwrank == 0:
#print("-----------------------------")
#print("sys.path: {}".format(sys.path))
#print("-----------------------------")
#############################################################
from ae_src.imports import get_function
from ae_src.tools.read_files import do_keys
from ae_src.tools.utils import sanity_checks
from ae_src.tools.wraps import wrap_getattr

sanity_checks(have_keys=False)
kwrds = do_keys()
sanity_checks(have_keys=True,kwrds=kwrds)

from ae_src.tools.utils import setup_prelims
setup_prelims(kwrds)

if kwrds["dotrain"]:
    train = get_function(mdnm=kwrds["maintrainfl"], fnnm=kwrds["mainfnnm"])
    train(kwrds)
elif kwrds["dosyn"]:
    syn = get_function(mdnm=kwrds["synfl"], fnnm=kwrds["synfnnm"])
    syn(kwrds)
elif kwrds["doinference"]:
    infer = get_function(mdnm=kwrds["inferfl"], fnnm=kwrds["inferfnnm"])
    if kwrds["use_spux"]:
        infer(kwrds)
    else:
        assert False, ":: You first have to develop EASY."
elif kwrds["dossvarw"]:
    ssvarw = get_function(mdnm=kwrds["ssvarwfl"], fnnm=kwrds["ssvarwfnnm"])
    ssvarw(kwrds)
elif kwrds["dopostproc"]:
    if kwrds ["get_samples_sabc"]:
        from ae_src.easy.loader import get_samples_sabc
        get_samples_sabc(kwrds)
    else:
        assert False, ":: Fatal: the rest of the post-processing still needs to be developed."
