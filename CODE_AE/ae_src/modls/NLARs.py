#
import importlib
import numpy as np
import tensorflow as tf
from ae_src.imports import get_function

def NLAR1_SquaredCub(c, x_old, sigma, epsilon):
    """lambda function makes code hanging, go figure"""
    if isinstance(c, float):
        assert isinstance(x_old,float) & isinstance(sigma,float) & isinstance(epsilon,float), ":: Fatal: inconsistent types in NLAR1_SquaredCub (1)."
        reslt = c * x_old**2 * (1. - x_old) + sigma * epsilon
    elif tf.is_tensor(c):
        assert tf.is_tensor(sigma) and tf.is_tensor(epsilon), ":: Fatal: inconsistent types in NLAR1_SquaredCub (2)."
        reslt = c * x_old**2 * (1. - x_old) + sigma * epsilon
    else:
        assert False, ":: Fatal: unrecognized type for c in NLAR1_SquaredExp."
    return reslt

def NLAR1_SquaredExp(c, x_old, sigma, epsilon):
    """Preferred way to 'fix' incompatibilities between numpy, tensorflow, python, and slow performances"""
    if isinstance(c, float):
        assert isinstance(x_old,float) & isinstance(sigma,float) & isinstance(epsilon,float), ":: Fatal: inconsistent types in NLAR1_SquaredExp (1)."
        reslt = c * x_old**2 * (np.exp(-x_old)) + sigma * epsilon
    elif tf.is_tensor(c):
        assert tf.is_tensor(sigma) and tf.is_tensor(epsilon), ":: Fatal: inconsistent types in NLAR1_SquaredExp (2)."
        reslt = c * x_old**2 * (tf.math.exp(-x_old)) + sigma * epsilon
    else:
        assert False, ":: Fatal: unrecognized type for c in NLAR1_SquaredExp."
    return reslt

def sample_nlar1_series(x0, c0, sigma0, prng=None, len_timeseries=200, fn=None):
    d = {'x_i': np.zeros((len_timeseries,)),
         'c_i':np.zeros((len_timeseries,)),
         'sigma_i': np.zeros((len_timeseries,)),
         'epsilon_i': np.zeros((len_timeseries,)),
         'c_0': float(c0),
         'sigma_0': float(sigma0),
         'x_0': float(x0),
         }
    x_old = x0
    c = c0
    sigma = sigma0
    for i in range(len_timeseries):
        epsilon = prng.normal(loc=0, scale=1)
        d['x_i'][i] = x_old
        d['c_i'][i] = c              # always the same value, a little wasteful or necessary for tf?
        d['sigma_i'][i] = sigma      # always the same value, a little wasteful or necessary for tf?
        d['epsilon_i'][i] = epsilon
        # get the next timeseries item.
        x_new = fn(c=c, x_old=x_old, sigma=sigma, epsilon=epsilon)
        # update parameters (when needed)
        x_old = x_new
    return d

class DataGenerator_NLAR1_Simplified():

    def __init__(self, **kwargs):

        self.prng = kwargs.get('prng', np.random.RandomState(seed=kwargs.get("seed_infer")))
        self.len_timeseries = kwargs.get('len_timeseries', int(200))
        self.prior = kwargs.get('prior',None)
        assert self.prior is not None, ":: Fatal: spec prior for model."
        # I have no idea why we train the AE on a fixed value for x_0 to then infer it on a unif distribition with SABC
        self.x_0 = kwargs.get('x_0', 0.25) # can come from specialized dictionary
        self.fn = get_function(kwargs.get("umodlfl"), kwargs.get("umodlfn"))

    def __iter__(self, **kwargs): # kwargs are simply not passed during training, but they can be during inference

        while True:
            inparams = kwargs.get('inparams',None)
            if inparams is None: # training
                parameters = self.prior.draw(self.prng) #.to_dict()
            else: # ssvarw and synthetic
                parameters = inparams

            x0 = parameters.get('x0', self.x_0) # takes it from prior if it is there, or from __init__ otherwise
            sample = sample_nlar1_series(x0=x0, c0=parameters['c0'], sigma0=parameters['sigma0'],
                                         len_timeseries=self.len_timeseries, prng=self.prng, fn=self.fn)

            noise = np.expand_dims(sample['epsilon_i'], -1) # shape [timeseries, 1]
            x = np.expand_dims(sample['x_i'], 1) # [len_timeseries, 1]
            #d = (x, tuple( np.array( list(parameters.values()) ) ), noise)
            d = (x, parameters, noise)

            yield d

class DataGenerator_NLAR1_Simplified_BatchSampler(DataGenerator_NLAR1_Simplified):
    
    def __init__(self, **kwargs):

        self.batch_size = kwargs.get('batch_size')
        super().__init__(**kwargs)

    def __iter__(self, **kwargs): # kwargs are simply not passed during training, but they can be during inference

        while True:
            inparams = kwargs.get('inparams',None)
            if inparams is None: # training
                parameters = self.prior.draw(self.prng) #.to_dict()
            else: # ssvarw and synthetic
                parameters = inparams

            l_observation = np.empty((self.batch_size, self.len_timeseries, 1))
            l_noise = np.empty((self.batch_size, self.len_timeseries, 1))
            x0 = parameters.get('x0', self.x_0) # takes it from prior if it is there, or from __init__ otherwise
            for i in range(self.batch_size):
                sample = sample_nlar1_series(x0=x0, c0=parameters['c0'], sigma0=parameters['sigma0'],
                                             len_timeseries=self.len_timeseries, prng=self.prng, fn=self.fn)
                l_observation[i, ...] = np.expand_dims(sample['x_i'], 1)
                l_noise[i,...] = np.expand_dims(sample['epsilon_i'], -1)

            params = (parameters['c0'], parameters['sigma0'])
            d = (l_observation, params, l_noise)

            yield d
