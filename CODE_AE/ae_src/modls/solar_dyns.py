#
import math
import numpy as np
#
def solar_dynamo_simply(randval_a,x_old,randval_e):

    reslt = randval_a * x_old * 0.5 * (1.0 + math.erf((x_old - 0.6) / 0.2)) * (1.0 - math.erf((x_old - 1.0) / 0.8)) + randval_e

    return reslt

def babcock_leighton_fn(p_n, b_1=0.6, w_1=0.2, b_2=1.0, w_2=0.8):
    '''
    Babcock-Leighton function measuring the efficiency of poloidal field production from the decay of active regions as
    a function of the deep-seated toroidal magnetic component.
    ----
    Function implements f(p_{n}), Eqn 2 in
    FLUCTUATIONS IN BABCOCK-LEIGHTON DYNAMOS. II. REVISITING THE GNEVYSHEV-OHL RULE,
    https://iopscience.iop.org/article/10.1086/511177/pdf,
    Parameter choice b_1=0.6, w_1=0.2, b_2=1.0, w_2=0.8 is based on the above manuscript.'''
    f_p_n = 0.5 * (1. + math.erf((p_n - b_1) / w_1)) * (1. - math.erf((p_n - b_2) / w_2))
    return f_p_n

def babcock_leighton(p_old, alpha, epsilon):
    '''
    Amplitude p_{n+1} of the upcoming cycle.
    -----
    Function implements Eqn 3 in
    FLUCTUATIONS IN BABCOCK-LEIGHTON DYNAMOS. II. REVISITING THE GNEVYSHEV-OHL RULE,
    https://iopscience.iop.org/article/10.1086/511177/pdf'''
    if epsilon < 0.0:
        raise AssertionError('Entered epsilon: %.3f is < 0!' % (epsilon))
    elif epsilon > 1.0:
        print('Entered epsilon: %.3f is not << 1!' % (epsilon))
    p_new = alpha * babcock_leighton_fn(p_old) * p_old + epsilon
    return p_new

def sample_pn_timeseries_v2(p0, alpha_min, alpha_max, epsilon_max, prng=None, len_timeseries=200):
    '''Function samples a time series of p_n values, then returns computed p_n, sampled alphas and epsilons'''

    if prng is None:
        prng = kwargs.get('prng', np.random.RandomState(seed=kwargs.get("seed_infer")))
    pn = p0
    l_p, l_a, l_e = [], [], []
    l_f = []
    for i in range(len_timeseries):
        a = prng.uniform(low=alpha_min, high=alpha_max)
        e = prng.uniform(low=0.0, high=epsilon_max)
        f = babcock_leighton_fn(p_n=pn)
        l_p.append(pn)
        pn = babcock_leighton(p_old=pn, alpha=a, epsilon=e)
        l_a.append(a)
        l_e.append(e)
        l_f.append(f)
    p = np.reshape(l_p, (len_timeseries,))
    a = np.reshape(l_a, (len_timeseries,))
    e = np.reshape(l_e, (len_timeseries,))
    f = np.reshape(l_f, (len_timeseries,))
    d = {'p':p, 'a':a, 'e':e, 'f':f}
    return d

class DataGenerator_SolarDynamo_Simplified():

    def __init__(self, **kwargs):
        self.prng = kwargs.get('prng', np.random.RandomState(seed=1923))
        self.len_timeseries = kwargs.get('len_timeseries', int(200))
        self.p0 = kwargs.get('p0', 1.0) # coming from specialized dictionary, not hardcoded anymore anyway
        self.prior = kwargs.get('prior',None)
        assert self.prior is not None, ":: Fatal: spec prior for model."

    def __iter__(self, **kwargs): # kwargs are simply not passed during training, but they can be during inference

        while True:

            inparams = kwargs.get('inparams',None)
            if inparams is None: # training
                parameters = self.prior.draw(self.prng)
            else: # ssvarw and synthetic
                parameters = inparams

            p0 = parameters.get('p0', self.p0)
            batch = sample_pn_timeseries_v2(p0=p0, alpha_min=parameters ['alpha1'],
                                            alpha_max=parameters ['alpha1'] + parameters ['delta'],
                                            epsilon_max=parameters ['epsmax'], prng=self.prng,
                                            len_timeseries=self.len_timeseries)
            d = self.stack(batch, parameters)
            yield d


    def stack(self, batch, params):
        noise = np.stack([batch['a'], batch['e']], axis=-1)
        x = np.expand_dims(batch['p'], 1)
        params = (params ['alpha1'], params ['delta'], params ['epsmax'])
        d = (x, params, noise)
        return d

class DataGenerator_SolarDynamo_Simplified_BatchSampler(DataGenerator_SolarDynamo_Simplified):

    def __init__(self, **kwargs):

        self.batch_size = kwargs.get('batch_size')
        super().__init__(**kwargs)

    def __iter__(self, **kwargs): # kwargs are simply not passed during training, but they can be during inference

        while True:
            inparams = kwargs.get('inparams',None)
            if inparams is None: # training
                parameters = self.prior.draw(self.prng)
            else: # ssvarw and synthetic
                parameters = inparams

            l_observation = np.empty((self.batch_size, self.len_timeseries, 1)) # shape [batch_size, timeseries, 1]
            l_noise = np.empty((self.batch_size, self.len_timeseries, 2)) # [batch_size, len_timeseries, 2]
            p0 = parameters.get('p0', self.p0)
            for i in range(self.batch_size):
                sample = sample_pn_timeseries_v2(p0=p0, alpha_min=parameters ['alpha1'],
                                                 alpha_max=parameters ['alpha1'] + parameters ['delta'],
                                                 epsilon_max=parameters ['epsmax'], prng=self.prng,
                                                 len_timeseries=self.len_timeseries)
                l_observation[i,...] = np.expand_dims(sample['p'], 1) # coul use self.stack, but...
                l_noise[i,...] = np.stack([sample['a'], sample['e']], axis=-1)

            params = (parameters ['alpha1'], parameters ['delta'], parameters ['epsmax'])
            d = (l_observation, params, l_noise)

            yield d
