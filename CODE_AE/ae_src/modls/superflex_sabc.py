"""
This code wrap the SUPERFLEX modelling framework using the C dll version.
Use the compile.sh file to compile the code.

Methods:
    load_model : loads SUPERFLEX and returns useful dimensions
    run_model : runs SUPERFLEX and returns the output
"""
# flake8: noqa
import sys
import os
import ctypes
import numpy as np
import time
import cloudpickle
import re
import io
import pandas as pd

from spux.models.model import Model
from mpi4py import MPI

from scipy import stats # noqa: F401
from spux.processes.ornsteinuhlenbeck import OrnsteinUhlenbeck #, OrnsteinUhlenbeck_numba_spec

class SuperSpuxFlex(Model):

    #states = None # mfd, 22 Jul 21

    def __init__(self, dt = 1, sandboxing = 0, so_path = None, twarmup = 0, modelinfl='modelInfo.dat', case_studies=None,
                 prior=None, tOUini=np.float64(0.0), len_timeseries=1461, seed=1923, verbosity=0, tIni=1, batchsz=0, nnc=1,
                 indtfl='exps/superflex/datasets/Thur/data/waengi.asc_sflx_daily', addcp=False, whichpsto=[True,False,False],
                 **kwargs):

        #sanity check
        assert so_path is not None, ":: Fatal: please, provide a valid path to DLL (so_path). Got: {}".format(so_path)
        assert twarmup >= 0, ':: Fatal: warm-up time must be positive. Got: {}'.format(twarmup)
        assert prior is not None, ":: Fatal: prior cannot be None. You may want to look at this paper: doi.org/10.1038/s41893-020-00603-4"

        self.dt = np.float64(dt)
        self.sandboxing = sandboxing
        if self.sandboxing:
            assert False, ":: Fatal: bah, I do not think you want sandboxes. Hack me if you know better."
        self.so_path = so_path
        self.twarmup = twarmup     # has to be the index of the warm up time in the time series
        self.modelinfl = modelinfl # I need it here it seems, otherwise traverse is not happy
        self.modelinflraw = modelinfl
        self.tOUini = tOUini
        assert tOUini == 0.0, ":: Fatal: I see little reasons for not having tOUini eq. to 0. Hack me if you know more"
        self.prior = prior
        #assert len_timeseries == 1461, ":: Fatal: wrong len_timeseries {}. Hack me if you know more.".format(len_timeseries) # I know more
        self.len_timeseries = len_timeseries
        assert self.len_timeseries > self.twarmup, ":: Fatal: warm-up leaves no time series available"
        self.rng = kwargs.get('rng',np.random.RandomState(seed=seed))
        self.indtfl = indtfl # used just as a safety check in self.find_case_studies()
        if case_studies is None:
            self.find_case_studies()
        else:
            assert False, ":: Fatal: I think passing case_studies should not be accepted. Hack me if you know."
        self.find_param_limits()
        self.verbosity = verbosity
        self.tIni = tIni
        assert int(self.tOUini) == (self.tIni-1), ":: Fatal: mismatch on tIni and tOUini. Hack me if you know."
        self.numfails = 1
        self.numinits = 0
        self.batchsz = batchsz # should be 0 for ENCA and >= 1 for INCA (really > 1)
        self.nnc = nnc
        self.addcp = addcp

        self.KFsto = whichpsto[0]
        self.KSsto = whichpsto[1]
        self.Dsto = whichpsto[2]

    def find_case_studies(self):
        """read it from superflex input file, do not pass it anymore"""
        with open(self.modelinfl, 'r') as readit:
            for n, line in enumerate(readit):
                if line[0:18] == '"$(CASE_STUDIES)$"':
                    assert n == 1, ":: Fatal: unrecognized layout of input file {}".format(self.modelinfl)
                    self.case_studies = re.split('\s+',line)[1]
                    if "\"" == self.case_studies[0]:
                        self.case_studies = self.case_studies[1:]
                    if "\"" == self.case_studies[-1]:
                        self.case_studies = self.case_studies[0:-1]
                    if "/" != self.case_studies[-1]:
                        self.case_studies = self.case_studies + "/"
                    break

        with open(self.modelinfl, 'r') as readit:
            for n, line in enumerate(readit):
                if line[0:35] == '"$(CASE_STUDIES)$Thur/controlFiles/':
                    assert n == 2, ":: Fatal: unrecognized layout of input file {}".format(self.modelinfl)
                    infofile = self.case_studies + str(re.split("\"",re.split('\$',line)[2])[0])

        with open(infofile, 'r') as readit:
            for n, line in enumerate(readit):
                if line[0:26] == '"$(CASE_STUDIES)$Thur/data':
                    assert n == 2, ":: Fatal: unrecognized layout of input file {}".format(infofile)
                    datafile = self.case_studies + str(re.split("\"",re.split('\$',line)[2])[0])
                    assert re.split('/',datafile)[-1] == re.split('/',self.indtfl)[-1], ":: Fatal: inconsistent user input data files {} and {}.".format(re.split('/',datafile)[-1],re.split('/',self.indtfl)[-1])
                if n == 6:
                    skip = int(line[0])
                    assert skip == 7, ":: Fatal: unrecognized skip {}.".format(skip)

        iStartWarmInfern, iStartInfern, iEndInfern = int(re.split(',',line)[0]), int(re.split(',',line)[1]), int(re.split('!',re.split(',',line)[2])[0])
        assert iStartWarmInfern == 1, ":: Fatal: iStartWarmInfern not 1."
        assert iStartInfern == 1, ":: Fatal: iStartInfern not 1."
        assert iEndInfern == self.len_timeseries, ":: Fatal: iEndInfern != len_timeseries."
        columns = ['P_Waengi,']
        self.rain = pd.read_csv (datafile, sep="\s+", index_col=None, usecols=columns, skiprows=skip-1).values.reshape(-1)

    def find_param_limits(self):
        if self.sandboxing:
            assert False, ":: Fatal: bah, I do not think you want sandboxes. Hack me if you know better."
            assert hasattr(self, 'modelinflraw'), ":: Fatal: self does not have modelinflraw attribute in set_modelinfl. This is a bug."
            path = self.sandbox()
            self.modelinfl = os.path.join(path, self.modelinflraw) #crucial to distinguish between modelinflraw and modelinfl

        counter = 0
        while not os.path.isfile(self.modelinfl) and counter < 1E07: #file system latency, can happen, especially on Daint
            counter += 1
        assert os.path.isfile(self.modelinfl), ':: Fatal: input {} file does not exist in set_modelinfl. This may be due to latency or simply a bug.'.format(self.modelinfl)

        with open(self.modelinfl, 'r') as readit:
            for line in readit:
                if 'flexConfig/modLum' in line:
                    conffl = line
                    break
        conffl = re.split(' ',conffl)[0]
        assert '.dat' in conffl, ':: Fatal: config file .dat not found.'
        conffl = conffl.replace('$(CASE_STUDIES)$',self.case_studies+'/')
        conffl = conffl.replace('"','')
        assert os.path.isfile(conffl), ":: Fatal: config file {} not found.".format(conffl)

        with open(conffl, 'r') as readit:
            for line in readit:
                if 'UNITS FILE NAME' in line:
                    conffl2 = next(readit)
                    break
        conffl2 = conffl2.replace('\n','').replace('*','').replace('"','')
        conffl2 = '/'.join(conffl.split('/')[0:-1]+[conffl2])

        assert os.path.isfile(conffl2), ":: Fatal: config file not found."

        self.support = {} # led by superflex, not by prior.py

        with io.open(conffl2, encoding='cp1252') as readit:
            for line in readit:
                for k in ['Cmlt.E', 'Smax.UR', 'BetaQq.UR', 'Dspl.SD', 'K.Qq.FR', 'K.Qq.SR', 'tRise.FL']: # support led by superflex
                    kk = k.replace('.','_')
                    kk = kk.replace('log_','')
                    if kk in line:
                        tmpstr = line.split(" ")
                        tmpstr = [i for i in tmpstr if i != '' and i != '"']
                        nbrs = []
                        for i in tmpstr:
                            try:
                                nbrs = nbrs + [float(i)]
                            except:
                                pass
                        assert len(nbrs)==7,":: Fatal: cannot be sure."
                        self.support [k] = [nbrs[1], nbrs[2]] # from superflex limits
                        break

    def check_make_params(self, k):
        """assign values to param variables according to most valuable information"""

        if 'log.' in k:
            klog = k
            k = klog[4:]
        else:
            klog = 'log.' + k

        isinprior = False

        if k in self.prior.labels:
            isinprior = True
            setattr(self, k.replace('.','_'), self.parameters[k])
            val = getattr(self, k.replace('.','_'))
            assert val == self.parameters[k], ":: Fatal: failed assigment."
        elif klog in self.prior.labels:
            isinprior = True
            setattr(self, k.replace('.','_'), np.exp(self.parameters[klog]))
            val = getattr(self, k.replace('.','_'))
            assert val == np.exp(self.parameters[klog]), ":: Fatal: failed log assigment."
        else: # not-inferred parameters, some of which still need to be passed to superflex
            if k == 'BetaQq.UR':
                setattr(self, k.replace('.','_'), 5.0)
                assert k not in self.parameters.keys()
                self.parameters [k] = 5.0
            elif k == 'Cmlt.E':
                setattr(self, k.replace('.','_'), 1.0)
                assert k not in self.parameters.keys()
                self.parameters [k] = 1.0
            elif k == 'Smax.UR':
                setattr(self, k.replace('.','_'), 200)
                assert k not in self.parameters.keys()
                self.parameters [k] = 200
            elif k == 'tRise.FL':
                setattr(self, k.replace('.','_'), 1.0)
                assert k not in self.parameters.keys()
                self.parameters [k] = 1.0
            elif k == 'Dspl.SD':
                setattr(self, k.replace('.','_'), 0.5)
                assert k not in self.parameters.keys()
                self.parameters [k] = 0.5
            elif k == 'K.Qq.SR':
                setattr(self, k.replace('.','_'), 0.01)
                assert k not in self.parameters.keys()
                self.parameters [k] = 0.01
            elif k == 'K.Qq.FR':
                setattr(self, k.replace('.','_'), 5.0)
                assert k not in self.parameters.keys()
                self.parameters [k] = 5.0
            elif k == 'tauOUK.Qq.FR' and self.KFsto:
                setattr(self, k.replace('.','_'), 6.0)
                assert k not in self.parameters.keys()
                self.parameters [k] = 6.0
                self.colpreds = self.colpreds + ['K.Qq.FR']
            elif k == 'tauOUK.Qq.SR' and self.KSsto:
                setattr(self, k.replace('.','_'), 28.0)
                assert k not in self.parameters.keys()
                self.parameters [k] = 28.0
                self.colpreds = self.colpreds + ['K.Qq.SR']
            elif k == 'tauOUDspl.SD' and self.Dsto:
                setattr(self, k.replace('.','_'), 2.0)
                assert k not in self.parameters.keys()
                self.parameters [k] = 2.0
                self.colpreds = self.colpreds + ['Dspl.SD']
            elif k == 'sigmaOUK.Qq.FR' and self.KFsto:
                setattr(self, k.replace('.','_'), 0.7)
                assert k not in self.parameters.keys()
                self.parameters [k] = 0.7
            elif k == 'sigmaOUK.Qq.SR' and self.KSsto:
                setattr(self, k.replace('.','_'), 0.7)
                assert k not in self.parameters.keys()
                self.parameters [k] = 0.7
            elif k == 'sigmaOUDspl.SD' and self.Dsto:
                setattr(self, k.replace('.','_'), 1.5)
                assert k not in self.parameters.keys()
                self.parameters [k] = 1.5
            #elif k == '$\\sigma$':
            #    self.sigmaBC = 0.3
            #    assert k not in self.parameters.keys()
            #    self.parameters [k] = 0.3
            elif k == 'm':
                self.m = 0.01
                assert k not in self.parameters.keys()
                self.parameters [k] = 0.01
            elif k == "chi0K.Qq.FR" and self.KFsto:
                setattr(self, k.replace('.','_'), 0.0)
                assert k not in self.parameters.keys()
                self.parameters [k] = 0.0
            elif k == "chi0K.Qq.SR" and self.KSsto:
                setattr(self, k.replace('.','_'), 0.0)
                assert k not in self.parameters.keys()
                self.parameters [k] = 0.0
            elif k == "chi0Dspl.SD" and self.Dsto:
                setattr(self, k.replace('.','_'), 0.0)
                assert k not in self.parameters.keys()
                self.parameters [k] = 0.0

        if k in self.support:
            val = getattr(self, k.replace('.','_'))
            if val < self.support [k][0] or val > self.support [k][1]:
                self.numfails += 1
                self.failed = True

    def init(self, inputset=None, parameters=None):
        """parameters should come from prior"""

        self.failed = False
        self.parameters = parameters
        self.colpreds = ['Q','Su','Sf','Ss']
        self.numinits += 1
        #print(self.parameters.keys()) # it does contain added constant parameters if I set acp to 1

        # all of the possible parameters
        pnms = ['log.Cmlt.E', 'log.Smax.UR', 'Dspl.SD', 'log.K.Qq.SR', 'log.K.Qq.FR', 'sigmaOUK.Qq.FR', 'tauOUK.Qq.FR',
                'chi0K.Qq.FR', 'sigmaOUK.Qq.SR', 'tauOUK.Qq.SR', 'chi0K.Qq.SR', 'sigmaOUDspl.SD', 'tauOUDspl.SD',
                'chi0Dspl.SD', 'BetaQq.UR', 'm'] #r'$\sigma$',
        for k in pnms:
            self.check_make_params(k)
        #print(self.Cmlt_E, self.Smax_UR, self.Dspl_SD, self.K_Qq_FR, self.K_Qq_SR)

        if self.numfails > 0.05*self.numinits == 0: # % 10 == 0
            #self.numfails += 1 # so that it writes it once (I know, it is approximate)
            #print(":: Warning: Failure number {} out of {} init calls".format(self.numfails-1,self.numinits)) # likely stdp out of range
            print(":: Warning: {} failures are more than 5 percent of the init calls. Resetting".format(self.numfails-1,self.numinits))
            self.numfails = 0

        Model.init(self, inputset, parameters)

        # set self.modelinfl and edit that file properly
        if self.sandboxing:
            assert False, ":: Fatal: bah, I do not think you want sandboxes. Hack me if you know better."
            self.set_modelinfl(first=True)

        self.dimensions = self.get_mdl_dims()
        assert self.dimensions[0] == self.len_timeseries

        # initial values
        self.states = np.zeros(self.dimensions[2])
        if 'Q0' in self.parameters:
            self.states[0] = self.parameters['Q0']
            self.states[1] = self.parameters['Q0']
        else:
            self.states[0] = 0.0
            self.states[1] = 0.0

        if 'Su0' in self.parameters:
            self.states[4] = self.parameters['Su0']
        else:
            self.states[4] = 0.0

        if 'Sf0' in self.parameters:
            self.states[5] = self.parameters['Sf0']
        else:
            self.states[5] = 0.0

        if 'Ss0' in self.parameters:
            self.states[6] = self.parameters['Ss0']
        else:
            self.states[6] = 0.0
        #if inputset is not None:
        #    self.states = np.zeros(self.dimensions[2])
        #    self.states[0] = inputset['Q']  # see st.out in ~/sfw/spux/examples/superflex_thur
        #    self.states[1] = inputset['Q']  # see st.out in ~/sfw/spux/examples/superflex_thur
        #    self.states[4] = inputset['Su'] # see st.out in ~/sfw/spux/examples/superflex_thur
        #    self.states[5] = inputset['Sf'] # ditto
        #    self.states[6] = inputset['Ss'] # ditto
        #    #print(self.states)
        #else:
        #    self.states = np.zeros(self.dimensions[2])

        # stochastic process
        if self.KFsto:
            notdone = True
            self.OUK_Qq_FR = OrnsteinUhlenbeck (self.tauOUK_Qq_FR)
            if hasattr(self,'initial_states'): #it doesn't I guess, lol, it's nowhere but here
                if 'K.Qq.FR' in self.initial_states:
                    chiOU = ( np.log(self.initial_states['K.Qq.FR'])-np.log(self.K_Qq_FR) ) / self.sigmaOUK_Qq_FR
                    self.OUK_Qq_FR.init (self.tOUini,np.float64(chiOU))
                    notdone = False
            self.OUK_Qq_FR.init (self.tOUini, np.float64(self.chi0K_Qq_FR))
        else:
            self.OUK_Qq_FR = None
        if self.KSsto:
            notdone = True
            self.OUK_Qq_SR = OrnsteinUhlenbeck (self.tauOUK_Qq_SR)
            if hasattr(self,'initial_states'):
                if 'K.Qq.SR' in self.initial_states:
                    chiOU = ( np.log(self.initial_states['K.Qq.SR'])-np.log(self.K_Qq_SR) ) / self.sigmaOUK_Qq_SR
                    self.OUK_Qq_SR.init (self.tOUini,np.float64(chiOU))
                    notdone = False
            self.OUK_Qq_SR.init (self.tOUini, np.float64(self.chi0K_Qq_SR))
        else:
            self.OUK_Qq_SR = None
        if self.Dsto:
            notdone = True
            self.OUDspl_SD = OrnsteinUhlenbeck (self.tauOUDspl_SD)
            if hasattr(self,'initial_states'):
                if 'Dspl.SD' in self.initial_states:
                    chiOU = (1 / self.sigmaOUDspl_SD) * ( np.log(
                            (self.initial_states['Dspl.SD'] - self.support ['Dspl.SD'][0]) / (self.support ['Dspl.SD'][1] - self.initial_states['Dspl.SD']) )
                            - self.Dspl_SD )
                    self.OUDspl_SD.init (self.tOUini,np.float64(chiOU))
                    notdone = False
            self.OUDspl_SD.init (self.tOUini, np.float64(self.chi0Dspl_SD))
        else:
            self.OUDspl_SD = None

        sys.stdout.flush()

    def __iter__(self):
        while True:
            counter = 0
            cntbtch = 0
            d = None
            self.failed = True
            xx = np.empty((max(1,self.batchsz), self.len_timeseries-self.twarmup, 1)) # has to be 1 at least in any case
            nn = np.empty((max(1,self.batchsz), self.len_timeseries-self.twarmup, self.nnc))
            while (self.failed == True and counter < self.batchsz + 1000) or (cntbtch < self.batchsz and counter < self.batchsz + 1000):

                params = self.prior.draw(rng=self.rng) # this will be the ones seen by the AE

                # extndparams: from superflex deprived of twarmup part and not log
                sample, extndparams, noise = self.sample_series(params=params)
                if self.addcp: # add all constant parameters that have been required
                    params = self.parameters.copy() # self.parameters worked out by init

                counter += 1
                if not self.failed:
                    x = np.array(sample['Q'])
                    #print("@@@@@@@@@@@@@@@@@@@@")
                    #print(xx[cntbtch,...].shape)
                    #print(np.expand_dims(x,-1).shape)
                    #print("@@@@@@@@@@@@@@@@@@@@")
                    if len(x.shape)==1:
                        x = np.expand_dims(x,-1)
                        xx [cntbtch,...] = x.copy() # hope so
                    else:
                        assert False, ":: Fatal: something is wrong with the dimension of x in __iter__ of superflex."
                    p = np.repeat(np.NaN,len(params))
                    cnt = 0
                    for k,v in params.iteritems(): # these are still log-valued and of length 1 per parameter
                        if k[0:3]=='log':
                            try:
                                p[cnt] = np.exp(v) # not log anymore
                            except:
                                assert False, "Should have failed at sample_series."
                        else:
                            p[cnt] = v
                        cnt += 1

                    n = np.array( list(noise.values()) ).transpose().reshape(-1) # it should work also for multidim, but dbl check
                    #if len(n.shape)==1: # not anymore due to stack of noise and input 31 Aug 21
                    #    n = np.expand_dims(n,-1)
                    nn [cntbtch,...] = np.stack([n, self.rain[self.twarmup:].copy()], axis=-1)
                    cntbtch += 1

            assert not self.failed, ":: Fatal: failed"
            assert counter < 1000, ":: Fatal: unable to find reasonable parameters values in 1000 attempts."

            # check as it is intricate for historical reasons
            for i, k in enumerate(extndparams.keys()): # these are still log-valued and of length 1 per parameter
                if i == len(p):
                    break
                if k in ['stoKF','stoKS','stoD']: # sto process not in p
                    continue
                assert p[i]==extndparams[k][0], ":: Fatal: mismatch between AE param and superflex param"

            if self.batchsz == 0: # pragmatic for the moment. Ideally, I should have xx pp nn also for batchsz 1 (ENCA archs, but...)
                n = np.stack([n, self.rain[self.twarmup:].copy()], axis=-1)
                d = (x, p, n)
            else:
                d = (xx, p, nn)

            yield d

    def sample_series(self, params):
        """Here params is a dict of raw values drawn from the prior (still log if log)"""

        self.init(parameters=params.copy()) # crucial to copy not to alter prior draw

        noise = {}
        parameters = {}
        for k,v in self.parameters.iteritems(): # self.parameters are set by init and contain all params (prior + superflex)
            if k[0:3]=='log':
                kk = k[4:(len(k))]
                try:
                    v = np.exp(v) # not log anymore
                except:
                    print("Failed exp of {}".format(v))
                    self.failed = True
                    assert False, "sample series fails (1)"
                    return None, None, None
            else:
                kk = k
            parameters [kk] = np.repeat(v,self.len_timeseries) # ok superflex: not log and long as tseries

        if self.KFsto:
            assert self.OUK_Qq_FR is not None, ":: Fatal: inconsistent state. This is a bug."
            parameters ['stoKF'], noise ['epsKF'] = self.calc_std_param(sprocess=self.OUK_Qq_FR,sparam='KF')

        if self.KSsto:
            assert self.OUK_Qq_SR is not None, ":: Fatal: inconsistent state. This is a bug."
            parameters ['stoKS'], noise ['epsKS'] = self.calc_std_param(sprocess=self.OUK_Qq_SR,sparam='KS')

        if self.Dsto:
            assert self.OUDspl_SD is not None, ":: Fatal: inconsistent state. This is a bug."
            parameters ['stoD'], noise ['epsD'] = self.calc_std_param(sprocess=self.OUDspl_SD,sparam='D')

        if self.failed:
            return None, parameters, noise

        preds = self.run_alltimes(tFin=self.len_timeseries,tIni=self.tIni,parameters=parameters)

        # do not use warmup phase for AE
        #for k in preds.keys(): # for predictions, I am doing this in run directly as I think I need it for spux
        #    preds [k] = preds [k] [self.twarmup:]
        for k in parameters.keys():
            parameters [k] = parameters [k] [self.twarmup:]
        for k in noise.keys():
            noise [k] = noise [k] [self.twarmup:]

        return preds, parameters, noise

    def calc_std_param(self, sprocess, sparam=None):
        """it returns transformed (eg, exponentiated) values of STD params and their epsilon (noise)"""

        assert sprocess is not None, ":: Fatal: got not stochastic process?"
        assert sparam is not None, ":: Fatal: stochastic parameter does not have a name"
        assert sparam in ['KF','KS','D'], ":: Fatal: unrecognized stochastic param"

        noise = self.rng.standard_normal(self.len_timeseries)
        E = np.repeat(np.nan,self.len_timeseries)
        S = np.repeat(np.nan,self.len_timeseries)

        for t in range(self.tOUini.astype(int)+1,self.len_timeseries+1):
            E[t-1], S[t-1] = sprocess.disass(t, self.rng)

        if sparam=='KF': # if we are here, self.KFsto is True
            k = 'K.Qq.FR'
            if k not in self.parameters:
                k = 'log.' + k
                assert self.K_Qq_FR == np.exp(self.parameters[k]), ":: Fatal: inconsistent K_Qq_FR information."
            else:
                assert self.K_Qq_FR == self.parameters[k], ":: Fatal: inconsistent K_Qq_FR information."
            logK_Qq_FR = np.log(self.K_Qq_FR) + E + self.sigmaOUK_Qq_FR * S * noise
            try:
                K_Qq_FR = np.exp(logK_Qq_FR)
            except (OverflowError, FloatingPointError) as e:
                #print(":: Warning: Failing because stoKF explodes.")
                self.numfails += 1
                self.failed = True
                if logK_Qq_FR > 0:
                    K_Qq_FR = np.inf
                else:
                    K_Qq_FR = np.float64(0.0)
            if any(K_Qq_FR < self.support ['K.Qq.FR'][0]) or any(K_Qq_FR > self.support ['K.Qq.FR'][1]):
                #print(":: Warning: Failing because stoKF is out of range.")
                self.numfails += 1
                self.failed = True
            return K_Qq_FR, noise

        elif sparam=='KS':
            k = 'K.Qq.SR'
            if k not in self.parameters:
                k = 'log.' + k
                assert self.K_Qq_SR == np.exp(self.parameters[k]), ":: Fatal: inconsistent K_Qq_SR information."
            else:
                assert self.K_Qq_SR == self.parameters[k], ":: Fatal: inconsistent K_Qq_SR information."
            logK_Qq_SR = np.log(self.K_Qq_SR) + E + self.sigmaOUK_Qq_SR * S * noise
            try:
                K_Qq_SR = np.exp(logK_Qq_SR)
            except (OverflowError, FloatingPointError) as e:
                #print(":: Warning: Failing because stoKS explodes.")
                self.numfails += 1
                self.failed = True
                if logK_Qq_SR > 0:
                    K_Qq_SR = np.inf
                else:
                    K_Qq_SR = np.float64(0.0)
            if any(K_Qq_SR < self.support ['log.K.Qq.SR'][0]) or any(K_Qq_SR > self.support ['log.K.Qq.SR'][1]):
                #print(":: Warning: Failing because log stoKS is out of range.")
                self.numfails += 1
                self.failed = True
            return K_Qq_SR, noise

        elif sparam=='D':
            logDspl_SD = np.log(self.Dspl_SD) + E + self.sigmaOUDspl_SD * S * noise
            try:
                Dspl_SD = (self.support ['Dspl.SD'][1] - self.support ['Dspl.SD'][0]) / ( 1 + np.exp(-Dspl_SDOU) ) + self.support ['Dspl.SD'][0] #np.exp(logDspl_SD)
            except (OverflowError, FloatingPointError) as e:
                if Dspl_SDOU > 0: #logDspl_SD > 0:
                    Dspl_SD = self.support ['Dspl.SD'][1] #np.inf
                else:
                    Dspl_SD = self.support ['Dspl.SD'][0] #np.float64(0.0)
            if any(Dspl_SD < self.support ['Dspl.SD'][0]) or Dspl_SD > any(self.support ['Dspl.SD'][1]):
                assert ":: Fatal: OU process is out of boundary and this should not happen anymore thank to logistic transform. This is a bug."
            return Dspl_SD, noise

    def run_alltimes(self, tFin=None, tIni=None, parameters=None):
        """
        This method runs the model
        """

        assert tIni is not None, ":: Fatal: tIni is None."
        assert tFin is not None, ":: Fatal: tFin is None."
        assert tFin == self.len_timeseries, ":: Fatal: tFin != self.len_timeseries. Hack me if you know."

        tIni_py = tIni - 1
        if hasattr(self,'tOUini'):
            assert tIni_py == self.tOUini, ":: Fatal: tIni_py != self.tOUini."
        tFin_py = tFin

        #print("@@@@@@@@")
        #print(parameters,type(parameters))
        #print(tIni,tFin)
        #print("@@@@@@@@")

        if self.sandboxing:
            assert False, ":: Fatal: bah, I do not think you want sandboxes. Hack me if you know better."
            self.edit_runtime(tIni,tFin)

        D = parameters['Dspl.SD']
        KFR = parameters['K.Qq.FR']
        KSR = parameters['K.Qq.SR']

        if self.Dsto:
            D = parameters ['stoD']
        if self.KFsto:
            KFR = parameters ['stoKF']
        if self.KSsto:
            KSR = parameters ['stoKS']

        parlst = [ parameters['Cmlt.E'], parameters['Smax.UR'], D, KFR, KSR ] # more available, but these reflect superflex settings
        params = np.atleast_2d( parlst ).transpose() # column i is parameter i and rows index time right now. Is this ok?

        #print("@@@@@@@@")
        #print(parlst)
        #print(tIni_py,tFin_py)
        #print("@@@@@@@@")
        #np.savetxt('parameters.txt', params, fmt='%.6e')

        # Check if the parameter input has the right dimensionality
        if self.nParTime == 1: # time constant parameters - I think this is obsolete, 22 Jul 21
            if params.shape != (self.nParTime, self.nPar):
                raise ValueError('The parameters array has the wrong dimensionality (1). {} instead of {}'.format(
                                 params.shape, (self.nParTime, self.nPar)))
        else:
            if params.shape != (tFin_py - tIni_py, self.nPar):
                raise ValueError('The parameters array has the wrong dimensionality (2). {} instead of {}'.format(
                                 params.shape, (tFin_py - tIni_py, self.nPar)))

        if self.states is None:
            raise ValueError('No states available')

        # Prepare the parameters array-  The Fortran code needs all the parameters (entire time series) and
        # they must have a valid value. So I set all at the first value of my parameter set
        if self.nParTime == 1:
            raise ValueError("Fatal. I thought we were doing stochastic modeling by now...")
            par_all = params
        else:
            par_all = np.ones((self.nParTime, self.nPar)) * params[0, :]
            par_all[tIni_py : tFin_py, :] = params

        # passing the right things to superflex beyond reasonable doubts, 13 Aug 21
        #print("@@@@@")
        #print(par_all[-1],par_all.shape)
        #print(params[-1],params.shape)
        #print(parameters ['stoKF'] [-1], parameters ['stoKF'].shape)
        #print("@@@@@")

        par_size = self.nPar * self.nParTime
        ParType = ctypes.c_double * par_size
        pars_list = par_all.reshape(par_size, order = 'F').tolist()
        pars_dum = ParType(*pars_list)       # to model run

        # Define the output array
        output_size = self.nT * self.nOut    # it's a flat array
        OutputType = ctypes.c_double * output_size
        output_dum = OutputType()            # to model run

        # Define sIni and sFin (1D arrays)
        sType = ctypes.c_double * self.nStates
        states_list = self.states.tolist()
        sIni_dum = sType(*states_list)       # to model run
        sFin_dum = sType()                   # to model run

        # Define the tIni, tFin, error
        tIni_dum = ctypes.c_int(tIni)        # to model run
        tFin_dum = ctypes.c_int(tFin)        # to model run
        err_dum = ctypes.c_int()             # to model run

        # Define the interface to the library
        self.superflex.rsfmd_pro.argtypes = [ctypes.POINTER(ctypes.c_double)] * 4 + [ctypes.POINTER(ctypes.c_int)] * 3
        self.superflex.rsfmd_pro.restype = None

        # Run the model
        self.superflex.rsfmd_pro(output_dum, pars_dum, sIni_dum, sFin_dum, tIni_dum, tFin_dum, err_dum)

        if err_dum.value != 0:
            print(par_all[tIni_py:tFin_py,:])
            sys.stdout.flush()
            raise RuntimeError('Error in rsfmd: see SuperFlexErrorMessage.txt')

        # comply with error models that require knowledge of the past
        self.last = self.states[0] #, 0 is Q of all HRUs of the model

        # Reshape the outputs
        output = np.array(output_dum[:]).reshape((self.nT, self.nOut), order = 'F')
        #np.savetxt('output.txt', output, fmt='%.6e')

        # Take care of the states
        self.states = np.array(sFin_dum[:])

        # order should not matter anymore thanks to explicit name usage in obserr.py
        # see output X, output y, output S in modelInfo (and datasets/Thur/reportFiles/out_feats.dat to understand those)
        Q  = output[tIni_py : tFin_py,3] #t his is C style, (starts from 0 the enumeration, so 3 is like 4 in blessed FTN)
        Su = output[tIni_py : tFin_py,4]
        Sf = output[tIni_py : tFin_py,5]
        Ss = output[tIni_py : tFin_py,6]

        preds = {'Q': Q, 'Su': Su, 'Sf': Sf, 'Ss': Ss} # currently STD params are stored in params in sample_series
# # # #  if you need std parameters also in predictions (e.g., for spux, although I don't think so, the below should work ok)
#        if self.OUDspl_SD is None:
#            if self.OUK_Qq_SR is None:
#                if self.OUK_Qq_FR is None:
#                    #preds = pd.DataFrame(data=np.c_[Q,Su,Sf,Ss],index=range(tIni,tFin+1),columns=self.colpreds)
#                    preds = {'Q': Q, 'Su': Su, 'Sf': Sf, 'Ss': Ss}
#                else:
#                   # preds = pd.DataFrame(data=np.c_[Q,Su,Sf,Ss,KFR],index=range(tIni,tFin+1),columns=self.colpreds)
#                    preds = {'Q': Q, 'Su': Su, 'Sf': Sf, 'Ss': Ss, 'K.Qq.FR': KFR}
#            else:
#                assert self.OUK_Qq_FR is None, ":: Fatal: wrong combo."
#                #preds = pd.DataFrame(data=np.c_[Q,Su,Sf,Ss,KSR],index=range(tIni,tFin+1),columns=self.colpreds)
#                preds = {'Q': Q, 'Su': Su, 'Sf': Sf, 'Ss': Ss, 'K.Qq.SR': KSR}
#        else:
#            if self.OUK_Qq_FR is None:
#                assert self.OUK_Qq_SR is None, ":: Fatal: wrong combo."
#                #preds = pd.DataFrame(data=np.c_[Q,Su,Sf,Ss,D],index=range(tIni,tFin+1),columns=self.colpreds)
#                preds = {'Q': Q, 'Su': Su, 'Sf': Sf, 'Ss': Ss, 'Dspl.SD': D}
#            else:
#                if self.OUK_Qq_SR is not None:
#                    #preds = pd.DataFrame(data=np.c_[Q,Su,Sf,Ss,KFR,KSR,D],index=range(tIni,tFin+1),columns=self.colpreds)
#                    preds = {'Q': Q, 'Su': Su, 'Sf': Sf, 'Ss': Ss, 'K.Qq.FR': KFR, 'K.Qq.SR': KSR, 'Dspl.SD': D}
#                else:
#                    #preds = pd.DataFrame(data=np.c_[Q,Su,Sf,Ss,KFR,D],index=range(tIni,tFin+1),columns=self.colpreds)
#                    preds = {'Q': Q, 'Su': Su, 'Sf': Sf, 'Ss': Ss, 'K.Qq.FR': KFR, 'Dspl.SD': D}
#
#        #pd.DataFrame.from_dict(preds).to_csv('preds.txt')
###########################################################
        for k in preds.keys(): # for predictions, I am doing this in run directly as I think I need it for spux
            preds [k] = preds [k] [self.twarmup:]

        return preds

    def get_mdl_dims(self):
        """
        This method initializes the model
        """

        # Load the dll
        if 1:
            self.superflex = ctypes.cdll.LoadLibrary(self.so_path)

        assert hasattr(self, 'modelinfl'), ":: Fatal: self does not have modelinfl attribute in get_mdl_dims. This is a bug."

        flnmlen_dum = ctypes.c_int(len(self.modelinfl))
        assert flnmlen_dum.value < 255, ":: Fatal: Just too long!"
        flnm_dum = self.modelinfl.encode('utf-8')
        self.superflex.getinflnm.argtypes = [ctypes.POINTER(ctypes.c_int),ctypes.c_char_p]
        self.superflex.getinflnm(flnmlen_dum,flnm_dum)

        # Define the outputs of the load library
        nT_dum = ctypes.c_int()
        nPar_dum = ctypes.c_int()
        nParTime_dum = ctypes.c_int()
        nStates_dum = ctypes.c_int()
        nOut_dum = ctypes.c_int()
        err_dum = ctypes.c_int()

        # Define the interface to the library
        #if not hasattr(self.superflex, 'lsfmd'): #segfault if this switch is on in place of if 1:
        if 1:
            self.superflex.lsfmd.argtypes = [ctypes.POINTER(ctypes.c_int)]*6
            self.superflex.lsfmd.restype = None

        # Load the model
        self.superflex.lsfmd(nT_dum, nPar_dum, nParTime_dum, nStates_dum, nOut_dum, err_dum)
        counter = 0
        if err_dum.value != 0:
            print(":: WARNING: something wrong with superflex fortran. Let's try again for a few times.")
            sys.stdout.flush()
            while err_dum.value != 0 and counter < 1E03:
                counter += 1
                nT_dum = ctypes.c_int()
                nPar_dum = ctypes.c_int()
                nParTime_dum = ctypes.c_int()
                nStates_dum = ctypes.c_int()
                nOut_dum = ctypes.c_int()
                err_dum = ctypes.c_int()
                self.superflex.lsfmd(nT_dum, nPar_dum, nParTime_dum, nStates_dum, nOut_dum, err_dum)

        #sys.stdout.flush()
        if counter > 1E07:
            assert 0, 'Died due to counter with this model input file: {}'.format(self.modelinfl)
        if err_dum.value != 0:
            assert 0, 'Error in lsfmd: see SuperFlexErrorMessage.txt'

        # Extract the numeric value
        self.nT = nT_dum.value
        self.nPar = nPar_dum.value
        self.nParTime = nParTime_dum.value
        self.nStates = nStates_dum.value
        self.nOut = nOut_dum.value

        # Return the dimensionality of the parameters
        return (self.nParTime, self.nPar, self.nStates)

    def free_mem(self):

        assert hasattr(self,'superflex'), "Fatal. In free_mem of superflex, got no superflex."

        self.superflex.clnsfmd.argtypes = None
        self.superflex.clnsfmd.restype = None
        self.superflex.clnsfmd()

    def set_modelinfl(self,first=True):

        assert hasattr(self, 'modelinflraw'), ":: Fatal: self does not have modelinflraw attribute in set_modelinfl. This is a bug."
        path = self.sandbox()
        self.modelinfl = os.path.join(path, self.modelinflraw) #crucial to distinguish between modelinflraw and modelinfl
        counter = 0
        while not os.path.isfile(self.modelinfl) and counter < 1E07: #file system latency, can happen, especially on Daint
            counter += 1
        assert os.path.isfile(self.modelinfl), ':: Fatal: sandboxed input {} file does not exist in set_modelinfl. This may be due to latency or simply a bug.'.format(self.modelinfl)

        path2cs = "/".join(self.modelinfl.split("/")[:-1]) #absolute path to case_studies (i.e. datasets folder)
        if path2cs[-1] != '/':
            path2cs = path2cs + '/'

        fldata = []
        counter = 0
        while len(fldata) < 14 and counter < 1E07: #14 is the number of lines of the file in question, and while loop is still cause I experience issues with file sys latency
            counter += 1
            with open(self.modelinfl, 'r') as fl:
                fldata = fl.readlines()

        if counter >= 1E07 or len(fldata) < 14:
            raise ValueError("Fatal: something is fundamentally wrong - set_modelinfl.",fldata,len(fldata),counter)

        l = fldata[2] #find this line (controlFiles specification) all the time, need to be before ''.join below
        lc = l.split("\"")[1]
        assert 'controlFiles' in lc, ":: Fatal: bad line guess or wrong input file."
        #
        if first: #due to internal functioning, I am bound to always be first = True, otherwise it crashes for a reason or another (days if not weeks of tests)
            l = fldata[1]
            subme = re.sub("\"","",l.split()[1]) #this only at first or after load
            fldata = ''.join(fldata)
            fldata = fldata.replace(subme, path2cs) #change this line only after init or after load - put the sandbox path into the "$(CASE_STUDIES)$" superflex path
            with open(self.modelinfl, 'w') as fl:
                fl.write(fldata)
        else:
            if path not in fldata[1] or path2cs not in fldata[1]: #I found this to fail, so first = True at all calls of this method
                raise ValueError("Fatal: path {} and/or path2cs {} is/are not in fldata[1] {} from modelinfl file {}".format(path,path2cs,fldata[1],self.modelinfl))
        #
        self.inffl = re.sub("\$\(CASE_STUDIES\)\$",path2cs,lc)
        counter = 0
        while not os.path.isfile(self.inffl) and counter < 1E07: #file system latency
            counter += 1
        assert os.path.isfile(self.inffl), ':: Fatal: info file {} does not exist'.format(self.inffl)

    def edit_runtime(self,tIni,tFin): #edit superflex runtime in input file

        assert hasattr(self,"sandboxing"), "Fatal, In edit_runtime of superflex, got superflex w/o sandboxing"
        if not self.sandboxing: #double layer secuCmlt_Erity
            return

        assert hasattr(self,"sandbox"), "Fatal, In edit_runtime of superflex, got superflex w/o sandbox"
        assert hasattr(self.sandbox,"copyin"), "Fatal, In edit_runtime of superflex, got self.sandbox w/o copyin"
        #
        self.set_modelinfl(first=True) #after weeks of struggling, seems this is crucial here with first=True (I think cause of mixing of so_libs and particles, which is inevitable currently)
        #
        #fldata = os.popen('cat ' + self.inffl + ' 2> /dev/null ').readlines() #works
        fldata = []
        with open(self.inffl, 'r') as fl:
            fldata = fl.readlines()
            assert len(fldata) == 23,":: Fatal: self.inffl {} has wrong length {} rather than 23.".format(self.inffl,len(fldata))
        #
        try:
            ll = fldata[-1].split("!")
        except:
            print('dying len(fldata): ',len(fldata))
            print('dying fldata: ',fldata)
            print('dying self.inffl',self.inffl)
            sys.stdout.flush()
            assert os.path.isfile(self.inffl), ':: Fatal: info file {} does not exist - edit runtime (2)'.format(self.inffl)
            raise ValueError("ciao")

        ll[0] = str(tIni)+', '+str(tIni)+', '+str(tFin)
        fldata[-1] = " !".join(ll)
        fldata = "".join(fldata)
        try:
            with open(self.inffl, 'w') as fl:
                fl.write(fldata)
        except:
            raise ValueError('Fatal. Obviously now I am dying at writing the list into the file. How to use os command here???')

    def save(self):
        """
        This method returns the states of the model at the last timestep run
        """

        if self.states is None:
            raise ValueError('No states available')

        #superflex is a dll, cannot be serialized by pickle: cherry-pick stuff
        stmp = { "states": self.states, "dimensions": self.dimensions, "nStates": self.nStates, "so_path": self.so_path,
                 "OUK_Qq_FR": self.OUK_Qq_FR, "OUK_Qq_SR": self.OUK_Qq_SR, "OUDspl_SD": self.OUDspl_SD, "parameters": self.parameters,
                 "failed": self.failed, "Cmlt.E": self.Cmlt_E, "Smax.UR": self.Smax_UR, "BetaQq.UR": self.BetaQq_UR, "tRise.FL": self.tRise_FL,
                 "colpreds": self.colpreds}

        # all the rest useless as we re-init. dimensions and nStates only for check.
        #,"nT": self.nT,"nPar": self.nPar,"nParTime": self.nParTime, "nOut": self.nOut}

        sout = cloudpickle.dumps(stmp)

        return sout

    def load(self, instates):
        """
        This method loads the states
        """

        states = cloudpickle.loads (instates)

        self.nStates = states["nStates"]
        self.states = states["states"]

        if len(self.states) != self.nStates:
            raise ValueError('The states array has the wrong dimensionality. {} instead of {}'.format(
                             self.states.shape, self.nStates))

        self.parameters = states["parameters"]
        so_path = states["so_path"]
        self.dimensions = states["dimensions"]
        #self.nT = states["nT"]
        #self.nPar = states["nPar"]
        #self.nParTime = states["nParTime"]
        #self.nOut = states["nOut"]
        self.OUK_Qq_FR = states["OUK_Qq_FR"]
        self.OUK_Qq_SR = states["OUK_Qq_SR"]
        self.OUDspl_SD = states["OUDspl_SD"]
        self.failed = states["failed"]
        self.Cmlt_E = states["Cmlt.E"]
        self.Smax_UR = states["Smax.UR"]
        self.BetaQq_UR = states["BetaQq.UR"]
        self.tRise_FL = states["tRise.FL"]
        self.colpreds = states["colpreds"]

        if not hasattr(self,'superflex'): #this is crucial if all particles are received
            #set first to True here too as I suspect that sandboxes of killed particles are killed too
            if self.sandboxing:
                assert False, ":: Fatal: bah, I do not think you want sandboxes. Hack me if you know better."
                self.set_modelinfl(first=True) #set self.modelinfl and edit that file properly
            tmpdim = self.dimensions
            self.dimensions = self.get_mdl_dims() #init library
            assert tmpdim == self.dimensions, ": Fatal :: got inconsistent self.dimensions in setup of superflex. This is a bug"

        return self

    def exit(self):

        Model.exit(self)

        assert hasattr(self,'superflex'), "Fatal. In exit of superflex, got no superflex."
        # Define the interface to the library
        #if hasattr(self,'superflex'):
        self.superflex.clnsfmd.argtypes = None
        self.superflex.clnsfmd.restype = None

        # Just, implode
        self.superflex.clnsfmd()

        return None

    def __call__(self, parameters, times):

        assert ( times[-1] % self.dt == 0.0 ) # may not be strictly necessary, but then approx times open up...
        profile = None
        info = {'successful' : True}
        info ['warmup_time'] = self.twarmup
        sample, extndparams, noise = self.sample_series(params=parameters)

        if self.failed:
            info ['successful'] = False
            return info, profile

        predictions = {}
        for k, v in sample.items():
            predictions [k] = v
        for k, v in noise.items():
            predictions [k] = v
        if self.Dsto:
            predictions ['stoD'] = extndparams ['stoD']
        if self.KFsto:
            predictions ['stoKF'] = extndparams ['stoKF']
        if self.KSsto:
            predictions ['stoKS'] = extndparams ['stoKS']

        predictions = pd.DataFrame(predictions.values(),columns=times,index=predictions.keys()).transpose()
        info ['predictions'] = predictions
        self.time = times[-1]  # without this, it might nevet stop. Just do it.

        return info, profile
