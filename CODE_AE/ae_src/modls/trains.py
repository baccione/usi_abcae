import time, sys, math
import tensorflow as tf
from ae_src.tools.utils import export_summary_scalars
import importlib
from ae_src.tools.wraps import wrap_getattr
#
##################################################################################################
@tf.function
def train_step_ENCA_BiLSTM(model, x, params, noise, optimizer, args=None, summary_writer=None,
               loss_reconstruction_fn=None, loss_regress_params_fn=None, loss=None):
    '''Used in: train_AE_tfv2_NLAR1_BiLSTM, train_AE_tfv2_solarDynamo_BiLSTM, train_AE_tfv2_NLAR1_BS_BiLSTM,
    Single training step. Likely critical for performance. So not sanity checks.'''

    with tf.GradientTape(persistent=False) as tape: # persistent=True if .gradient() is called multiple times (e.g., multiple losses)
        z_latent = model.encoder(x, training=True)  # points to return tf.keras.Model(bla)
        x_reconst = model.decoder((z_latent, noise), training=True)
        dict_reconstruction_mse = loss_reconstruction_fn(x, x_reconst, return_each_dim=True, loss_fn=loss,
                                                         args=args)
        loss_reconstruction = tf.math.reduce_sum(list(dict_reconstruction_mse.values()))
        #tf.print("#################")
        #tf.print(loss_reconstruction)
        # if (loss_reconstruction > 10**15): # due to blowfly
        #     loss_reconstruction = loss_reconstruction / 1000
        # elif (loss_reconstruction > 10**14):
        #     loss_reconstruction = loss_reconstruction / 100
        # elif (loss_reconstruction > 10**13):
        #     loss_reconstruction = loss_reconstruction / 10
        dict_regress_params_mse = loss_regress_params_fn(params, z_latent, return_each_dim=True, loss_fn=loss,
                                                         args=args)
        loss_regress_params = tf.math.reduce_sum(list(dict_regress_params_mse.values()))
        #tf.print("#################")
        #tf.print(loss_regress_params)
        # if (loss_regress_params > 10**15): # due to blofly
        #     loss_regress_params = loss_regress_params / 1000
        # elif (loss_regress_params > 10**14):
        #     loss_regress_params = loss_regress_params / 100
        # elif (loss_regress_params > 10**13):
        #     loss_regress_params = loss_regress_params / 10
        loss = loss_reconstruction + loss_regress_params
        trainable_variables = model.encoder.trainable_variables + \
                              model.decoder.trainable_variables # likely interesting for monitoring the learning process
        gradients = tape.gradient(loss, trainable_variables) # likely interesting for monitoring the learning process
        ggcn = wrap_getattr(args,'global_gradient_clipnorm')
        freq_log = wrap_getattr(args,'freq_log')
        if ggcn is not None:
            gradients, global_norm = tf.clip_by_global_norm(gradients, clip_norm=ggcn, name='clip_gradients_by_global_norm')
        if tf.equal(optimizer.iterations % freq_log, 0):
            l_avg_grads= [tf.math.reduce_mean(g) for g in gradients]
            l_absmax_grads= [tf.math.reduce_max(tf.math.abs(g)) for g in gradients]
            l_gradmean_names = ['gradient_mean_'+v.name for v in trainable_variables]
            l_gradabsmax_names = ['gradient_absmax_'+v.name for v in trainable_variables]
            # export avg and absolute max gradients of variables to tensorboard
            d_grads = {**dict(zip(l_gradmean_names, l_avg_grads)), **dict(zip(l_gradabsmax_names, l_absmax_grads))}
            export_summary_scalars(dict_name_and_val=d_grads, step=optimizer.iterations, writer=summary_writer)
        optimizer.apply_gradients(zip(gradients, trainable_variables))
    return (loss_reconstruction, loss_regress_params), (z_latent, x_reconst), (dict_reconstruction_mse, dict_regress_params_mse)
##################################################################################################

##################################################################################################
@tf.function
def train_step_INCA_FCmulB(model, x, params, noise, optimizer, args=None, summary_writer=None,
               loss_aggregator_fn=None, loss_regress_params_fn=None, loss=None):
    '''Single training step.'''

    with tf.GradientTape(persistent=False) as tape: #persistent=True if .gradient() will be called multiple times (e.g., multiple losses)
        z_latent = model.encoder(x, training=True) # shape: [num_different_mb, batch_size, num_latent_dims]
        theta_reconst = model.aggregator(z_latent, training=True) # shape: [num_different_mb, num_model_params]
        lambda_theta = wrap_getattr(args,'batch_size')
        lambda_z = 1.
        dict_regress_theta_mse = loss_aggregator_fn(params, theta_reconst, return_each_dim=True, loss_fn=loss,
                                                    args=args)
        loss_regress_theta = tf.math.reduce_sum(list(dict_regress_theta_mse.values()))
        #tf.print("#################")
        #tf.print(loss_regress_theta)
        # if (loss_regress_theta > 10**15): # due to blowfly
        #     loss_regress_theta = loss_regress_theta / 1000
        # elif (loss_regress_theta > 10**14):
        #     loss_regress_theta = loss_regress_theta / 100
        # elif (loss_regress_theta > 10**13):
        #     loss_regress_theta = loss_regress_theta / 10
        dict_regress_z_mse = loss_regress_params_fn(params, z_latent, return_each_dim=True, loss_fn=loss,
                                                    args=args)
        loss_regress_z = tf.math.reduce_sum(list(dict_regress_z_mse.values()))
        #tf.print("#################")
        #tf.print(loss_regress_z)
        # if (loss_regress_z > 10**15): # due to blowfly
        #     loss_regress_z = loss_regress_z / 1000
        # elif (loss_regress_z > 10**14):
        #     loss_regress_z = loss_regress_z / 100
        # elif (loss_regress_z > 10**13):
        #     loss_regress_z = loss_regress_z / 10
        loss = lambda_theta*loss_regress_theta + lambda_z*loss_regress_z
        trainable_variables = model.encoder.trainable_variables + model.aggregator.trainable_variables
        gradients = tape.gradient(loss, trainable_variables)
        ggcn = wrap_getattr(args,'global_gradient_clipnorm')
        ggcv = wrap_getattr(args,'gradient_clip_value')
        freq_log = wrap_getattr(args,'freq_log')
        if ggcn is not None:
            gradients, global_norm = tf.clip_by_global_norm(gradients, clip_norm=ggcn, name='clip_gradients_by_global_norm')
        if ggcv is not None:
            gradients = [tf.clip_by_value(grad, clip_value_min=-gradient_clip_value, clip_value_max=gradient_clip_value, name='clip_gradients_by_value') for grad in gradients]
        if tf.equal(optimizer.iterations % freq_log, 0):
            l_avg_grads= [tf.math.reduce_mean(g) for g in gradients]
            l_absmax_grads= [tf.math.reduce_max(tf.math.abs(g)) for g in gradients]
            l_gradmean_names = ['gradient_mean_'+v.name for v in trainable_variables]
            l_gradabsmax_names = ['gradient_absmax_'+v.name for v in trainable_variables]
            # export avg and absolute max gradients of variables to tensorboard
            d_grads = {**dict(zip(l_gradmean_names, l_avg_grads)), **dict(zip(l_gradabsmax_names, l_absmax_grads))}
            export_summary_scalars(dict_name_and_val=d_grads, step=optimizer.iterations, writer=summary_writer)
        optimizer.apply_gradients(zip(gradients, trainable_variables))
    return (loss_regress_theta, loss_regress_z), (theta_reconst, z_latent), (dict_regress_theta_mse, dict_regress_z_mse)
##################################################################################################
