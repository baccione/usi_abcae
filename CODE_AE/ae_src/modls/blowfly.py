#
import numpy as np
import pandas
import sys
#
def blowfly_eqn(P, n_lagged, n_0, e, n_old, delta, eps):
    ## Computes N_{t+1} for blowfly population discretised differential equation. http://proceedings.mlr.press/v51/park16.pdf , page 402. ##
    return P * n_lagged * np.math.exp(-(n_lagged)/n_0) * e + n_old * np.math.exp(-delta * eps)

def sample_blowfly_timeseries(P, n_0, sigma_d, sigma_p, tau, delta, prng=None, len_timeseries=200, **kwargs):

    if prng is None:
        prng = kwargs.get('prng', np.random.RandomState(seed=kwargs.get("seed_infer")))
    tau = int(np.round(tau))
    l_e, l_eps = [], [] #noise timeseries
    l_N = [] # blowfly timeseries
    for i in range(len_timeseries):
        e = prng.gamma(shape=1/(sigma_p**2), scale=sigma_p**2)
        eps = prng.gamma(shape=1/(sigma_d**2), scale=sigma_d**2)
        l_e.append(e)
        l_eps.append(eps)
        if len(l_N) >= tau:
            n_lagged = l_N[-tau]
        else:
            n_lagged = n_0
        if i == 0:
            n_old = n_0
        else:
            n_old = n_new
        n_new = blowfly_eqn(P, n_lagged, n_0, e, n_old, delta, eps)
        l_N.append(n_new)
    n = np.reshape(l_N, (len_timeseries,))
    e = np.reshape(l_e, (len_timeseries,))
    eps = np.reshape(l_eps, (len_timeseries,))
    d = {'N':n, 'e':e, 'eps':eps }
    return d

class DataGenerator_Blowfly_Simplified():

    def __init__(self, **kwargs):
        self.prng = kwargs.get('prng', np.random.RandomState(seed=1923))
        self.len_timeseries = kwargs.get('len_timeseries', int(200))
        self.synth = bool(kwargs.get('synthesize', 0))

        # could be assigned with USER_MODEL_INARGS_DIC but it is much easier to use a unif distr with width 0
        #self.P_fixed = kwargs.get('P_fixed', None) # these can be assigned with the inputdic (see keyword USER_MODEL_INARGS_DIC)
        #self.n_0_fixed = kwargs.get('n_0_fixed', None)
        #self.sigma_d_fixed = kwargs.get('sigma_d_fixed', None)
        #self.sigma_p_fixed = kwargs.get('sigma_p_fixed', None)
        #self.tau_fixed = kwargs.get('tau_fixed', None)
        #self.delta_fixed = kwargs.get('delta_fixed', None)

        self.prior = kwargs.get('prior',None)
        assert self.prior is not None, ":: Fatal: spec prior for model."

        #self.log_P_args = kwargs.get('log_P_args', [2, 2**2]) # why 2**2 as standard dev? Looks more like a variance this way. Error?
        #self.log_n_0_args = kwargs.get('log_n_0_args', [6, 0.5**2])
        #self.log_sigma_d_args = kwargs.get('log_sigma_d_args', [-0.75, 1**2])
        #self.log_sigma_p_args = kwargs.get('log_sigma_p_args', [-0.5, 1**2])
        #self.log_tau_args = kwargs.get('log_tau_args', [2.7, 0.1**2])
        #self.log_delta_args = kwargs.get('log_delta_args', [-1.8, 0.4**2])

    def __iter__(self, **kwargs):

        while True:

            inparams = kwargs.get('inparams',None)
            if inparams is None: # training
                parameters = self.prior.draw(self.prng).to_dict()
            else: # ssvarw and synthetic
                parameters = inparams

            if 'log.P' in parameters:
                P = np.exp(parameters['log.P'])
            else:
                P = parameters['P']
            if 'log.n0' in parameters:
                n_0 = np.exp(parameters['log.n0'])
            else:
                n_0 = parameters['n0']
            if 'log.sigmad' in parameters:
                sigma_d = np.exp(parameters['log.sigmad'])
            else:
                sigma_d = parameters['sigmad']
            if 'log.sigmap' in parameters:
                sigma_p = np.exp(parameters['log.sigmap'])
            else:
                sigma_d = parameters['sigmap']
            if 'log.tau' in parameters:
                tau = np.exp(parameters['log.tau'])
            else:
                tau = parameters['tau']
            if 'log.delta' in parameters:
                delta = np.exp(parameters['log.delta'])
            else:
                delta = parameters['delta']

            batch = sample_blowfly_timeseries(P=P, n_0=n_0, sigma_d=sigma_d, sigma_p=sigma_p, tau=tau, delta=delta,
                                              prng=self.prng, len_timeseries=self.len_timeseries)

            noise = np.stack([batch['e'], batch['eps']], axis=-1)
            x = np.expand_dims(batch['N'], 1)
            params = (P, n_0, sigma_d, sigma_p, tau, delta)
            d = (x, params, noise)

            yield d

class DataGenerator_Blowfly_Simplified_BatchSampler(DataGenerator_Blowfly_Simplified):

    def __init__(self, **kwargs):

        self.batch_size = kwargs.get('batch_size')
        super().__init__(**kwargs)

    def __iter__(self, **kwargs):

        while True:
            inparams = kwargs.get('inparams',None)
            if inparams is None: # training
                parameters = self.prior.draw(self.prng).to_dict()
            else: # ssvarw and synthetic
                parameters = inparams

            if 'log.P' in parameters:
                P = np.exp(parameters['log.P'])
            else:
                P = parameters['P']
            if 'log.n0' in parameters:
                n_0 = np.exp(parameters['log.n0'])
            else:
                n_0 = parameters['n0']
            if 'log.sigmad' in parameters:
                sigma_d = np.exp(parameters['log.sigmad'])
            else:
                sigma_d = parameters['sigmad']
            if 'log.sigmap' in parameters:
                sigma_p = np.exp(parameters['log.sigmap'])
            else:
                sigma_d = parameters['sigmap']
            if 'log.tau' in parameters:
                tau = np.exp(parameters['log.tau'])
            else:
                tau = parameters['tau']
            if 'log.delta' in parameters:
                delta = np.exp(parameters['log.delta'])
            else:
                delta = parameters['delta']

            l_observation = np.empty((self.batch_size, self.len_timeseries, 1)) # shape [batch_size, timeseries, 1]
            l_noise = np.empty((self.batch_size, self.len_timeseries, 2)) # [batch_size, len_timeseries, 2]
            for i in range(self.batch_size):
                sample = sample_blowfly_timeseries(P=P, n_0=n_0, sigma_d=sigma_d, sigma_p=sigma_p, tau=tau,
                                                   delta=delta, prng=self.prng, len_timeseries=self.len_timeseries)
                l_observation[i,...] = np.expand_dims(sample['N'], 1)
                l_noise[i,...] = np.stack([sample['e'], sample['eps']], axis=-1)

            params = (P, n_0, sigma_d, sigma_p, tau, delta) # tuple of scalars
            d = (l_observation, params, l_noise)

            yield d
