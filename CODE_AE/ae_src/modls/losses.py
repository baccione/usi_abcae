#
import tensorflow as tf
import tensorflow_probability as tfp
import importlib
from ae_src.tools.wraps import wrap_getattr

class ChiSquareStatistic_a(tf.keras.losses.Loss):
    @tf.function
    def call(self, y_true, y_pred):
        if tf.not_equal(tf.rank(y_true), tf.rank(y_pred)):
            y_true = tf.expand_dims(y_true, axis=1)
        sd = tf.math.squared_difference(y_true, y_pred)
        if tf.rank(sd) > 1:
            return tf.math.reduce_sum(sd / tf.math.maximum(tf.math.pow(y_true, 2), 1e-6), axis= -1)
        else:
            return sd / tf.math.maximum(tf.math.pow(y_true, 2), 1e-6)

class ChiSquareStatistic_a_renorm_lgstc(tf.keras.losses.Loss):
    """normalization term is logistic function of ae prediction"""
    @tf.function
    def call(self, y_true, y_pred):
        if tf.not_equal(tf.rank(y_true), tf.rank(y_pred)):
            y_true = tf.expand_dims(y_true, axis=1)
        sd = tf.math.squared_difference(y_true, y_pred)
        dn = tf.math.maximum(tf.math.pow(y_true, 2) / (1 + tf.math.exp( -y_pred / tf.math.maximum(y_pred,1.0) )), 1e-06)
        if tf.rank(sd) > 1:
            return tf.math.reduce_sum(sd / dn, axis= -1)
        else:
            return sd / dn

class ChiSquareStatistic_a_nonorm(tf.keras.losses.Loss):
    """no normalization term"""
    @tf.function
    def call(self, y_true, y_pred):
        if tf.not_equal(tf.rank(y_true), tf.rank(y_pred)):
            y_true = tf.expand_dims(y_true, axis=1)
        sd = tf.math.squared_difference(y_true, y_pred)
        if tf.rank(sd) > 1:
            return tf.math.reduce_sum(sd, axis= -1)
        else:
            return sd

@tf.function
def loss_reconstruction_fn_a(x, x_pred, return_each_dim=False, loss_fn=None, args=None): # basically, BiLSTM
    """Used with: train_AE_tfv2_NLAR1_BiLSTM, train_AE_tfv2_NLAR1_BS_BiLSTM,
       train_AE_tfv2_solarDynamo_BiLSTM"""
    lt = wrap_getattr(args,'len_timeseries')
    if return_each_dim:
        d = {}
        for i in range(x.shape[-1]): # do over all channels of observation
            d['ChiSquare_x_ch_%d'%(i+1)] = loss_fn(y_true=x[...,i], y_pred=x_pred[...,i]) / lt
        return d
    else:
        return loss_fn(x,x_pred) / lt # we are not using call rather __call__

@tf.function
def loss_regress_params_fn_a(params, params_pred, return_each_dim=False, loss_fn=None, args=None):
    num_params = params._shape_as_list()[-1]
    nmp = wrap_getattr(args,'num_model_parameters')
    assert nmp == num_params, ":: Fatal: mismatch on model params, but might still be ok, double check."
    if return_each_dim:
        d = {}
        for i in range(num_params):
            d['ChiSquare_z_%d'%(i+1)] = loss_fn(params[...,i], params_pred[...,i]) / nmp
        return d
    else:
        loss = 0.
        for i in range(num_params):
            loss += loss_fn(params[...,i], params_pred[...,i]) / nmp
        return loss

@tf.function
def loss_aggregator_fn_a(params, theta_pred, return_each_dim=False, loss_fn=None, args=None):
    num_params = params._shape_as_list()[-1]
    nmp = wrap_getattr(args,'num_model_parameters')
    assert nmp == num_params, ":: Fatal: mismatch on model params, but might still be ok, double check. (1)"
    assert nmp == params.shape[-1], ":: Fatal: mismatch on model params, but might still be ok, double check. (2)"
    if return_each_dim:
        d = {}
        for i in range(params.shape[-1]):
            d['ChiSquare_theta_ch_%d'%(i+1)] = loss_fn(params[:,i], theta_pred[:,i]) / nmp
        return d
    else:
        loss = 0.
        for i in range(num_params):
            loss += loss_fn(params[:,i], theta_pred[:,i]) / nmp
        return loss

class MSE_withMedian_Loss(tf.keras.losses.Loss): #granted it's more of a regularizer than a loss
    def call(self, y_true, y_pred):
        l_mse_z = []
        for i in range(y_pred._shape_as_list()[-1]): #iterate over ldims
            median_z = tfp.stats.percentile(y_pred[...,i], 50.0, interpolation='midpoint')
            mse_z = tf.math.reduce_mean(tf.math.squared_difference(y_pred[...,i], median_z))
            l_mse_z.append(mse_z)
        return tf.math.reduce_sum(l_mse_z)

class MAE_withMedian_Loss(tf.keras.losses.Loss): #granted it's more of a regularizer than a loss
    def call(self, y_true, y_pred):
        l_mae_z = []
        for i in range(y_pred._shape_as_list()[-1]): #iterate over ldims
            median_z = tfp.stats.percentile(y_pred[...,i], 50.0, interpolation='midpoint')
            mae_z = tf.math.reduce_mean(tf.math.abs(y_pred[...,i]- median_z))
            l_mae_z.append(mae_z)
        return tf.math.reduce_sum(l_mae_z)
