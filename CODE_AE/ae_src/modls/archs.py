# where to collect all ANNs architechtures in a meaningful and cetralized way
import tensorflow as tf
assert tf.__version__[0] == '2'
import numpy as np
from copy import deepcopy
from ae_src.tools.utils import get_ckptname
from ae_src.tools.utils import Manage_Hyper_Parameters
from ae_src.tools.wraps import wrap_hasattr
from ae_src.tools.wraps import wrap_getattr
from ae_src.tools.wraps import wrap_setattr
from ae_src.tools.wraps import wrap_dir

def data2ss(data, ae, prune=True):
    """handy function to transform typical dataset in summary stats"""

    if not ae.is_aggregator:
        data_for_ae = np.reshape(data, (1, len(data)))
        ss = ae.encoder(data_for_ae)
    else:
        inpudim = ae.encoder.input_shape[1]
        assert inpudim == ae.args['batch_size'], ":: Fatal: mismatch with ndims latent. Debug required."
        if len(data.shape) == 3:
            data = data [0]
        data_for_ae = np.expand_dims(np.stack([data]*inpudim),axis=0) # go figure...
        ss = ae.encoder(data_for_ae)
        if prune:
            ss = ss[0,0,:]

    return ss

class Arch():
    """This is the base-class for all architectures"""

    def __init__(self, **kwargs):
        #I am a bit unhappy with keeping load_weights separate as I could think of a keyword that load_weights and restart...

        assert kwargs is not None, ":: Fatal: provide user / default input args."
        self.args = deepcopy(kwargs)

        ncf = wrap_getattr(self.args,"num_conv_filters")
        self.encoder = self.encoder_fn(num_conv_filters=ncf)

        nu = wrap_getattr(self.args,'ae_num_units')
        self.decoder = self.decoder_fn(num_units=nu)

        nflp = wrap_getattr(self.args,'ndims_free_latent_parameters')
        self.aggregator = self.aggregator_fn(num_free_parameters=nflp)

        self.encoder.summary()
        if self.aggregator is not None:
            print("Initializing an encoder-aggregator (INCA) type of arch")
            self.is_aggregator = True
            self.old_weights_de = deepcopy(self.aggregator.weights)
            self.aggregator.summary()
        else:
            print("Initializing an encoder-decoder (ENCA) type of arch")
            self.is_aggregator = False
            self.old_weights_de = deepcopy(self.decoder.weights)
            self.decoder.summary()

        self.old_weights_en = deepcopy(self.encoder.weights)

        if self.args['ae_load_weights']:
            self.load_weights()

    def encoder_fn(self,*args,**kwargs):
        """Skeleton method"""
        return None

    def decoder_fn(self,*args,**kwargs):
        """Skeleton method"""
        return None

    def aggregator_fn(self,*args,**kwargs):
        """Skeleton method"""
        return None

    def check_hyper_params(self):
        isfatal = False
        self.hp_manager = Manage_Hyper_Parameters(logdir=wrap_getattr(self.args,'logdir'))
        if self.hp_manager.args is None: # these args come from loaded file from training
            assert False, 'Hyper-parameter file {} has generated errors.'.format(self.hp_manager.param_config_fn)
        else:
            donotcheck = ['dotrain','max_training_steps','dossvarw','write_ss_obs','ndims_free_latent_parameters'
                          'write_params','write_output','write_noise','write_ss_obs','write_sstats']
            for attr in wrap_dir(self.args): # self.args is copy of input args by user+default, hp_manager.args are loaded ones
                if not attr.startswith('__') and attr not in donotcheck:
                    if not wrap_hasattr(self.hp_manager.args, attr):
                        print('Saved checkpoint file misses attribute %s. Will ignore.' % str(attr)) # this should become fatal
                    else:
                        if wrap_getattr(self.args, attr) != wrap_getattr(self.hp_manager.args, attr):
                            isfatal = wrap_getattr(self.args,"fatal_hyper_param_mism")
                            print( ":: Warning: mismatch in loaded and specified parameter {}: {} vs. {}. Maybe a not required at time of training?!?.".format( attr, wrap_getattr(self.args, attr), wrap_getattr(self.hp_manager.args, attr) ) )
        if isfatal:
            assert False, "Mismatched paramters."

    def load_weights(self):

        assert wrap_getattr(self.args,'logdir') is not None, ":: Fatal: got logdir none in loading."
        logdir = wrap_getattr(self.args,'logdir')
        basename = wrap_getattr(self.args,'load_fl_basename')

        self.check_hyper_params()

        if self.is_aggregator:
            print("Loading an encoder-aggregator type of model")
            ckpt = tf.train.Checkpoint(encoder=self.encoder, aggregator=self.aggregator)
        else:
            print("Loading an encoder-decoder type of model")
            ckpt = tf.train.Checkpoint(encoder=self.encoder, decoder=self.decoder)

        save_manager = tf.train.CheckpointManager(checkpoint=ckpt, directory=logdir,
                                                  max_to_keep=3, checkpoint_name=basename)
        # Restore model weights
        ckptname = get_ckptname(logdir=logdir, id=basename)
        try:
            ckpt.restore(ckptname).assert_existing_objects_matched().expect_partial()
        except:
            assert False, ":: Fatal: this is bad."
        print('Model weights loaded from %s.' % ckptname)

        # sanity checks
        self.new_weights_en = deepcopy(self.encoder.weights)
        if self.is_aggregator:
            self.new_weights_de = deepcopy(self.aggregator.weights)
        else:
            self.new_weights_de = deepcopy(self.decoder.weights)
        changed_de = [False]*len(self.new_weights_de)
        for i in range(len(self.new_weights_de)):
            changed_de [i] = (self.old_weights_de[i] != self.new_weights_de[i]).numpy().any()
        assert any(changed_de), ":: Fatal:: no changes in ecoder model weights?"

        changed_en = [False]*len(self.new_weights_en)
        for i in range(len(self.new_weights_en)):
            changed_en [i] = (self.old_weights_en[i] != self.new_weights_en[i]).numpy().any()
        assert any(changed_en), ":: Fatal:: no changes in decoder model weights?"

class BiLSTM(Arch):
    '''This example uses a fully convolutional encoder and a Bidirectional-LSTM-based decoder.
    Used in: train_AE_tfv2_NLAR1_BiLSTM, train_AE_tfv2_NLAR1_BS_BiLSTM, train_AE_tfv2_NLAR1'''

    def encoder_fn(self,**kwargs):
        '''x_input size: [bs, #len_timeseries, #num_input_channels]
        Implements encoder of only convolutional and maxpooling operators'''

        assert wrap_getattr(self.args,"len_timeseries") is not None, ":: Fatal: len_timeseries is None in BiLSTM"
        assert wrap_getattr(self.args,"num_input_channels") is not None, ":: Fatal: \
                                                                          self.num_input_channels is None in BiLSTM"
        assert wrap_getattr(self.args,"ndims_latent") is not None, ":: Fatal: ndims_latent is None in BiLSTM"
        assert wrap_getattr(self.args,"num_conv_filters") is not None, ":: Fatal: num_conv_filters is None in BiLSTM"

        lt = wrap_getattr(self.args,'len_timeseries')
        nic = wrap_getattr(self.args,'num_input_channels')
        ncf = wrap_getattr(self.args,'num_conv_filters')
        ndl = wrap_getattr(self.args,'ndims_latent')

        conv_fn = lambda filters, act=None, name=None: tf.keras.layers.Conv1D(filters=filters,
                                                                              kernel_size=3,
                                                                              activation=act, name=name)
        x_input = tf.keras.layers.Input(shape=[lt, nic], name='x_observation')
        x = x_input
        for i in range(len(ncf)):
            if i != 0:
                x = tf.keras.layers.MaxPool1D(pool_size=2, name='maxpool%d'%(i+1))(x)
            for j in range(len(ncf[i])):
                x = conv_fn(filters=ncf[i][j], act='relu', name='conv%d_%d'%((i+1), (j+1)))(x) #[batch_size, len_timeseries, num_conv_filters[-1]]

        x = conv_fn(filters=ndl, act=None, name='final_conv')(x)  # [batch_size, len_timeseries, ndims_latent]
        latent_space = tf.keras.layers.GlobalAveragePooling1D(name='global_avg_pool')(x)
        #reslt = tf.keras.Model(inputs=x_input, outputs=latent_space)
        return tf.keras.Model(inputs=x_input, outputs=latent_space)

    def decoder_fn(self,**kwargs):
        '''latent_mappings size: [bs, #ndims_latent]
        noise_vectors size: [bs, #len_timeseries, #num_noise_channels]
        output: [bs, #len_timeseries, #input_channels]'''

        assert wrap_getattr(self.args,"len_timeseries") is not None, ":: Fatal: len_timeseries is None in BiLSTM"
        assert wrap_getattr(self.args,"num_input_channels") is not None, ":: Fatal: \
                                                                          self.num_input_channels is None in BiLSTM"
        assert wrap_getattr(self.args,"ndims_latent") is not None, ":: Fatal: ndims_latent is None in BiLSTM"
        assert wrap_getattr(self.args,"num_conv_filters") is not None, ":: Fatal: num_conv_filters is None in BiLSTM"
        assert wrap_getattr(self.args,"num_noise_channels") is not None, ":: Fatal: num_noise_channels is None in BiLSTM"

        lt = wrap_getattr(self.args,'len_timeseries')
        nic = wrap_getattr(self.args,'num_input_channels')
        ncf = wrap_getattr(self.args,'num_conv_filters')
        ndl = wrap_getattr(self.args,'ndims_latent')
        nnc = wrap_getattr(self.args,'num_noise_channels')

        # tile latent_mappings to timeseries length of the noise vectors.
        latent_mappings = tf.keras.layers.Input(shape=[ndl], name='latent_representations')
        noise_vectors = tf.keras.layers.Input(shape=[lt, nnc], name='noise_vectors')
        tile_ldims_layer = tf.keras.layers.Lambda(function=lambda x: tf.tile(tf.expand_dims(x, axis=1),
                                                                             multiples=[1, lt, 1]),
                                                                             name='tile_latent_space')
        concat_inputs = tf.keras.layers.Concatenate(axis=-1,
                        name='concatenate_noise_and_latent_dims')([tile_ldims_layer(latent_mappings), noise_vectors])
        num_units = kwargs['num_units']

        x = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(units=num_units, return_sequences=True, dtype=tf.float32, name='lstm_cell_1'), name='Bi-cell-1')(concat_inputs)
        x = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(units=num_units, return_sequences=True, dtype=tf.float32, name='lstm_cell_2'), name='Bi-cell-2')(x)
        x = tf.keras.layers.Dense(units=nic, activation=None, name='pred')(x)
        x = tf.keras.layers.Reshape([lt, nic], name='output_shape')(x)

        return tf.keras.Model(inputs=(latent_mappings, noise_vectors), outputs=x)

class FC_multibatch(Arch):
    '''Used in: train_Regressor4_tfv2_NLAR1_FC_multiBatch, train_Regressor4_tfv2_SolarDynamo_FC_multiBatch.
    Use this class to customize the architecture to be used.
    This example uses a fully convolutional encoder and a FC aggregator.'''

    def encoder_fn(self,**kwargs):
        '''x_input size: [bs, #len_timeseries, #num_input_channels]
        Implements encoder of only convolutional and maxpooling operators'''

        conv_fn = lambda filters, act=None, name=None: tf.keras.layers.Conv1D(filters=filters, kernel_size=3, activation=act, name=name)

        def maxpool1D_nd_fn(x):
            '''First reshape input into 3 dims, apply maxpool1d, reshape back to ndims'''
            shp = x.shape
            x = tf.reshape(x, shape=[-1, shp[-2], shp[-1]])
            x = tf.keras.layers.MaxPool1D(pool_size=2)(x)
            x = tf.reshape(x, shape=[-1] + [s for s in shp[1:-2]] + [x.shape[-2], shp[-1]])
            return x

        class Conv1D_ND(tf.keras.layers.Layer):
            def __init__(self, filters, act='relu', **kwargs):
                super(Conv1D_ND, self).__init__(**kwargs)
                self.filters = filters
                self.act = act
            def build(self, input_shape):
                self.conv = tf.keras.layers.Conv1D(filters=self.filters, kernel_size=3, activation=self.act)
            def call(self, inputs):
                shp = inputs.shape
                x = tf.reshape(inputs, shape=[-1, shp[-2], shp[-1]])
                x = self.conv(x)
                x = tf.reshape(x, shape=[-1] + [s for s in shp[1:-2]] + [x.shape[-2], self.filters])
                return x

        assert wrap_getattr(self.args,"len_timeseries") is not None, ":: Fatal: len_timeseries is None in FC_mulb"
        assert wrap_getattr(self.args,"num_input_channels") is not None, ":: Fatal: num_input_channels is None in FC_mulb"
        assert wrap_getattr(self.args,"batch_size") is not None, ":: Fatal: batch_size is None in FC_mulb"
        assert wrap_getattr(self.args,"num_conv_filters") is not None, ":: Fatal: num_conv_filters is None in FC_mulb"
        assert wrap_getattr(self.args,"ndims_latent") is not None, ":: Fatal: ndims_latent is None in FC_mulb"

        lt = wrap_getattr(self.args,'len_timeseries')
        nic = wrap_getattr(self.args,'num_input_channels')
        ncf = wrap_getattr(self.args,'num_conv_filters')
        bsz = wrap_getattr(self.args,'batch_size')
        ndl = wrap_getattr(self.args,'ndims_latent')

        x_input = tf.keras.layers.Input(shape=[bsz, lt, nic], name='x_observation')
        x = x_input
        for i in range(len(ncf)):
            if i != 0:
                x = tf.keras.layers.Lambda(maxpool1D_nd_fn, name='maxpool%d'%(i+1))(x)
            for j in range(len(ncf[i])):
                x = Conv1D_ND(filters=ncf[i][j], act='relu', name='conv%d_%d'%((i+1), (j+1)))(x)

        x = Conv1D_ND(filters=ndl, act=None, name='final_conv')(x)
        latent_space = tf.keras.layers.Lambda(lambda x:tf.math.reduce_mean(x, axis=-2), name='global_avg_pool')(x)

        return tf.keras.Model(inputs=x_input, outputs=latent_space)

    def aggregator_fn(self,num_free_parameters,**kwargs):
        '''latent_mappings size: [bs, #ndims_latent]
        output: [#model_parameters]'''

        assert wrap_getattr(self.args,"batch_size") is not None, ":: Fatal: batch_size is None in FC_mulb"
        assert wrap_getattr(self.args,"ndims_latent") is not None, ":: Fatal: ndims_latent is None in FC_mulb"

        ndl = wrap_getattr(self.args,'ndims_latent')
        bsz = wrap_getattr(self.args,'batch_size')

        if ndl <= num_free_parameters:
            raise AssertionError('#latent dimensions is <= #free_parameters. This makes no sense! You are doing something wrong.')
        # tile latent_mappings to timeseries length of the noise vectors.
        latent_mappings = tf.keras.layers.Input(shape=[bsz, ndl], name='latent_representations')
        # Define number of nodes in intermediate hidden layers. Uses a heuristic to pick based on number of free parameters.
        if num_free_parameters < 2:
            num_ihn = [3, 10, 3]
        elif num_free_parameters < 10:
            num_ihn = [10, 100, 10]
        else:
            raise AssertionError('Number of free parameters %d is too large! You are probably doing something wrong.' % num_free_parameters)

        slice_op = tf.keras.layers.Lambda(lambda x:x[..., ndl - num_free_parameters:], name='slice_free_parameters')
        x = slice_op(latent_mappings)
        for i in range(len(num_ihn)):
            x = tf.keras.layers.Dense(units=num_ihn[i], activation=tf.keras.layers.LeakyReLU(alpha=0.3), name='fc_%d'%(i+1))(x)
        weighting_vector = tf.keras.layers.Dense(units=1, activation=tf.keras.activations.sigmoid, name='fc_%d'%(len(num_ihn)+1))(x)
        weighting_vector = tf.clip_by_value(weighting_vector, clip_value_min=1e-8, clip_value_max=tf.math.reduce_max(weighting_vector))
        weighting_vector = weighting_vector / tf.math.reduce_sum(weighting_vector, axis=1, keepdims=True) # scale weighting vector such that it adds up to 1. Small epsilon for stability.
        scale_op = tf.keras.layers.Lambda(lambda x: x[0][..., : ndl - num_free_parameters] * x[1], name='scale_model_parameters')
        scaled_model_params = scale_op((latent_mappings, weighting_vector))

        weighted_average = tf.math.reduce_sum(scaled_model_params, axis=1, keepdims=False, name='pred_model_parameters') # size: [num_different_mb, num_model_parameters]
        return tf.keras.Model(inputs=latent_mappings, outputs=weighted_average)

### outdated - to be reimplemented ###############################################
#class FC(BiLSTM):
#    '''This example uses a fully convolutional encoder and an LSTM-based decoder.
#    Used in: ttrain_AE_tfv2_NLAR1_FC'''
#
#    def decoder_fn(self,*args,**kwargs):
#        '''latent_mappings size: [bs, #ndims_latent]
#        noise_vectors size: [bs, #len_timeseries, #num_noise_channels]
#        output: [bs, #len_timeseries, #input_channels]'''
#
#        assert self.ndims_latent is not None, ":: Fatal: self.ndims_latent is None in FC"
#        assert self.len_timeseries is not None, ":: Fatal: self.len_timeseries is None in FC"
#        assert self.num_input_channels is not None, ":: Fatal: self.num_input_channels is None in FC"
#        assert 'num_units' in kwargs, ":: Fatal: no num_conv_filters in FC"
#
#        # tile latent_mappings to timeseries length of the noise vectors.
#        latent_mappings = tf.keras.layers.Input(shape=[self.ndims_latent], name='latent_representations')
#        noise_vectors = tf.keras.layers.Input(shape=[self.len_timeseries, self.num_noise_channels],name='noise_vectors')
#        pre_concat = tf.keras.layers.Concatenate(axis=-1,
#                                                 name='flat_concatenate_noise_and_latent_dims')([latent_mappings,tf.keras.layers.Reshape(target_shape=[self.len_timeseries * self.num_noise_channels],
#                                                 name='flatten_noise_vectors')(noise_vectors)])
#
#        num_total_vars = self.len_timeseries * self.num_noise_channels + self.ndims_latent
#
#        pre_fc1 = tf.keras.layers.Dense(units=num_total_vars, activation=tf.keras.layers.LeakyReLU(alpha=0.3),
#                                        name='pre_fc_1')(pre_concat)
#        pre_fc2 = tf.keras.layers.Dense(units=num_total_vars*3, activation=tf.keras.layers.LeakyReLU(alpha=0.3),
#                                        name='pre_fc_2')(pre_fc1)
#        pre_fc3 = tf.keras.layers.Dense(units=num_total_vars, activation=tf.keras.layers.LeakyReLU(alpha=0.3),
#                                        name='pre_fc_3')(pre_fc2)
#        latent_mappings_modified = pre_fc3[..., :self.ndims_latent]
#        noise_vectors_modified = tf.keras.layers.Reshape(target_shape=[self.len_timeseries,
#                                                         self.num_noise_channels],
#                                                         name='reshape_modified_noise_vectors')(pre_fc3[...,
#                                                         self.ndims_latent:])
#        tile_ldims_layer = tf.keras.layers.Lambda(function=lambda x: tf.tile(tf.expand_dims(x, axis=1),
#                                                  multiples=[1, self.len_timeseries, 1]), name='tile_latent_space')
#        concat_inputs = tf.keras.layers.Concatenate(axis=-1,
#                                                    name='concatenate_noise_and_latent_dims')([tile_ldims_layer(
#                                                    latent_mappings_modified), noise_vectors_modified])
#        num_units = kwargs['num_units']
#
#        x = tf.keras.layers.LSTM(units=num_units, return_sequences=True, dtype=tf.float32, name='lstm_cell_1')(concat_inputs)
#        x = tf.keras.layers.LSTM(units=num_units, return_sequences=True, dtype=tf.float32, name='lstm_cell_2')(x)
#        x = tf.keras.layers.Dense(units=self.num_input_channels, activation=None, name='pred')(x)
#        x = tf.keras.layers.Reshape([self.len_timeseries, self.num_input_channels], name='output_shape')(x)
#
#        return tf.keras.Model(inputs=(latent_mappings, noise_vectors), outputs=x)
#
#class Regressor_a(BiLSTM):
#    '''This example uses a fully convolutional encoder and an dense decoder.
#    Used in: train_Regressor_tfv2_NLAR1_FC, train_Regressor1.2_tfv2_NLAR1_FC and others'''
#
#    def decoder_fn(self,*args,**kwargs):
#        '''latent_mappings size: [bs, #ndims_latent]
#        noise_vectors size: [bs, #len_timeseries, #noise_channels]
#        output: [bs, #model_parameters]'''
#
#        assert self.ndims_latent is not None, ":: Fatal: self.ndims_latent is None in Regressor_a"
#        assert self.len_timeseries is not None, ":: Fatal: self.len_timeseries is None in Regressor_a"
#        assert self.num_noise_channels is not None, ":: Fatal: self.num_noise_channels is None in Regressor_a"
#
#        # tile latent_mappings to timeseries length of the noise vectors.
#        latent_mappings = tf.keras.layers.Input(shape=[self.ndims_latent], name='latent_representations')
#        noise_vectors = tf.keras.layers.Input(shape=[self.len_timeseries, self.num_noise_channels], name='noise_vectors')
#        pre_concat = tf.keras.layers.Concatenate(axis=-1,
#                                                 name='flat_concatenate_noise_and_latent_dims')([latent_mappings, tf.keras.layers.Reshape(target_shape=[self.len_timeseries * self.num_noise_channels], name='flatten_noise_vectors')(noise_vectors)])
#        num_total_vars = self.len_timeseries * self.num_noise_channels + self.ndims_latent
#
#        pre_fc1 = tf.keras.layers.Dense(units=num_total_vars, activation=tf.keras.layers.LeakyReLU(alpha=0.3),
#                                        name='pre_fc_1')(pre_concat)
#        pre_fc2 = tf.keras.layers.Dense(units=num_total_vars*3, activation=tf.keras.layers.LeakyReLU(alpha=0.3),
#                                        name='pre_fc_2')(pre_fc1)
#        pre_fc3 = tf.keras.layers.Dense(units=num_total_vars, activation=tf.keras.layers.LeakyReLU(alpha=0.3),
#                                        name='pre_fc_3')(pre_fc2)
#
#        x = tf.keras.layers.Dense(units=self.ndims_output*8, activation=tf.keras.layers.LeakyReLU(alpha=0.3), name='fc_1')(pre_fc3)
#        x = tf.keras.layers.Dense(units=self.ndims_output*4, activation=tf.keras.layers.LeakyReLU(alpha=0.3), name='fc_2')(x)
#        x = tf.keras.layers.Dense(units=self.ndims_output*2, activation=tf.keras.layers.LeakyReLU(alpha=0.3), name='fc_3')(x)
#        x = tf.keras.layers.Dense(units=self.ndims_output, activation=tf.keras.layers.LeakyReLU(alpha=0.3), name='fc_4')(x)
#        return tf.keras.Model(inputs=(latent_mappings, noise_vectors), outputs=x)
#
#class Regressor_b(BiLSTM):
#    '''This example uses a fully convolutional encoder and an dense decoder.
#    Used in: train_Regressor3_tfv2__NLAR1_FC'''
#
#    def decoder_fn(self,*args,**kwargs):
#        '''latent_mappings size: [bs, #ndims_latent]
#        noise_vectors size: [bs, #len_timeseries, #noise_channels]
#        output: [bs, #model_parameters]'''
#
#        assert self.ndims_latent is not None, ":: Fatal: self.ndims_latent is None in Regressor_a"
#        assert self.len_timeseries is not None, ":: Fatal: self.len_timeseries is None in Regressor_a"
#        assert self.num_noise_channels is not None, ":: Fatal: self.num_noise_channels is None in Regressor_a"
#
#        # tile latent_mappings to timeseries length of the noise vectors.
#        latent_mappings = tf.keras.layers.Input(shape=[self.ndims_latent], name='latent_representations')
#        num_total_vars = self.len_timeseries * self.num_noise_channels + self.ndims_latent
#
#        x = tf.keras.layers.Dense(units=self.ndims_output*8, activation=tf.keras.layers.LeakyReLU(alpha=0.3), name='fc_1')(latent_mappings)
#        x = tf.keras.layers.Dense(units=self.ndims_output*4, activation=tf.keras.layers.LeakyReLU(alpha=0.3), name='fc_2')(x)
#        x = tf.keras.layers.Dense(units=self.ndims_output*2, activation=tf.keras.layers.LeakyReLU(alpha=0.3), name='fc_3')(x)
#        x = tf.keras.layers.Dense(units=self.ndims_output, activation=tf.keras.layers.LeakyReLU(alpha=0.3), name='fc_4')(x)
#        return tf.keras.Model(inputs=latent_mappings, outputs=x)
#
#class Regressor_c(BiLSTM):
#    '''This example uses a fully convolutional encoder and an dense decoder.
#    Used in: train_Regressor4_tfv2_NLAR1_FC, train_Regressor4_tfv2_NLAR1_FC_bs5, train_Encoder_Reg4_tfv2_NLAR1_FC_bs5'''
#
#    def aggregator_fn(self,num_free_parameters,*args,**kwargs):
#        '''latent_mappings size: [bs, #ndims_latent]
#        output: [#model_parameters]'''
#        if self.ndims_latent <= num_free_parameters:
#            raise AssertionError('#latent dimensions is <= #free_parameters. This makes no sense! You are doing something wrong.')
#        # tile latent_mappings to timeseries length of the noise vectors.
#        latent_mappings = tf.keras.layers.Input(shape=[self.ndims_latent], name='latent_representations')
#        # Define number of nodes in intermediate hidden layers. Uses a heuristic to pick based on number of free parameters.
#        if num_free_parameters < 2:
#            num_ihn = [3, 10, 3]
#        elif num_free_parameters < 10:
#            num_ihn = [10, 100, 10]
#        else:
#            raise AssertionError('Number of free parameters %d is too large! You are probably doing something wrong.' % num_free_parameters)
#        slice_op = tf.keras.layers.Lambda(lambda x:x[:, self.ndims_latent-num_free_parameters:], name='slice_free_parameters')
#        x = slice_op(latent_mappings)
#        for i in range(len(num_ihn)):
#            x = tf.keras.layers.Dense(units=num_ihn[i], activation=tf.keras.layers.LeakyReLU(alpha=0.3), name='fc_%d'%(i+1))(x)
#        weighting_vector = tf.keras.layers.Dense(units=1, activation=tf.keras.activations.sigmoid, name='fc_%d'%(len(num_ihn)+1))(x) # size: [batch_size, 1]
#        weighting_vector = tf.clip_by_value(weighting_vector, clip_value_min=1e-8, clip_value_max=tf.math.reduce_max(weighting_vector))
#        weighting_vector = weighting_vector / tf.math.reduce_sum(weighting_vector) # scale weighting vector such that it adds up to 1. Small epsilon for stability.
#        # weighting_vector = weighting_vector / (tf.math.reduce_sum(weighting_vector) + 1e-8) # scale weighting vector such that it adds up to 1. Small epsilon for stability.
#        scale_op = tf.keras.layers.Lambda(lambda x: x[0][:, :self.ndims_latent-num_free_parameters] * x[1], name='scale_model_parameters')
#        scaled_model_params = scale_op((latent_mappings, weighting_vector)) # size: [batch_size, num_model_parameters]
#
#        weighted_average = tf.math.reduce_sum(scaled_model_params, axis=0, keepdims=False, name='pred_model_parameters') # size: [num_model_parameters]
#        return tf.keras.Model(inputs=latent_mappings, outputs=weighted_average)
#
#class Regressor_d(BiLSTM):
#    '''This example uses a fully convolutional encoder and an dense decoder.
#    Used in: train_Regressor4_tfv2_NLAR1_FC_bs5_OLD'''
#
#    def aggregator_fn(self,num_free_parameters,*args,**kwargs):
#        '''latent_mappings size: [bs, #ndims_latent]
#        output: [#model_parameters]'''
#        if self.ndims_latent <= num_free_parameters:
#            raise AssertionError('#latent dimensions is <= #free_parameters. This makes no sense! You are doing something wrong.')
#        # tile latent_mappings to timeseries length of the noise vectors.
#        latent_mappings = tf.keras.layers.Input(shape=[self.ndims_latent], name='latent_representations')
#        # Define number of nodes in intermediate hidden layers. Uses a heuristic to pick based on number of free parameters.
#        if num_free_parameters < 2:
#            num_ihn = [3, 10, 3]
#        elif num_free_parameters < 10:
#            num_ihn = [10, 100, 10]
#        else:
#            raise AssertionError('Number of free parameters %d is too large! You are probably doing something wrong.' % num_free_parameters)
#        slice_op = tf.keras.layers.Lambda(lambda x:x[:, self.ndims_latent-num_free_parameters:], name='slice_free_parameters')
#        x = slice_op(latent_mappings)
#        for i in range(len(num_ihn)):
#            x = tf.keras.layers.Dense(units=num_ihn[i], activation=tf.keras.layers.LeakyReLU(alpha=0.3), name='fc_%d'%(i+1))(x)
#        weighting_vector = tf.keras.layers.Dense(units=1, activation=tf.keras.activations.sigmoid, name='fc_%d'%(len(num_ihn)+1))(x) # size: [batch_size, 1]
#        scale_op = tf.keras.layers.Lambda(lambda x: x[0][:, :self.ndims_latent-num_free_parameters] * x[1], name='scale_model_parameters')
#        scaled_model_params = scale_op((latent_mappings, weighting_vector)) # size: [batch_size, num_model_parameters]
#
#        weighted_average = tf.math.reduce_mean(scaled_model_params, axis=0, keepdims=False, name='pred_model_parameters') # size: [num_model_parameters]
#        return tf.keras.Model(inputs=latent_mappings, outputs=weighted_average)
#
#class Regressor_e(BiLSTM):
#    '''This example uses a fully convolutional encoder and an dense decoder.
#    Used in: train_Regressor5_tfv2_NLAR1_FC'''
#
#    def aggregator_fn(self,num_free_parameters,*args,**kwargs):
#        '''latent_mappings size: [bs, #ndims_latent]
#        output: [#model_parameters]'''
#        if self.ndims_latent <= num_free_parameters:
#            raise AssertionError('#latent dimensions is <= #free_parameters. This makes no sense! You are doing something wrong.')
#        # tile latent_mappings to timeseries length of the noise vectors.
#        latent_mappings = tf.keras.layers.Input(shape=[self.ndims_latent], name='latent_representations')
#        # Define number of nodes in intermediate hidden layers. Uses a heuristic to pick based on number of free parameters.
#        if num_free_parameters < 2:
#            num_ihn = [3, 10, 3]
#        elif num_free_parameters < 10:
#            num_ihn = [10, 100, 10]
#        else:
#            raise AssertionError('Number of free parameters %d is too large! You are probably doing something wrong.' % num_free_parameters)
#        num_regressed_parameters = self.ndims_latent-num_free_parameters
#        slice_op = tf.keras.layers.Lambda(lambda x:tf.stack([tf.concat((x[:, rp:rp+1], x[:, self.ndims_latent-num_free_parameters:]), axis=1) for rp in range(num_regressed_parameters)], axis=0), name='slice_free_parameters') # out shape [num_regressed_parameters, batch_size, num_free_parameters+1]
#        x = slice_op(latent_mappings) # shape: [num_regressed_parameters, batch_size, num_free_parameters+1]
#        for i in range(len(num_ihn)):
#            x = tf.keras.layers.Dense(units=num_ihn[i], activation=tf.keras.layers.LeakyReLU(alpha=0.3), name='fc_%d'%(i+1))(x)
#        modified_regressed_params = tf.keras.layers.Dense(units=1, activation=tf.keras.activations.sigmoid, name='fc_%d'%(len(num_ihn)+1))(x) # size: [num_regressed_parameters, batch_size, 1]
#        modified_regressed_params = tf.math.reduce_mean(modified_regressed_params[...,0], axis=1, keepdims=False, name='reduce_minibatch_dim') # size: [num_model_parameters]
#        return tf.keras.Model(inputs=latent_mappings, outputs=modified_regressed_params)
#
#class BS(BiLSTM): # BS_sharpldims appears to be exactly the same afa architecture is concerned
#    '''This example uses a fully convolutional encoder and an LSTM-based decoder (but different from FC).'''
#
#    def decoder_fn(self,*args,**kwargs):
#        '''latent_mappings size: [bs, #ndims_latent]
#        noise_vectors size: [bs, #len_timeseries, #num_noise_channels]
#        output: [bs, #len_timeseries, #input_channels]'''
#
#        assert self.ndims_latent is not None, ":: Fatal: self.ndims_latent is None in BS"
#        assert self.len_timeseries is not None, ":: Fatal: self.len_timeseries is None in BS"
#        assert self.num_input_channels is not None, ":: Fatal: self.num_input_channels is None in BS"
#        assert 'num_units' in kwargs, ":: Fatal: no num_conv_filters in BS"
#
#        # tile latent_mappings to timeseries length of the noise vectors.
#        latent_mappings = tf.keras.layers.Input(shape=[self.ndims_latent], name='latent_representations')
#        noise_vectors = tf.keras.layers.Input(shape=[self.len_timeseries, self.num_noise_channels],
#                                              name='noise_vectors')
#        tile_ldims_layer = tf.keras.layers.Lambda(function=lambda x: tf.tile(tf.expand_dims(x, axis=1),
#                                                  multiples=[1, self.len_timeseries, 1]), name='tile_latent_space')
#        concat_inputs = tf.keras.layers.Concatenate(axis=-1,
#                                                    name='concatenate_noise_and_latent_dims')([tile_ldims_layer(latent_mappings), noise_vectors])
#        num_units = kwargs['num_units']
#
#        x = tf.keras.layers.LSTM(units=num_units, return_sequences=True, dtype=tf.float32,
#                                 name='lstm_cell_1')(concat_inputs)
#        x = tf.keras.layers.LSTM(units=num_units, return_sequences=True, dtype=tf.float32, name='lstm_cell_2')(x)
#        x = tf.keras.layers.Dense(units=self.num_input_channels, activation=None, name='pred')(x)
#        x = tf.keras.layers.Reshape([self.len_timeseries, self.num_input_channels], name='output_shape')(x)
#
#        return tf.keras.Model(inputs=(latent_mappings, noise_vectors), outputs=x)
#
#class Conv1D_Batchwise(tf.keras.layers.Layer):
#        '''Used in: train_AE_tfv2_NLAR1_SingleModelMB
#        1D Conv kernel applied along minibatch axis'''
#        def __init__(self, filters, kernel_size, activation=None, name=None, **kwargs):
#            super().__init__()
#            conv_name = None if name is None else name+'_conv'
#            self.conv = tf.keras.layers.Conv1D(filters=filters, kernel_size=kernel_size, activation=activation, name=conv_name)
#        def call(self, inputs):
#            # permute to swap minibatch and feature channel:
#            x = tf.transpose(inputs, perm=[1, 0, 2]) # replace time axis with minibatch axis (conv runs on second dim)
#            x = self.conv(x)
#            x = tf.transpose(x, perm=[1, 0, 2]) # put everything back to their original place
#            return x
#
#class MaxPool1D_Batchwise(tf.keras.layers.Layer):
#        '''Used in: train_AE_tfv2_NLAR1_SingleModelMB
#        1D Conv kernel applied along minibatch axis'''
#        def __init__(self, pool_size=2, name=None, **kwargs):
#            super().__init__()
#            mp_name = None if name is None else name+'_maxpool_along_minibatch'
#            self.mp = tf.keras.layers.MaxPool1D(pool_size=pool_size, name=mp_name)
#        def call(self, inputs):
#            x = tf.transpose(inputs, perm=[1, 0, 2]) # replace time axis with minibatch axis (maxpool1d runs on second dim)
#            x = self.mp(x)
#            x = tf.transpose(x, perm=[1, 0, 2]) # put everything back to their original place
#            return x
#
#class GlobalAveragePooling1D_2Axes(tf.keras.layers.Layer):
#        '''Used in: train_AE_tfv2_NLAR1_SingleModelMB
#        1D Conv kernel applied along minibatch axis'''
#        def __init__(self, name=None, **kwargs):
#            super().__init__()
#            gap_name = None if name is None else name+'_global_avg_pool_along_time_and_minibatch'
#            self.glob_avg_pool = tf.keras.layers.GlobalAveragePooling1D(name=gap_name)
#        def call(self, inputs):
#            # permute to swap minibatch and feature channel:
#            shp = tf.shape(inputs)
#            x = tf.reshape(inputs, [1, tf.math.reduce_prod(shp[:2]), shp[-1]]) # flatten minibatch and timeseries along timeseries dimension
#            x = self.glob_avg_pool(x)
#            x = tf.reshape(x, [1, shp[-1]])
#            return x
#
#class SMMB (Arch):
#    '''Used in: train_AE_tfv2_NLAR1_SingleModelMB. Use this class to customize the architecture to be used.
#    This example uses a fully convolutional encoder and an LSTM-based decoder.'''
#
#    def encoder_fn(self,*args,**kwargs):
#        '''x_input size: [bs, #len_timeseries, #num_input_channels]
#        Implements encoder of only convolutional and maxpooling operators'''
#
#        assert self.len_timeseries is not None, ":: Fatal: self.len_timeseries is None in SMMB"
#        assert self.num_input_channels is not None, ":: Fatal: self.num_input_channels is None in SMMB"
#        assert self.ndims_latent is not None, ":: Fatal: self.ndims_latent is None in SMMB"
#        assert 'num_conv_filters' in kwargs, ":: Fatal: no num_conv_filters in SMMB"
#        assert 'num_batchwise_conv_filters' in kwargs, ":: Fatal: no num_batchwise_conv_filters in "
#
#        conv_fn = lambda filters, act=None, name=None: tf.keras.layers.Conv1D(filters=filters, kernel_size=3, activation=act, name=name)
#        conv_bw_fn = lambda filters, act=None, name=None: Conv1D_Batchwise(filters=filters, kernel_size=3, activation=act, name=name)
#        x_input = tf.keras.layers.Input(shape=[self.len_timeseries, self.num_input_channels], name='x_observation')
#        x = x_input
#        self.num_conv_filters = kwargs['num_conv_filters']
#        self.num_batchwise_conv_filters = kwargs['num_batchwise_conv_filters']
#
#        ## Convolutions along timeseries axis
#        for i in range(len(self.num_conv_filters)):
#            if i != 0:
#                x = tf.keras.layers.MaxPool1D(pool_size=2, name='maxpool%d'%(i))(x)
#            for j in range(len(self.num_conv_filters[i])):
#                x = conv_fn(filters=self.num_conv_filters[i][j], act='relu', name='conv%d_%d'%((i+1), (j+1)))(x) #[batch_size, len_timeseries_valid_region_after_conv, num_conv_filters[-1]]
#        ## Convolutions along minibatch axis
#        for i in range(len(self.num_batchwise_conv_filters)):
#            if i != 0:
#                x = MaxPool1D_Batchwise(pool_size=2, name='maxpool_bw%d'%(i))(x)
#            for j in range(len(self.num_batchwise_conv_filters[i])):
#                x = conv_bw_fn(filters=self.num_batchwise_conv_filters[i][j], act='relu', name='conv_bw%d_%d'%((i+1), (j+1)))(x) #[batch_size_valid_region_after_conv, len_timeseries_valid_region_after_conv, num_batchwise_conv_filters[-1]]
#        x = conv_fn(filters=self.ndims_latent, act=None, name='final_conv')(x)  # [batch_size_valid_region_after_conv, len_timeseries_valid_region_after_conv, ndims_latent]
#        latent_space = GlobalAveragePooling1D_2Axes(name='global_avg_pool')(x)
#        reslt = tf.keras.Model(inputs=x_input, outputs=latent_space)
#        return reslt
#
#    def decoder_fn(self,*args,**kwargs):
#        '''latent_mappings size: [1, #ndims_latent]
#        noise_vectors size: [bs, #len_timeseries, #num_noise_channels]
#        output: [bs, #len_timeseries, #input_channels]'''
#
#        assert 'num_units' in kwargs, ":: Fatal: no num_units in BiLSTM"
#
#        # tile latent_mappings to timeseries length of the noise vectors.
#        latent_mappings = tf.keras.layers.Input(shape=[self.ndims_latent], name='latent_representations')
#        noise_vectors = tf.keras.layers.Input(shape=[self.len_timeseries, self.num_noise_channels], name='noise_vectors')
#
#        # batch_size = noise_vectors._shape_as_list()[0]
#        batch_size = self.batch_size
#        tile_ldims_layer = tf.keras.layers.Lambda(function=lambda x: tf.tile(tf.expand_dims(x, axis=1), multiples=[batch_size, self.len_timeseries, 1]), name='tile_latent_space')
#        concat_inputs = tf.keras.layers.Concatenate(axis=-1, name='concatenate_noise_and_latent_dims')([tile_ldims_layer(latent_mappings), noise_vectors])
#        num_units = kwargs['num_units']
#
#        x = tf.keras.layers.LSTM(units=num_units, return_sequences=True, dtype=tf.float32, name='lstm_cell_1')(concat_inputs)
#        x = tf.keras.layers.LSTM(units=num_units, return_sequences=True, dtype=tf.float32, name='lstm_cell_2')(x)
#        x = tf.keras.layers.Dense(units=self.num_input_channels, activation=None, name='pred')(x)
#        x = tf.keras.layers.Reshape([self.len_timeseries, self.num_input_channels], name='output_shape')(x)
#
#        return tf.keras.Model(inputs=(latent_mappings, noise_vectors), outputs=x)
#
#class ConvFCEnc_LSTMDec(Arch):
#    '''Used in: train_AE_tfv2_queue.
#    This example uses a convolutional encoder with fully-connected layers in the end and an LSTM-based decoder.'''
#
#    def encoder_fn(self,*args,**kwargs):
#        '''x_input size: [bs, #len_timeseries, #num_input_channels]
#        Implements encoder of convolutional, maxpooling, and fully-connected operators'''
#
#        assert self.len_timeseries is not None, ":: Fatal: self.len_timeseries is None in ConvFCEnv_LSTMDec"
#        assert self.num_input_channels is not None, ":: Fatal: self.num_input_channels is None in ConvFCEnv_LSTMDec"
#        assert self.ndims_latent is not None, ":: Fatal: self.ndims_latent is None in ConvFCEnv_LSTMDec"
#        assert 'num_conv_filters' in kwargs, ":: Fatal: no num_conv_filters in ConvFCEnv_LSTMDec"
#
#        conv_fn = lambda filters, act=None, name=None: tf.keras.layers.Conv1D(filters=filters, kernel_size=3, activation=act, name=name)
#        x_input = tf.keras.layers.Input(shape=[self.len_timeseries, self.num_input_channels], name='x_observation')
#        x = x_input
#        self.num_conv_filters = kwargs['num_conv_filters']
#        #print("$$$$$$$$$$$$$$$$$")
#        #print(self.len_timeseries)
#        #print(self.num_input_channels)
#        #print(self.num_conv_filters)
#        #print(x)
#        #print("$$$$$$$$$$$$$$$$$")
#
#        for i in range(len(self.num_conv_filters)):
#            print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
#            if i != 0:
#                x = tf.keras.layers.MaxPool1D(pool_size=2, name='maxpool%d'%(i+1))(x)
#                print(x)
#            for j in range(len(self.num_conv_filters[i])):
#                x = conv_fn(filters=self.num_conv_filters[i][j], act='relu', name='conv%d_%d'%((i+1), (j+1)))(x) #[batch_size, len_timeseries-x, num_conv_filters[-1]]
#                print(x)
#        # #######################
#        # ## Fully convolutional network
#        # x = conv_fn(filters=self.ndims_latent, act=None, name='final_conv')(x)  # [batch_size, len_timeseries, ndims_latent]
#        # latent_space = tf.keras.layers.GlobalAveragePooling1D(name='global_avg_pool')(x)
#        ##########################
#        ## Add Fully connected nodes in the end
#        #print("#########")
#        #print('type(x):',type(x))
#        #print('x:',x)
#        ##print('x.shape[1:]',tf.print(x.shape[1:]))
#        #print("#########")
#        #tmp = tf.math.reduce_prod(x.shape[1:])
#        #print('type(tmp):',type(tmp))
#        #print('tmp:',tf.print(tmp))
#        #print('isnone:',tmp is None)
#        #for iii in dir(tmp):
#        #    try:
#        #        print(iii,getattr(tmp,iii))
#        #    except:
#        #        pass
#        #print("#########")
#        x = tf.keras.layers.Reshape(target_shape=[tf.math.reduce_prod(x.shape[1:])], name='flatten')(x) # size: [batch_size, num_conv_filters[-1]*(len_timeseries-x)]
#        x = tf.keras.layers.Dense(units=100, activation=tf.keras.layers.LeakyReLU(), name='fc1')(x) # size: [batch_size, num_conv_filters[-1]*(len_timeseries-x)] -> [batch_size, 100]
#        x = tf.keras.layers.Dense(units=10, activation=tf.keras.layers.LeakyReLU(), name='fc2')(x)
#        latent_space = tf.keras.layers.Dense(units=self.ndims_latent, activation=None, name='latent_space')(x)
#        ##########################
#        return tf.keras.Model(inputs=x_input, outputs=latent_space, name='encoder')
#
#    def decoder_fn(self,*args,**kwargs):
#        '''latent_mappings size: [bs, #ndims_latent]
#        noise_vectors size: [bs, #len_timeseries, #num_noise_channels]
#        output: [bs, #len_timeseries, #input_channels]'''
#
#        assert 'num_units' in kwargs, ":: Fatal: no num_units in BiLSTM"
#
#        # tile latent_mappings to timeseries length of the noise vectors.
#        latent_mappings = tf.keras.layers.Input(shape=[self.ndims_latent], name='latent_representations')
#        noise_vectors = tf.keras.layers.Input(shape=[self.len_timeseries, self.num_noise_channels], name='noise_vectors')
#        tile_ldims_layer = tf.keras.layers.Lambda(function=lambda x: tf.tile(tf.expand_dims(x, axis=1), multiples=[1, self.len_timeseries, 1]), name='tile_latent_space')
#        concat_inputs = tf.keras.layers.Concatenate(axis=-1, name='concatenate_noise_and_latent_dims')([tile_ldims_layer(latent_mappings), noise_vectors]) #size: [batch_size, len_timeseries, #ndims_latent + #num_noise_channels]
#        x = concat_inputs
#
#        ##################
#        ## Adding a few layers of FC nodes to approximate any time independent relations prior to LSTM decoding the whole signal.
#        x = tf.keras.layers.Dense(units=100, activation=tf.keras.layers.LeakyReLU(), name='fc1')(concat_inputs) # size: [batch_size, len_timeseries, 100]
#        x = tf.keras.layers.Dense(units=100, activation=tf.keras.layers.LeakyReLU(), name='fc2')(x) # size: [batch_size, len_timeseries, 100]
#        x = tf.keras.layers.Dense(units=100, activation=tf.keras.layers.LeakyReLU(), name='fc3')(x) # size: [batch_size, len_timeseries, 100]
#        x = tf.keras.layers.Dense(units=self.ndims_latent+self.num_noise_channels, activation=None, name='fc4')(x) # size: [batch_size, len_timeseries,  #ndims_latent + #num_noise_channels]
#        ##################
#        # ## Adding a few layers of FC nodes for soft-attention on the input signal to the decoder.
#        # x_ = tf.keras.layers.Dense(units=100, activation=tf.keras.layers.LeakyReLU(), name='at_fc1')(concat_inputs) # size: [batch_size, len_timeseries, 100]
#        # x_ = tf.keras.layers.Dense(units=100, activation=tf.keras.layers.LeakyReLU(), name='at_fc2')(x_) # size: [batch_size, len_timeseries, 100]
#        # x_ = tf.keras.layers.Dense(units=100, activation=tf.keras.layers.LeakyReLU(), name='at_fc3')(x_) # size: [batch_size, len_timeseries, 100]
#        # x_ = tf.keras.layers.Dense(units=self.ndims_latent+self.num_noise_channels, activation='softmax', name='at_fc4')(x_) # size: [batch_size, len_timeseries,  #ndims_latent + #num_noise_channels]
#        # x = tf.keras.layers.Multiply(name='apply_soft_attention')([x, x_])
#        ##################
#
#        num_units = kwargs['num_units']
#
#        x = tf.keras.layers.LSTM(units=num_units, return_sequences=True, dtype=tf.float32, name='lstm_cell_1')(x)
#        x = tf.keras.layers.LSTM(units=num_units, return_sequences=True, dtype=tf.float32, name='lstm_cell_2')(x)
#        x = tf.keras.layers.Dense(units=self.num_input_channels, activation=None, name='pred')(x)
#        x = tf.keras.layers.Reshape([self.len_timeseries, self.num_input_channels], name='output_shape')(x)
#        return tf.keras.Model(inputs=(latent_mappings, noise_vectors), outputs=x, name='decoder')
#
#class ConvEnc_LSTMDec(Arch):
#    '''Used in: train_AE_tfv2_queue_simple.
#    This example uses a convolutional encoder with fully-connected layers in the end and an LSTM-based decoder.'''
#
#    def encoder_fn(self,*args,**kwargs):
#        '''x_input size: [bs, #len_timeseries, #num_input_channels]
#        Implements encoder of convolutional, maxpooling, and fully-connected operators'''
#
#        assert self.len_timeseries is not None, ":: Fatal: self.len_timeseries is None in ConvFCEnv_LSTMDec"
#        assert self.num_input_channels is not None, ":: Fatal: self.num_input_channels is None in ConvFCEnv_LSTMDec"
#        assert self.ndims_latent is not None, ":: Fatal: self.ndims_latent is None in ConvFCEnv_LSTMDec"
#        assert 'num_conv_filters' in kwargs, ":: Fatal: no num_conv_filters in ConvFCEnv_LSTMDec"
#
#        conv_fn = lambda filters, act=None, name=None: tf.keras.layers.Conv1D(filters=filters, kernel_size=3, activation=act, name=name)
#
#        x_input = tf.keras.layers.Input(shape=[self.len_timeseries, self.num_input_channels], name='x_observation')
#        x = x_input
#        self.num_conv_filters = kwargs['num_conv_filters']
#
#        for i in range(len(self.num_conv_filters)):
#            if i != 0:
#                x = tf.keras.layers.MaxPool1D(pool_size=2, name='maxpool%d'%(i+1))(x)
#            for j in range(len(self.num_conv_filters[i])):
#                x = conv_fn(filters=self.num_conv_filters[i][j], act='relu', name='conv%d_%d'%((i+1), (j+1)))(x) #[batch_size, len_timeseries-x, num_conv_filters[-1]]
#
#        # Fully convolutional network
#        x = conv_fn(filters=self.ndims_latent, act=None, name='final_conv')(x)  # [batch_size, len_timeseries, ndims_latent]
#        latent_space = tf.keras.layers.GlobalAveragePooling1D(name='global_avg_pool')(x)
#
#        return tf.keras.Model(inputs=x_input, outputs=latent_space, name='encoder')
#
#    def decoder_fn(self,*args,**kwargs):
#        '''latent_mappings size: [bs, #ndims_latent]
#        noise_vectors size: [bs, #len_timeseries, #num_noise_channels]
#        output: [bs, #len_timeseries, #input_channels]'''
#
#        assert 'num_units' in kwargs, ":: Fatal: no num_units in BiLSTM"
#
#        # tile latent_mappings to timeseries length of the noise vectors.
#        latent_mappings = tf.keras.layers.Input(shape=[self.ndims_latent], name='latent_representations')
#        noise_vectors = tf.keras.layers.Input(shape=[self.len_timeseries, self.num_noise_channels], name='noise_vectors')
#        tile_ldims_layer = tf.keras.layers.Lambda(function=lambda x: tf.tile(tf.expand_dims(x, axis=1), multiples=[1, self.len_timeseries, 1]), name='tile_latent_space')
#        concat_inputs = tf.keras.layers.Concatenate(axis=-1, name='concatenate_noise_and_latent_dims')([tile_ldims_layer(latent_mappings), noise_vectors]) #size: [batch_size, len_timeseries, #ndims_latent + #num_noise_channels]
#
#        x = concat_inputs
#
#        num_units = kwargs['num_units']
#
#        x = tf.keras.layers.LSTM(units=num_units, return_sequences=True, dtype=tf.float32, name='lstm_cell_1')(x)
#        x = tf.keras.layers.LSTM(units=num_units, return_sequences=True, dtype=tf.float32, name='lstm_cell_2')(x)
#        x = tf.keras.layers.Dense(units=self.num_input_channels, activation=None, name='pred')(x)
#        x = tf.keras.layers.Reshape([self.len_timeseries, self.num_input_channels], name='output_shape')(x)
#
#        return tf.keras.Model(inputs=(latent_mappings, noise_vectors), outputs=x, name='decoder')
#
#class ConvUnEnc_LSTMDec(ConvEnc_LSTMDec):
#    '''Used in: train_AE_tfv2_queue_simple_uniformEnc.
#    This example uses a convolutional encoder with fully-connected layers in the end and an LSTM-based decoder.'''
#
#    def encoder_fn(self,*args,**kwargs):
#        '''x_input size: [bs, #len_timeseries, #num_input_channels]
#        Implements encoder of convolutional, maxpooling, and fully-connected operators'''
#
#        assert self.len_timeseries is not None, ":: Fatal: self.len_timeseries is None in ConvFCEnv_LSTMDec"
#        assert self.num_input_channels is not None, ":: Fatal: self.num_input_channels is None in ConvFCEnv_LSTMDec"
#        assert self.ndims_latent is not None, ":: Fatal: self.ndims_latent is None in ConvFCEnv_LSTMDec"
#        assert 'num_conv_filters' in kwargs, ":: Fatal: no num_conv_filters in ConvFCEnv_LSTMDec"
#
#        pre_lat_ndims_latent = 3 * self.ndims_latent
#        conv_fn = lambda filters, act=None, name=None: tf.keras.layers.Conv1D(filters=filters, kernel_size=3, activation=act, name=name)
#
#        x_input = tf.keras.layers.Input(shape=[self.len_timeseries, self.num_input_channels], name='x_observation')
#        x = x_input
#        self.num_conv_filters = kwargs['num_conv_filters']
#
#        for i in range(len(self.num_conv_filters)):
#            if i != 0:
#                x = tf.keras.layers.MaxPool1D(pool_size=2, name='maxpool%d'%(i+1))(x)
#            for j in range(len(self.num_conv_filters[i])):
#                x = conv_fn(filters=self.num_conv_filters[i][j], act='relu', name='conv%d_%d'%((i+1), (j+1)))(x) #[batch_size, len_timeseries-x, num_conv_filters[-1]]
#
#        # #######################
#        ## Fully convolutional network
#        x = conv_fn(filters=pre_lat_ndims_latent, act=None, name='final_conv')(x)  # [batch_size, len_timeseries, ndims_latent]
#        pre_latent_space = tf.keras.layers.GlobalAveragePooling1D(name='global_avg_pool')(x)
#        ##########################
#        ## # Apply CDF of normal dist to map onto uniform dist
#        # cdf_fn = lambda x, mu=0., sigma=1.: 0.5 * (1. + tf.math.erf((x - mu)/(sigma*tf.math.sqrt(2))))
#        # cdf = tf.keras.layers.Lambda(lambda x: cdf_fn(x[0], x[1], x[2]), name='CDF_of_normal_dist')
#        cdf = tf.keras.layers.Lambda(lambda x: 0.5 * (1. + tf.math.erf((x)/(tf.math.sqrt(2.)))), name='CDF_of_normal_dist') #using standard normal, hoping parameters converge to it.
#        l = tf.split(pre_latent_space, 3, axis=1, name='split_pre_latent_space')
#        latent_space = tf.keras.layers.Lambda(lambda l:(2*(cdf(l[0])-0.5) * l[2]) + l[1], name='apply_uniformness')(l)
#        ###################### # Apply tanh to map onto something resembling uniform dist
#        # tanh = tf.keras.activations.tanh
#        # latent_space = tf.keras.layers.Lambda(lambda l:(tf.keras.activations.tanh(l[0])+l[1]) * l[2], name='apply_uniformness')(l)
#        return tf.keras.Model(inputs=x_input, outputs=latent_space, name='encoder')
#
