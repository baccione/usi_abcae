#!/bin/bash

if [ "$#" -ne 1 ]; then
  echo "wrong usage - need one input string."
  exit 1
fi

#grep -nr $1 . | grep -v "Binary" | grep -v "tox" | grep -v "html" #| grep ".py" # too slow, too many files...
for i in `find . -name "*.py"`; do
  grep -q $1 $i
  stts=`echo $?`
  if [[ ${stts} -eq 0 ]]; then
    out=`grep $1 $i`
    echo $i $out
  fi
done

