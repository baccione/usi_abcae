#
################
# This training pipeline has an encoder-encoder-like architecture. From input timeseries to sufficient statistics space, then
# using the free parameters in summary stats space, find a weighting vector to average corresponding regressed parameters in statistics space.


import tensorflow as tf
assert tf.__version__[0] == '2' # this script is intended for tf v2 (2.2.0 to be precise.)
# import tensorflow_addons as tfa
tf.config.list_physical_devices('GPU')
tf.config.set_visible_devices([], 'GPU') # do not use any GPU
import os, glob
import datetime
import numpy as np
import sys
import importlib
import logging
root = logging.getLogger()
root.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)

_DEBUG = False
if _DEBUG:
    os.chdir(os.path.join(os.getcwd(), 'exps'))
    tf.config.run_functions_eagerly(True)

import src.wraps.generators
import src.wraps.utils

#from src.modls.archs import FC_multibatch as Architecture # importing rather than embedding: +10% exetime small run, 16Jun21
from src.tools.utils import Manage_Hyper_Parameters

from src.tools.utils import ExpSetup



from src.modls.losses import loss_aggregator_fn_blowfly  as loss_aggregator_fn
from src.modls.losses import loss_regress_params_fn_blowfly_FCmulB as loss_regress_params_fn

from src.tools.utils import export_summary_scalars
from src.tools.utils import export_summary_histograms

##################################################################################################
# wrap a training step for performance gains.
@tf.function
def train_step(model, x, params, noise, optimizer, args=None, summary_writer=None):
    '''Single training step.'''
    with tf.GradientTape(persistent=False) as tape: #persistent=True if .gradient() will be called multiple times (e.g., multiple losses)
        z_latent = model.encoder(x, training=True) # shape: [num_different_mb, batch_size, num_latent_dims]
        theta_reconst = model.aggregator(z_latent, training=True) # shape: [num_different_mb, num_model_params]
        lambda_theta = args.batch_size
        lambda_z = 1.
        dict_regress_theta_mse = loss_aggregator_fn(params, theta_reconst, return_each_dim=True, args=args)
        loss_regress_theta = tf.math.reduce_sum(list(dict_regress_theta_mse.values()))
        dict_regress_z_mse = loss_regress_params_fn(params, z_latent, return_each_dim=True, args=args)
        loss_regress_z = tf.math.reduce_sum(list(dict_regress_z_mse.values()))
        loss = lambda_theta*loss_regress_theta + lambda_z*loss_regress_z
        trainable_variables = model.encoder.trainable_variables + model.aggregator.trainable_variables
        gradients = tape.gradient(loss, trainable_variables)
        if args.global_gradient_clipnorm is not None:
            gradients, global_norm = tf.clip_by_global_norm(gradients, clip_norm=args.global_gradient_clipnorm, name='clip_gradients_by_global_norm')
        if args.gradient_clip_value is not None:
            gradients = [tf.clip_by_value(grad, clip_value_min=-gradient_clip_value, clip_value_max=gradient_clip_value, name='clip_gradients_by_value') for grad in gradients]
        if tf.equal(optimizer.iterations % args.freq_log, 0):
            l_avg_grads= [tf.math.reduce_mean(g) for g in gradients]
            l_absmax_grads= [tf.math.reduce_max(tf.math.abs(g)) for g in gradients]
            l_gradmean_names = ['gradient_mean_'+v.name for v in trainable_variables]
            l_gradabsmax_names = ['gradient_absmax_'+v.name for v in trainable_variables]
            # export avg and absolute max gradients of variables to tensorboard
            d_grads = {**dict(zip(l_gradmean_names, l_avg_grads)), **dict(zip(l_gradabsmax_names, l_absmax_grads))}
            export_summary_scalars(dict_name_and_val=d_grads, step=optimizer.iterations, writer=summary_writer)
        optimizer.apply_gradients(zip(gradients, trainable_variables))
    # return (loss_reconstruction, loss_regress_theta, loss_regress_z), (x_reconst, theta_reconst, z_latent), (dict_reconstruction_mse, dict_regress_theta_mse, dict_regress_z_mse)
    return (loss_regress_theta, loss_regress_z), (theta_reconst, z_latent), (dict_regress_theta_mse, dict_regress_z_mse)
##################################################################################################

##################################################################################################
def main(args):

    args = ExpSetup(args)

    if not os.path.isdir(args.logdir):
        os.makedirs(args.logdir)

    tf.keras.backend.clear_session()
    physical_devices = tf.config.list_physical_devices('GPU')
    for gpu_instance in physical_devices:
        tf.config.experimental.set_memory_growth(gpu_instance, True)

    arch_module = importlib.import_module("src.modls.archs")
    print('Architecture:',args.aem2load)
    assert "FC_multibatch" == str(args.aem2load), ":: Fatal: this file is intended to be used only with FC_multibatch arch."
    Architecture = getattr(arch_module,args.aem2load)

    # Define a generator function for observations, parameters, and noise vectors
    user_module = importlib.import_module("src.modls."+args.umodlfl)
    user_class = getattr(user_module, args.umodlcl)
    print("User class: ",user_class)
    assert "DataGenerator_Blowfly_Simplified_BatchSampler" in str(user_class), ":: Fatal: this main is inteded for DataGenerator_Blowfly_Simplified"

    # new hardcoded part, why not
    param_names = ['P_fixed', 'n_0_fixed', 'sigma_d_fixed', 'sigma_p_fixed', 'tau_fixed', 'delta_fixed']
    param_means = np.exp([2, 5, -0.5, -0.5, 2, -1]) # using Park et al. prior log means
    dict_uniform_lims = {}
    if args.is_uniform_prior:
        is_uniform_model_param_priors = True
        keys = ['P', 'n_0', 'sigma_d', 'sigma_p', 'tau', 'delta']
        log_param_normal_args = [[2,2], [5,0.5], [-.5,1], [-.5, 1], [2,1], [-1,.4]]
        for i in range(len(keys)):
            k = keys[i]
            m,std = log_param_normal_args[i]
            z_ = 4.
            dict_uniform_lims[k+'_uniform_lims'] = [np.exp(m - z_*std), np.exp(m + z_*std)]
    else:
        is_uniform_model_param_priors = False
    dict_other_params = {}
    for i in args.indices_of_fixed_params:
        dict_other_params[param_names[i]] = param_means[i]
    dict_other_params = {**dict_other_params, **dict_uniform_lims} ## merge the two dicts to be passed as kwargs

    gen_train = user_class(len_timeseries=args.len_timeseries, is_uniform_model_param_priors=args.is_uniform_prior,
                           batch_size=args.batch_size, burnin=args.burnin, **dict_other_params)

    dataset_train = tf.data.Dataset.from_generator(lambda: gen_train, output_types=(tf.float32, tf.float32, tf.float32))
    dataset_train = dataset_train.repeat(count=1)
    dataset_train = dataset_train.batch(args.num_different_mb, drop_remainder=True) # shape: [num_different_mb, batch_size, ...]
    dataset_train = dataset_train.prefetch(buffer_size=10) # #number of minibatches to pre-fetch.
    # # Define model function to produce observation x given model parameters c, sigma and noise vector epsilon

    # Define the architecture
    num_free_latent_parameters = args.ndims_latent - (args.num_model_parameters - len(args.indices_of_fixed_params))
    model_obj = Architecture(ndims_latent=args.ndims_latent,
                             ndims_free_latent_parameters=num_free_latent_parameters,
                             len_timeseries=args.len_timeseries,
                             num_input_channels=args.num_input_channels,
                             num_noise_channels=args.num_noise_channels,
                             ndims_output=args.num_model_parameters,
                             mb_of_same_realizations=args.batch_size)
    model_obj.encoder.summary()
    model_obj.aggregator.summary()

    # Define optimizer
    #lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(initial_learning_rate=1.e-3, decay_steps=int(6*1e3), decay_rate=0.92, staircase=True)
    lr_schedule = src.wraps.utils.LearningRateScheduleExponentialDecayWithLinearWarmup(steps_warmup=args.linear_warmup_steps, initial_learning_rate=1.e-3, decay_steps=int(6*1e3), decay_rate=0.92, staircase=True)
    optimizer = tf.keras.optimizers.Adam(learning_rate=lr_schedule)

    #global_gradient_clipnorm = None
    #gradient_clip_value = None
    #gradient_clip_norm = 5
    if getattr(args,'global_gradient_clipnorm',None) is None:
        setattr(args,'global_gradient_clipnorm',None) # temporary legacy - should come from keyfile
    if getattr(args,'gradient_clip_value',None) is None:
        setattr(args,'gradient_clip_value',None) # temporary legacy - should come from keyfile
    if getattr(args,'gradient_clip_norm',None) is None:
        setattr(args,'gradient_clip_norm',5) # temporary legacy - should come from keyfile

    # Define ckpt managers to save model weights throughout optimization
    ckpt = tf.train.Checkpoint(optimizer=optimizer, encoder=model_obj.encoder, aggregator=model_obj.aggregator)
    save_manager = tf.train.CheckpointManager(checkpoint=ckpt, directory=args.logdir, max_to_keep=3, checkpoint_name='model_ckpt')
    save_manager_best = tf.train.CheckpointManager(checkpoint=ckpt, directory=args.logdir, max_to_keep=3, checkpoint_name='model_best_ckpt') # manager for early stopping.
    save = lambda save_manager, ckpt_number=None: save_manager.save(checkpoint_number=ckpt_number)
    # Define a manager object for training hyper-parameters
    hp_manager = Manage_Hyper_Parameters(logdir=args.logdir)

    # Restore a previously interrupted training session (if exists)
    ckpt.restore(save_manager.latest_checkpoint)
    if save_manager.latest_checkpoint:
        print(f"Restored from {save_manager.latest_checkpoint}.")
        # Double check if experiment setup matches the one in the saved chkpt dir
        hp_manager.check_args_maybe_append(args)
    else:
        print("Initializing training from scratch. \nLogdir: %s" % args.logdir)
        # Dump hyper-parameters to ckpt dir for future reference.
        hp_manager.save_parameters(args)

    # Setup summaries for tensorboard
    summary_writer = tf.summary.create_file_writer(logdir=os.path.join(args.logdir, 'train'))

    # Define a few metrics to export to tensorboard
    avg_loss_total = tf.keras.metrics.Mean(name='loss_total', dtype=tf.float32) # variable will keep track of average of loss value since last freq_log
    # avg_loss_recon_x = tf.keras.metrics.Mean(name='loss_reconstruction_x', dtype=tf.float32)
    avg_loss_reg_theta = tf.keras.metrics.Mean(name='loss_regress_theta', dtype=tf.float32)
    avg_loss_reg_z = tf.keras.metrics.Mean(name='loss_regress_z', dtype=tf.float32)
    dict_avg_loss_recon_x_items = {} #will create mean metric on the fly since we don't know names in advance.
    dict_avg_loss_reg_theta_items = {}
    dict_avg_loss_reg_z_items = {}
    dict_avg_rmse_recon_x_items = {}
    dict_avg_rmse_reg_theta_items = {}
    dict_avg_rmse_reg_z_items = {}

    # Define a metric to keep track of best reconstruction over a longer window
    avg_loss_long_term = tf.keras.metrics.Mean(name='loss_long_term', dtype=tf.float32)
    curr_best_loss = np.inf
    if len(args.indices_of_fixed_params) > 0:
        inds_keep = [i for i in range(args.num_model_parameters) if i not in args.indices_of_fixed_params]
    # Execute training loop
    for x, params, noise in dataset_train:
        num_step = optimizer.iterations
        if num_step >= args.max_training_steps:
            logging.info('Training is completed.')
            break
        if args.regress_log_params: # if True, use enforce log(theta) for first p summary statistics
            params = tf.math.log(params)
        if len(args.indices_of_fixed_params) > 0:
            params = tf.stack([params[:,i] for i in inds_keep], axis=-1) ## drop indices that are fixed.
        loss_tuple, x_theta_z, dict_mse = train_step(model=model_obj, x=x, params=params, noise=noise,
                                                    optimizer=optimizer, args=args, summary_writer=summary_writer)
        theta_reconst, z_latent = x_theta_z
        loss_regress_theta, loss_regress_z = loss_tuple
        loss_total = loss_regress_theta + loss_regress_z
        dict_regress_theta_mse, dict_regress_z_mse = dict_mse

        # Update loggers for tensorboard
        avg_loss_reg_theta.update_state(loss_regress_theta)
        avg_loss_reg_z.update_state(loss_regress_z)
        avg_loss_total.update_state(loss_total) # aggregate values since last flush
        avg_loss_long_term.update_state(loss_total)
        # A little patchy way to keep track of each reconstructed signal and noise channel
        # if not is_early_training:
        #     for k in dict_reconstruction_mse:
        #         if k not in dict_avg_loss_recon_x_items:
        #             dict_avg_loss_recon_x_items[k] = tf.keras.metrics.Mean(name=k, dtype=tf.float32)
        #         dict_avg_loss_recon_x_items[k].update_state(dict_reconstruction_mse[k])
        for k in dict_regress_theta_mse:
            if k not in dict_avg_loss_reg_theta_items:
                dict_avg_loss_reg_theta_items[k] = tf.keras.metrics.Mean(name=k, dtype=tf.float32)
            dict_avg_loss_reg_theta_items[k].update_state(dict_regress_theta_mse[k])
        for k in dict_regress_z_mse:
            if k not in dict_avg_loss_reg_z_items:
                dict_avg_loss_reg_z_items[k] = tf.keras.metrics.Mean(name=k, dtype=tf.float32)
            dict_avg_loss_reg_z_items[k].update_state(dict_regress_z_mse[k])

        # Record RMSE for reconstruction and regressed parameters regardless of the loss
        # if not is_early_training:
        #     for i_ in range(x_reconst.shape[-1]):
        #         k = 'RMSE_x_ch_%d'%(i_+1)
        #         if k not in dict_avg_rmse_recon_x_items:
        #             dict_avg_rmse_recon_x_items[k] = tf.keras.metrics.RootMeanSquaredError(name='rmse_reconstruction', dtype=tf.float32)
        #         dict_avg_rmse_recon_x_items[k].update_state(y_true=x[...,i_], y_pred=x_reconst[...,i_])
        for i_ in range(params.shape[-1]):
            k = 'RMSE_theta_ch_%d'%(i_+1)
            if k not in dict_avg_rmse_reg_theta_items:
                dict_avg_rmse_reg_theta_items[k] = tf.keras.metrics.RootMeanSquaredError(name='rmse_theta', dtype=tf.float32)
            dict_avg_rmse_reg_theta_items[k].update_state(y_true=params[:,i_], y_pred=theta_reconst[:,i_]) # params.shape: [num_different_mb, num_model_params], theta_reconst.shape: [num_different_mb, num_model_params]
        for i_ in range(params.shape[-1]):
            k = 'RMSE_z_ch_%d'%(i_+1)
            if k not in dict_avg_rmse_reg_z_items:
                dict_avg_rmse_reg_z_items[k] = tf.keras.metrics.RootMeanSquaredError(name='rmse_z', dtype=tf.float32)
            # dict_avg_rmse_reg_z_items[k].update_state(y_true=params[...,i_], y_pred=z_latent[...,i_]
            dict_avg_rmse_reg_z_items[k].update_state(y_true=params[...,i_:i_+1], y_pred=z_latent[...,i_]) # params.shape: [num_different_mb, num_model_params], z_latent.shape: [num_different_mb, args.batch_size, ndims_latent]
        # Export status to tensorboard
        if tf.equal(optimizer.iterations % args.freq_log, 0):
            d_scalars = {'loss_total': avg_loss_total.result(), 'loss_regress_theta': avg_loss_reg_theta.result(), 'loss_regress_z': avg_loss_reg_z.result()}
            # for k in dict_avg_loss_recon_x_items:
            #     d_scalars[k] = dict_avg_loss_recon_x_items[k].result()
            #     dict_avg_loss_recon_x_items[k].reset_states()
            for k in dict_avg_loss_reg_theta_items:
                d_scalars[k] = dict_avg_loss_reg_theta_items[k].result()
                dict_avg_loss_reg_theta_items[k].reset_states()
            for k in dict_avg_loss_reg_z_items:
                d_scalars[k] = dict_avg_loss_reg_z_items[k].result()
                dict_avg_loss_reg_z_items[k].reset_states()
            # for k in dict_avg_rmse_recon_x_items:
            #     d_scalars[k] = dict_avg_rmse_recon_x_items[k].result()
            #     dict_avg_rmse_recon_x_items[k].reset_states()
            for k in dict_avg_rmse_reg_theta_items:
                d_scalars[k] = dict_avg_rmse_reg_theta_items[k].result()
                dict_avg_rmse_reg_theta_items[k].reset_states()
            for k in dict_avg_rmse_reg_z_items:
                d_scalars[k] = dict_avg_rmse_reg_z_items[k].result()
                dict_avg_rmse_reg_z_items[k].reset_states()
            d_scalars['lr_schedule'] = lr_schedule(step=num_step)
            export_summary_scalars(dict_name_and_val=d_scalars, step=optimizer.iterations, writer=summary_writer)
            # Print current loss status to terminal
            logging.info('Step %d: avg loss: %.3f, parameter (theta) regression loss: %.3f, latent_z regression loss: %.3f.' % \
                    (num_step, d_scalars['loss_total'], d_scalars['loss_regress_theta'], d_scalars['loss_regress_z']))
            avg_loss_total.reset_states() #reset kept history of loss
            # avg_loss_recon_x.reset_states()
            avg_loss_reg_theta.reset_states()
            # Export trainable variables to tboard histogram
            # TODO: consider speeding this up.
            l_enc = list(zip(*[['enc_'+v.name, v.value()] for v in model_obj.encoder.trainable_variables]))
            l_agg = list(zip(*[['agg_'+v.name, v.value()] for v in model_obj.aggregator.trainable_variables]))
            d_histograms = {**dict(zip(l_enc[0], l_enc[1])), **dict(zip(l_agg[0], l_agg[1]))}
            export_summary_histograms(dict_name_and_val=d_histograms, step=optimizer.iterations, writer=summary_writer)
            # Save the current state of the model weights on disk.
            save(save_manager, ckpt_number=optimizer.iterations)
        # Check loss for long term best reconstruction
        if tf.equal(optimizer.iterations % (10 * args.freq_log), 0):
            if avg_loss_long_term.result() < curr_best_loss:
                curr_best_loss = avg_loss_long_term.result()
                avg_loss_long_term.reset_states()
                logging.info('New long term best loss found: %.3f. Saving.' % curr_best_loss)
                save(save_manager_best, ckpt_number=optimizer.iterations)

    ##################################################################################################
    # Save model once again on the exit
    save(save_manager, ckpt_number=optimizer.iterations)

if __name__ == "__main__":
    # tf.app.run()
    main(0)
