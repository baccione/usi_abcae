#Author: Firat Ozdemir, August 2020, firat.ozdemir@datascience.ch
################
# This training pipeline has only an encoder-decoder-like architecture. From input timeseries to sufficient statistics space, then
# using the noise vectors back to the reconstruction of the initial input signal.

import tensorflow as tf
assert tf.__version__[0] == '2' # this script is intended for tf v2 (2.2.0 to be precise.)
import os, glob
import datetime
import numpy as np
import sys
import logging
root = logging.getLogger()
root.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)

import src.wraps.generators

from src.modls.archs import Regressor_a as Architecture
from src.tools.utils import Manage_Hyper_Parameters

from src.tools.utils import ExpSetup

from src.modls.losses import loss_reconstruction_fn_ba as loss_reconstruction_fn
from src.modls.losses import loss_regress_params_fn_ba as loss_regress_params_fn
from src.modls.losses import loss_decoder_fn

from src.tools.utils import export_summary_scalars
from src.tools.utils import export_summary_histograms
##################################################################################################
# wrap a training step for performance gains.
@tf.function
def train_step(model, x, params, noise, optimizer, args=None, summary_writer=None):
    '''Single training step.'''
    with tf.GradientTape(persistent=False) as tape: #persistent=True if .gradient() will be called multiple times (e.g., multiple losses)
        z_latent = model.encoder(x, training=True)
        theta_reconst = model.decoder((z_latent, noise), training=True)
        x_reconst = NLAR1_gen_fn(c_minibatch=theta_reconst[...,0], sigma_minibatch=theta_reconst[...,1], noise_minibatch=noise, return_as_tensor=True)
        dict_reconstruction_mse = loss_reconstruction_fn(x, x_reconst, return_each_dim=True)
        loss_reconstruction = tf.math.reduce_sum(list(dict_reconstruction_mse.values()))
        if optimizer.iterations > args.reconstruction_loss_start_step:
            lambda_x, lambda_theta, lambda_z = 1., 1., 1.
        else:
            lambda_x, lambda_theta, lambda_z = 0., 1.5, 1.5
        dict_regress_theta_mse = loss_decoder_fn(params, theta_reconst, return_each_dim=True)
        loss_regress_theta = tf.math.reduce_sum(list(dict_regress_theta_mse.values()))
        dict_regress_z_mse = loss_regress_params_fn(params, z_latent, return_each_dim=True)
        loss_regress_z = tf.math.reduce_sum(list(dict_regress_z_mse.values()))
        loss = lambda_x*loss_reconstruction + lambda_theta*loss_regress_theta + lambda_z*loss_regress_z
        trainable_variables = model.encoder.trainable_variables + model.decoder.trainable_variables
        gradients = tape.gradient(loss, trainable_variables)
        if args.global_gradient_clipnorm is not None:
            gradients, global_norm = tf.clip_by_global_norm(gradients, clip_norm=args.global_gradient_clipnorm, name='clip_gradients_by_global_norm')
        if args.gradient_clip_value is not None:
            gradients = [tf.clip_by_value(grad, clip_value_min=-args.gradient_clip_value, clip_value_max=args.gradient_clip_value, name='clip_gradients_by_value') for grad in gradients]
        if tf.equal(optimizer.iterations % args.freq_log, 0):
            l_avg_grads= [tf.math.reduce_mean(g) for g in gradients]
            l_absmax_grads= [tf.math.reduce_max(tf.math.abs(g)) for g in gradients]
            l_gradmean_names = ['gradient_mean_'+v.name for v in trainable_variables]
            l_gradabsmax_names = ['gradient_absmax_'+v.name for v in trainable_variables]
            # export avg and absolute max gradients of variables to tensorboard
            d_grads = {**dict(zip(l_gradmean_names, l_avg_grads)), **dict(zip(l_gradabsmax_names, l_absmax_grads))}
            export_summary_scalars(dict_name_and_val=d_grads, step=optimizer.iterations, writer=summary_writer)
        optimizer.apply_gradients(zip(gradients, trainable_variables))
    return (loss_reconstruction, loss_regress_theta, loss_regress_z), (x_reconst, theta_reconst, z_latent), (dict_reconstruction_mse, dict_regress_theta_mse, dict_regress_z_mse)

##################################################################################################
def main(args):

    args = ExpSetup(args)

    if not os.path.isdir(args.logdir):
        os.makedirs(args.logdir)

    tf.keras.backend.clear_session()
    physical_devices = tf.config.list_physical_devices('GPU')
    for gpu_instance in physical_devices:
        tf.config.experimental.set_memory_growth(gpu_instance, True)

    ##################################################################################################
    # Define a generator function for observations, parameters, and noise vectors
    x_0 = 0.25
    c_lims = [4.0, 7.0]
    sigma_lims = [0.005, 0.25]


    gen_train = src.wraps.generators.DataGenerator_NLAR1_Extended(len_timeseries=args.len_timeseries, x_0=x_0, c_lims=c_lims,
                                                                    sigma_lims=sigma_lims)
    dataset_train = tf.data.Dataset.from_generator(lambda: gen_train, output_types=(tf.float32, tf.float32, tf.float32))
    dataset_train = dataset_train.repeat(count=1)
    dataset_train = dataset_train.batch(args.batch_size, drop_remainder=True)
    dataset_train = dataset_train.prefetch(buffer_size=10) # #number of minibatches to pre-fetch.
    # Define model function to produce observation x given model parameters c, sigma and noise vector epsilon
    def NLAR1_gen_fn(c_minibatch, sigma_minibatch, noise_minibatch, return_as_tensor=True):
        fn = lambda c, x_old, sigma, epsilon: c * x_old**2 * (1. - x_old) + sigma * epsilon
        x = [tf.convert_to_tensor(np.tile(x_0, args.batch_size), dtype=tf.float32)]
        for i in range(1, args.len_timeseries): # this can be sped up if needed.
            x_i = fn(c=c_minibatch, x_old=x[i-1], sigma=sigma_minibatch, epsilon=noise_minibatch[:,i-1,0])
            x.append(x_i)
        x = tf.stack(x) # shape: [len_timeseries, batch_size]
        x = tf.transpose(x, [1,0]) # shape: [batch_size=300, len_timeseries=200]
        x = tf.expand_dims(x, axis=-1) # shape: [batch_size, len_timeseries, #channels=1]
        if return_as_tensor: 
            return x
        else:
            return x.eval() #not sure about this.
    ##################################################################################################
    # Define the architecture
    model_obj = Architecture(ndims_latent=args.ndims_latent, len_timeseries=args.len_timeseries, num_input_channels=1, num_noise_channels=args.num_noise_channels, ndims_output=args.num_model_parameters)
    model_obj.encoder.summary()
    model_obj.decoder.summary()

    # Define optimizer
    #lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(initial_learning_rate=5*1.e-6, decay_steps=int(6*1e3), decay_rate=0.92, staircase=True)
    lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(initial_learning_rate=0.1, decay_steps=int(6*1e3), decay_rate=0.92, staircase=True)
    # optimizer = tf.keras.optimizers.Adam(learning_rate=lr_schedule, clipnorm=1) #, clipnorm=1, clipvalue=1.) #clips |gradients| above clipvalue to prevent exploding., #clipnorm: preventative measure for divergence. Unfortunately both are clipping individually for each gradient, which can change the direction of the gradients..
    optimizer = tf.keras.optimizers.SGD(learning_rate=lr_schedule, nesterov=False)




    #global_gradient_clipnorm = 1
    #gradient_clip_value = 1

    if getattr(args,'global_gradient_clipnorm',None) is None:
        setattr(args,'global_gradient_clipnorm',1) # temporary legacy - should come from keyfile
    if getattr(args,'gradient_clip_value',None) is None:
        setattr(args,'gradient_clip_value',1) # temporary legacy - should come from keyfile
    # Define ckpt managers to save model weights throughout optimization
    ckpt = tf.train.Checkpoint(optimizer=optimizer, encoder=model_obj.encoder, decoder=model_obj.decoder)
    save_manager = tf.train.CheckpointManager(checkpoint=ckpt, directory=args.logdir, max_to_keep=3, checkpoint_name='model_ckpt')
    save_manager_best = tf.train.CheckpointManager(checkpoint=ckpt, directory=args.logdir, max_to_keep=3, checkpoint_name='model_best_ckpt') # manager for early stopping.
    save = lambda save_manager, ckpt_number=None: save_manager.save(checkpoint_number=ckpt_number)
    # Define a manager object for training hyper-parameters
    hp_manager = Manage_Hyper_Parameters(logdir=args.logdir)

    # Restore a previously interrupted training session (if exists)
    ckpt.restore(save_manager.latest_checkpoint)
    if save_manager.latest_checkpoint:
        print(f"Restored from {save_manager.latest_checkpoint}.")
        # Double check if experiment setup matches the one in the saved chkpt dir
        hp_manager.check_args_maybe_append(args)
    else:
        print("Initializing training from scratch. \nLogdir: %s" % args.logdir)
        # Dump hyper-parameters to ckpt dir for future reference.
        hp_manager.save_parameters(args)

    # Setup summaries for tensorboard
    summary_writer = tf.summary.create_file_writer(logdir=os.path.join(args.logdir, 'train'))

    # Define a few metrics to export to tensorboard
    avg_loss_total = tf.keras.metrics.Mean(name='loss_total', dtype=tf.float32) # variable will keep track of average of loss value since last freq_log
    avg_loss_recon_x = tf.keras.metrics.Mean(name='loss_reconstruction_x', dtype=tf.float32)
    avg_loss_reg_theta = tf.keras.metrics.Mean(name='loss_regress_theta', dtype=tf.float32)
    avg_loss_reg_z = tf.keras.metrics.Mean(name='loss_regress_z', dtype=tf.float32)
    dict_avg_loss_recon_x_items = {} #will create mean metric on the fly since we don't know names in advance.
    dict_avg_loss_reg_theta_items = {}
    dict_avg_loss_reg_z_items = {}
    dict_avg_rmse_recon_x_items = {}
    dict_avg_rmse_reg_theta_items = {}
    dict_avg_rmse_reg_z_items = {}

    # Define a metric to keep track of best reconstruction over a longer window
    avg_loss_long_term = tf.keras.metrics.Mean(name='loss_long_term', dtype=tf.float32)
    curr_best_loss = np.inf

    # Execute training loop
    for x, params, noise in dataset_train:
        num_step = optimizer.iterations
        if num_step >= args.max_training_steps:
            logging.info('Training is completed.')
            break

        loss_tuple, x_theta_z, dict_mse = train_step(model=model_obj, x=x, params=params, noise=noise, optimizer=optimizer)
        x_reconst, theta_reconst, z_latent = x_theta_z
        loss_reconstruction, loss_regress_theta, loss_regress_z = loss_tuple
        loss_total = loss_reconstruction + loss_regress_theta + loss_regress_z
        dict_reconstruction_mse, dict_regress_theta_mse, dict_regress_z_mse = dict_mse
        # Update loggers for tensorboard
        avg_loss_recon_x.update_state(loss_reconstruction)
        avg_loss_reg_theta.update_state(loss_regress_theta)
        avg_loss_reg_z.update_state(loss_regress_z)
        avg_loss_total.update_state(loss_total) # aggregate values since last flush
        avg_loss_long_term.update_state(loss_total)
        # A little patchy way to keep track of each reconstructed signal and noise channel
        for k in dict_reconstruction_mse:
            if k not in dict_avg_loss_recon_x_items:
                dict_avg_loss_recon_x_items[k] = tf.keras.metrics.Mean(name=k, dtype=tf.float32)
            dict_avg_loss_recon_x_items[k].update_state(dict_reconstruction_mse[k])                
        for k in dict_regress_theta_mse:
            if k not in dict_avg_loss_reg_theta_items:
                dict_avg_loss_reg_theta_items[k] = tf.keras.metrics.Mean(name=k, dtype=tf.float32)
            dict_avg_loss_reg_theta_items[k].update_state(dict_regress_theta_mse[k])
        for k in dict_regress_z_mse:
            if k not in dict_avg_loss_reg_z_items:
                dict_avg_loss_reg_z_items[k] = tf.keras.metrics.Mean(name=k, dtype=tf.float32)
            dict_avg_loss_reg_z_items[k].update_state(dict_regress_z_mse[k])

        # Record RMSE for reconstruction and regressed parameters regardless of the loss
        for i_ in range(x_reconst.shape[-1]):
            k = 'RMSE_x_ch_%d'%(i_+1)
            if k not in dict_avg_rmse_recon_x_items:
                dict_avg_rmse_recon_x_items[k] = tf.keras.metrics.RootMeanSquaredError(name='rmse_reconstruction', dtype=tf.float32)
            dict_avg_rmse_recon_x_items[k].update_state(y_true=x[...,i_], y_pred=x_reconst[...,i_])
        for i_ in range(params.shape[-1]):
            k = 'RMSE_theta_ch_%d'%(i_+1)
            if k not in dict_avg_rmse_reg_theta_items:
                dict_avg_rmse_reg_theta_items[k] = tf.keras.metrics.RootMeanSquaredError(name='rmse_reconstruction', dtype=tf.float32)
            dict_avg_rmse_reg_theta_items[k].update_state(y_true=params[...,i_], y_pred=theta_reconst[...,i_])
        for i_ in range(params.shape[-1]):
            k = 'RMSE_z_ch_%d'%(i_+1)
            if k not in dict_avg_rmse_reg_z_items:
                dict_avg_rmse_reg_z_items[k] = tf.keras.metrics.RootMeanSquaredError(name='rmse_regularization', dtype=tf.float32)
            dict_avg_rmse_reg_z_items[k].update_state(y_true=params[...,i_], y_pred=z_latent[...,i_])
        # Export status to tensorboard
        if tf.equal(optimizer.iterations % args.freq_log, 0):
            d_scalars = {'loss_total': avg_loss_total.result(), 'loss_reconstruction_x': avg_loss_recon_x.result(), 'loss_regress_theta': avg_loss_reg_theta.result(), 'loss_regress_z': avg_loss_reg_z.result()}
            for k in dict_avg_loss_recon_x_items:
                d_scalars[k] = dict_avg_loss_recon_x_items[k].result()
                dict_avg_loss_recon_x_items[k].reset_states()
            for k in dict_avg_loss_reg_theta_items:
                d_scalars[k] = dict_avg_loss_reg_theta_items[k].result()
                dict_avg_loss_reg_theta_items[k].reset_states()
            for k in dict_avg_loss_reg_z_items:
                d_scalars[k] = dict_avg_loss_reg_z_items[k].result()
                dict_avg_loss_reg_z_items[k].reset_states()
            for k in dict_avg_rmse_recon_x_items:
                d_scalars[k] = dict_avg_rmse_recon_x_items[k].result()
                dict_avg_rmse_recon_x_items[k].reset_states()
            for k in dict_avg_rmse_reg_theta_items:
                d_scalars[k] = dict_avg_rmse_reg_theta_items[k].result()
                dict_avg_rmse_reg_theta_items[k].reset_states()
            for k in dict_avg_rmse_reg_z_items:
                d_scalars[k] = dict_avg_rmse_reg_z_items[k].result()
                dict_avg_rmse_reg_z_items[k].reset_states()
            d_scalars['lr_schedule'] = lr_schedule(step=num_step)
            export_summary_scalars(dict_name_and_val=d_scalars, step=optimizer.iterations, writer=summary_writer)
            # Print current loss status to terminal
            logging.info('Step %d: avg loss: %.3f, x reconstruction loss: %.3f, parameter (theta) regression loss: %.3f, latent_z regression loss: %.3f.' % \
                    (num_step, d_scalars['loss_total'], d_scalars['loss_reconstruction_x'], d_scalars['loss_regress_theta'], d_scalars['loss_regress_z']))
            avg_loss_total.reset_states() #reset kept history of loss
            avg_loss_recon_x.reset_states()
            avg_loss_reg_theta.reset_states()
            # Export trainable variables to tboard histogram
            # TODO: consider speeding this up.
            l_enc = list(zip(*[['enc_'+v.name, v.value()] for v in model_obj.encoder.trainable_variables]))
            l_dec = list(zip(*[['dec_'+v.name, v.value()] for v in model_obj.decoder.trainable_variables]))
            d_histograms = {**dict(zip(l_enc[0], l_enc[1])), **dict(zip(l_dec[0], l_dec[1]))}
            export_summary_histograms(dict_name_and_val=d_histograms, step=optimizer.iterations, writer=summary_writer)
            # Save the current state of the model weights on disk.
            save(save_manager, ckpt_number=optimizer.iterations)
        # Check loss for long term best reconstruction
        if tf.equal(optimizer.iterations % (10 * args.freq_log), 0):
            if avg_loss_long_term.result() < curr_best_loss:
                curr_best_loss = avg_loss_long_term.result()
                avg_loss_long_term.reset_states()
                logging.info('New long term best loss found: %.3f. Saving.' % curr_best_loss)
                save(save_manager_best, ckpt_number=optimizer.iterations)

    ##################################################################################################
    # Save model once again on the exit
    save(save_manager, ckpt_number=optimizer.iterations)

class Sampler:
    '''Class provides user friendly access to low-dimensional space for summary statistics analysis.'''
    def __init__(self, generator=None, iterator=None, **kwargs):
        '''Constructor builds NN model and loads its weights. In addition, a generator with true sun parameters is initialized.'''
        self.args = ExpSetup() # get experiment parameters from the ExpSetup class in this file. Make sure logdir is accurate.
        self.basename = kwargs.get('basename', 'model_best_ckpt')
        if 'logdir' in kwargs:
            self.args.logdir = kwargs.get('logdir')
        if not os.path.isdir(self.args.logdir):
            raise AssertionError('logdir %s from ExpSetup is not found. Quitting.' % self.args.logdir)
        self.check_hyper_params()
        self.prng = kwargs.get('prng', np.random.RandomState(1999))
        self.model_obj = None
        self.build_model()
        self.load_model(basename=self.basename)
        self.generator = generator
        self.iterator = iterator

    def sample(self, num_samples=10, return_noise_vectors=False, params=None):
        '''Function samples #num_samples observed vectors, then returns the mapped representation for each (size: [#num_samples, #stats]).
        if params is not None, existing generator is ignored and a new custom one is initialized.'''
        if params is None: # if none, fallback to default generator of the class instance
            if self.iterator is None:
                raise AssertionError('iterator is not defined. You must provide a parameter dict for the generator.')
            iterator = self.iterator
        else:
            _, iterator = self.build_custom_generator(return_generator=True, **params)

        summary_space = np.zeros((num_samples, self.args.ndims_latent))
        ndarray_noise_timeseries = np.zeros((num_samples, self.args.len_timeseries, self.args.num_noise_channels))
        for i in range(num_samples):
            b = next(iterator) # sample contains a tuple of form (x, params, noise)
            x_i = np.expand_dims(b[0], axis=0).astype(np.float32) #creating minibatch size 1.
            noise_i = np.expand_dims(b[2], axis=0).astype(np.float32) #creating minibatch size 1.
            o_latent = self.model_obj.encoder(x_i, training=False).numpy()
            summary_space[i,...] = o_latent
            ndarray_noise_timeseries[i,...] = noise_i
        if return_noise_vectors:
            return summary_space, ndarray_noise_timeseries
        else:
            return summary_space

    def encode(self, samples):
        '''Function generates latent representations of the given observations (samples).
        samples should be of shape: [num_samples, len_timeseries, num_channels=1]'''
        o_latent = self.model_obj.encoder(samples)
        return o_latent

    def reconstruct(self, num_samples=10, params=None):
        '''Function samples #num_samples p_n vectors, then returns the reconstructions of the from their latent representation for each (size: [#num_samples, len_timeseries]
        if params is not None, existing generator is ignored and a new custom one is initialized.'''
        if params is None: # if none, fallback to default generator of the class instance
            if self.iterator is None:
                raise AssertionError('iterator is not defined. You must provide a parameter dict for the generator.')
            iterator = self.iterator
        else:
            _, iterator = self.build_custom_generator(return_generator=True, **params)
        ndarray_model_parameters = np.zeros((num_samples, self.args.num_model_parameters))
        for i in range(num_samples):
            b = next(iterator) # sample contains a tuple of form (x, params, noise)
            x_i = np.expand_dims(b[0], axis=0).astype(np.float32) #creating minibatch size 1.
            noise_i = np.expand_dims(b[2], axis=0).astype(np.float32) #creating minibatch size 1.
            z_latent = self.model_obj.encoder(x_i, training=False)
            o_reconst = self.model_obj.decoder((z_latent, noise_i), training=False).numpy()
            ndarray_model_parameters[i,...] = np.squeeze(o_reconst)
        return ndarray_model_parameters

    def decode(self, tuple_summary_and_noise):
        '''Function reconstruct timeseries for a given tuple of (latent_representations, noise vectors).'''
        o_latent = tuple_summary_and_noise[0]
        noise_i = tuple_summary_and_noise[1]
        o_reconst = self.model_obj.decoder((o_latent, noise_i), training=False).numpy()
        return o_reconst

    def check_hyper_params(self):
        hp_manager = Manage_Hyper_Parameters(logdir=self.args.logdir)
        if hp_manager.args is None:
            raise AssertionError('Hyper-parameter configuration file %s is not found. Quitting.' % (hp_manager.param_config_fn))
        else:
            for attr in dir(self.args):
                if not attr.startswith('__') and attr not in ['logdir', 'batch_size']:  # don't get methods, ignore logdir and batch_size, as the optimization could have been done elsewhere.
                    if getattr(self.args, attr) != getattr(hp_manager.args, attr):
                        raise AssertionError('Mismatch of hyper-parameter attributes in the logdir %s and ExpSetup file in this script for attribute: %s' % (self.args.logdir, attr))

    def build_model(self):
        self.model_obj = Architecture(ndims_latent=self.args.ndims_latent, len_timeseries=self.args.len_timeseries, num_noise_channels=self.args.num_noise_channels, ndims_output=self.args.num_model_parameters)

    def load_model(self, basename='model_best_ckpt'):
        ckpt = tf.train.Checkpoint(encoder=self.model_obj.encoder, decoder=self.model_obj.decoder)
        save_manager = tf.train.CheckpointManager(checkpoint=ckpt, directory=self.args.logdir, max_to_keep=3, checkpoint_name=basename)
        # Restore model weights
        ckptname = get_ckptname(logdir=self.args.logdir, id=basename)
        try:
            ckpt.restore(ckptname).assert_existing_objects_matched().expect_partial()
        except:
            raise AssertionError('Model weights with basename %s not found in logdir %s. Quitting.' % (basename, self.args.logdir))
        print('Model weights loaded from %s.' % ckptname)
        # ckpt.restore(save_manager.latest_checkpoint).expect_partial()
        # if save_manager.latest_checkpoint:
        #     print('Model weights loaded from %s.' % save_manager.latest_checkpoint)
        # else:
        #     raise AssertionError('Model weights with basename %s not found in logdir %s. Quitting.' % (basename, self.args.logdir))

    def build_custom_generator(self, return_generator=False, **kwargs):
        '''See inside DataGenerator_SolarDynamo for the parameters one can modify for custom generator.'''
        if 'prng' in kwargs:
            prng = kwargs.pop('prng')
        else:
            prng = self.prng
        x_0 = kwargs.pop('x_0', 0.25)
        c_lims = kwargs.pop('c_lims', [4.0, 7.0])
        sigma_limt = kwargs.pop('sigma_lims', [0.005, 0.25])





        generator = wrappers.generators.DataGenerator_NLAR1_Extended(len_timeseries=self.args.len_timeseries, prng=prng, x_0=x_0, c_lims=c_lims, sigma_lims=sigma_lims, **kwargs)
        iterator = generator.__iter__()
        if return_generator:
            return generator, iterator
        else:
            self.generator = generator
            self.iterator = iterator

def get_ckptname(logdir, id):
    fl = glob.glob(os.path.join(logdir, '*')) # full paths of all files
    fn = [it for it in fl if id in os.path.basename(it)] #keep only files that match id in the basenames
    flb = [os.path.basename(it) for it in fn] #keep only basenames of matching abs path filenames
    ind = np.argmax([int(it[len(id)+1:].split('.')[0]) for it in flb]) #get highest ckpt ind of matching filenames
    fname = fn[ind] #get abs filename of the matching index file
    fname = '.'.join(fname.split('.')[:-1]) # truncate extension of the file: e.g., /path/to/file/{id}-{ind}.{extension} -> /path/to/file/{id}-{ind}
    return fname

if __name__ == "__main__":
    # tf.app.run()
    main(0)
