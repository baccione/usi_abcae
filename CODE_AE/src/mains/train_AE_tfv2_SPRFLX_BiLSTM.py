#
################

import tensorflow as tf
assert tf.__version__[0] == '2' # this script is intended for tf v2 (2.2.0 to be precise.)
import os
import datetime
import numpy as np
import sys
import importlib
import logging
import pickle
root = logging.getLogger()
root.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)

import src.wraps.generators
import src.wraps.utils

#from src.modls.archs import BiLSTM as Architecture
from src.tools.utils import Manage_Hyper_Parameters

from src.tools.utils import ExpSetup

from src.modls.losses import loss_reconstruction_fn_a as loss_reconstruction_fn
from src.modls.losses import loss_regress_params_fn_a as loss_regress_params_fn

from src.tools.utils import export_summary_scalars
from src.tools.utils import export_summary_histograms

from src.tools.utils import get_rank

from exps.superflex.parse_args import main_args

##################################################################################################
# wrap a training step for performance gains.
@tf.function # comment to print
def train_step(model, x, params, noise, optimizer, args=None, summary_writer=None):
    '''Used in: train_AE_tfv2_NLAR1_BiLSTM, train_AE_tfv2_solarDynamo_BiLSTM, train_AE_tfv2_NLAR1_BS_BiLSTM,
    Single training step. Likely critical for performance. So not sanity checks.'''
    with tf.GradientTape(persistent=False) as tape: #persistent=True if .gradient() is called multiple times (e.g., multiple losses)
        z_latent = model.encoder(x, training=True) # points to return tf.keras.Model(bla)
        x_reconst = model.decoder((z_latent, noise), training=True)
        dict_reconstruction_mse = loss_reconstruction_fn(x, x_reconst, return_each_dim=True, args=args)
        loss_reconstruction = tf.math.reduce_sum(list(dict_reconstruction_mse.values()))
        dict_regress_params_mse = loss_regress_params_fn(params, z_latent, return_each_dim=True)
        loss_regress_params = tf.math.reduce_sum(list(dict_regress_params_mse.values()))
        loss = loss_reconstruction + loss_regress_params
        trainable_variables = model.encoder.trainable_variables + \
                              model.decoder.trainable_variables # likely interesting for monitoring the learning process
        gradients = tape.gradient(loss, trainable_variables) # likely interesting for monitoring the learning process
        if args.global_gradient_clipnorm is not None:
            gradients, global_norm = tf.clip_by_global_norm(gradients, clip_norm=args.global_gradient_clipnorm, name='clip_gradients_by_global_norm')
        if tf.equal(optimizer.iterations % args.freq_log, 0):
            l_avg_grads= [tf.math.reduce_mean(g) for g in gradients]
            l_absmax_grads= [tf.math.reduce_max(tf.math.abs(g)) for g in gradients]
            l_gradmean_names = ['gradient_mean_'+v.name for v in trainable_variables]
            l_gradabsmax_names = ['gradient_absmax_'+v.name for v in trainable_variables]
            # export avg and absolute max gradients of variables to tensorboard
            d_grads = {**dict(zip(l_gradmean_names, l_avg_grads)), **dict(zip(l_gradabsmax_names, l_absmax_grads))}
            export_summary_scalars(dict_name_and_val=d_grads, step=optimizer.iterations, writer=summary_writer)
        optimizer.apply_gradients(zip(gradients, trainable_variables))
    #tmp1 = loss_reconstruction
    #tmp2 = loss_regress_params
    #print(tmp2,type(tmp2)) # to see this printout you need to comment @tf.function so to restore eager execution
    #print('exit deb')
    #exit()
    return (loss_reconstruction, loss_regress_params), (z_latent, x_reconst), (dict_reconstruction_mse, dict_regress_params_mse)
##################################################################################################

def main(args):

    #argsnone = parse_args(None)
    #print(argsnone.logdir) # in pdb mode, this is resolved to string even without p args.logdir !
    #rargs = parse_args() # get the true command line arguments
    #argsnew = ExpSetup(args=vars(rargs)) # namespace to dict, finally this works as argsNone
    #print(argsnew.logdir)

    ### set user model ###
    dt = 1
    sandboxing = 0
    margs = main_args(args)
    so_path = margs['so_path']     # '/home/mbacci/sfw/superflexpython/SuperSpuxFlex/libmars/superflex_spux'
    modelinfl = margs['modelinfl'] # '/home/mbacci/temp/AErun/superflex/datasets/modelInfoDaily.dat'
    addconstp = bool(margs['add_const_params'])
    whichpsto = [ bool(k) for k in margs['which_param_sto'] ]
    # horrible, but...at some point I need to be pragmatic
    spec = importlib.util.spec_from_file_location('piopio',margs['datasetpy']) # first arg seems quite irrelevant...
    foo = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(foo)
    warmup_time = foo.warmup_time
    usr_dfl = foo.datafile
    spec = importlib.util.spec_from_file_location('piapia',margs['priorpy'])
    foo = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(foo)
    prior = foo.prior
    ######################

    args = ExpSetup(args)
    #print(args.logdir)  # all equivalent: difference is that pdb resolves args.logdir only with p args.logdir !

    if not os.path.isdir(args.logdir):
        os.makedirs(args.logdir)

    # all that is loaded should come with full path and loaded with spec_from_file_location so to forget about PYTHONPATH
    arch_module = importlib.import_module("src.modls.archs")
    print('Architecture:',args.aem2load)
    Architecture = getattr(arch_module,args.aem2load)

    tf.keras.backend.clear_session()
    physical_devices = tf.config.list_physical_devices('GPU')
    for gpu_instance in physical_devices:
        tf.config.experimental.set_memory_growth(gpu_instance, True)

    # Define a generator function for observations, parameters, and noise vectors
    x_0 = 0.25
    c_lims = [4.2, 5.8]
    sigma_lims = [0.005, 0.025]
    # fn = lambda c, x_old, sigma, epsilon: c * x_old**2 * (1. - x_old) + sigma * epsilon
    # fn = lambda c, x_old, sigma, epsilon: c * x_old**2 * (np.exp(-x_old)) + sigma * epsilon
    user_module = importlib.import_module("src.modls."+args.umodlfl)
    user_class = getattr(user_module, args.umodlcl)
    print("User class: ",user_class)

    gen_train = user_class(dt=dt, sandboxing=sandboxing, so_path=so_path, twarmup=warmup_time,
                           modelinfl=modelinfl, prior=prior,
                           len_timeseries=args.len_timeseries, nnc=args.num_noise_channels,
                           indtfl=usr_dfl,addcp=addconstp,whichpsto=whichpsto)

    dataset_train = tf.data.Dataset.from_generator(lambda: gen_train, output_types=(tf.float32, tf.float32, tf.float32))
    dataset_train = dataset_train.repeat(count=1)
    dataset_train = dataset_train.batch(args.batch_size, drop_remainder=True)
    dataset_train = dataset_train.prefetch(buffer_size=10) # #number of minibatches to pre-fetch.

    # Define the architecture
    model_obj = Architecture(ndims_latent=args.ndims_latent, len_timeseries=args.len_timeseries-warmup_time, num_input_channels=args.num_input_channels, num_noise_channels=args.num_noise_channels)
    model_obj.encoder.summary() # model_obj.encoder points to return tf.keras.Model(bla)
    model_obj.decoder.summary()

    # Define optimizer
    lr_schedule = src.wraps.utils.LearningRateScheduleExponentialDecayWithLinearWarmup(steps_warmup=args.linear_warmup_steps,
                                                                                       initial_learning_rate=1.e-3,
                                                                                       decay_steps=int(6*1e3),
                                                                                       decay_rate=0.92,
                                                                                       staircase=True)
    optimizer = tf.keras.optimizers.Adam(learning_rate=lr_schedule)

    print(optimizer)

    #global_gradient_clipnorm = 1e5 # out of the blue
    if getattr(args,'global_gradient_clipnorm',None) is None:
        setattr(args,'global_gradient_clipnorm',1e5) # temporary legacy - should come from keyfile

    # Define ckpt managers to save model weights throughout optimization
    ckpt = tf.train.Checkpoint(optimizer=optimizer, encoder=model_obj.encoder, decoder=model_obj.decoder)
    save_manager = tf.train.CheckpointManager(checkpoint=ckpt, directory=args.logdir, max_to_keep=3, checkpoint_name='model_ckpt')
    save_manager_best = tf.train.CheckpointManager(checkpoint=ckpt, directory=args.logdir, max_to_keep=3, checkpoint_name='model_best_ckpt') # manager for early stopping.
    save = lambda save_manager, ckpt_number=None: save_manager.save(checkpoint_number=ckpt_number) # obj in args ?
    # Define a manager object for training hyper-parameters
    hp_manager = Manage_Hyper_Parameters(logdir=args.logdir)

    # Restore a previously interrupted training session (if exists)
    ckpt.restore(save_manager.latest_checkpoint)
    if save_manager.latest_checkpoint:
        print(f"Restored from {save_manager.latest_checkpoint}.")
        # Double check if experiment setup matches the one in the saved chkpt dir
        hp_manager.check_args_maybe_append(args)
    else:
        print("Initializing training from scratch. \nLogdir: %s" % args.logdir)
        # Dump hyper-parameters to ckpt dir for future reference.
        hp_manager.save_parameters(args)

    # Setup summaries for tensorboard
    summary_writer = tf.summary.create_file_writer(logdir=os.path.join(args.logdir, 'train'))

    # Define a few metrics to export to tensorboard
    avg_loss_total = tf.keras.metrics.Mean(name='loss_total', dtype=tf.float32) # variable will keep track of average of loss value since last freq_log
    avg_loss_recon = tf.keras.metrics.Mean(name='loss_reconstruction', dtype=tf.float32)
    avg_loss_reg_p = tf.keras.metrics.Mean(name='loss_regress_params', dtype=tf.float32)
    dict_avg_loss_recon_items = {} #will create mean metric on the fly since we don't know names in advance.
    dict_avg_loss_reg_p_items = {}
    dict_avg_rmse_recon_items = {}
    dict_avg_rmse_reg_p_items = {}

    # Define a metric to keep track of best reconstruction over a longer window
    avg_loss_long_term = tf.keras.metrics.Mean(name='loss_long_term', dtype=tf.float32)
    curr_best_loss = np.inf

    # Execute training loop
    for x, params, noise in dataset_train: # x is model realization (like observation, but it's training synthetic data)
        num_step = optimizer.iterations
        if num_step >= args.max_training_steps:
            logging.info('Training is completed.')
            break

        loss_tuple, z_and_x, dict_mse = train_step(model=model_obj, x=x, params=params, noise=noise, optimizer=optimizer,
                                                   args=args, summary_writer=summary_writer)

        #tmp1 = loss_tuple
        #tmp2 = z_and_x
        #print(tmp2,type(tmp2))
        #print('exit deb')
        #exit()

        z_latent, x_reconst = z_and_x
        loss_reconstruction, loss_regress_params = loss_tuple
        loss_total = loss_reconstruction + loss_regress_params
        dict_rec, dict_reg = dict_mse
        # Update loggers for tensorboard
        avg_loss_recon.update_state(loss_reconstruction)
        avg_loss_reg_p.update_state(loss_regress_params)
        avg_loss_total.update_state(loss_total) # aggregate values since last flush
        avg_loss_long_term.update_state(loss_total)
        # A little patchy way to keep track of each reconstructed signal and noise channel
        for k in dict_rec: #in this dict we have the loss val from reduce_sum(squared_difference(y_true, y_pred)...
            if k not in dict_avg_loss_recon_items:
                dict_avg_loss_recon_items[k] = tf.keras.metrics.Mean(name=k, dtype=tf.float32) #store avg_loss_recon
            dict_avg_loss_recon_items[k].update_state(dict_rec[k])
        for k in dict_reg:
            if k not in dict_avg_loss_reg_p_items:
                dict_avg_loss_reg_p_items[k] = tf.keras.metrics.Mean(name=k, dtype=tf.float32)
            dict_avg_loss_reg_p_items[k].update_state(dict_reg[k])

        #tmp1 = dict_avg_loss_recon_items
        #tmp2 = dict_avg_loss_reg_p_items
        #print(tf.print(tmp1),type(tmp1))
        #print('exit deb')
        #exit()

        # Record RMSE for reconstruction and regressed parameters regardless of the loss
        for i_ in range(x.shape[-1]): # dim(batch_size,len_timeseries-warmup_time,1), 1 should num_input_channels I believe
            k = 'RMSE_x_ch_%d'%(i_+1)
            if k not in dict_avg_rmse_recon_items:
                dict_avg_rmse_recon_items[k] = tf.keras.metrics.RootMeanSquaredError(name='rmse_reconstruction', dtype=tf.float32)
            dict_avg_rmse_recon_items[k].update_state(y_true=x[...,i_], y_pred=x_reconst[...,i_])
        for i_ in range(params.shape[-1]):
            k = 'RMSE_z_ch_%d'%(i_+1)
            if k not in dict_avg_rmse_reg_p_items:
                dict_avg_rmse_reg_p_items[k] = tf.keras.metrics.RootMeanSquaredError(name='rmse_regularization', dtype=tf.float32)
            dict_avg_rmse_reg_p_items[k].update_state(y_true=params[...,i_], y_pred=z_latent[...,i_])
        # Export status to tensorboard
        if tf.equal(optimizer.iterations % args.freq_log, 0):
            d_scalars = {'loss_total': avg_loss_total.result(), 'loss_reconstruction': avg_loss_recon.result(), 'loss_regress_params': avg_loss_reg_p.result()}
            mydic = {'z_latent': z_latent, 'x_recons': x_reconst, 'params': params, 'x': x, 'lr': loss_reconstruction, 'lp': loss_regress_params, 'lt':loss_total,
                    'alr':avg_loss_recon.result(), 'alp':avg_loss_reg_p.result(), 'alt':avg_loss_total.result(),'allt':avg_loss_long_term.result()}
            sffx = str(optimizer.iterations.numpy()).zfill(9)
            fl = open(args.logdir+"/rawdata_"+sffx+".dat", "wb")
            pickle.dump(mydic,fl)
            fl.close()
            for k in dict_avg_loss_recon_items:
                d_scalars[k] = dict_avg_loss_recon_items[k].result()
                dict_avg_loss_recon_items[k].reset_states()
            for k in dict_avg_loss_reg_p_items:
                d_scalars[k] = dict_avg_loss_reg_p_items[k].result()
                dict_avg_loss_reg_p_items[k].reset_states()
            for k in dict_avg_rmse_recon_items:
                d_scalars[k] = dict_avg_rmse_recon_items[k].result()
                dict_avg_rmse_recon_items[k].reset_states()
            for k in dict_avg_rmse_reg_p_items:
                d_scalars[k] = dict_avg_rmse_reg_p_items[k].result()
                dict_avg_rmse_reg_p_items[k].reset_states()
            d_scalars['lr_schedule'] = lr_schedule(step=num_step)
            export_summary_scalars(dict_name_and_val=d_scalars, step=optimizer.iterations, writer=summary_writer)
            # Print current loss status to terminal
            logging.info('Step %d: avg loss: %.3f, reconstruction loss: %.3f, parameter regression loss: %.3f.' % \
                    (num_step, d_scalars['loss_total'], d_scalars['loss_reconstruction'], d_scalars['loss_regress_params']))
            avg_loss_total.reset_states() #reset kept history of loss
            avg_loss_recon.reset_states()
            avg_loss_reg_p.reset_states()
            # Export trainable variables to tboard histogram
            # TODO: consider speeding this up.
            l_enc = list(zip(*[['enc_'+v.name, v.value()] for v in model_obj.encoder.trainable_variables]))
            l_dec = list(zip(*[['dec_'+v.name, v.value()] for v in model_obj.decoder.trainable_variables]))
            d_histograms = {**dict(zip(l_enc[0], l_enc[1])), **dict(zip(l_dec[0], l_dec[1]))}
            export_summary_histograms(dict_name_and_val=d_histograms, step=optimizer.iterations, writer=summary_writer)
            # Save the current state of the model weights on disk.
            save(save_manager, ckpt_number=optimizer.iterations)
        # Check loss for long term best reconstruction
        if tf.equal(optimizer.iterations % (10 * args.freq_log), 0):
            if avg_loss_long_term.result() < curr_best_loss:
                old_best_loss = curr_best_loss
                curr_best_loss = avg_loss_long_term.result()
                avg_loss_long_term.reset_states()
                logging.info('New long term best avg. loss found: %.3f (prev: %.3f). Saving.' % (curr_best_loss,old_best_loss))
                save(save_manager_best, ckpt_number=optimizer.iterations)

    ##################################################################################################
    # Save model once again on the exit
    save(save_manager, ckpt_number=optimizer.iterations)

if __name__ == "__main__":
    # tf.app.run()
    main(0) # if you change this zero, you need to know what you are doing or you'll get what you deserves
