#Author: Firat Ozdemir, October 2019, firat.ozdemir@datascience.ch
import math
import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
import numpy as np
from collections import namedtuple
import os
import glob
import json
import shutil
import tensorflow as tf
import sys
import datetime

def babcock_leighton_fn(p_n, b_1=0.6, w_1=0.2, b_2=1.0, w_2=0.8):
    '''
    Babcock-Leighton function measuring the efficiency of poloidal field production from the decay of active regions as
    a function of the deep-seated toroidal magnetic component.
    ----
    Function implements f(p_{n}), Eqn 2 in
    FLUCTUATIONS IN BABCOCK-LEIGHTON DYNAMOS. II. REVISITING THE GNEVYSHEV-OHL RULE,
    https://iopscience.iop.org/article/10.1086/511177/pdf,
    Parameter choice b_1=0.6, w_1=0.2, b_2=1.0, w_2=0.8 is based on the above manuscript.'''
    f_p_n = 0.5 * (1. + math.erf((p_n - b_1) / w_1)) * (1. - math.erf((p_n - b_2) / w_2))
    return f_p_n


def babcock_leighton(p_old, alpha, epsilon):
    '''
    Amplitude p_{n+1} of the upcoming cycle.
    -----
    Function implements Eqn 3 in
    FLUCTUATIONS IN BABCOCK-LEIGHTON DYNAMOS. II. REVISITING THE GNEVYSHEV-OHL RULE,
    https://iopscience.iop.org/article/10.1086/511177/pdf'''
    if epsilon < 0.0:
        raise AssertionError('Entered epsilon: %.3f is < 0!' % (epsilon))
    elif epsilon > 1.0:
        logging.warning('Entered epsilon: %.3f is not << 1!' % (epsilon))
    #Note: According to manuscript, epsilon <= 0.39 is the safe zone.
    p_new = alpha * babcock_leighton_fn(p_old) * p_old + epsilon
    return p_new

def uniform_sampler(val_min, val_max, prng=None, num_samples=1):
    '''Function uniformly samples a value btw [val_min, val_max]
        It is highly recommended to provide a pseudorandom number generator for reproducible results.'''
    if prng is None:
        logging.warning('prng not shared with uniform_sampler! Experiments will NOT be reproducible!')
        prng = np.random.RandomState(seed=None)
    return prng.uniform(low=val_min, high=val_max, size=num_samples)

def alpha_sampler(alpha_min, alpha_max, prng=None, num_samples=1):
    '''Function returns sample alpha value(s).'''
    return uniform_sampler(val_min=alpha_min, val_max=alpha_max, prng=prng, num_samples=num_samples)

def epsilon_sampler(epsilon_max, prng=None, num_samples=1):
    '''Function returns sample epsilon value(s).'''
    return uniform_sampler(val_min=0., val_max=epsilon_max, prng=prng, num_samples=num_samples)

def sample_pn_timeseries_tuple(p0, alpha_min, alpha_max, epsilon_max, prng=None, len_timeseries=10000):
    '''Function samples a time series of p_n values, then returns computed p_n, sampled alphas and epsilons'''
    #TODO: move to a class obj
    Pn_Tuple = namedtuple('PN_tuple', ['p', 'a', 'e', 'f'])
    if prng is None:
        logging.warning('sample_pn_timeseries should receive a prng for reproducible results.')
        prng = np.random.RandomState(seed=1923)
    pn = p0
    l_p, l_a, l_e = [], [], []
    l_f = []
    for i in range(len_timeseries):
        a = alpha_sampler(alpha_min=alpha_min, alpha_max=alpha_max, prng=prng)
        e = epsilon_sampler(epsilon_max=epsilon_max, prng=prng)
        f = babcock_leighton_fn(p_n = pn)
        pn = babcock_leighton(p_old=pn, alpha=a, epsilon=e)
        l_p.append(pn)
        l_a.append(a)
        l_e.append(e)
        l_f.append(f)
    pn_tuple = Pn_Tuple(p=l_p, a=l_a, e=l_e, f=l_f)
    return pn_tuple

def sample_pn_timeseries(p0, alpha_min, alpha_max, epsilon_max, prng=None, len_timeseries=10000):
    '''Function samples a time series of p_n values, then returns computed p_n, sampled alphas and epsilons'''
    #TODO: move to a class obj
    # Pn_Tuple = namedtuple('PN_tuple', ['p', 'a', 'e', 'f'])
    # logging.info('Bug found in function (02.12.2019): p_n values are shifted one step to future. While results should not change much, please use sample_pn_timeseries_v2 instead.')
    if prng is None:
        logging.warning('sample_pn_timeseries should receive a prng for reproducible results.')
        prng = np.random.RandomState(seed=1923)
    pn = p0
    l_p, l_a, l_e = [], [], []
    l_f = []
    for i in range(len_timeseries):
        a = alpha_sampler(alpha_min=alpha_min, alpha_max=alpha_max, prng=prng)
        e = epsilon_sampler(epsilon_max=epsilon_max, prng=prng)
        f = babcock_leighton_fn(p_n = pn)
        pn = babcock_leighton(p_old=pn, alpha=a, epsilon=e)
        l_p.append(pn)
        l_a.append(a)
        l_e.append(e)
        l_f.append(f)
    p = np.reshape(l_p, (len_timeseries,))
    a = np.reshape(l_a, (len_timeseries,))
    e = np.reshape(l_e, (len_timeseries,))
    f = np.reshape(l_f, (len_timeseries,))
    d = {'p':p, 'a':a, 'e':e, 'f':f}
    # pn_tuple = Pn_Tuple(p=l_p, a=l_a, e=l_e, f=l_f)
    return d

def sample_pn_timeseries_v2(p0, alpha_min, alpha_max, epsilon_max, prng=None, len_timeseries=1000):
    '''Function samples a time series of p_n values, then returns computed p_n, sampled alphas and epsilons'''
    #TODO: move to a class obj
    # Pn_Tuple = namedtuple('PN_tuple', ['p', 'a', 'e', 'f'])
    if prng is None:
        logging.warning('sample_pn_timeseries should receive a prng for reproducible results.')
        prng = np.random.RandomState()
    pn = p0
    l_p, l_a, l_e = [], [], []
    l_f = []
    for i in range(len_timeseries):
        a = alpha_sampler(alpha_min=alpha_min, alpha_max=alpha_max, prng=prng)
        e = epsilon_sampler(epsilon_max=epsilon_max, prng=prng)
        f = babcock_leighton_fn(p_n=pn)
        l_p.append(pn)
        pn = babcock_leighton(p_old=pn, alpha=a, epsilon=e)
        l_a.append(a)
        l_e.append(e)
        l_f.append(f)
    p = np.reshape(l_p, (len_timeseries,))
    a = np.reshape(l_a, (len_timeseries,))
    e = np.reshape(l_e, (len_timeseries,))
    f = np.reshape(l_f, (len_timeseries,))
    d = {'p':p, 'a':a, 'e':e, 'f':f}
    # pn_tuple = Pn_Tuple(p=l_p, a=l_a, e=l_e, f=l_f)
    return d

def check_tf_ckpt(path, pattern):
    '''Function checks path for TF model checkpoints that match the pattern and return the latest, if any.'''
    l_candidates = glob.glob(os.path.join(path, '*%s*.index' % (pattern)))
    if len(l_candidates) == 0:
        return None, None
    else:
        curr_latest = 0
        full_path_name = ''
        ckpt_num = None
        for full_path in l_candidates:
            fname = os.path.basename(full_path)
            _, ckpt, _ = fname.split('.')
            ckpt_num = int(ckpt.split('-')[-1])
            if ckpt_num > curr_latest:
                full_path_name = full_path[:-1*len('.index')]
                curr_latest = ckpt_num
        return full_path_name, curr_latest

def get_latest_ckpt(path, id_model='model', return_ckpt_num=False):
    assert (os.path.isdir(path)) #provided path should be a directory, not a file.
    fl = glob.glob(os.path.join(path, id_model+'.ckpt*meta*'))
    # print('fl is:\n%s' % (fl))
    if len(fl) == 0:
        raise AssertionError('Directory does not contain valid checkpoint files.')
    highest_ckpt = 0
    for fname in fl:
        ckpt_num = int(os.path.basename(fname).split('.')[1].split('-')[1])
        if ckpt_num > highest_ckpt:
            highest_ckpt = ckpt_num
    fname_tmp = glob.glob(os.path.join(path, '*-'+str(highest_ckpt)+'.meta'))[0]
    ckpt_name = fname_tmp[:-1*(len(fname_tmp.split('.')[-1])+1)]
    if return_ckpt_num:
        return ckpt_name, highest_ckpt
    else:
        return ckpt_name

def get_tf_optmzr(pkg=None):
    """Depending on the pkg package you have, the same thing is called in different ways"""

    assert pkg is not None, ":: Fatal: tensorflow not passed?"

    if hasattr(pkg.train,'AdamOptimizer'):
        res = pkg.train.AdamOptimizer

    elif hasattr(pkg.optimizers,'Adam'):
        res = pkg.optimizers.Adam
    else:
        assert False, ":: Fatal: Adam optimizer not found in provided tensorflow version"

    return res

def get_mk_initlz_iter (pkg=None):
    """Depending on the pkg package you have, the same thing is called different ways"""

    assert pkg is not None, ":: Fatal: dataset_train not passed?"

    if hasattr(pkg,'make_initializable_iterator'):
        res = pkg.make_initializable_iterator()
    elif hasattr(pkg,'data.make_initializable_iterator'):
        res = pkg.data.make_initializable_iterator()
    else:
        assert False, ":: Fatal: make_initializable_iterator not found"

#
### MB ###
class Args_():
    "convert dict to a class with attributes (to match code similar with training)"
    def __init__(self, d):
        for k in d.keys():
            setattr(self, k, d[k])

class Manage_Hyper_Parameters:
    '''A simple class to manage everything regarding hyper parameter setup of an experiment.
    Warn for modifications in the experiment setup if training was interrupted.'''

    def __init__(self, logdir):
        self.param_config_fn = os.path.join(logdir, 'hyper_parameters.json')
        if not os.path.isfile(self.param_config_fn):
            self.args = None
        else:
            with open(self.param_config_fn) as fh:
                args = json.load(fh)
            self.args = Args_(args) # convert dict to a class with attributes (to match code similar with training)
        self.logdir = logdir
    def check_args_maybe_append(self, args):
        if self.args is None:
            return None
        save_args = False
        # for k in args:
        tmplst = []
        for k in dir(args):
            if k.startswith('__') or k == 'max_training_steps': # you can change max_training_steps
                continue
            if not hasattr(self.args, k):
                print('WARNING: key "%s" was missing in the new hyper-parameter configuration; will renew file. This sets fatal_hyper_param_mism to False.' % k)
                save_args = True
                self.args.fatal_hyper_param_mism = False
            else:
                if getattr(self.args, k) != getattr(args, k):
                    tmplst = tmplst + [k]
        if len(tmplst) > 0:
            if hasattr(self.args, 'fatal_hyper_param_mism'):
                if self.args.fatal_hyper_param_mism:
                    assert False, ":: Fatal: Mismatch in hyper-parameter settings for: {}".format(tmplst) # could be acceptable is some case
            else:
                print(":: Warning: mismatch on these hyper-parameters: ",tmplst)
        else:
            print("No mismatch on hyper-parameters found.")
        if save_args:
            self.save_parameters(args)
    def save_parameters(self, args):
        d_args = {}
        for attr in dir(args):
            if not attr.startswith('__'):  # don't get methods
                d_args[attr] = getattr(args, attr)
        d = d_args
        keys = d.keys()
        k_drop = []
        for k in keys:
            try:
                json.dumps(d[k]) # why should this fail?
            except:
                print('dropping key: %s' % k)
                k_drop.append(k)
        for k in k_drop:
            d.pop(k, None)
        with open(self.param_config_fn, 'w') as fh:
            json.dump(d, fh, sort_keys=True, indent=4)
        # Copy source file (remove this line if source filename changes.)
        current_main = os.path.abspath(sys.modules['__main__'].__file__)
        out_file = os.path.join(self.logdir,os.path.basename(current_main))
        if os.path.isfile(out_file): # if outfile already exists, save another copy.
            date = datetime.datetime.now()
            datestr = date.strftime('%Y%m%d%H%M%S')
            out_file = os.path.join(self.logdir,datestr+'.'+os.path.basename(current_main))
        shutil.copyfile(current_main, out_file) # difficult to get why copying - is running_script going to be edited at runtime?

def get_path2__file__(fl=None):
    """Intended for __file__ of calling routine. Make a few checks and gives back the best guess
    (finally in python3.9 they seem to have got that abs path is best, better late than never)"""

    assert fl is not None, ":: Fatal: fl in get_path2__file__ not passed?"

    res = os.path.join(os.getcwd(),fl) #getcwd usually means where the script runs from, not where utils.py is

    if not os.path.isfile(res): # may not work if we run with abs path to script
        res = fl

    assert os.path.isfile(res),":: Fatal: get__filepath__ failed to find the running script."

    return res

def get_rank(comm=None,doprints=False):
    """get mpi rank in communicator comm, or get 0"""
    rnk = 0
    if comm is None:
        try:
            from mpi4py import MPI # not sure if this will ever work
            comm = MPI.COMM_WORLD
            rnk = comm.Get_rank()
            if doprints:
                print('I am rank:',rnk,'(mpi4py worked (this could still be serial))')
        except:
            if doprints:
                print(':: Warning: mpi failed, assigning rank 0')
            rnk = 0
    else:
        rnk = comm.Get_rank()
        if doprints:
            print('I am rank: ',rnk)

    return rnk

class ExpSetup():
    '''Use this class to define the hyper parameters to be used for the AE.'''

    #def __init__(self,**kwargs):
    def __init__(self, args=None):
        rnk = get_rank()
        ### TO BE REMOVED AS SOON AS POSSIBLE BY SUBBING WITH DEFAULT VALUES FOR KEYS - 13Mar21 ###
        if args is None: # these defaults have been moved to parse_args
            print(":: Warning: args is None, deprecated, and soon to be removed!")
            #self.logdir = '/home/firat/data/saved_models/solar-forecasting/NLAR1.2_encConv_decBiLSTM_ld3_ChiSquare'
            #self.logdir = '/cfs/earth/scratch/ulzg/spux/examples/NLAR1/solar-forecasting-logs/Temp/NLAR1_encConv_decBiLSTM_ld3_ChiSquare_new'
            self.logdir = '/home/mbacci/sfw/solar-forecasting/tmplogdir'
            self.ndims_latent = 3 # #summary stats
            self.num_noise_channels = 1 # #different noise vectors
            self.num_model_parameters = 2 # NLAR1 model has c and sigma as model parameters.
            self.len_timeseries = 200
            self.batch_size = 300
            self.max_training_steps = int(200) #int(3*1e6) # maximum number of training steps unless optimization gets killed.
            self.freq_log = 100 # frequency to update logged values in tensorboard.
            self.linear_warmup_steps = int(5*1e3) # #steps with linear warmup from very low lr to init_lr
            # self.lambda_x = 0.
            self.num_different_mb = 60
            self.reconstruction_loss_start_step = -1 # int(4*1e4) int(1e3) # need to give regression of estimated model parameters some time to produce "realistic" observation generations.
            self.x_recon_precision = None
            self.loss_scale_optimizer = False ## Enabling this requires TF v:2.4+ (Hence CUDA v11, CuDNN v8)
            # self.decoder_str = 'LSTM' # LSTM with 2 layers is the only option
            # self.encoder_str = 'convfc'
            self.step_num_activate_sharp_embedding = int(3*1e5)
            self.num_input_channels = 1 # forgotten
            self.aem2load = 'BiLSTM' #basename for the AE to be imported
            #
            # inference specifics (for now)
            self.load_file_basename = 'model_ckpt' #'model_best_ckpt' # basename of what to load from logdir
            self.tf_cpp_min_log_level = '1' # or any {'0', '1', '2'}
            self.numexpr_max_threads = '8'
            self.varstats_file = './varstats/NLAR1_ssvar_ae3_c53_s0015_p0025_p200_mean_v2.dat' #valid only in SPUX folder
            #self.p_model_weights = '/home/mbacci/sfw/solar-forecasting/model_weights/marco/AE_tfv2_NLAR1_BiLSTM' # use logdir
            self.samplrfl = 'sam' # could have whole path? Prolly, but then to import in Python given a file path, a mess...
            self.samplrcl = 'Sampler_AE_tfv2_NLAR1_BiLSTM'
            self.mb_of_same_realizations = 1
            #
            # oh man
            self.umodlfl = 'NLARs'
            self.umodlcl = 'NLAR_SquaredExp'
            # blowfly thingys
            self.burin = 0
            self.regress_log_params = True
            self.log_observations = True
            self.eps = 1e-20
            self.loss = 'mse'
        #########################################################
        elif isinstance(args,dict):
            if rnk==0:
                print('args is dict (perhaps derived from keyfile)')
            for key, value in args.items():
                setattr(self, key, value)
        elif (args==0): # forcing reading from command line by main(0) callA
            if rnk==0:
                print('getting args from command line args')
            parse_args(caller=self,rnk=rnk)
        else: # do nothing in practise, rely on default or on futher reading of checkpoint file
            #print(":: Warning: default arguments will be used. This is likely appropriate only for inference or for continuation, where a check_point file is read.")
            #parse_args(caller=self,id=-1)
            assert False, ":: Fatal: wrong arg value/type {}.".format(args)

def parse_args(caller=None,rnk=0):
    """Rudimentary argument parser before I am given the freedom to develop a keyfile, that is,
    to dev code cdc"""

    import argparse

    #assert caller is None, ":: Fatal: calling with caller not None is disabled as pdb tells me it may not do what I want."

    parser = argparse.ArgumentParser()
    parser.add_argument("--logdir",type=str,default='./tmplogdir',
                        help="directory to save model and export tensorboard")
    parser.add_argument("--ndims_latent",type=int,default=3,
                        help="Number of latent_dims. Must be >=3 (why?!? - ask Carlo). Default to 3.")
    parser.add_argument("--len_timeseries",type=int,default=200,
                        help="length of timeseries.")
    parser.add_argument("--num_noise_channels",type=int,default=1,
                        help="different noise vectors (we'll get what it really is later).")
    parser.add_argument("--batch_size",type=int,default=300,
                        help="batch size (we'll get what it really is later).")
    parser.add_argument("--max_training_steps",type=int,default=int(3*1e6), #doesn't get default to type
                        help="max number of training steps.")
    parser.add_argument("--freq_log",type=int,default=100,
                        help="frequency to update logged values in tensorboard.")
    parser.add_argument("--linear_warmup_steps",type=int,default=int(5*1e3),
                        help="steps with linear warmup from very low lr to init_lr.")
    parser.add_argument("--gpu",type=int,default=None,
                        help="which GPU to use: Default runs on CPUs. For CSCS? We'll see.")
    parser.add_argument("--num_model_parameters",type=int,default=2,
                        help="number of model parameters. Default to 2.")
    parser.add_argument("--reconstruction_loss_start_step",type=int,default=-1,
                        help="Needed to give regression of estimated model paramters some time to produce realistic observations. Default to -1 (which is what behavior?).")
    parser.add_argument("--num_different_mb",type=int,default=60,
                        help="Number of parameters draws for INCA. Default to 60.")
    parser.add_argument("--x_recon_precision",type=int,default=None,
                        help="I don't know yet what is this. Default to None.")
    parser.add_argument("--loss_scale_optimizer",type=int,default=False,
                        help="I don't know yet what is this. Default to False.")
    parser.add_argument("--step_num_activate_sharp_embedding",type=int,default=int(3*1e5),
                        help="I don't know yet what is this. Default to 3*1e5.")
    parser.add_argument("--num_input_channels",type=int,default=1,
                        help="Number of input channels. Default to 1.")
    parser.add_argument("--load_file_basename",type=str,default='model_ckpt',
                        help="base name of file to be loaded for AE weights. Default to model_ckpt") # model_ckpt ?
    parser.add_argument("--tf_cpp_min_log_level",type=str,default='1',
                        help="TF_CPP_MIN_LOG_LEVEL os env variable. Default to '1'.")
    parser.add_argument("--numexpr_max_threads",type=str,default='8',
                        help="NUMEXPR_MAX_THREADS env variable. Default to '8'.")
    parser.add_argument("--varstats_file",type=str,
                        default='./varstats/NLAR1_ssvar_ae3_c53_s0015_p0025_p200_mean_v2.dat',
                        help="File with weights for summary statistics. Default to 'NLAR1_ssvar_ae3_c53_s0015_p0025_p200_mean_v2.dat'")
    parser.add_argument("--aem2load",type=str,default='BiLSTM',
                        help="AE main to be loaded. Default to 'BiLSTM'.")
    ### mfd: we use logdir as this is what was thought even before ###
    #parser.add_argument("-pmw","--p_model_weights",type=str,
    #                    default='./solar-forecasting-logs/Simone/NLAR1_encConv_decBiLSTM_ld3_ChiSquare',
    #                    help="Folder with AE weights. Default to './solar-forecasting-logs/Simone/NLAR1_encConv_decBiLSTM_ld3_ChiSquare'.")
    ##################################################################
    parser.add_argument("--samplrfl",type=str,default='sam',
                        help="The sampler file. Default to sam.")
    parser.add_argument("--samplrcl",type=str,default='Sampler_AE_tfv2_NLAR1_BiLSTM',
                        help="The sampler class. Default to Sampler_AE_tfv2_NLAR1_BiLSTM.")
    parser.add_argument("--umodlfl",type=str,default='NLARs',
                        help="The user model file. Default to NLARs.")
    parser.add_argument("--umodlcl",type=str,default='NLAR_SquaredExp',
                        help="The user model class. Default to NLAR_SquaredExp.")
    parser.add_argument("--mb_of_same_realizations",type=int,default=10,
                        help="ENCA: batch_size. INCA: number of replicas (particles). Default to 10.")
    parser.add_argument("--check_len_timeseries",type=int,default=1,
                        help="Deprecated. Likely to be removed soon. Whether or not to ignore len_timeseries in inference (due to warmup). Default to 1 (True).")
    parser.add_argument("--fatal_hyper_param_mism",type=int,default=0,
                        help="Whether or not to fatal on differences in hyper parameters between runs. Default to 0 (False).")
    parser.add_argument("--burnin",type=int,default=0,
                        help="Burnin param for custom user needs. Default to 0.")
    parser.add_argument("--is_uniform_prior",type=int,default=0,
                        help="Are model param priors uniform dists? Default to 0 (False).")
    parser.add_argument("--indices_of_fixed_params",nargs='+', default=[], type=int,
                        help="Indeces of fixed params (a blowfly thing I believe). Default to [].")
    parser.add_argument("--regress_log_params", default=1, type=int,
                        help="A blowfly thing I believe. Default to 1 (True).")
    parser.add_argument("--log_observations", default=1, type=int,
                        help="A blowfly thing I believe. Default to 1 (True).")
    parser.add_argument("--eps", default=1e-20, type=float,
                        help="A blowfly thing of which I'd like to know nothing. Default to 1e-20.")
    parser.add_argument("--loss", default='mse', type=str,
                        help="A blowfly thing of which I'd like to know nothing. Default to chisquare.")

    args, unknown = parser.parse_known_args()
    #print(args)

    if caller is not None:
        for arg in vars(args):
            setattr(caller,str(arg),getattr(args,arg))
            if rnk==0:
                print ('initializing ',arg,' to ',getattr(args, arg))

    return args

def pdbtr():
    """To set a pdb-debugger stop"""
    import pdb; pdb.set_trace()

@tf.function
def export_summary_scalars(dict_name_and_val, step, writer):
    '''dict_name_and_val = {'name1': value1, 'name2': value2, ...}'''
    if not isinstance(dict_name_and_val, dict):
        raise AssertionError('dict_name_and_val must be a dictionary.')
    with writer.as_default():
        for k in dict_name_and_val.keys():
            tf.summary.scalar(k, dict_name_and_val[k], step=step)

@tf.function
def export_summary_histograms(dict_name_and_val, step, writer):
    if not isinstance(dict_name_and_val, dict):
        raise AssertionError('dict_name_and_val must be a dictionary.')
    with writer.as_default():
        for k in dict_name_and_val.keys():
            tf.summary.histogram(k, dict_name_and_val[k], step=step)

def get_ckptname(logdir, id):
    """get the checkpoint file to load I guess"""
    fl = glob.glob(os.path.join(logdir, '*')) # full paths of all files
    fn = [it for it in fl if id in os.path.basename(it)] #keep only files that match id in the basenames
    flb = [os.path.basename(it) for it in fn] #keep only basenames of matching abs path filenames
    ind = np.argmax([int(it[len(id)+1:].split('.')[0]) for it in flb]) #get highest ckpt ind of matching filenames
    fname = fn[ind] #get abs filename of the matching index file
    fname = '.'.join(fname.split('.')[:-1]) # truncate extension of the file: e.g., /path/to/file/{id}-{ind}.{extension} -> /path/to/file/{id}-{ind}
    return fname
