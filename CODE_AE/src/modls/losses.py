#
import numpy
import tensorflow as tf
import tensorflow_probability as tfp

class ChiSquareStatistic_a(tf.keras.losses.Loss):
    """Used in: train_AE_tfv2_NLAR1_BiLSTM, train_AE_tfv2_NLAR1, train_AE_tfv2_NLAR1_BS_BiLSTM,
       train_Regressor_tfv2_NLAR1_FC, train_Regressor1.2_tfv2_NLAR1_FC,
       train_Regressor2_tfv2_NLAR1_FC_noX, train_Regressor2_tfv2_NLAR1_FC,
       train_Regressor2_tfv2_expNLAR1_FC train_Regressor3_tfv2_NLAR1_FC, train_AE_tfv2_NLAR1_SingleModelMB
       (it is also used for train_AE_tfv2_NLAR1_BS but only in Simone's branch),
       train_AE_tfv2_solarDynamo_BiLSTM_s4 (used for solar dynamo prodiction with BiLSTM)"""
    @tf.function
    def call(self, y_true, y_pred):
        '''Implements \sum{ [ (y_true[ibatch,itime]-y_pred[ibatch,itime])**2 / y_true[ibatch,itime]**2 ] }'''
        #y_true = tf.convert_to_tensor(y_true)
        #y_pred = tf.convert_to_tensor(y_pred)
        #numpy.savetxt('ytrue.csv',y_true.numpy(),delimiter=",")
        #numpy.savetxt('ypred.csv',y_pred.numpy(),delimiter=",")
        sd = tf.math.squared_difference(y_true, y_pred) # it still has batch sz rows and time length cols
        #numpy.savetxt('sd.csv',sd.numpy(),delimiter=",") # each entry is: (y_true[ibatch,itime,1] - y_pred[ibatch,itime,1])^2
        # return tf.math.reduce_sum(sd / y_true, axis= -1)
        # return tf.math.reduce_sum(sd / tf.math.maximum(tf.math.abs(y_true), 1e-5), axis= -1)
        ###################################################################################################
        # by using this return with train_AE_tfv2_NLAR1_BiLSTM.py:
        # return tf.math.reduce_sum(sd / tf.math.maximum(tf.math.pow(y_true, 2), 1e-6) )
        # we do not even get the mismatch on the batch_size anymore by using this one - 16Mar21 & 10 Jun 21,
        # namely the tmpcheck pipeline in loss_reconstruction_fn of src/mains/train_AE_tfv2_NLAR1_BiLSTM.py
        # gives the same results as with current (__call__) one. However, this is irrelevant as long as the
        # code in loss_reconstruction_fn is such that __call__ of tf.keras.losses.Loss is called
        ###################################################################################################
        den = tf.math.maximum(tf.math.pow(y_true, 2), 1e-6)
        result = tf.math.reduce_sum(sd / tf.math.maximum(tf.math.pow(y_true, 2), 1e-6), axis=-1) # it sums over times for data after having divided each entry for the respective y_true, entry by entry
        #numpy.savetxt('den.csv',den.numpy(),delimiter=",") #
        #numpy.savetxt('result.csv',numpy.atleast_1d(result.numpy()),delimiter=",")
        return result # for data, it has still batch size dimension here, however, keras calls are such that the mean is taken before returning. For a parameter, it is a scalar.

class ChiSquareStatistic_a_renorm(tf.keras.losses.Loss):
    """Used in new superflex attempts train_AE_tfv2_SPRFLX_BiLSTM_renorm.py
    Sep 2021: Tests show that the AE does not start learning. This is perhaps
    because y_pred is very bad initially. Hence, I can try to switch to this
    one to refine training."""
    @tf.function
    def call(self, y_true, y_pred):

        sd = tf.math.squared_difference(y_true, y_pred)
        result = tf.math.reduce_sum(sd / tf.math.maximum(tf.math.pow(y_pred, 2), 1e-6), axis=-1)

        return result

class ChiSquareStatistic_a_renorm_mean(tf.keras.losses.Loss):
    """Used in new superflex attempts train_AE_tfv2_SPRFLX_BiLSTM_renorm_mean.py
    Sep 2021: Since AE does not start learning with renorm, I try to take the average."""
    @tf.function
    def call(self, y_true, y_pred):

        sd = tf.math.squared_difference(y_true, y_pred)
        ave = (y_true + y_pred) / 2 # entry by entry, Sep 29 2021

        result = tf.math.reduce_sum(sd / tf.math.maximum( tf.math.pow(ave, 2), 1e-6 ), axis=-1)

        return result

class ChiSquareStatistic_a_nonorm(tf.keras.losses.Loss):
    """Do not normalize"""
    @tf.function
    def call(self, y_true, y_pred):

        sd = tf.math.squared_difference(y_true, y_pred)
        result = tf.math.reduce_sum(sd, axis=-1)

        return result

class ChiSquareStatistic_b0(tf.keras.losses.Loss):
    """Used in: train_Regressor4_tfv2_NLAR1_FC"""
    @tf.function
    #what I had before and was not working, 22 Jun 21 ###
    #def call(self, y_true, y_pred):
    #     '''Implements \sum( (y_true - y_pred) ** 2 / y_true **2.
    #     Expects rank1 array (num_different_mb) y_pred for theta, and rank 2 array (num_different_mb, same_mb) y_pred for latent_z'''
    #     if tf.not_equal(tf.rank(y_true), tf.rank(y_pred)):
    #         #y_true = tf.expand_dims(tf.tensor(y_true), axis=1) # fails, changes nothing afaict
    #         pass
    #     sd = tf.math.squared_difference(y_true, y_pred)
    #     if tf.rank(sd) > 0:
    #         return tf.math.reduce_sum(sd / tf.math.maximum(tf.math.pow(y_true, 2), 1e-6), axis= -1)
    #    else:
    #         return sd / tf.math.maximum(tf.math.pow(y_true, 2), 1e-6)
    #
    # how I think I fixed it, 22 Jun 21
    def call(self, y_true, y_pred):
        '''Implements \sum( (y_true - y_pred) ** 2 / y_true **2.
        Expects rank1 array (num_different_mb) y_pred for theta, and rank 2 array (num_different_mb, same_mb) y_pred for latent_z'''

        rep = tf.math.maximum(1, tf.size(y_pred))
        y_true = tf.repeat(y_true,rep)
        sd = tf.cast(tf.math.squared_difference(y_true, y_pred),tf.float32)

        return tf.math.reduce_sum(sd / tf.math.maximum(tf.math.pow(y_true, 2), 1e-6), axis= -1)

class ChiSquareStatistic_b1(tf.keras.losses.Loss): # multibatch thingy apparently
    """Used in: train_Regressor4_tfv2_NLAR1_FC_multiBatch, train_Regressor4_tfv2_SolarDynamo_FC_multiBatch"""
    @tf.function
    def call(self, y_true, y_pred):
        '''Implements \sum( (y_true - y_pred) ** 2 / y_true **2.
        Expects rank1 array (num_different_mb) y_pred for theta, and rank 2 array (num_different_mb, same_mb) y_pred for latent_z'''
        if tf.not_equal(tf.rank(y_true), tf.rank(y_pred)):
            y_true = tf.expand_dims(y_true, axis=1)
            # for i, v in enumerate(tf.not_equal(y_true.shape, y_pred.shape)):
            #     if v:
            #         y_true = tf.expand_dims(y_true, axis=i)
        sd = tf.math.squared_difference(y_true, y_pred)
        if tf.rank(sd) > 1:
            return tf.math.reduce_sum(sd / tf.math.maximum(tf.math.pow(y_true, 2), 1e-6), axis= -1)
        else:
            return sd / tf.math.maximum(tf.math.pow(y_true, 2), 1e-6)

class ChiSquareStatistic_b1_renorm(tf.keras.losses.Loss): # multibatch thingy apparently
    """Used in: train_AE_tfv2_SPRFLX_FC_multiBatch_renorm"""
    @tf.function
    def call(self, y_true, y_pred):
        '''Implements \sum( (y_true - y_pred) ** 2 / y_pred **2.
        Expects rank1 array (num_different_mb) y_pred for theta, and rank 2 array (num_different_mb, same_mb) y_pred for latent_z'''
        if tf.not_equal(tf.rank(y_true), tf.rank(y_pred)):
            y_true = tf.expand_dims(y_true, axis=1)
        sd = tf.math.squared_difference(y_true, y_pred)
        if tf.rank(sd) > 1:
            return tf.math.reduce_sum(sd / tf.math.maximum(tf.math.pow(y_pred, 2), 1e-6), axis= -1)
        else:
            return sd / tf.math.maximum(tf.math.pow(y_pred, 2), 1e-6)

class ChiSquareStatistic_c(tf.keras.losses.Loss):
    """Used in: train_AE_tfv2_NLAR1_BS, train_AE_tfv2_NLAR1_BS_sharpldims, train_AE_tfv2_NLAR1_FC,
       train_AE_tfv2_NLAR1_SingleModelMB"""
    def call(self, y_true, y_pred):
        '''Implements \sum( (y_true - y_pred) ** 2 / y_true**2'''
        # y_true = tf.convert_to_tensor(y_true)
        # y_pred = tf.convert_to_tensor(y_pred)
        sd = tf.math.squared_difference(y_true, y_pred)
        #return tf.math.reduce_sum(sd / y_true, axis= -1)
        return tf.math.reduce_sum(sd / tf.math.maximum(tf.math.pow(y_true, 2), 1e-6), axis=-1) # simone

class ChiSquareStatistic_d(tf.keras.losses.Loss):
    """Used in: train_Regressor5_tfv2_NLAR1_FC"""
    @tf.function
    def call(self, y_true, y_pred):
        '''Implements \sum( (y_true - y_pred) ** 2 / y_true**2 )'''
        if tf.not_equal(tf.rank(y_true), tf.rank(y_pred)):
            y_true = tf.expand_dims(y_true, axis=1) # failing miserably, I have to come back to this
            pass
        sd = tf.math.squared_difference(y_true, y_pred)
        if len(sd.shape) > 1:
            return tf.math.reduce_sum(sd / tf.math.maximum(tf.math.pow(y_true, 2), 1e-6), axis= -1)
        else:
            return sd / tf.math.maximum(tf.math.pow(y_true, 2), 1e-6)

class ChiSquareStatistic_e(tf.keras.losses.Loss):
    """Used in: Regressor4_tfv2_NLAR1_FC_bs5, train_Encoder_Reg4_tfv2_NLAR1_FC_bs5"""
    @tf.function
    def call(self, y_true, y_pred):
        '''Implements \sum( (y_true - y_pred) ** 2 / y_true **2'''
        sd = tf.math.squared_difference(y_true, y_pred)
        if len(sd.shape) > 1:
            return tf.math.reduce_sum(sd / tf.math.maximum(tf.math.pow(y_true, 2), 1e-6), axis= -1)
        else:
            return sd / tf.math.maximum(tf.math.pow(y_true, 2), 1e-6)

@tf.function
def loss_reconstruction_fn_a(x, x_pred, return_each_dim=False, args=None): # basically, BiLSTM
    """Used with: train_AE_tfv2_NLAR1_BiLSTM, train_AE_tfv2_NLAR1_BS_BiLSTM,
       train_AE_tfv2_solarDynamo_BiLSTM"""
    if return_each_dim:
        d = {}
        #tmpcheck = {} # we do not use call of ChiSquareStatistic but __call__ of tf.keras.losses.Loss 15Mar21
        #CSS = ChiSquareStatistic(name='ChiSquare')
        for i in range(x.shape[-1]): # do over all channels of observation
            ## d['MSE_x_ch_%d'%(i+1)] = tf.keras.losses.MeanSquaredError(name='MSE')(x[...,i],x_pred[...,i]) # default behavior is sum over batch size: (average over minibatch size)
            #take average of ChiSquare across the timeseries elements (as to scale similarly with ChiSquare of latent parameters)
            #
            #print(x[0,:,i],x_pred[0,:,i]) # single trajs, first dim is batch sz, second dim is time sz, last dim is output sz
            d['ChiSquare_x_ch_%d'%(i+1)] = ChiSquareStatistic_a(name='ChiSquare')(y_true=x[...,i], y_pred=x_pred[...,i]) / args.len_timeseries # __init__ and __call__ in one shot
            #print(d['ChiSquare_x_ch_%d'%(i+1)]) this is not "result" returned by ChiSquareStatistic_a, this is mean(result) / args.len_timeseries, Jan 31 2022
            #
            #tmpcheck['ChiSquare_x_ch_%d'%(i+1)] = CSS.call(y_true=x[...,i],y_pred=x_pred[...,i]) / args.len_timeseries # different from d
            ##tmpcheck['ChiSquare_x_ch_%d'%(i+1)] = CSS.__call__(y_true=x[...,i],y_pred=x_pred[...,i]) / args.len_timeseries # same as d
            #tf.print('zio: ',d['ChiSquare_x_ch_%d'%(i+1)])
            #tf.print('zia: ',tmpcheck['ChiSquare_x_ch_%d'%(i+1)])
        #return (d,tmpcheck) # tmpcheck pipeline - due to my mods, gives same results 16Mar21
        return d
    else:
        ## return tf.keras.losses.MeanSquaredError(name='MSE')(x,x_pred)
        #CSS = ChiSquareStatistic(name='ChiSquare') # tmpcheck pipeline - never tested here
        #tmpcheck = CSS.call(x,x_pred) / args.len_timeseries # tmpcheck pipeline - never tested here
        return ChiSquareStatistic_a(name='ChiSquare')(x,x_pred) / args.len_timeseries # we are not using call rather __call__
        #return (ChiSquareStatistic(name='ChiSquare')(x,x_pred) / args.len_timeseries, tmpcheck) # tmpcheck pipeline

@tf.function
def loss_regress_params_fn_a(params, params_pred, return_each_dim=False):
    '''Used with: train_AE_tfv2_NLAR1_BiLSTM, train_AE_tfv2_NLAR1_BS_BiLSTM, train_AE_tfv2_solarDynamo_BiLSTM,
    Notice params_pred (fetures in AE bottleneck) can have more dimensions than params
    (free dimensions). If not using this loss, return 0. instead.''' # seems a repetition of loss_reconstruction
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        #print(params.shape) # nrows = batch size (e.g. 50), ncol= nparams (e.g. 7)
        for i in range(num_params):
            #print(params[...,i]) this are all the batch size sample parameter by parameter
            #
            # When dealing with parameters, we never divide by batch_size as reduce_sum in ChiSquareStatistic_a gives back a
            # scalar and so Kera's __call__ does not see that it has to take the mean along batch_size
            d['ChiSquare_z_%d'%(i+1)] = ChiSquareStatistic_a(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i]) / num_params # div by num params to get closer to the mean loss in train_step with reduce_sum.
            #
        return d
    else:
        loss = 0.
        for i in range(num_params):
            loss += ChiSquareStatistic_a(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i]) / num_params
        return loss

@tf.function
def loss_reconstruction_fn_a_renorm(x, x_pred, return_each_dim=False, args=None): # basically, BiLSTM
    """Used with: train_AE_tfv2_SPRFLX_BiLSTM_renorm"""
    if return_each_dim:
        d = {}
        for i in range(x.shape[-1]):
            #print(x_pred[...,i]) # shape=(-bsz, -lt (with warm-up time removed) )
            #print(x.shape[-1]) # 1
            d['ChiSquare_x_ch_%d'%(i+1)] = ChiSquareStatistic_a_renorm(name='ChiSquare')(y_true=x[...,i], y_pred=x_pred[...,i]) / args.len_timeseries
        return d
    else:
        return ChiSquareStatistic_a_renorm(name='ChiSquare')(x,x_pred) / args.len_timeseries

@tf.function
def loss_regress_params_fn_a_renorm(params, params_pred, return_each_dim=False):
    '''Used with: train_AE_tfv2_SPRFLX_BiLSTM_renorm'''
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(num_params):
            #print(params[...,i]) # shape=(-bsz, )
            #print(num_params) # how many parameter we have, not really -nmp
            d['ChiSquare_z_%d'%(i+1)] = ChiSquareStatistic_a_renorm(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i]) / num_params
        return d
    else:
        loss = 0.
        for i in range(num_params):
            loss += ChiSquareStatistic_a_renorm(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i]) / num_params
        return loss

@tf.function
def loss_reconstruction_fn_a_renorm_mean(x, x_pred, return_each_dim=False, args=None): # basically, BiLSTM
    """Used with: train_AE_tfv2_SPRFLX_BiLSTM_renorm_mean"""
    if return_each_dim:
        d = {}
        for i in range(x.shape[-1]):
            #print(x_pred[...,i]) # shape=(-bsz, -lt (with warm-up time removed) )
            #print(x.shape[-1]) # 1
            d['ChiSquare_x_ch_%d'%(i+1)] = ChiSquareStatistic_a_renorm_mean(name='ChiSquare')(y_true=x[...,i], y_pred=x_pred[...,i]) / args.len_timeseries
        return d
    else:
        return ChiSquareStatistic_a_renorm_mean(name='ChiSquare')(x,x_pred) / args.len_timeseries

@tf.function
def loss_regress_params_fn_a_renorm_mean(params, params_pred, return_each_dim=False):
    '''Used with: train_AE_tfv2_SPRFLX_BiLSTM_renorm_mean'''
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(num_params):
            #print(params[...,i]) # shape=(-bsz, )
            #print(num_params) # how many parameter we have, not really -nmp
            d['ChiSquare_z_%d'%(i+1)] = ChiSquareStatistic_a_renorm_mean(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i]) / num_params
        return d
    else:
        loss = 0.
        for i in range(num_params):
            loss += ChiSquareStatistic_a_renorm_mean(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i]) / num_params
        return loss

@tf.function
def loss_reconstruction_fn_a_nonorm(x, x_pred, return_each_dim=False, args=None):
    """Used with: train_AE_tfv2_SPRFLX_BiLSTM_nonorm"""
    if return_each_dim:
        d = {}
        for i in range(x.shape[-1]):
            d['ChiSquare_x_ch_%d'%(i+1)] = ChiSquareStatistic_a_nonorm(name='ChiSquare')(y_true=x[...,i], y_pred=x_pred[...,i]) / args.len_timeseries
        return d
    else:
        return ChiSquareStatistic_a_nonorm(name='ChiSquare')(x,x_pred) / args.len_timeseries

@tf.function
def loss_regress_params_fn_a_nonorm(params, params_pred, return_each_dim=False):
    '''Used with: train_AE_tfv2_SPRFLX_BiLSTM_nonorm'''
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(num_params):
            d['ChiSquare_z_%d'%(i+1)] = ChiSquareStatistic_a_nonorm(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i]) / num_params
        return d
    else:
        loss = 0.
        for i in range(num_params):
            loss += ChiSquareStatistic_a_nonorm(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i]) / num_params
        return loss

@tf.function # makes little or no performance gain
def loss_reconstruction_fn_NLAR1(x, x_pred, return_each_dim=False, args=None):
    """Used in: train_AE_tfv2_NLAR1, train_AE_tfv2_solarDynamo_BiLSTM_s4 (production of solar dynamo with BiLSTM)"""
    if return_each_dim:
        d = {}
        for i in range(x.shape[-1]):
            d['ChiSquare_x_ch_%d'%(i+1)] = ChiSquareStatistic_a(name='ChiSquare')(y_true=x[...,i], y_pred=x_pred[...,i]) / args.len_timeseries # take average of ChiSquare across the timeseries elements (as to scale similarly with ChiSquare of latent parameters)
        return d
    else:
        return ChiSquareStatistic_a(name='ChiSquare')(x,x_pred)

@tf.function # makes little or no performance gain
def loss_regress_params_fn_NLAR1(params, params_pred, return_each_dim=False):
    '''Used in train_AE_tfv2_NLAR1_FC, train_AE_tfv2_NLAR1, train_AE_tfv2_NLAR1_SingleModelMB,
    train_AE_tfv2_solarDynamo_BiLSTM, train_AE_tfv2_solarDynamo_BiLSTM_s4 (production of solar dynamo with BiLSTM).
    Notice params_pred can have more dimensions than params (free dimensions).
    If not using this loss, return 0. instead.'''
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(num_params):
            d['ChiSquare_z_%d'%(i+1)] = ChiSquareStatistic_a(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i])
        return d
    else:
        loss = 0.
        for i in range(num_params):
            loss += ChiSquareStatistic_a(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i])
        return loss

@tf.function
def loss_reconstruction_fn_blowfly(x, x_pred, return_each_dim=False, args=None):
    if return_each_dim:
        d = {}
        for i in range(x.shape[-1]): # do over all channels of observation (it's just 1 in our example.)
            if str(args.loss).lower() == 'mse':
                d['MSE_x_ch_%d'%(i+1)] = tf.keras.losses.MeanSquaredError(name='MSE')(x[...,i],x_pred[...,i]) / args.len_timeseries # default behavior is sum over batch size: (average over minibatch size)
            elif str(args.loss).lower() == 'mae':
                d['MAE_x_ch_%d'%(i+1)] = tf.keras.losses.MeanAbsoluteError(name='MAE')(x[...,i],x_pred[...,i]) / args.len_timeseries # default behavior is sum over batch size: (average over minibatch size)
            elif str(args.loss).lower() == 'chisquare':
                d['ChiSquare_x_ch_%d'%(i+1)] = ChiSquareStatistic(name='ChiSquare')(y_true=x[...,i], y_pred=x_pred[...,i]) / args.len_timeseries # take average of ChiSquare across the timeseries elements (as to scale similarly with ChiSquare of latent parameters)
        return d
    else:
        # return tf.keras.losses.MeanSquaredError(name='MSE')(x,x_pred)
        return ChiSquareStatistic(name='ChiSquare')(x,x_pred)/ args.len_timeseries

@tf.function
def loss_regress_params_fn_blowfly(params, params_pred, return_each_dim=False, args=None):
    ''' Notice params_pred can have more dimensions than params (free dimensions).
    If not using this loss, return 0. instead.'''
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(num_params):
            if str(args.loss).lower() == 'mse':
                d['MSE_z_%d'%(i+1)] = tf.keras.losses.MeanSquaredError(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i]) # default behavior is sum over batch size: (average over minibatch size)
            elif str(args.loss).lower() == 'mae':
                d['MAE_z_%d'%(i+1)] = tf.keras.losses.MeanAbsoluteError(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i]) # default behavior is sum over batch size: (average over minibatch size)
            elif str(args.loss).lower() == 'chisquare':
                d['ChiSquare_z_%d'%(i+1)] = ChiSquareStatistic(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i]) # / num_params
        return d
    else:
        loss = 0.
        for i in range(num_params):
            if str(args.loss).lower() == 'mse':
                loss +=  tf.keras.losses.MeanSquaredError(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i]) # default behavior is sum over batch size: (average over minibatch size)
            elif str(args.loss).lower() == 'mae':
                loss +=  tf.keras.losses.MeanAbsoluteError(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i]) # default behavior is sum over batch size: (average over minibatch size)
            elif str(args.loss).lower() == 'chisquare':
                loss += ChiSquareStatistic(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i]) # / num_params
        return loss

def loss_reconstruction_fn_ba(x, x_pred, return_each_dim=False, args=None):
    """Used in train_Regressor1.2_tfv2_NLAR1_FC, train_Regressor2_tfv2_NLAR1_FC_noX,
    train_Regressor2_tfv2_NLAR1_FC, train_Regressor2_tfv2_expNLAR1_FC, train_Regressor3_tfv2_NLAR1_FC"""
    if return_each_dim:
        d = {}
        for i in range(x.shape[-1]): # do over all channels of observation
            d['ChiSquare_x_ch_%d'%(i+1)] = ChiSquareStatistic_a(name='ChiSquare_x_%d'%(i+1))(y_true=x[...,i], y_pred=x_pred[...,i]) / args.len_timeseries # take average of ChiSquare across length of timeseries (as to scale similarly with ChiSquare of latent parameters)
        return d
    else:
        return ChiSquareStatistic_a(name='ChiSquare_x')(x,x_pred) / args.len_timeseries

def loss_regress_params_fn_ba(params, params_pred, return_each_dim=False, args=None):
    '''Used in train_Regressor1.2_tfv2_NLAR1_FC, train_Regressor2_tfv2_expNLAR1_FC,
    train_Regressor2_tfv2_NLAR1_FC_noX, train_Regressor2_tfv2_NLAR1_FC, train_Regressor3_tfv2_NLAR1_FC.
    Notice params_pred can have more dimensions than params (free dimensions).
    This function is to be used for latent dims (z_latent). If not using this loss, return 0. instead.'''
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(num_params):
            d['ChiSquare_z_%d'%(i+1)] = ChiSquareStatistic_a(name='ChiSquare_z_%d'%(i+1))(params[...,i], params_pred[...,i]) / args.num_model_parameters
        return d
    else:
        loss = 0.
        for i in range(num_params):
            loss += ChiSquareStatistic_a(name='ChiSquare_z_%d'%(i+1))(params[...,i], params_pred[...,i]) / args.num_model_parameters
        return loss

@tf.function
def loss_regress_params_fn_bb0(params, params_pred, return_each_dim=False, args=None):
    '''Used in train_Regressor4_tfv2_NLAR1_FC.'''
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(num_params):
            tmp = ChiSquareStatistic_b0(name='ChiSquare_z_%d'%(i+1)) # prolly not needed to sep in 2
            d['ChiSquare_z_%d'%(i+1)] = tmp(params[...,i], params_pred[...,i]) / args.num_model_parameters
        return d
    else:
        loss = 0.
        for i in range(num_params):
            loss += ChiSquareStatistic_b0(name='ChiSquare_z_%d'%(i+1))(params[...,i], params_pred[...,i]) / args.num_model_parameters
        return loss

@tf.function
def loss_reconstruction_fn_Regressor4_tfv2_NLAR1_FC(x, x_pred, return_each_dim=False, args=None):
    """Used in: Regressor4_tfv2_NLAR1_FC"""
    if return_each_dim:
        d = {}
        for i in range(x.shape[-1]):
            # d['MSE_x_ch_%d'%(i+1)] = tf.keras.losses.MeanSquaredError(name='MSE')(x[...,i],x_pred[...,i]) # default behavior s sum over batch size: (average over minibatch size)
            d['ChiSquare_x_ch_%d'%(i+1)] = ChiSquareStatistic_b0(name='ChiSquare_x_%d'%(i+1))(y_true=x[...,i], y_pred=x_pred[...,i]) / args.len_timeseries # take average of ChiSquare across length of timeseries (as to scale similarly with ChiSquare of latent parameters)
        return d
    else:
        # return tf.keras.losses.MeanSquaredError(name='MSE')(x,x_pred)
        return ChiSquareStatistic_b0(name='ChiSquare_x')(x,x_pred) / args.len_timeseries

@tf.function
def loss_reconstruction_fn_b1(x, x_pred, return_each_dim=False, args=None):
    """Used in train_Regressor4_tfv2_NLAR1_FC_multiBatch.py, Regressor4_tfv2_SolarDynamo_FC_multiBatch"""
    if return_each_dim:
        d = {}
        for i in range(x.shape[-1]): # do over all channels of observation
            d['ChiSquare_x_ch_%d'%(i+1)] = ChiSquareStatistic_b1(name='ChiSquare_x_%d'%(i+1))(y_true=x[...,i], y_pred=x_pred[...,i]) / args.len_timeseries # take average of ChiSquare across length of timeseries (as to scale similarly with ChiSquare of latent parameters)
        return d
    else:
        return ChiSquareStatistic_b1(name='ChiSquare_x')(x,x_pred) / args.len_timeseries

@tf.function
def loss_aggregator_fn_Regressor4_tfv2_NLAR1_FC(params, theta_pred, return_each_dim=False, args=None):
    """Used in: Regressor4_tfv2_NLAR1_FC"""
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(params.shape[-1]):
            tmp = ChiSquareStatistic_b0(name='ChiSquare_theta_%d'%(i+1)) # prolly not needed to sep in 2
            d['ChiSquare_theta_ch_%d'%(i+1)] = tmp(y_true=params[i], y_pred=theta_pred[i]) / args.num_model_parameters
        return d
    else:
        loss = 0.
        for i in range(num_params):
            loss += ChiSquareStatistic_b0(name='ChiSquare_theta_%d'%(i+1))(params[i], theta_pred[i]) / args.num_model_parameters
        return loss

@tf.function
def loss_aggregator_fn_Regressor5_tfv2_NLAR1_FC(params, theta_pred, return_each_dim=False, args=None):
    """Used in: train_Regressor5_tfv2_NLAR1_FC"""
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(params.shape[-1]):
            d['ChiSquare_theta_ch_%d'%(i+1)] = ChiSquareStatistic_d(name='ChiSquare_theta_%d'%(i+1))(y_true=params[i], y_pred=theta_pred[i]) / args.num_model_parameters
        return d
    else:
        loss = 0.
        for i in range(num_params):
            loss += ChiSquareStatistic_d(name='ChiSquare_theta_%d'%(i+1))(params[i], theta_pred[i]) / args.num_model_parameters
        return loss

@tf.function #- this does not make a performance difference
def loss_aggregator_fn_Regressor4_tfv2_NLAR1_FC_bs(params, theta_pred, return_each_dim=False, args=None):
    """Used in: train_Regressor4_tfv2_NLAR1_FC_bs5"""
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(params.shape[-1]):
            d['ChiSquare_theta_ch_%d'%(i+1)] = ChiSquareStatistic_e(name='ChiSquare_theta_%d'%(i+1))(y_true=params[i], y_pred=theta_pred[i]) / args.num_model_parameters
        return d
    else:
        loss = 0.
        for i in range(num_params):
            loss += ChiSquareStatistic_e(name='ChiSquare_theta_%d'%(i+1))(params[i], theta_pred[i]) / args.num_model_parameters
        return loss

@tf.function
def loss_aggregator_fn_b1(params, theta_pred, return_each_dim=False, args=None):
    """Used in: train_Regressor4_tfv2_NLAR1_FC_multiBatch, Regressor4_tfv2_SolarDynamo_FC_multiBatch"""
    # params.shape: [args.num_different_mb, args.num_model_parameters]
    # theta_pred.shape: [args.num_different_mb, args.num_model_parameters]
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(params.shape[-1]):
            d['ChiSquare_theta_ch_%d'%(i+1)] = ChiSquareStatistic_b1(name='ChiSquare_theta_%d'%(i+1))(y_true=params[:,i], y_pred=theta_pred[:,i]) / args.num_model_parameters
        return d
    else:
        loss = 0.
        for i in range(num_params):
            loss += ChiSquareStatistic_b1(name='ChiSquare_theta_%d'%(i+1))(params[:,i], theta_pred[:,i]) / args.num_model_parameters
        return loss

@tf.function
def loss_regress_params_fn_b1(params, params_pred, return_each_dim=False, args=None):
    '''Used in: train_Regressor4_tfv2_NLAR1_FC_multiBatch, train_Regressor4_tfv2_SolarDynamo_FC_multiBatch.py
    Notice params_pred can have more dimensions than params (free dimensions).
    This function is to be used for latent dims (z_latent). If not using this loss, return 0. instead.'''
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(num_params):
            d['ChiSquare_z_%d'%(i+1)] = ChiSquareStatistic_b1(name='ChiSquare_z_%d'%(i+1))(params[...,i], params_pred[...,i]) / args.num_model_parameters
        return d
    else:
        loss = 0.
        for i in range(num_params):
            loss += ChiSquareStatistic_b1(name='ChiSquare_z_%d'%(i+1))(params[...,i], params_pred[...,i]) / args.num_model_parameters
        return loss

@tf.function
def loss_aggregator_fn_blowfly(params, theta_pred, return_each_dim=False, args=None):
    # params.shape: [args.num_different_mb, num_regressed_parameters]
    # theta_pred.shape: [args.num_different_mb, num_regressed_parameters]
    num_regressed_parameters = args.num_model_parameters - len(args.indices_of_fixed_params)
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(params.shape[-1]):
            if str(args.loss).lower() == 'mse':
                d['MSE_theta_ch_%d'%(i+1)] = tf.keras.losses.MeanSquaredError(name='MSE_theta_%d'%(i+1))(y_true=params[:,i], y_pred=theta_pred[:,i]) / num_regressed_parameters # default behavior is sum over batch size: (average over minibatch size)
            elif str(args.loss).lower() == 'mae':
                d['MAE_theta_ch_%d'%(i+1)] = tf.keras.losses.MeanAbsoluteError(name='MAE_theta_%d'%(i+1))(y_true=params[:,i], y_pred=theta_pred[:,i]) / num_regressed_parameters # default behavior is sum over batch size: (average over minibatch size)
            elif str(args.loss).lower() == 'chisquare':
                d['ChiSquare_theta_ch_%d'%(i+1)] = ChiSquareStatistic(name='ChiSquare_theta_%d'%(i+1))(y_true=params[:,i], y_pred=theta_pred[:,i]) / num_regressed_parameters
        return d
    else:
        loss = 0.
        for i in range(num_params):
            if str(args.loss).lower() == 'mse':
                loss += tf.keras.losses.MeanSquaredError(name='MSE_theta_%d'%(i+1))(params[:,i], theta_pred[:,i]) / num_regressed_parameters
            elif str(args.loss).lower() == 'mae':
                loss += tf.keras.losses.MeanAbsoluteError(name='MAE_theta_%d'%(i+1))(params[:,i], theta_pred[:,i]) / num_regressed_parameters
            elif str(args.loss).lower() == 'chisquare':
                loss += ChiSquareStatistic(name='ChiSquare_theta_%d'%(i+1))(params[:,i], theta_pred[:,i]) / num_regressed_parameters
        return loss

@tf.function
def loss_regress_params_fn_blowfly_FCmulB(params, params_pred, return_each_dim=False, args=None):
    ''' Notice params_pred can have more dimensions than params (free dimensions). This function is to be used for latent dims (z_latent)
    If not using this loss, return 0. instead.'''
    # params.shape: [args.num_different_mb, num_regressed_parameters]
    # params_pred.shape: [args.num_different_mb, args.batch_size, args.ndims_latent]
    num_regressed_parameters = args.num_model_parameters - len(args.indices_of_fixed_params)
    num_params = params._shape_as_list()[-1]
    params = tf.expand_dims(params, axis=1) ## match dim-size of GT params. Size: [minibatch size, #regressed_params] -> [minibatch size, 1, #regressed_params]
    if return_each_dim:
        d = {}
        for i in range(num_params):
            if str(args.loss).lower() == 'mse':
                d['MSE_z_%d'%(i+1)] = tf.keras.losses.MeanSquaredError(name='MSE_z_%d'%(i+1))(params[...,i], params_pred[...,i]) / num_regressed_parameters
            elif str(args.loss).lower() == 'mae':
                d['MAE_z_%d'%(i+1)] = tf.keras.losses.MeanAbsoluteError(name='MAE_z_%d'%(i+1))(params[...,i], params_pred[...,i]) / num_regressed_parameters
            elif str(args.loss).lower() == 'chisquare':
                d['ChiSquare_z_%d'%(i+1)] = ChiSquareStatistic(name='ChiSquare_z_%d'%(i+1))(params[...,i], params_pred[...,i]) / num_regressed_parameters
        return d
    else:
        loss = 0.
        for i in range(num_params):
            if str(args.loss).lower() == 'mse':
                loss += tf.keras.losses.MeanSquaredError(name='MSE_z_%d'%(i+1))(params[...,i], params_pred[...,i]) / num_regressed_parameters
            elif str(args.loss).lower() == 'mae':
                loss += tf.keras.losses.MeanAbsoluteError(name='MAE_z_%d'%(i+1))(params[...,i], params_pred[...,i]) / num_regressed_parameters
            elif str(args.loss).lower() == 'chisquare':
                loss += ChiSquareStatistic(name='ChiSquare_z_%d'%(i+1))(params[...,i], params_pred[...,i]) / num_regressed_parameters
        return loss

@tf.function
def loss_aggregator_fn_b1_renorm(params, theta_pred, return_each_dim=False, args=None):
    """Used in: train_AE_tfv2_SPRFLX_FC_multiBatch_renorm.py"""
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(params.shape[-1]):
            #print(theta_pred[:,i])  # shape=(-ndmb,) = (#this_param_draws,)
            #print(params.shape[-1]) # = -nmp
            d['ChiSquare_theta_ch_%d'%(i+1)] = ChiSquareStatistic_b1_renorm(name='ChiSquare_theta_%d'%(i+1))(y_true=params[:,i], y_pred=theta_pred[:,i]) / args.num_model_parameters
        return d
    else:
        loss = 0.
        for i in range(num_params):
            loss += ChiSquareStatistic_b1_renorm(name='ChiSquare_theta_%d'%(i+1))(params[:,i], theta_pred[:,i]) / args.num_model_parameters
        return loss

@tf.function
def loss_regress_params_fn_b1_renorm(params, params_pred, return_each_dim=False, args=None):
    '''Used in: train_AE_tfv2_SPRFLX_FC_multiBatch_renorm.py'''
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(num_params):
            #print(params_pred[...,i])  # shape=(-ndmb, -bsz) = (#this_param_draws, #particles)
            #print(num_params) # = -nmp
            d['ChiSquare_z_%d'%(i+1)] = ChiSquareStatistic_b1_renorm(name='ChiSquare_z_%d'%(i+1))(params[...,i], params_pred[...,i]) / args.num_model_parameters
        return d
    else:
        loss = 0.
        for i in range(num_params):
            loss += ChiSquareStatistic_b1_renorm(name='ChiSquare_z_%d'%(i+1))(params[...,i], params_pred[...,i]) / args.num_model_parameters
        return loss

def loss_reconstruction_fn_Regressor5_tfv2_NLAR1_FC(x, x_pred, return_each_dim=False, args=None):
    """Used in train_Regressor5_tfv2_NLAR1_FC"""
    if return_each_dim:
        d = {}
        for i in range(x.shape[-1]): # do over all channels of observation
            d['ChiSquare_x_ch_%d'%(i+1)] = ChiSquareStatistic_d(name='ChiSquare_x_%d'%(i+1))(y_true=x[...,i], y_pred=x_pred[...,i]) / args.len_timeseries # take average of ChiSquare across length of timeseries (as to scale similarly with ChiSquare of latent parameters)
        return d
    else:
        return ChiSquareStatistic_d(name='ChiSquare_x')(x,x_pred) / args.len_timeseries

@tf.function
def loss_regress_params_fn_Regressor5_tfv2_NLAR1_FC(params, params_pred, return_each_dim=False, args=None):
    '''Used in  train_Regressor5_tfv2_NLAR1_FC.
    Notice params_pred can have more dimensions than params (free dimensions).
    This function is to be used for latent dims (z_latent). If not using this loss, return 0. instead.'''
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(num_params):
            d['ChiSquare_z_%d'%(i+1)] = ChiSquareStatistic_d(name='ChiSquare_z_%d'%(i+1))(params[...,i], params_pred[...,i]) / args.num_model_parameters
        return d
    else:
        loss = 0.
        for i in range(num_params):
            loss += ChiSquareStatistic_d(name='ChiSquare_z_%d'%(i+1))(params[...,i], params_pred[...,i]) / args.num_model_parameters
        return loss

@tf.function #- this does not make a performance difference
def loss_regress_params_fn_Regressor4_tfv2_NLAR1_FC_bs(params, params_pred, return_each_dim=False, args=None):
    '''Used in  train_Regressor4_tfv2_NLAR1_FC_bs5.
    Notice params_pred can have more dimensions than params (free dimensions).
    This function is to be used for latent dims (z_latent). If not using this loss, return 0. instead.'''
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(num_params):
            d['ChiSquare_z_%d'%(i+1)] = ChiSquareStatistic_e(name='ChiSquare_z_%d'%(i+1))(params[...,i], params_pred[...,i]) / args.num_model_parameters
        return d
    else:
        loss = 0.
        for i in range(num_params):
            loss += ChiSquareStatistic_e(name='ChiSquare_z_%d'%(i+1))(params[...,i], params_pred[...,i]) / args.num_model_parameters
        return loss

def loss_reconstruction_fn_c(x, x_pred, return_each_dim=False, args=None): # basically, no BiLSTM
    """Used with: train_AE_tfv2_NLAR1_BS, train_AE_tfv2_NLAR1_FC
       train_AE_tfv2_NLAR1_SingleModelMB"""
    if return_each_dim:
        d = {}
        for i in range(x.shape[-1]):
            d['ChiSquare_x_ch_%d'%(i+1)] = ChiSquareStatistic_c(name='ChiSquare')(y_true=x[...,i], y_pred=x_pred[...,i]) / args.len_timeseries # take average of ChiSquare across the timeseries elements (as to scale similarly with ChiSquare of latent parameters)
        return d
    else:
        return ChiSquareStatistic_c(name='ChiSquare')(x,x_pred)

def loss_regress_params_fn_c(params, params_pred, return_each_dim=False):
    '''Used in: train_AE_tfv2_NLAR1_BS, train_AE_tfv2_NLAR1_FC, train_AE_tfv2_NLAR1_SingleModelMB,
    Notice params_pred can have more dimensions than params (free dimensions).
    If not using this loss, return 0. instead.'''
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(num_params):
            d['ChiSquare_z_%d'%(i+1)] = ChiSquareStatistic_c(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i])
        return d
    else:
        loss = 0.
        for i in range(num_params):
            loss += ChiSquareStatistic_c(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i])
        return loss

def loss_reconstruction_fn_Regressor_tfv2_NLAR1_FC(x, x_pred, return_each_dim=False, args=None):
    """Used only in train_Regressor_tfv2_NLAR1_FC so far (asaict, 11 Jun 21)"""
    if return_each_dim:
        d = {}
        for i in range(x.shape[-1]): # do over all channels of observation
            d['ChiSquare_x_ch_%d'%(i+1)] = ChiSquareStatistic_a(name='ChiSquare')(y_true=x[...,i], y_pred=x_pred[...,i]) / args.num_model_parameters # take average of ChiSquare across the number of outputs (as to scale similarly with ChiSquare of latent parameters)
        return d
    else:
        return ChiSquareStatistic_a(name='ChiSquare')(x,x_pred)

@tf.function
def loss_reconstruction_fn_Regressor4_tfv2_NLAR1_FC_bs5(x, x_pred, return_each_dim=False, args=None):
    if return_each_dim:
        d = {}
        for i in range(x.shape[-1]):
            d['ChiSquare_x_ch_%d'%(i+1)] = ChiSquareStatistic_e(name='ChiSquare_x_%d'%(i+1))(y_true=x[...,i], y_pred=x_pred[...,i]) / args.len_timeseries # take average of ChiSquare across length of timeseries (as to scale similarly with ChiSquare of latent parameters)
        return d
    else:
        return ChiSquareStatistic_e(name='ChiSquare_x')(x,x_pred) / args.len_timeseries

@tf.function
def loss_regress_params_fn_Regressor_tfv2_NLAR1_FC(params, params_pred, return_each_dim=False):
    '''Used in train_Regressor_tfv2_NLAR1_FC.
    Notice params_pred can have more dimensions than params (free dimensions).
    If not using this loss, return 0. instead.'''
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(num_params):
            d['ChiSquare_z_%d'%(i+1)] = ChiSquareStatistic_a(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i])
        return d
    else:
        loss = 0.
        for i in range(num_params):
            loss += ChiSquareStatistic_a(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i])
        return loss

@tf.function
def loss_reconstruction_fn_queue(x, x_pred, return_each_dim=False):
    """Used in all train_AE_tfv2_queue_* files so far (asaict, 11 Jun 21)"""
    if return_each_dim:
        d = {}
        for i in range(x.shape[-1]):
            d['MSE_x_ch_%d'%(i+1)] = tf.keras.losses.MeanSquaredError(name='MSE')(x[...,i],x_pred[...,i]) # default behavior is sum over batch size: (average over minibatch size)
        return d
    else:
        return tf.keras.losses.MeanSquaredError(name='MSE')(x,x_pred)

@tf.function
def loss_regress_params_fn_queue(params, params_pred, return_each_dim=False):
    '''Used in: train_AE_tfv2_queue_SA, train_AE_tfv2_queue_simple_uniformEnc, train_AE_tfv2_queue_simple,
    train_AE_tfv2_queue.
    Notice params_pred can have more dimensions than params (free dimensions).
    If not using this loss, return 0. instead.'''
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(num_params):
            d['MSE_z_%d'%(i+1)] = tf.keras.losses.MeanSquaredError(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i])
        return d
    else:
        loss = 0.
        for i in range(num_params):
            loss += tf.keras.losses.MeanSquaredError(name='loss_param_%d'%(i+1))(params[...,i], params_pred[...,i])
        return loss

class MSE_withMedian_Loss(tf.keras.losses.Loss): #granted it's more of a regularizer than a loss
    def call(self, y_true, y_pred):
        l_mse_z = []
        for i in range(y_pred._shape_as_list()[-1]): #iterate over ldims
            median_z = tfp.stats.percentile(y_pred[...,i], 50.0, interpolation='midpoint')
            mse_z = tf.math.reduce_mean(tf.math.squared_difference(y_pred[...,i], median_z))
            l_mse_z.append(mse_z)
        return tf.math.reduce_sum(l_mse_z)

class MAE_withMedian_Loss(tf.keras.losses.Loss): #granted it's more of a regularizer than a loss
    def call(self, y_true, y_pred):
        l_mae_z = []
        for i in range(y_pred._shape_as_list()[-1]): #iterate over ldims
            median_z = tfp.stats.percentile(y_pred[...,i], 50.0, interpolation='midpoint')
            mae_z = tf.math.reduce_mean(tf.math.abs(y_pred[...,i]- median_z))
            l_mae_z.append(mae_z)
        return tf.math.reduce_sum(l_mae_z)

@tf.function
def loss_reconstruction_fn_NLAR1_BS_sharpldims(x, x_pred, return_each_dim=False):
    """Used only in train_AE_tfv2_NLAR1_BS_sharpldims so far (asaict, 11 Jun 21)"""
    if return_each_dim:
        d = {}
        for i in range(x._shape_as_list()[-1]):
            d['MSE_x_ch_%d'%(i+1)] = tf.keras.losses.MeanSquaredError(name='MSE')(x[...,i],x_pred[...,i]) # default behavior is sum over batch size: (average over minibatch size)
        return d
    else:
        return tf.keras.losses.MeanSquaredError(name='MSE')(x,x_pred)

@tf.function
def loss_regress_params_fn_NLAR1_BS_sharpldims(params, params_pred, return_each_dim=False):
    ''' Notice params_pred can have more dimensions than params (free dimensions).
    If not using this loss, return 0. instead.'''
    num_params = params._shape_as_list()[-1]
    num_ldims = params_pred._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(num_ldims):
            d['MAE2median_z_%d'%(i+1)] = MAE_withMedian_Loss(name='loss_param_%d'%(i+1))(y_true=None, y_pred=params_pred[...,i])
        return d
    else:
        loss = 0.
        for i in range(num_ldims):
            loss += MAE_withMedian_Loss(name='loss_param_%d'%(i+1))(y_true=None, y_pred=params_pred[...,i])
        return loss

def loss_decoder_fn(params, theta_pred, return_each_dim=False, args=None):
    """Used in: train_Regressor2_tfv2_NLAR1_FC_noX, train_Regressor1.2_tfv2_NLAR1_FC,
    train_Regressor3_tfv2_NLAR1_FC, train_Regressor2_tfv2_NLAR1_FC, train_Regressor2_tfv2_expNLAR1_FC"""
    num_params = params._shape_as_list()[-1]
    if return_each_dim:
        d = {}
        for i in range(params.shape[-1]):
            # d['MSE_theta_ch_%d'%(i+1)] = tf.keras.losses.MeanSquaredError(name='MSE')(params[...,i],theta_pred[...,i]) # default behavior is sum over batch size: (average over minibatch size)
            d['ChiSquare_theta_ch_%d'%(i+1)] = ChiSquareStatistic_a(name='ChiSquare_theta_%d'%(i+1))(y_true=params[...,i], y_pred=theta_pred[...,i]) / args.num_model_parameters # take average of ChiSquare across the number of outputs (as to scale similarly with ChiSquare of latent parameters)
        return d
    else:
        loss = 0.
        for i in range(num_params):
            # loss += tf.keras.losses.MeanSquaredError(name='ChiSquare_theta_%d'%(i+1))(params[...,i], params_pred[...,i])
            loss += ChiSquareStatistic_a(name='ChiSquare_theta_%d'%(i+1))(params[...,i], theta_pred[...,i]) / args.num_model_parameters
    return loss
