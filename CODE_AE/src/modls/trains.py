import time, sys
import tensorflow as tf
from src.tools.utils import export_summary_scalars
from src.modls.losses import loss_reconstruction_fn_a, loss_regress_params_fn_a, loss_reconstruction_fn_c
from src.modls.losses import loss_regress_params_fn_c, loss_reconstruction_fn_NLAR1, loss_regress_params_fn_NLAR1
from src.modls.losses import loss_reconstruction_fn_Regressor_tfv2_NLAR1_FC, loss_regress_params_fn_Regressor_tfv2_NLAR1_FC
from src.modls.losses import loss_reconstruction_fn_ba, loss_regress_params_fn_ba
from src.modls.losses import loss_regress_params_fn_bb0, loss_aggregator_fn_Regressor4_tfv2_NLAR1_FC
from src.modls.losses import loss_regress_params_fn_Regressor4_tfv2_NLAR1_FC_bs, loss_aggregator_fn_Regressor4_tfv2_NLAR1_FC_bs
from src.modls.losses import loss_decoder_fn
from src.modls.NLARs import NLAR1_gen_fn
#
# wrap a training step for performance gains.
@tf.function
def train_step_a(model, x, params, noise, optimizer, args=None, summary_writer=None):
    '''Used in: train_AE_tfv2_NLAR1_BiLSTM, train_AE_tfv2_solarDynamo_BiLSTM, train_AE_tfv2_NLAR1_BS_BiLSTM,
    Single training step. Likely critical for performance. So not sanity checks.'''
    with tf.GradientTape(persistent=False) as tape: #persistent=True if .gradient() is called multiple times (e.g., multiple losses)
        z_latent = model.encoder(x, training=True) # points to return tf.keras.Model(bla)
        x_reconst = model.decoder((z_latent, noise), training=True)
        dict_reconstruction_mse = loss_reconstruction_fn_a(x, x_reconst, return_each_dim=True, args=args)
        #dict_reconstruction_mse, tmpcheck = loss_reconstruction_fn_a(x, x_reconst, return_each_dim=True) # tmpcheck pipeline
        loss_reconstruction = tf.math.reduce_sum(list(dict_reconstruction_mse.values()))
        #loss_recons = tf.math.reduce_sum(list(tmpcheck.values())) # tmpcheck pipeline
        #tf.print('zio: ',loss_reconstruction,loss_recons) # see issue #5 and comments in call of ChiSquare
        dict_regress_params_mse = loss_regress_params_fn_a(params, z_latent, return_each_dim=True)
        loss_regress_params = tf.math.reduce_sum(list(dict_regress_params_mse.values()))
        loss = loss_reconstruction + loss_regress_params
        trainable_variables = model.encoder.trainable_variables + model.decoder.trainable_variables
        gradients = tape.gradient(loss, trainable_variables)
        if args.global_gradient_clipnorm is not None:
            gradients, global_norm = tf.clip_by_global_norm(gradients, clip_norm=args.global_gradient_clipnorm, name='clip_gradients_by_global_norm')
        if tf.equal(optimizer.iterations % args.freq_log, 0):
            l_avg_grads= [tf.math.reduce_mean(g) for g in gradients]
            l_absmax_grads= [tf.math.reduce_max(tf.math.abs(g)) for g in gradients]
            l_gradmean_names = ['gradient_mean_'+v.name for v in trainable_variables]
            l_gradabsmax_names = ['gradient_absmax_'+v.name for v in trainable_variables]
            # export avg and absolute max gradients of variables to tensorboard
            d_grads = {**dict(zip(l_gradmean_names, l_avg_grads)), **dict(zip(l_gradabsmax_names, l_absmax_grads))}
            export_summary_scalars(dict_name_and_val=d_grads, step=optimizer.iterations, writer=summary_writer)
        optimizer.apply_gradients(zip(gradients, trainable_variables))
    return (loss_reconstruction, loss_regress_params), (z_latent, x_reconst), (dict_reconstruction_mse, dict_regress_params_mse)

@tf.function
def train_step_AE_tfv2_NLAR1(model, x, params, noise, optimizer, args=None, summary_writer=None):
    '''Used in: train_AE_tfv2_NLAR1.
    Single training step.'''
    with tf.GradientTape(persistent=False) as tape: #persistent=True if .gradient() will be called multiple times (e.g., multiple losses)
        z_latent = model.encoder(x, training=True)
        x_reconst = model.decoder((z_latent, noise), training=True)
        dict_reconstruction_mse = loss_reconstruction_fn_NLAR1(x, x_reconst, return_each_dim=True, args=args)
        loss_reconstruction = tf.math.reduce_sum(list(dict_reconstruction_mse.values()))
        dict_regress_params_mse = loss_regress_params_fn_NLAR1(params, z_latent, return_each_dim=True)
        loss_regress_params = tf.math.reduce_sum(list(dict_regress_params_mse.values()))
        loss = loss_reconstruction + loss_regress_params
        trainable_variables = model.encoder.trainable_variables + model.decoder.trainable_variables
        gradients = tape.gradient(loss, trainable_variables)
        if args.global_gradient_clipnorm is not None:
            gradients, global_norm = tf.clip_by_global_norm(gradients, clip_norm=args.global_gradient_clipnorm, name='clip_gradients_by_global_norm')
        if tf.equal(optimizer.iterations % args.freq_log, 0):
            l_avg_grads= [tf.math.reduce_mean(g) for g in gradients]
            l_absmax_grads= [tf.math.reduce_max(tf.math.abs(g)) for g in gradients]
            l_gradmean_names = ['gradient_mean_'+v.name for v in trainable_variables]
            l_gradabsmax_names = ['gradient_absmax_'+v.name for v in trainable_variables]
            # export avg and absolute max gradients of variables to tensorboard
            d_grads = {**dict(zip(l_gradmean_names, l_avg_grads)), **dict(zip(l_gradabsmax_names, l_absmax_grads))}
            export_summary_scalars(dict_name_and_val=d_grads, step=optimizer.iterations, writer=summary_writer)
        optimizer.apply_gradients(zip(gradients, trainable_variables))
    return (loss_reconstruction, loss_regress_params), (z_latent, x_reconst), (dict_reconstruction_mse, dict_regress_params_mse)

@tf.function
def train_step_c(model, x, params, noise, optimizer):
    '''Used in: train_AE_tfv2_NLAR1_SingleModelMB, train_AE_tfv2_NLAR1_FC, train_AE_tfv2_NLAR1_BS.
    Single training step.'''
    with tf.GradientTape(persistent=False) as tape: #persistent=True if .gradient() will be called multiple times (e.g., multiple losses)
        z_latent = model.encoder(x, training=True)
        x_reconst = model.decoder((z_latent, noise), training=True)
        dict_reconstruction_mse = loss_reconstruction_fn_c(x, x_reconst, return_each_dim=True)
        loss_reconstruction = tf.math.reduce_sum(list(dict_reconstruction_mse.values()))
        dict_regress_params_mse = loss_regress_params_fn_c(params, z_latent, return_each_dim=True)
        loss_regress_params = tf.math.reduce_sum(list(dict_regress_params_mse.values()))
        loss = loss_reconstruction + loss_regress_params
        trainable_variables = model.encoder.trainable_variables + model.decoder.trainable_variables
        gradients = tape.gradient(loss, trainable_variables)
        if global_gradient_clipnorm is not None:
            gradients, global_norm = tf.clip_by_global_norm(gradients, clip_norm=global_gradient_clipnorm, name='clip_gradients_by_global_norm')
        if tf.equal(optimizer.iterations % args.freq_log, 0):
            l_avg_grads= [tf.math.reduce_mean(g) for g in gradients]
            l_absmax_grads= [tf.math.reduce_max(tf.math.abs(g)) for g in gradients]
            l_gradmean_names = ['gradient_mean_'+v.name for v in trainable_variables]
            l_gradabsmax_names = ['gradient_absmax_'+v.name for v in trainable_variables]
            # export avg and absolute max gradients of variables to tensorboard
            d_grads = {**dict(zip(l_gradmean_names, l_avg_grads)), **dict(zip(l_gradabsmax_names, l_absmax_grads))}
            export_summary_scalars(dict_name_and_val=d_grads, step=optimizer.iterations, writer=summary_writer)
        optimizer.apply_gradients(zip(gradients, trainable_variables))
    return (loss_reconstruction, loss_regress_params), (z_latent, x_reconst), (dict_reconstruction_mse, dict_regress_params_mse)

@tf.function
def train_step_AE_tfv2_NLAR1_BS_sharpldims(model, x, params, noise, optimizer, regress_loss_weight):
    '''Used in: train_AE_tfv2_NLAR1_BS_sharpldims.
    Single training step.'''
    with tf.GradientTape(persistent=False) as tape: #persistent=True if .gradient() will be called multiple times (e.g., multiple losses)
        z_latent = model.encoder(x, training=True)
        x_reconst = model.decoder((z_latent, noise), training=True)
        dict_reconstruction_mse = loss_reconstruction_fn(x, x_reconst, return_each_dim=True)
        loss_reconstruction = tf.math.reduce_sum(list(dict_reconstruction_mse.values()))
        dict_regress_params_mse = loss_regress_params_fn(params, z_latent, return_each_dim=True)
        loss_regress_params = tf.math.reduce_sum(list(dict_regress_params_mse.values()))
        loss = loss_reconstruction + regress_loss_weight * loss_regress_params
        trainable_variables = model.encoder.trainable_variables + model.decoder.trainable_variables
        gradients = tape.gradient(loss, trainable_variables)
        if global_gradient_clipnorm is not None:
            gradients, global_norm = tf.clip_by_global_norm(gradients, clip_norm=global_gradient_clipnorm, name='clip_gradients_by_global_norm')
        if tf.equal(optimizer.iterations % args.freq_log, 0):
            l_avg_grads= [tf.math.reduce_mean(g) for g in gradients]
            l_absmax_grads= [tf.math.reduce_max(tf.math.abs(g)) for g in gradients]
            l_gradmean_names = ['gradient_mean_'+v.name for v in trainable_variables]
            l_gradabsmax_names = ['gradient_absmax_'+v.name for v in trainable_variables]
            # export avg and absolute max gradients of variables to tensorboard
            d_grads = {**dict(zip(l_gradmean_names, l_avg_grads)), **dict(zip(l_gradabsmax_names, l_absmax_grads))}
            export_summary_scalars(dict_name_and_val=d_grads, step=optimizer.iterations, writer=summary_writer)
        optimizer.apply_gradients(zip(gradients, trainable_variables))
    return (loss_reconstruction, loss_regress_params), (z_latent, x_reconst), (dict_reconstruction_mse, dict_regress_params_mse)

@tf.function
def train_step_Regressor_tfv2_NLAR1_FC(model, x, params, noise, optimizer, args=None, summary_writer=None):
    '''Used in train_Regressor_tfv2_NLAR1_FC.
    Single training step.'''
    with tf.GradientTape(persistent=False) as tape: #persistent=True if .gradient() will be called multiple times (e.g., multiple losses)
        z_latent = model.encoder(x, training=True)
        x_reconst = model.decoder((z_latent, noise), training=True)
        dict_reconstruction_mse = loss_reconstruction_fn_Regressor_tfv2_NLAR1_FC(params, x_reconst, return_each_dim=True, args=args)
        loss_reconstruction = tf.math.reduce_sum(list(dict_reconstruction_mse.values()))
        dict_regress_params_mse = loss_regress_params_fn_Regressor_tfv2_NLAR1_FC(params, z_latent, return_each_dim=True)
        loss_regress_params = tf.math.reduce_sum(list(dict_regress_params_mse.values()))
        loss = loss_reconstruction + loss_regress_params
        trainable_variables = model.encoder.trainable_variables + model.decoder.trainable_variables
        gradients = tape.gradient(loss, trainable_variables)
        if args.global_gradient_clipnorm is not None:
            gradients, global_norm = tf.clip_by_global_norm(gradients, clip_norm=args.global_gradient_clipnorm, name='clip_gradients_by_global_norm')
        if tf.equal(optimizer.iterations % args.freq_log, 0):
            l_avg_grads= [tf.math.reduce_mean(g) for g in gradients]
            l_absmax_grads= [tf.math.reduce_max(tf.math.abs(g)) for g in gradients]
            l_gradmean_names = ['gradient_mean_'+v.name for v in trainable_variables]
            l_gradabsmax_names = ['gradient_absmax_'+v.name for v in trainable_variables]
            # export avg and absolute max gradients of variables to tensorboard
            d_grads = {**dict(zip(l_gradmean_names, l_avg_grads)), **dict(zip(l_gradabsmax_names, l_absmax_grads))}
            export_summary_scalars(dict_name_and_val=d_grads, step=optimizer.iterations, writer=summary_writer)
        optimizer.apply_gradients(zip(gradients, trainable_variables))
    return (loss_reconstruction, loss_regress_params), (z_latent, x_reconst), (dict_reconstruction_mse, dict_regress_params_mse)

def train_step_b1(model, x, params, noise, optimizer):
    '''Used in: train_Regressor4_tfv2_NLAR1_FC_multiBatch, train_Regressor4_tfv2_SolarDynamo_FC_multiBatch.
    Single training step.'''
    with tf.GradientTape(persistent=False) as tape: #persistent=True if .gradient() will be called multiple times (e.g., multiple losses)
        z_latent = model.encoder(x, training=True) # shape: [num_different_mb, batch_size, num_latent_dims]
        theta_reconst = model.aggregator(z_latent, training=True) # shape: [num_different_mb, num_model_params]
        lambda_theta = args.batch_size
        lambda_z = 1.
        dict_regress_theta_mse = loss_aggregator_fn(params, theta_reconst, return_each_dim=True)
        loss_regress_theta = tf.math.reduce_sum(list(dict_regress_theta_mse.values()))
        dict_regress_z_mse = loss_regress_params_fn(params, z_latent, return_each_dim=True)
        loss_regress_z = tf.math.reduce_sum(list(dict_regress_z_mse.values()))
        loss = lambda_theta*loss_regress_theta + lambda_z*loss_regress_z
        trainable_variables = model.encoder.trainable_variables + model.aggregator.trainable_variables
        gradients = tape.gradient(loss, trainable_variables)
        if global_gradient_clipnorm is not None:
            gradients, global_norm = tf.clip_by_global_norm(gradients, clip_norm=global_gradient_clipnorm, name='clip_gradients_by_global_norm')
        if gradient_clip_value is not None:
            gradients = [tf.clip_by_value(grad, clip_value_min=-gradient_clip_value, clip_value_max=gradient_clip_value, name='clip_gradients_by_value') for grad in gradients]
        if tf.equal(optimizer.iterations % args.freq_log, 0):
            l_avg_grads= [tf.math.reduce_mean(g) for g in gradients]
            l_absmax_grads= [tf.math.reduce_max(tf.math.abs(g)) for g in gradients]
            l_gradmean_names = ['gradient_mean_'+v.name for v in trainable_variables]
            l_gradabsmax_names = ['gradient_absmax_'+v.name for v in trainable_variables]
            # export avg and absolute max gradients of variables to tensorboard
            d_grads = {**dict(zip(l_gradmean_names, l_avg_grads)), **dict(zip(l_gradabsmax_names, l_absmax_grads))}
            export_summary_scalars(dict_name_and_val=d_grads, step=optimizer.iterations, writer=summary_writer)
        optimizer.apply_gradients(zip(gradients, trainable_variables))
    return (loss_regress_theta, loss_regress_z), (theta_reconst, z_latent), (dict_regress_theta_mse, dict_regress_z_mse)

def train_step_Regressor1dot2_tfv2_NLAR1_FC(model, x, params, noise, optimizer, args=None, summary_writer=None, user_model=None):
    '''Used in: train_Regressor1.2_tfv2_NLAR1_FC
    Single training step.'''
    with tf.GradientTape(persistent=False) as tape: #persistent=True if .gradient() will be called multiple times (e.g., multiple losses)
        z_latent = model.encoder(x, training=True)
        theta_reconst = model.decoder((z_latent, noise), training=True)
        # x_reconst = NLAR1_gen_fn(c_minibatch=theta_reconst[...,0], sigma_minibatch=theta_reconst[...,1], noise_minibatch=noise)
        # dict_reconstruction_mse = loss_reconstruction_fn(x, x_reconst, return_each_dim=True)
        # loss_reconstruction = tf.math.reduce_sum(list(dict_reconstruction_mse.values()))
        lambda_theta, lambda_z = 1., 1.
        dict_regress_theta_mse = loss_decoder_fn(params, theta_reconst, return_each_dim=True, args=args)
        loss_regress_theta = tf.math.reduce_sum(list(dict_regress_theta_mse.values()))
        dict_regress_z_mse = loss_regress_params_fn_ba(params, z_latent, return_each_dim=True, args=args)
        loss_regress_z = tf.math.reduce_sum(list(dict_regress_z_mse.values()))
        # loss = lambda_x*loss_reconstruction + lambda_theta*loss_regress_theta + lambda_z*loss_regress_z
        loss = lambda_theta*loss_regress_theta + lambda_z*loss_regress_z
        trainable_variables = model.encoder.trainable_variables + model.decoder.trainable_variables
        gradients = tape.gradient(loss, trainable_variables)
        if args.global_gradient_clipnorm is not None:
            gradients, global_norm = tf.clip_by_global_norm(gradients, clip_norm=args.global_gradient_clipnorm, name='clip_gradients_by_global_norm')
        if args.gradient_clip_value is not None:
            gradients = [tf.clip_by_value(grad, clip_value_min=-args.gradient_clip_value, clip_value_max=args.gradient_clip_value, name='clip_gradients_by_value') for grad in gradients]
        if tf.equal(optimizer.iterations % args.freq_log, 0):
            l_avg_grads= [tf.math.reduce_mean(g) for g in gradients]
            l_absmax_grads= [tf.math.reduce_max(tf.math.abs(g)) for g in gradients]
            l_gradmean_names = ['gradient_mean_'+v.name for v in trainable_variables]
            l_gradabsmax_names = ['gradient_absmax_'+v.name for v in trainable_variables]
            # export avg and absolute max gradients of variables to tensorboard
            d_grads = {**dict(zip(l_gradmean_names, l_avg_grads)), **dict(zip(l_gradabsmax_names, l_absmax_grads))}
            export_summary_scalars(dict_name_and_val=d_grads, step=optimizer.iterations, writer=summary_writer)
        optimizer.apply_gradients(zip(gradients, trainable_variables))
    # return (loss_reconstruction, loss_regress_theta, loss_regress_z), (x_reconst, theta_reconst, z_latent), (dict_reconstruction_mse, dict_regress_theta_mse, dict_regress_z_mse)
    return (loss_regress_theta, loss_regress_z), (theta_reconst, z_latent), (dict_regress_theta_mse, dict_regress_z_mse)@tf.function

def train_step_first_Regressor2_tfv2_NLAR1_FC(model, x, params, noise, optimizer, args=None, summary_writer=None, user_model=None, x_0=0.25):
    '''Single training step.
    Used in: Regressor2_tfv2_NLAR1_FC_noX (they are all different of course)'''
    #start = time.time()
    with tf.GradientTape(persistent=False) as tape: #persistent=True if .gradient() will be called multiple times (e.g., multiple losses)
        z_latent = model.encoder(x, training=True)
        theta_reconst = model.decoder((z_latent, noise), training=True)
        lambda_theta, lambda_z = 1., 1.
        dict_regress_theta_mse = loss_decoder_fn(params, theta_reconst, return_each_dim=True, args=args)
        loss_regress_theta = tf.math.reduce_sum(list(dict_regress_theta_mse.values()))
        dict_regress_z_mse = loss_regress_params_fn_ba(params, z_latent, return_each_dim=True, args=args)
        loss_regress_z = tf.math.reduce_sum(list(dict_regress_z_mse.values()))
        loss = lambda_theta*loss_regress_theta + lambda_z*loss_regress_z
        trainable_variables = model.encoder.trainable_variables + model.decoder.trainable_variables
        gradients = tape.gradient(loss, trainable_variables)
        if args.global_gradient_clipnorm is not None:
            gradients, global_norm = tf.clip_by_global_norm(gradients, clip_norm=args.global_gradient_clipnorm, name='clip_gradients_by_global_norm')
        if args.gradient_clip_value is not None:
            gradients = [tf.clip_by_value(grad, clip_value_min=-args.gradient_clip_value, clip_value_max=args.gradient_clip_value, name='clip_gradients_by_value') for grad in gradients]
        if tf.equal(optimizer.iterations % args.freq_log, 0):
            l_avg_grads= [tf.math.reduce_mean(g) for g in gradients]
            l_absmax_grads= [tf.math.reduce_max(tf.math.abs(g)) for g in gradients]
            l_gradmean_names = ['gradient_mean_'+v.name for v in trainable_variables]
            l_gradabsmax_names = ['gradient_absmax_'+v.name for v in trainable_variables]
            d_grads = {**dict(zip(l_gradmean_names, l_avg_grads)), **dict(zip(l_gradabsmax_names, l_absmax_grads))}
            export_summary_scalars(dict_name_and_val=d_grads, step=optimizer.iterations, writer=summary_writer)
        optimizer.apply_gradients(zip(gradients, trainable_variables))
    #end = time.time()
    #print('exe time train_step_first_Regressor2_tfv2_NLAR1_FC:',end - start)
    #sys.stdout.flush()
    return (loss_regress_theta, loss_regress_z), (theta_reconst, z_latent), (dict_regress_theta_mse, dict_regress_z_mse)

#@tf.function
def train_step_second_Regressor2_tfv2_NLAR1_FC(model, x, params, noise, optimizer, args=None, summary_writer=None, user_model=None, x_0=0.25):
    '''Single training step.'''
    #start = time.time()
    with tf.GradientTape(persistent=True) as tape: #persistent=True if .gradient() will be called multiple times (e.g., multiple losses)
        z_latent = model.encoder(x, training=True)
        theta_reconst = model.decoder((z_latent, noise), training=True)
        if args.x_recon_precision == 64:
            theta_reconst64 = math_ops.cast(theta_reconst, tf.float64)
            noise64 = math_ops.cast(noise, tf.float64)
            x64 = math_ops.cast(x, tf.float64)
            x_reconst = NLAR1_gen_fn(c_minibatch=theta_reconst64[...,0], sigma_minibatch=theta_reconst64[...,1], noise_minibatch=noise64, x_0=x_0, args=args, model=user_model)
            dict_reconstruction_mse = loss_reconstruction_fn_ba(x64, x_reconst, return_each_dim=True, args=args)
        else:
            x_reconst = NLAR1_gen_fn(c_minibatch=theta_reconst[...,0], sigma_minibatch=theta_reconst[...,1], noise_minibatch=noise, x_0=x_0, args=args, model=user_model)
            dict_reconstruction_mse = loss_reconstruction_fn_ba(x, x_reconst, return_each_dim=True, args=args)
        loss_reconstruction = tf.math.reduce_sum(list(dict_reconstruction_mse.values()))
        lambda_x, lambda_theta, lambda_z = 1., 1., 1.
        dict_regress_theta_mse = loss_decoder_fn(params, theta_reconst, return_each_dim=True, args=args)
        loss_regress_theta = tf.math.reduce_sum(list(dict_regress_theta_mse.values()))
        dict_regress_z_mse = loss_regress_params_fn_ba(params, z_latent, return_each_dim=True, args=args)
        loss_regress_z = tf.math.reduce_sum(list(dict_regress_z_mse.values()))
        if args.x_recon_precision == 64:
            loss = math_ops.cast(lambda_x, tf.float64)*loss_reconstruction + math_ops.cast(lambda_theta*loss_regress_theta + lambda_z*loss_regress_z, tf.float64)
            x_reconst = math_ops.cast(x_reconst, tf.float32)
            loss_reconstruction = math_ops.cast(loss_reconstruction, tf.float32)
            for k in dict_reconstruction_mse:
                dict_reconstruction_mse[k] = math_ops.cast(dict_reconstruction_mse[k], tf.float32)
        else:
            # loss = lambda_x*loss_reconstruction + lambda_theta*loss_regress_theta + lambda_z*loss_regress_z
            loss_x = lambda_x*loss_reconstruction
            loss_rest = lambda_theta*loss_regress_theta + lambda_z*loss_regress_z

    trainable_variables = model.encoder.trainable_variables + model.decoder.trainable_variables
    if args.loss_scale_optimizer:
        scaled_loss = optimizer.get_scaled_loss(loss)
        scaled_gradients = tape.gradient(scaled_loss, trainable_variables)
        (gradients, ) = optimizer.get_unscaled_gradients([scaled_gradients])
    else:
        # gradients = tape.gradient(loss, trainable_variables)
        gradients_x = tape.gradient(loss_x, trainable_variables)
        gradients_rest = tape.gradient(loss_rest, trainable_variables)
        gradients = []
        for i in range(len(trainable_variables)):
            g_x = gradients_x[i]
            g_x = tf.where(tf.math.is_nan(g_x), x=tf.zeros_like(g_x), y=g_x)
            g_x = tf.where(tf.math.is_inf(g_x), x=tf.zeros_like(g_x), y=g_x)
            if args.gradient_clip_norm is not None:
                g_x = tf.clip_by_norm(g_x, clip_norm=args.gradient_clip_norm, name='clip_grad_lossX_by_norm')
            gradients.append(g_x + gradients_rest[i]) # add the two gradients after cleaning up gradients of x.
        if args.global_gradient_clipnorm is not None:
            gradients, global_norm = tf.clip_by_global_norm(gradients, clip_norm=args.global_gradient_clipnorm, name='clip_gradients_by_global_norm')
        if args.gradient_clip_value is not None:
            gradients = [tf.clip_by_value(grad, clip_value_min=-args.gradient_clip_value, clip_value_max=args.gradient_clip_value, name='clip_gradients_by_value') for grad in gradients]

    if tf.equal(optimizer.iterations % args.freq_log, 0):
        l_avg_grads= [tf.math.reduce_mean(g) for g in gradients]
        l_absmax_grads= [tf.math.reduce_max(tf.math.abs(g)) for g in gradients]
        l_gradmean_names = ['gradient_mean_'+v.name for v in trainable_variables]
        l_gradabsmax_names = ['gradient_absmax_'+v.name for v in trainable_variables]
        # export avg and absolute max gradients of variables to tensorboard
        d_grads = {**dict(zip(l_gradmean_names, l_avg_grads)), **dict(zip(l_gradabsmax_names, l_absmax_grads))}
        export_summary_scalars(dict_name_and_val=d_grads, step=optimizer.iterations, writer=summary_writer)
    optimizer.apply_gradients(zip(gradients, trainable_variables))
    #end = time.time()
    #print('exe time train_step_second_Regressor2_tfv2_NLAR1_FC:',end - start)
    #sys.stdout.flush()
    return (loss_reconstruction, loss_regress_theta, loss_regress_z), (x_reconst, theta_reconst, z_latent), (dict_reconstruction_mse, dict_regress_theta_mse, dict_regress_z_mse)

#@tf.function # if I activate this I get: Cannot convert a symbolic Tensor (Neg:0) to a numpy array.
##@tf.autograph.experimental.do_not_convert # this does not allow avoiding the error
def train_step_second_Regressor2_tfv2_NLAR1_FC_noX(model, x, params, noise, optimizer, args=None, summary_writer=None, user_model=None, x_0=0.25):
    '''Single training step.'''
    with tf.GradientTape(persistent=False) as tape: #persistent=True if .gradient() will be called multiple times (e.g., multiple losses)
        z_latent = model.encoder(x, training=True)
        theta_reconst = model.decoder((z_latent, noise), training=True)
        x_reconst = NLAR1_gen_fn(c_minibatch=theta_reconst[...,0], sigma_minibatch=theta_reconst[...,1], x_0=x_0, noise_minibatch=noise, args=args, model=user_model)
        dict_reconstruction_mse = loss_reconstruction_fn_ba(x, x_reconst, return_each_dim=True, args=args)
        loss_reconstruction = tf.math.reduce_sum(list(dict_reconstruction_mse.values()))
        lambda_x, lambda_theta, lambda_z = 1., 1., 1.
        dict_regress_theta_mse = loss_decoder_fn(params, theta_reconst, return_each_dim=True, args=args)
        loss_regress_theta = tf.math.reduce_sum(list(dict_regress_theta_mse.values()))
        dict_regress_z_mse = loss_regress_params_fn_ba(params, z_latent, return_each_dim=True, args=args)
        loss_regress_z = tf.math.reduce_sum(list(dict_regress_z_mse.values()))
        loss = lambda_x*loss_reconstruction + lambda_theta*loss_regress_theta + lambda_z*loss_regress_z
        trainable_variables = model.encoder.trainable_variables + model.decoder.trainable_variables
        gradients = tape.gradient(loss, trainable_variables)
        if args.global_gradient_clipnorm is not None:
            gradients, global_norm = tf.clip_by_global_norm(gradients, clip_norm=args.global_gradient_clipnorm, name='clip_gradients_by_global_norm')
        if args.gradient_clip_value is not None:
            gradients = [tf.clip_by_value(grad, clip_value_min=-args.gradient_clip_value, clip_value_max=args.gradient_clip_value, name='clip_gradients_by_value') for grad in gradients]
        if tf.equal(optimizer.iterations % args.freq_log, 0):
            l_avg_grads= [tf.math.reduce_mean(g) for g in gradients]
            l_absmax_grads= [tf.math.reduce_max(tf.math.abs(g)) for g in gradients]
            l_gradmean_names = ['gradient_mean_'+v.name for v in trainable_variables]
            l_gradabsmax_names = ['gradient_absmax_'+v.name for v in trainable_variables]
            d_grads = {**dict(zip(l_gradmean_names, l_avg_grads)), **dict(zip(l_gradabsmax_names, l_absmax_grads))}
            export_summary_scalars(dict_name_and_val=d_grads, step=optimizer.iterations, writer=summary_writer)
        optimizer.apply_gradients(zip(gradients, trainable_variables))
    return (loss_reconstruction, loss_regress_theta, loss_regress_z), (x_reconst, theta_reconst, z_latent), (dict_reconstruction_mse, dict_regress_theta_mse, dict_regress_z_mse)

@tf.function
def train_step_first_Regressor3_tfv2_NLAR1_FC(model, x, params, noise, optimizer, args=None, summary_writer=None, user_model=None, x_0=0.25):
    """Single training step.
    Used in train_Regressor3_tfv2_NLAR1_FC"""
    with tf.GradientTape(persistent=False) as tape: #persistent=True if .gradient() will be called multiple times (e.g., multiple losses)
        z_latent = model.encoder(x, training=True)
        theta_reconst = model.decoder(z_latent, training=True)
        lambda_theta = 1.
        dict_regress_theta_mse = loss_decoder_fn(params, theta_reconst, return_each_dim=True, args=args)
        loss_regress_theta = tf.math.reduce_sum(list(dict_regress_theta_mse.values()))
        loss = lambda_theta*loss_regress_theta
        trainable_variables = model.encoder.trainable_variables + model.decoder.trainable_variables
        gradients = tape.gradient(loss, trainable_variables)
        if args.global_gradient_clipnorm is not None:
            gradients, global_norm = tf.clip_by_global_norm(gradients, clip_norm=args.global_gradient_clipnorm, name='clip_gradients_by_global_norm')
        if args.gradient_clip_value is not None:
            gradients = [tf.clip_by_value(grad, clip_value_min=-args.gradient_clip_value, clip_value_max=args.gradient_clip_value, name='clip_gradients_by_value') for grad in gradients]
        if tf.equal(optimizer.iterations % args.freq_log, 0):
            l_avg_grads= [tf.math.reduce_mean(g) for g in gradients]
            l_absmax_grads= [tf.math.reduce_max(tf.math.abs(g)) for g in gradients]
            l_gradmean_names = ['gradient_mean_'+v.name for v in trainable_variables]
            l_gradabsmax_names = ['gradient_absmax_'+v.name for v in trainable_variables]
            d_grads = {**dict(zip(l_gradmean_names, l_avg_grads)), **dict(zip(l_gradabsmax_names, l_absmax_grads))}
            export_summary_scalars(dict_name_and_val=d_grads, step=optimizer.iterations, writer=summary_writer)
        optimizer.apply_gradients(zip(gradients, trainable_variables))
    return (loss_regress_theta), (theta_reconst), (dict_regress_theta_mse)

#@tf.function
def train_step_second_Regressor3_tfv2_NLAR1_FC(model, x, params, noise, optimizer, args=None, summary_writer=None, user_model=None, x_0=0.25):
    '''Single training step.
    Used in: train_Regressor3_tfv2_NLAR1_FC'''
    #start = time.time()
    with tf.GradientTape(persistent=True) as tape: #persistent=True if .gradient() will be called multiple times (e.g., multiple losses)
        z_latent = model.encoder(x, training=True)
        theta_reconst = model.decoder(z_latent, training=True)
        x_reconst = NLAR1_gen_fn(c_minibatch=theta_reconst[...,0], sigma_minibatch=theta_reconst[...,1], noise_minibatch=noise, x_0=x_0, args=args, model=user_model)
        dict_reconstruction_mse = loss_reconstruction_fn_ba(x, x_reconst, return_each_dim=True, args=args)
        loss_reconstruction = tf.math.reduce_sum(list(dict_reconstruction_mse.values()))
        lambda_x, lambda_theta = 1., 1.
        dict_regress_theta_mse = loss_decoder_fn(params, theta_reconst, return_each_dim=True, args=args)
        loss_regress_theta = tf.math.reduce_sum(list(dict_regress_theta_mse.values()))
        loss_x = lambda_x*loss_reconstruction
        loss_rest = lambda_theta*loss_regress_theta
        trainable_variables = model.encoder.trainable_variables + model.decoder.trainable_variables
    gradients_x = tape.gradient(loss_x, trainable_variables)
    gradients_rest = tape.gradient(loss_rest, trainable_variables)
    gradients = []
    for i in range(len(trainable_variables)):
        g_x = gradients_x[i]
        g_x = tf.where(tf.math.is_nan(g_x), x=tf.zeros_like(g_x), y=g_x)
        g_x = tf.where(tf.math.is_inf(g_x), x=tf.zeros_like(g_x), y=g_x)
        if args.gradient_clip_norm is not None:
            g_x = tf.clip_by_norm(g_x, clip_norm=args.gradient_clip_norm, name='clip_grad_lossX_by_norm')
        gradients.append(g_x + gradients_rest[i]) # add the two gradients after cleaning up gradients of x.
    if args.global_gradient_clipnorm is not None:
        gradients, global_norm = tf.clip_by_global_norm(gradients, clip_norm=args.global_gradient_clipnorm, name='clip_gradients_by_global_norm')
    if args.gradient_clip_value is not None:
        gradients = [tf.clip_by_value(grad, clip_value_min=-args.gradient_clip_value, clip_value_max=args.gradient_clip_value, name='clip_gradients_by_value') for grad in gradients]

    if tf.equal(optimizer.iterations % args.freq_log, 0):
        l_avg_grads= [tf.math.reduce_mean(g) for g in gradients]
        l_absmax_grads= [tf.math.reduce_max(tf.math.abs(g)) for g in gradients]
        l_gradmean_names = ['gradient_mean_'+v.name for v in trainable_variables]
        l_gradabsmax_names = ['gradient_absmax_'+v.name for v in trainable_variables]
        d_grads = {**dict(zip(l_gradmean_names, l_avg_grads)), **dict(zip(l_gradabsmax_names, l_absmax_grads))}
        export_summary_scalars(dict_name_and_val=d_grads, step=optimizer.iterations, writer=summary_writer)
    optimizer.apply_gradients(zip(gradients, trainable_variables))
    #end = time.time()
    #print('exe time train_step_second_Regressor3_tfv2_NLAR1_FC:',end - start)
    #sys.stdout.flush()
    return (loss_reconstruction, loss_regress_theta), (x_reconst, theta_reconst), (dict_reconstruction_mse, dict_regress_theta_mse)

@tf.function
def train_step_Regressor5_tfv2_NLAR1_FC(model, x, params, noise, optimizer, args=None, summary_writer=None):
    '''Used in train_step_Regressor5_tfv2_NLAR1_FC.
    Single training step.'''
    with tf.GradientTape(persistent=False) as tape: #persistent=True if .gradient() will be called multiple times (e.g., multiple losses)
        z_latent = model.encoder(x, training=True)
        theta_reconst = model.aggregator(z_latent, training=True)
        lambda_theta = args.batch_size
        lambda_z = 1.
        dict_regress_theta_mse = loss_aggregator_fn_Regressor5_tfv2_NLAR1_FC(params, theta_reconst, return_each_dim=True, args=args)
        loss_regress_theta = tf.math.reduce_sum(list(dict_regress_theta_mse.values()))
        dict_regress_z_mse = loss_regress_params_fn_Regressor5_tfv2_NLAR1_FC(params, z_latent, return_each_dim=True, args=args)
        loss_regress_z = tf.math.reduce_sum(list(dict_regress_z_mse.values()))
        loss = lambda_theta*loss_regress_theta + lambda_z*loss_regress_z
        trainable_variables = model.encoder.trainable_variables + model.aggregator.trainable_variables
        gradients = tape.gradient(loss, trainable_variables)
        if args.global_gradient_clipnorm is not None:
            gradients, global_norm = tf.clip_by_global_norm(gradients, clip_norm=args.global_gradient_clipnorm, name='clip_gradients_by_global_norm')
        if args.gradient_clip_value is not None:
            gradients = [tf.clip_by_value(grad, clip_value_min=-args.gradient_clip_value, clip_value_max=args.gradient_clip_value, name='clip_gradients_by_value') for grad in gradients]
        if tf.equal(optimizer.iterations % args.freq_log, 0):
            l_avg_grads= [tf.math.reduce_mean(g) for g in gradients]
            l_absmax_grads= [tf.math.reduce_max(tf.math.abs(g)) for g in gradients]
            l_gradmean_names = ['gradient_mean_'+v.name for v in trainable_variables]
            l_gradabsmax_names = ['gradient_absmax_'+v.name for v in trainable_variables]
            # export avg and absolute max gradients of variables to tensorboard
            d_grads = {**dict(zip(l_gradmean_names, l_avg_grads)), **dict(zip(l_gradabsmax_names, l_absmax_grads))}
            export_summary_scalars(dict_name_and_val=d_grads, step=optimizer.iterations, writer=summary_writer)
        optimizer.apply_gradients(zip(gradients, trainable_variables))
    # return (loss_reconstruction, loss_regress_theta, loss_regress_z), (x_reconst, theta_reconst, z_latent), (dict_reconstruction_mse, dict_regress_theta_mse, dict_regress_z_mse)
    return (loss_regress_theta, loss_regress_z), (theta_reconst, z_latent), (dict_regress_theta_mse, dict_regress_z_mse)

@tf.function # this does not make a performance difference
def train_step_Regressor4_tfv2_NLAR1_FC_bs(model, x, params, noise, optimizer, args=None, summary_writer=None):
    '''Used in train_step_Regressor4_tfv2_NLAR1_FC_bs5.
    Single training step.'''
    with tf.GradientTape(persistent=False) as tape: #persistent=True if .gradient() will be called multiple times (e.g., multiple losses)
        z_latent = model.encoder(x, training=True)
        theta_reconst = model.aggregator(z_latent, training=True)
        lambda_theta = args.batch_size
        lambda_z = 1.
        dict_regress_theta_mse = loss_aggregator_fn_Regressor4_tfv2_NLAR1_FC_bs(params, theta_reconst, return_each_dim=True, args=args)
        loss_regress_theta = tf.math.reduce_sum(list(dict_regress_theta_mse.values()))
        dict_regress_z_mse = loss_regress_params_fn_Regressor4_tfv2_NLAR1_FC_bs(params, z_latent, return_each_dim=True, args=args)
        loss_regress_z = tf.math.reduce_sum(list(dict_regress_z_mse.values()))
        loss = lambda_theta*loss_regress_theta + lambda_z*loss_regress_z
        trainable_variables = model.encoder.trainable_variables + model.aggregator.trainable_variables
        gradients = tape.gradient(loss, trainable_variables)
        if args.global_gradient_clipnorm is not None:
            gradients, global_norm = tf.clip_by_global_norm(gradients, clip_norm=args.global_gradient_clipnorm, name='clip_gradients_by_global_norm')
        if args.gradient_clip_value is not None:
            gradients = [tf.clip_by_value(grad, clip_value_min=-args.gradient_clip_value, clip_value_max=args.gradient_clip_value, name='clip_gradients_by_value') for grad in gradients]
        if tf.equal(optimizer.iterations % args.freq_log, 0):
            l_avg_grads= [tf.math.reduce_mean(g) for g in gradients]
            l_absmax_grads= [tf.math.reduce_max(tf.math.abs(g)) for g in gradients]
            l_gradmean_names = ['gradient_mean_'+v.name for v in trainable_variables]
            l_gradabsmax_names = ['gradient_absmax_'+v.name for v in trainable_variables]
            # export avg and absolute max gradients of variables to tensorboard
            d_grads = {**dict(zip(l_gradmean_names, l_avg_grads)), **dict(zip(l_gradabsmax_names, l_absmax_grads))}
            export_summary_scalars(dict_name_and_val=d_grads, step=optimizer.iterations, writer=summary_writer)
        optimizer.apply_gradients(zip(gradients, trainable_variables))
    # return (loss_reconstruction, loss_regress_theta, loss_regress_z), (x_reconst, theta_reconst, z_latent), (dict_reconstruction_mse, dict_regress_theta_mse, dict_regress_z_mse)
    return (loss_regress_theta, loss_regress_z), (theta_reconst, z_latent), (dict_regress_theta_mse, dict_regress_z_mse)

def train_step_Regressor4_tfv2_NLAR1_FC(model, x, params, noise, optimizer, args=None, summary_writer=None):
    '''Used in: train_Regressor4_tfv2_NLAR1_FC.
    Single training step.'''
    with tf.GradientTape(persistent=False) as tape: #persistent=True if .gradient() will be called multiple times (e.g., multiple losses)
        z_latent = model.encoder(x, training=True)
        theta_reconst = model.aggregator(z_latent, training=True)
        lambda_theta = args.batch_size
        lambda_z = 1.
        dict_regress_theta_mse = loss_aggregator_fn_Regressor4_tfv2_NLAR1_FC(params, theta_reconst, return_each_dim=True, args=args)
        loss_regress_theta = tf.math.reduce_sum(list(dict_regress_theta_mse.values()))
        dict_regress_z_mse = loss_regress_params_fn_bb0(params, z_latent, return_each_dim=True, args=args)
        loss_regress_z = tf.math.reduce_sum(list(dict_regress_z_mse.values()))
        loss = lambda_theta*loss_regress_theta + lambda_z*loss_regress_z
        trainable_variables = model.encoder.trainable_variables + model.aggregator.trainable_variables
        gradients = tape.gradient(loss, trainable_variables)
        if args.global_gradient_clipnorm is not None:
            gradients, global_norm = tf.clip_by_global_norm(gradients, clip_norm=args.global_gradient_clipnorm, name='clip_gradients_by_global_norm')
        if args.gradient_clip_value is not None:
            gradients = [tf.clip_by_value(grad, clip_value_min=-args.gradient_clip_value, clip_value_max=args.gradient_clip_value, name='clip_gradients_by_value') for grad in gradients]
        if tf.equal(optimizer.iterations % args.freq_log, 0):
            l_avg_grads= [tf.math.reduce_mean(g) for g in gradients]
            l_absmax_grads= [tf.math.reduce_max(tf.math.abs(g)) for g in gradients]
            l_gradmean_names = ['gradient_mean_'+v.name for v in trainable_variables]
            l_gradabsmax_names = ['gradient_absmax_'+v.name for v in trainable_variables]
            # export avg and absolute max gradients of variables to tensorboard
            d_grads = {**dict(zip(l_gradmean_names, l_avg_grads)), **dict(zip(l_gradabsmax_names, l_absmax_grads))}
            export_summary_scalars(dict_name_and_val=d_grads, step=optimizer.iterations, writer=summary_writer)
        optimizer.apply_gradients(zip(gradients, trainable_variables))
    # return (loss_reconstruction, loss_regress_theta, loss_regress_z), (x_reconst, theta_reconst, z_latent), (dict_reconstruction_mse, dict_regress_theta_mse, dict_regress_z_mse)
    return (loss_regress_theta, loss_regress_z), (theta_reconst, z_latent), (dict_regress_theta_mse, dict_regress_z_mse)

def train_step_queue(model, x, params, noise, optimizer):
    '''Used in: train_AE_tfv2_queue_*
    Single training step.'''
    with tf.GradientTape(persistent=False) as tape: #persistent=True if .gradient() will be called multiple times (e.g., multiple losses)
        z_latent = model.encoder(x, training=True)
        x_reconst = model.decoder((z_latent, noise), training=True)
        dict_reconstruction_mse = loss_reconstruction_fn(x, x_reconst, return_each_dim=True)
        loss_reconstruction = tf.math.reduce_sum(list(dict_reconstruction_mse.values()))
        dict_regress_params_mse = loss_regress_params_fn(params, z_latent, return_each_dim=True)
        loss_regress_params = tf.math.reduce_sum(list(dict_regress_params_mse.values()))
        loss = loss_reconstruction + loss_regress_params
        trainable_variables = model.encoder.trainable_variables + model.decoder.trainable_variables
        gradients = tape.gradient(loss, trainable_variables)
        if global_gradient_clipnorm is not None:
            gradients, global_norm = tf.clip_by_global_norm(gradients, clip_norm=global_gradient_clipnorm, name='clip_gradients_by_global_norm')
        if tf.equal(optimizer.iterations % args.freq_log, 0):
            l_avg_grads= [tf.math.reduce_mean(g) for g in gradients]
            l_absmax_grads= [tf.math.reduce_max(tf.math.abs(g)) for g in gradients]
            l_gradmean_names = ['gradient_mean_'+v.name for v in trainable_variables]
            l_gradabsmax_names = ['gradient_absmax_'+v.name for v in trainable_variables]
            # export avg and absolute max gradients of variables to tensorboard
            d_grads = {**dict(zip(l_gradmean_names, l_avg_grads)), **dict(zip(l_gradabsmax_names, l_absmax_grads))}
            export_summary_scalars(dict_name_and_val=d_grads, step=optimizer.iterations, writer=summary_writer)
        optimizer.apply_gradients(zip(gradients, trainable_variables))
    return (loss_reconstruction, loss_regress_params), x_reconst, (dict_reconstruction_mse, dict_regress_params_mse)
