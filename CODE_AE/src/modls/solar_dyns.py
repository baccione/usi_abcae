#
import math
#
def solar_dynamo_simply(randval_a,x_old,randval_e):

    reslt = randval_a * x_old * 0.5 * (1.0 + math.erf((x_old - 0.6) / 0.2)) * (1.0 - math.erf((x_old - 1.0) / 0.8)) + randval_e

    return reslt
