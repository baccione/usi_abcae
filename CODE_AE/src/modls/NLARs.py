#
import numpy as np
import tensorflow as tf
import time
import sys
#
# other way to deal with incompatibilities between numpy, tensorflow, python, and slow performances
#NLAR_SquaredExp = lambda c, x_old, sigma, epsilon: c * x_old**2 * (np.exp(-x_old)) + sigma * epsilon
#NLAR_SquaredExp_tf = lambda c, x_old, sigma, epsilon: c * x_old**2 * (tf.math.exp(-x_old)) + sigma * epsilon
#
#
NLAR_SquaredCub = lambda c, x_old, sigma, epsilon: c * x_old**2 * (1. - x_old) + sigma * epsilon
#
def NLAR_SquaredExp(c, x_old, sigma, epsilon):
    """Preferred way to 'fix' incompatibilities between numpy, tensorflow, python, and slow performances"""
    if isinstance(c,float):
        reslt = c * x_old**2 * (np.exp(-x_old)) + sigma * epsilon
        assert isinstance(x_old,float) & isinstance(sigma,float) & isinstance(epsilon,float), ":: Fatal: inconsistent types in NLAR_SquaredExp (1)."
    elif tf.is_tensor(c):
        assert tf.is_tensor(sigma) and tf.is_tensor(epsilon), ":: Fatal: inconsistent types in NLAR_SquaredExp (2)."
        reslt = c * x_old**2 * (tf.math.exp(-x_old)) + sigma * epsilon
    else:
        assert False, ":: Fatal: unrecognized type for c in NLAR_SquaredExp."
    return reslt
#
# Define model function to produce observation x given model parameters c, sigma and noise vector epsilon
@tf.function
##@tf.autograph.experimental.do_not_convert #serves to nothing
def NLAR1_gen_fn(c_minibatch, sigma_minibatch, noise_minibatch, x_0=0.25, args=None, model=None):
    """Was defined in train_Regressor1.2_tfv2_NLAR1_FC & co., but is it ever used?!?"""
    #start = time.time()
    x = [None]*args.len_timeseries
    x [0] = [x_0 for i in range(0,args.batch_size)]
    #
    # It is counterproductive to have complex sfw projects without strong typing.
    if c_minibatch.dtype == tf.float32:
        xprev = np.tile(x_0, args.batch_size).astype(np.float32)
    elif c_minibatch.dtype == tf.float64:
        xprev = np.tile(x_0, args.batch_size).astype(np.float64)
    else:
        assert False,":: Fatal: unrecognized type. Maybe an int?!? Extend me?!?."
    #
    # xprev = tf.convert_to_tensor(xprev) #mfd due to explicit check in NLAR_SquaredExp
    for i in range(1, args.len_timeseries):
        x_i = model(c=c_minibatch, x_old=xprev, sigma=sigma_minibatch, epsilon=noise_minibatch[:,i-1,0])
        x[i] = x_i
        xprev = x_i
    x = tf.stack(x) # shape: [len_timeseries, batch_size]
    x = tf.transpose(x, [1,0]) # shape: [batch_size=300, len_timeseries=200]
    x = tf.expand_dims(x, axis=-1) # shape: [batch_size, len_timeseries, #channels=1]
    #end = time.time()
    #print('exe time NLAR1_gen_fn:',end - start)
    #sys.stdout.flush()
    return x
