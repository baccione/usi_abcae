#
import numpy as np
#
#
def blowfly_eqn(P, n_lagged, n_0, e, n_old, delta, eps):
    ## Computes N_{t+1} for blowfly population discretised differential equation. http://proceedings.mlr.press/v51/park16.pdf , page 402. ##
    return P * n_lagged * np.math.exp(-(n_lagged)/n_0) * e + n_old * np.math.exp(-delta * eps)

def sample_blowfly_timeseries(P, n_0, sigma_d, sigma_p, tau, delta, burnin=0, prng=None, len_timeseries=180):
    if prng is None:
        logging.warning('sample_pn_timeseries should receive a prng for reproducible results.')
        prng = np.random.RandomState()
    tau = int(np.round(tau))
    l_e, l_eps = [], [] #noise timeseries
    l_N = [] # blowfly timeseries
    for i in range(len_timeseries+burnin):
        e = prng.gamma(shape=1/(sigma_p**2), scale=sigma_p**2)
        eps = prng.gamma(shape=1/(sigma_d**2), scale=sigma_d**2)
        l_e.append(e)
        l_eps.append(eps)
        if len(l_N) > 0 and len(l_N) >= tau:
            n_lagged = l_N[-tau]
        else:
            n_lagged = n_0
        if i == 0:
            n_old = n_0
        elif i < burnin+1:
            n_old = n_0
        else:
            n_old = n_new
        n_new = blowfly_eqn(P, n_lagged, n_0, e, n_old, delta, eps)
        l_N.append(n_new)
    l_N = l_N[burnin:]
    l_e = l_e[burnin:]
    l_eps = l_eps[burnin:]
    n = np.reshape(l_N, (len_timeseries,))
    e = np.reshape(l_e, (len_timeseries,))
    eps = np.reshape(l_eps, (len_timeseries,))
    d = {'N':n, 'e':e, 'eps':eps }
    return d

class DataGenerator_Blowfly_Simplified:
    def __init__(self, len_timeseries=180, is_uniform_model_param_priors=False, **kwargs):
        self.prng = kwargs.get('prng', np.random.RandomState(seed=1))
        self.len_timeseries = int(len_timeseries)
        self.is_uniform_model_param_priors = is_uniform_model_param_priors
        ###########################################
        self.burnin = kwargs.get('burnin', 0)
        ### keywords to sample observations based on fixed parameters ###
        self.P_fixed = kwargs.get('P_fixed', None)
        self.n_0_fixed = kwargs.get('n_0_fixed', None)
        self.sigma_d_fixed = kwargs.get('sigma_d_fixed', None)
        self.sigma_p_fixed = kwargs.get('sigma_p_fixed', None)
        self.tau_fixed = kwargs.get('tau_fixed', None)
        self.delta_fixed = kwargs.get('delta_fixed', None)
        ###########################################
        ## Defaults from Park et al:
        # self.log_P_args = kwargs.get('log_P_args', [2, 2])
        # self.log_n_0_args = kwargs.get('log_n_0_args', [5, 0.5])
        # self.log_sigma_d_args = kwargs.get('log_sigma_d_args', [-0.5, 1])
        # self.log_sigma_p_args = kwargs.get('log_sigma_p_args', [-0.5, 1])
        # self.log_tau_args = kwargs.get('log_tau_args', [2, 0.1])
        # self.log_delta_args = kwargs.get('log_delta_args', [-1, 0.4])
        ## Defaults from Meeds et al (from paper, not codebase...and slightly modified):
        self.log_P_args = kwargs.get('log_P_args', [2, 2])
        self.log_n_0_args = kwargs.get('log_n_0_args', [6.7, 0.5])
        self.log_sigma_d_args = kwargs.get('log_sigma_d_args', [-0.75, 1])
        self.log_sigma_p_args = kwargs.get('log_sigma_p_args', [-0.5, 1])
        self.log_tau_args = kwargs.get('log_tau_args', [2.0, 0.1])
        self.log_delta_args = kwargs.get('log_delta_args', [-1.8, 0.4])
        if self.is_uniform_model_param_priors:
            self.P_uniform_lims = kwargs.get('P_uniform_lims', None)
            self.n_0_uniform_lims = kwargs.get('n_0_uniform_lims', None)
            self.sigma_d_uniform_lims = kwargs.get('sigma_d_uniform_lims', None)
            self.sigma_p_uniform_lims = kwargs.get('sigma_p_uniform_lims', None)
            self.tau_uniform_lims = kwargs.get('tau_uniform_lims', None)
            self.delta_uniform_lims = kwargs.get('delta_uniform_lims', None)

    def sample_parameters(self):
        if self.P_fixed is not None:
            P = self.P_fixed
        elif self.is_uniform_model_param_priors:
            P = np.math.exp(self.prng.uniform(self.P_uniform_lims[0], self.P_uniform_lims[1]))
        else:
            P = np.math.exp(self.prng.normal(self.log_P_args[0], self.log_P_args[1]))
        if self.n_0_fixed is not None:
            n_0 = self.n_0_fixed
        elif self.is_uniform_model_param_priors:
            n_0 = np.math.exp(self.prng.uniform(self.n_0_uniform_lims[0], self.n_0_uniform_lims[1]))
        else:
            n_0 = np.math.exp(self.prng.normal(self.log_n_0_args[0], self.log_n_0_args[1]))
        if self.sigma_d_fixed is not None:
            sigma_d = self.sigma_d_fixed
        elif self.is_uniform_model_param_priors:
            sigma_d = np.math.exp(self.prng.uniform(self.sigma_d_uniform_lims[0], self.sigma_d_uniform_lims[1]))
        else:
            sigma_d = np.math.exp(self.prng.normal(self.log_sigma_d_args[0], self.log_sigma_d_args[1]))
        if self.sigma_p_fixed is not None:
            sigma_p = self.sigma_p_fixed
        elif self.is_uniform_model_param_priors:
            sigma_p = np.math.exp(self.prng.uniform(self.sigma_p_uniform_lims[0], self.sigma_p_uniform_lims[1]))
        else:
            sigma_p = np.math.exp(self.prng.normal(self.log_sigma_p_args[0], self.log_sigma_p_args[1]))
        if self.tau_fixed is not None:
            tau = self.tau_fixed
        elif self.is_uniform_model_param_priors:
            tau = np.math.exp(self.prng.uniform(self.tau_uniform_lims[0], self.tau_uniform_lims[1]))
        else:
            tau = np.math.exp(self.prng.normal(self.log_tau_args[0], self.log_tau_args[1]))
        if self.delta_fixed is not None:
            delta = self.delta_fixed
        elif self.is_uniform_model_param_priors:
            delta = np.math.exp(self.prng.uniform(self.delta_uniform_lims[0], self.delta_uniform_lims[1]))
        else:
            delta = np.math.exp(self.prng.normal(self.log_delta_args[0], self.log_delta_args[1]))
        return (P, n_0, sigma_d, sigma_p, tau, delta)

    def __iter__(self):
        while True:
            P, n_0, sigma_d, sigma_p, tau, delta = self.sample_parameters()
            batch = sample_blowfly_timeseries(P=P, n_0=n_0, sigma_d=sigma_d, sigma_p=sigma_p, tau=tau, delta=delta, burnin=self.burnin, prng=self.prng, len_timeseries=self.len_timeseries)
            noise = np.stack([batch['e'], batch['eps']], axis=-1) # [len_timeseries, N=2]
            x = np.expand_dims(batch['N'], 1) # observations [len_timeseries, 1]
            params = (P, n_0, sigma_d, sigma_p, tau, delta) # tuple of scalars
            d = (x, params, noise)
            yield d

class DataGenerator_Blowfly_Simplified_BatchSampler(DataGenerator_Blowfly_Simplified):
    def __init__(self, len_timeseries=180, batch_size=None, **kwargs):
        self.batch_size = batch_size
        super().__init__(len_timeseries=len_timeseries, **kwargs)

    def __iter__(self):
        assert self.batch_size is not None # at this point, batch size must have been already defined.
        while True:
            P, n_0, sigma_d, sigma_p, tau, delta = self.sample_parameters()

            l_observation = np.empty((self.batch_size, self.len_timeseries, 1)) # shape [batch_size, timeseries, 1]
            l_noise = np.empty((self.batch_size, self.len_timeseries, 2)) # [batch_size, len_timeseries, 2]
            params = (P, n_0, sigma_d, sigma_p, tau, delta) # tuple of scalars
            for i in range(self.batch_size):
                sample = sample_blowfly_timeseries(P=P, n_0=n_0, sigma_d=sigma_d, sigma_p=sigma_p, tau=tau, delta=delta,
                                                   burnin=self.burnin, prng=self.prng, len_timeseries=self.len_timeseries)
                l_observation[i,...] = np.expand_dims(sample['N'], 1)
                l_noise[i,...] = np.stack([sample['e'], sample['eps']], axis=-1)
            d = (l_observation, params, l_noise)
            yield d
