##############################################
# 08 October 2021 - Baccione                 #
# ideally we remove this file as soon as     #
# it will be possible to load the ckp also   #
# for the FC_multibatch, see branch          #
# dev_marco_failed_ae_load                   #
##############################################
# where to collect samplers for inference
import os
import importlib
import tensorflow as tf
import numpy as np

import src.wraps.generators
import src.wraps.generatorq
from src.tools.utils import get_ckptname
from src.tools.utils import Manage_Hyper_Parameters
from copy import deepcopy

# TODO: I need to write sanity checks for matching samplers and archs...

class Sampler_AE_tfv2_NLAR1_BiLSTM():
    '''Class provides user friendly access to low-dimensional space for summary statistics analysis.
    Used in: train_AE_tfv2_NLAR1_BiLSTM.py'''

    def __init__(self, generator=None, iterator=None, **kwargs):
        '''Constructor builds NN model and loads its weights. In addition, a generator with true sun parameters is initialized.''' # sun parameters?

        if 'args' in kwargs:
            self.args = kwargs.get('args') # this take into account user's command line specs
        else:
            assert False, ":: Fatal: user input args should be passed to Sampler."
            #self.args = ExpSetup(0)
        assert os.path.isdir(self.args.logdir), ':: Fatal: dir {} from input specs is not found.'.format(self.args.logdir)

        self.arch_module = importlib.import_module("src.modls.archs") # should give path, but...
        self.arch = getattr(self.arch_module, self.args.aem2load)

        self.basename = self.args.load_file_basename #kwargs.get('basename', 'model_best_ckpt')
        self.mb_of_same_realizations = self.args.mb_of_same_realizations
        self.batch_size = self.args.batch_size
        self.check_hyper_params()
        self.prng = kwargs.get('prng', np.random.RandomState(1999))
        self.model_obj = None
        self.build_model()
        self.load_model(basename=self.basename)
        self.generator = generator
        self.iterator = iterator

        # user_model is never used, as only encode is really used afaict, go figure how much garbage...
        try:
            self.user_module = importlib.import_module("src.modls."+self.args.umodlfl)
            self.user_model = getattr(self.user_module, self.args.umodlcl)
            print("User model: ",self.user_model)
        except:
            assert False, ":: Fatal: could not load the specifies user module. Hack me if you are know what you are doing, like, your model is superflex, it is not picklable, and you passing None here, and want to continue sfw dev."

    def sample(self, num_samples=10, return_noise_vectors=False, return_model_parameters=False, return_observations=False, params=None):
        '''Function samples #num_samples observed vectors, then returns the mapped representation for each (size: [#num_samples, #stats]).
        if params is not None, existing generator is ignored and a new custom one is initialized.'''
        if params is None: # if none, fallback to default generator of the class instance
            if self.iterator is None:
                raise AssertionError('iterator is not defined. You must provide a parameter dict for the generator.')
            iterator = self.iterator
        else:
            _, iterator = self.build_custom_generator(return_generator=True, **params)

        summary_space = np.zeros((num_samples, self.args.ndims_latent))
        model_params = np.zeros((num_samples, self.args.num_model_parameters))
        ndarray_noise_timeseries = np.zeros((num_samples, self.args.len_timeseries, self.args.num_noise_channels))
        observations = np.zeros((num_samples, self.args.len_timeseries))
        for i in range(num_samples):
            b = next(iterator) # sample contains a tuple of form (x, params, noise)
            x_i = np.expand_dims(b[0], axis=0).astype(np.float32) #creating minibatch size 1.
            noise_i = np.expand_dims(b[2], axis=0).astype(np.float32) #creating minibatch size 1.
            o_latent = self.model_obj.encoder(x_i, training=False).numpy()
            summary_space[i,...] = o_latent
            ndarray_noise_timeseries[i,...] = noise_i
            model_params[i,...] = b[1]
            observations[i,...] = np.squeeze(b[0])
        elms_return = [summary_space]
        if return_noise_vectors:
            #return summary_space, ndarray_noise_timeseries
            elms_return += [ndarray_noise_timeseries]
        if return_model_parameters:
            elms_return += [model_params]
        if return_observations:
            elms_return += [observations]
        if len(elms_return) == 1:
            return elms_return[0]
        else:
            return elms_return

    def encode(self, samples):
        '''Function generates latent representations of the given observations (samples).
        samples should be of shape: [num_samples, len_timeseries, num_channels=1]'''
        o_latent = self.model_obj.encoder(samples)
        return o_latent

    def reconstruct(self, num_samples=10, params=None):
        '''Function samples #num_samples p_n vectors, then returns the reconstructions of them from their latent representation for each (size: [#num_samples, len_timeseries]
        if params is not None, existing generator is ignored and a new custom one is initialized.'''
        if params is None: # if none, fallback to default generator of the class instance
            if self.iterator is None:
                raise AssertionError('iterator is not defined. You must provide a parameter dict for the generator.')
            iterator = self.iterator
        else:
            _, iterator = self.build_custom_generator(return_generator=True, **params)
        ndarray_timeseries = np.zeros((num_samples, self.args.len_timeseries))
        for i in range(num_samples):
            b = next(iterator) # sample contains a tuple of form (x, params, noise)
            x_i = np.expand_dims(b[0], axis=0).astype(np.float32) #creating minibatch size 1.
            noise_i = np.expand_dims(b[2], axis=0).astype(np.float32) #creating minibatch size 1.
            z_latent = self.model_obj.encoder(x_i, training=False)
            o_reconst = self.model_obj.decoder((z_latent, noise_i), training=False).numpy()
            ndarray_timeseries[i,...] = np.squeeze(o_reconst)
        return ndarray_timeseries

    def decode(self, tuple_summary_and_noise):
        '''Function reconstruct timeseries for a given tuple of (latent_representations, noise vectors).'''
        o_latent = tuple_summary_and_noise[0]
        noise_i = tuple_summary_and_noise[1]
        o_reconst = self.model_obj.decoder((o_latent, noise_i), training=False).numpy()
        return o_reconst

    def check_hyper_params(self):
        isfatal = False
        hp_manager = Manage_Hyper_Parameters(logdir=self.args.logdir)
        if hp_manager.args is None: # these args come from loaded file from training
            raise AssertionError('Hyper-parameter configuration file %s is not found. Quitting.' % (hp_manager.param_config_fn))
        else:
            donotcheck = ['logdir', 'batch_size', 'max_training_steps','varstats_file','samplrfl',
                          'samplrcl','umodlfl','umodlcl','check_len_timeseries','load_file_basename']
            if not self.args.check_len_timeseries:
                print(":: Warning: not checking len_timeseries (this is ok if using superflex "+
                         "due to warmup time intricacies)")
                donotcheck += ['len_timeseries']
            for attr in dir(self.args): # self.args are input arguments by user+default, hp_manager.args are loaded ones
                if not attr.startswith('__') and attr not in donotcheck:
                    if not hasattr(hp_manager.args, attr):
                        print('Saved ExpSetup file is missing attribute %s. Will ignore.' % str(attr)) # this should become fatal at some point.
                    else:
                        if getattr(self.args, attr) != getattr(hp_manager.args, attr):
                            isfatal = self.args.fatal_hyper_param_mism
                            print( ":: Warning: mismatch in loaded and specified parameter {}: {} vs. {}. Maybe a required but unspecified paramter at time of training?!?.".format( attr, getattr(self.args, attr), getattr(hp_manager.args, attr) ) )
        if isfatal:
            assert False, "Mismatched paramters."

    def build_model(self):
        if not hasattr(self.args,'num_input_channels'):
            print(":: Warning: num_input_channels missing. This should be an old training file, or we have a problem.")
            #assert False, ":: Fatal: num_input_channels missing. # We should aim to this sanity check, 23 June 21
            setattr(self.args,'num_input_channels',1)
        self.model_obj = self.arch(ndims_latent=self.args.ndims_latent, len_timeseries=self.args.len_timeseries, num_input_channels=self.args.num_input_channels, num_noise_channels=self.args.num_noise_channels)

    def load_model(self, basename='model_best_ckpt'):
        self.model_obj.encoder.summary()
        self.model_obj.decoder.summary()
        ckpt = tf.train.Checkpoint(encoder=self.model_obj.encoder, decoder=self.model_obj.decoder)
        save_manager = tf.train.CheckpointManager(checkpoint=ckpt, directory=self.args.logdir, max_to_keep=3, checkpoint_name=basename)
        # Restore model weights - by default load weights at best perform and not last weights?
        ckptname = get_ckptname(logdir=self.args.logdir, id=basename)
        try:
            ckpt.restore(ckptname).assert_existing_objects_matched().expect_partial()
        except:
            raise AssertionError('Model weights with basename %s not found in logdir %s - Quitting.' % (basename, self.args.logdir))
        print('Model weights loaded from %s.' % ckptname)
        # ckpt.restore(save_manager.latest_checkpoint).expect_partial()
        # if save_manager.latest_checkpoint:
        #     print('Model weights loaded from %s.' % save_manager.latest_checkpoint)
        # else:
        #     raise AssertionError('Model weights with basename %s not found in logdir %s. Quitting.' % (basename, self.args.logdir))

    def build_custom_generator(self, return_generator=False, **kwargs):
        '''See inside DataGenerator_SolarDynamo for the parameters one can modify for custom generator.'''
        if 'prng' in kwargs:
            prng = kwargs.pop('prng')
        else:
            prng = self.prng
        x_0 = kwargs.pop('x_0', 0.25) # pop: returns and remove
        c_lims = kwargs.pop('c_lims', [4.2, 5.8])
        sigma_lims = kwargs.pop('sigma_lims', [0.005, 0.025])
        #############################################
        # fn = lambda c, x_old, sigma, epsilon: c * x_old**2 * (np.exp(-x_old)) + sigma * epsilon
        ## fn = lambda c, x_old, sigma, epsilon: c * x_old**2 * (1. - x_old) + sigma * epsilon
        #############################################
        # func = kwargs.pop('fn', fn)
        #generator = src.wraps.generators.DataGenerator_NLAR1_Simplified(len_timeseries=self.args.len_timeseries, prng=prng, x_0=x_0, c_lims=c_lims, sigma_lims=sigma_lims, fn=func, **kwargs)
        generator = src.wraps.generators.DataGenerator_NLAR1_Simplified(len_timeseries=self.args.len_timeseries, prng=prng, x_0=x_0, c_lims=c_lims, sigma_lims=sigma_lims, fn=self.user_model, **kwargs)
        iterator = generator.__iter__()
        if return_generator:
            return generator, iterator
        else:
            self.generator = generator
            self.iterator = iterator

class Sampler_AE_tfv2_Superflex_BiLSTM(Sampler_AE_tfv2_NLAR1_BiLSTM):

    def sample(self, num_samples=10, return_noise_vectors=False, return_model_parameters=False, return_observations=False, params=None):

        assert False, ":: Fatal: I am unaware that sample is called for spux and superflex"

    def encode(self, samples):
        '''Function generates latent representations of the given observations (samples).
        samples should be of shape: [num_samples, len_timeseries, num_channels=1]'''
        o_latent = self.model_obj.encoder(samples)
        return o_latent

    def reconstruct(self, num_samples=10, params=None):
        '''Function samples #num_samples p_n vectors, then returns the reconstructions of them from their latent representation for each (size: [#num_samples, len_timeseries]
        if params is not None, existing generator is ignored and a new custom one is initialized.'''

        assert False, ":: Fatal: I am unaware that reconstruct is called for spux and superflex"

    def decode(self, tuple_summary_and_noise):
        '''Function reconstruct timeseries for a given tuple of (latent_representations, noise vectors).'''

        assert False, ":: Fatal: I am unaware that decode is called for spux and superflex"

    #def check_hyper_params(self):
        # used and inherited

    #def build_model(self):
        # used and inherited

    #def load_model(self, basename='model_best_ckpt'):
        # used and inherited

    def build_custom_generator(self, return_generator=False, **kwargs):
        assert False, ":: Fatal: I am unaware that build_custom_generator is called for spux and superflex"

class Sampler_AE_tfv2_NLAR1_SingleModelMB(Sampler_AE_tfv2_NLAR1_BiLSTM):
    '''Class provides user friendly access to low-dimensional space for summary statistics analysis.
    Used in: train_AE_tfv2_NLAR1_SingleModelMB.py'''

    def sample(self, num_samples=10, return_noise_vectors=False, params=None):
        '''Function samples #num_samples observed vectors, then returns the mapped representation for each (size: [#num_samples, #stats]).
        if params is not None, existing generator is ignored and a new custom one is initialized.'''
        if params is None: # if none, fallback to default generator of the class instance
            if self.iterator is None:
                raise AssertionError('iterator is not defined. You must provide a parameter dict for the generator.')
            iterator = self.iterator
        else:
            _, iterator = self.build_custom_generator(return_generator=True, **params)

        # summary_space = np.zeros((num_samples, self.args.ndims_latent))
        # ndarray_noise_timeseries = np.zeros((num_samples, self.args.len_timeseries, self.args.num_noise_channels))
        b = next(iterator) # sample contains a tuple of form (x, params, noise) where x and noise already have minibatch within them.
        x_i = b[0]
        if x_i.shape[0] != num_samples:
            print('WARNING, mismatch of batch size and num_samples, there can be errors.')
        noise_i = b[2]
        o_latent = self.model_obj.encoder(x_i, training=False).numpy()
        summary_space = o_latent
        ndarray_noise_timeseries = noise_i
        if return_noise_vectors:
            return summary_space, ndarray_noise_timeseries
        else:
            return summary_space

    def reconstruct(self, num_samples=12, params=None):
        '''Function samples #num_samples p_n vectors, then returns the reconstructions of the from their latent representation for each (size: [#num_samples, len_timeseries]
        if params is not None, existing generator is ignored and a new custom one is initialized.'''
        if params is None: # if none, fallback to default generator of the class instance
            if self.iterator is None:
                raise AssertionError('iterator is not defined. You must provide a parameter dict for the generator.')
            iterator = self.iterator
        else:
            params['batch_size'] = num_samples
            _, iterator = self.build_custom_generator(return_generator=True, **params)
        # ndarray_timeseries = np.zeros((num_samples, self.args.len_timeseries))
        b = next(iterator) # sample contains a tuple of form (x, params, noise) where x and noise already have minibatch within them.
        x_i = b[0]
        if x_i.shape[0] != num_samples:
            print('WARNING, mismatch of batch size and num_samples, there can be errors.')
        noise_i = b[2]
        z_latent = self.model_obj.encoder(x_i, training=False)
        o_reconst = self.model_obj.decoder((z_latent, noise_i), training=False).numpy()
        ndarray_timeseries = np.squeeze(o_reconst)
        return ndarray_timeseries

    def build_model(self):
        if not hasattr(self.args,'num_input_channels'):
            print(":: Warning: num_input_channels missing. This should be an old training file, or we have a problem.")
            #assert False, ":: Fatal: num_input_channels missing. # We should aim to this sanity check, 23 June 21
            setattr(self.args,'num_input_channels',1)
        self.model_obj = self.arch(ndims_latent=self.args.ndims_latent, len_timeseries=self.args.len_timeseries,
        num_input_channels=self.args.num_input_channels, num_noise_channels=self.args.num_noise_channels,
        batch_size=self.batch_size)

    def build_custom_generator(self, return_generator=False, **kwargs):

        if 'prng' in kwargs:
            prng = kwargs.pop('prng')
        else:
            prng = self.prng

        try:
            batch_size = kwargs.pop('batch_size', self.batch_size)
        except:
            assert False, ":: Fatal: could not pop batch_size in build_custom_generator of sam.py. Hack me if you know."
            batch_size = self.batch_size
        assert batch_size == self.batch_size, ":: Fatal: something is wrong with popped batch_size and read-in batch_size. Hack me if you know."

        generator = src.wraps.generators.DataGenerator_NLAR1_Simplified_BatchSampler(len_timeseries=self.args.len_timeseries, batch_size=batch_size, prng=prng, **kwargs)
        iterator = generator.__iter__()
        if return_generator:
            return generator, iterator
        else:
            self.generator = generator
            self.iterator = iterator

class Sampler_AE_tfv2_NLAR1(Sampler_AE_tfv2_NLAR1_BiLSTM):
    '''Class provides user friendly access to low-dimensional space for summary statistics analysis.
    Used in: train_AE_tfv2_NLAR1.py, train_AE_tfv2_NLAR1_FC.py'''

    def sample(self, num_samples=10, return_noise_vectors=False, params=None):
        '''Function samples #num_samples observed vectors, then returns the mapped representation for each (size: [#num_samples, #stats]).
        if params is not None, existing generator is ignored and a new custom one is initialized.'''
        if params is None: # if none, fallback to default generator of the class instance
            if self.iterator is None:
                raise AssertionError('iterator is not defined. You must provide a parameter dict for the generator.')
            iterator = self.iterator
        else:
            _, iterator = self.build_custom_generator(return_generator=True, **params)

        summary_space = np.zeros((num_samples, self.args.ndims_latent))
        ndarray_noise_timeseries = np.zeros((num_samples, self.args.len_timeseries, self.args.num_noise_channels))
        for i in range(num_samples):
            b = next(iterator) # sample contains a tuple of form (x, params, noise)
            x_i = np.expand_dims(b[0], axis=0).astype(np.float32) #creating minibatch size 1.
            noise_i = np.expand_dims(b[2], axis=0).astype(np.float32) #creating minibatch size 1.
            o_latent = self.model_obj.encoder(x_i, training=False).numpy()
            summary_space[i,...] = o_latent
            ndarray_noise_timeseries[i,...] = noise_i
        if return_noise_vectors:
            return summary_space, ndarray_noise_timeseries
        else:
            return summary_space

    def encode(self, samples):
        '''Function generates latent representations of the given observations (samples).
        samples should be of shape: [num_samples, len_timeseries, num_channels=1]'''
        # o_latent = self.model_obj.encoder(samples)
        ###################################################
        # simone adds .numpy() to get a numpy array instead of tf tensor
        ###################################################
        o_latent = self.model_obj.encoder(samples).numpy()
        ###################################################
        return o_latent

    def build_custom_generator(self, return_generator=False, **kwargs):

        if 'prng' in kwargs:
            prng = kwargs.pop('prng')
        else:
            prng = self.prng

        generator = src.wraps.generators.DataGenerator_NLAR1_Simplified(len_timeseries=self.args.len_timeseries, prng=prng, **kwargs)
        iterator = generator.__iter__()
        if return_generator:
            return generator, iterator
        else:
            self.generator = generator
            self.iterator = iterator

class Sampler_AE_tfv2_NLAR1_BS(Sampler_AE_tfv2_NLAR1):
    '''Class provides user friendly access to low-dimensional space for summary statistics analysis.
    Used in: train_AE_tfv2_NLAR1_BS, train_AE_tfv2_NLAR1_BS_sharpldims'''

    def encode(self, samples):
        '''Function generates latent representations of the given observations (samples).
        samples should be of shape: [num_samples, len_timeseries, num_channels=1]'''
        o_latent = self.model_obj.encoder(samples)
        return o_latent

class Sampler_AE_tfv2_queue(Sampler_AE_tfv2_NLAR1):
    '''Class provides user friendly access to low-dimensional space for summary statistics analysis.
    Used in: train_AE_tfv2_queue.py, train_AE_tfv2_queue_simple_uniformEnc.py'''

    def encode(self, samples):
        assert False, ":: Fatal: according to original code, this method should not be used here"

    def load_model(self, basename='model_best_ckpt'):
        ckpt = tf.train.Checkpoint(encoder=self.model_obj.encoder, decoder=self.model_obj.decoder)
        save_manager = tf.train.CheckpointManager(checkpoint=ckpt, directory=self.args.logdir, max_to_keep=3, checkpoint_name=basename)
        # Restore model weights
        ckpt.restore(save_manager.latest_checkpoint).expect_partial()
        if save_manager.latest_checkpoint:
            print('Model weights loaded from %s.' % save_manager.latest_checkpoint)
        else:
            raise AssertionError('Model weights with basename %s not found in logdir %s. Quitting.' % (basename, self.args.logdir))

    def build_custom_generator(self, return_generator=False, **kwargs):

        if 'prng' in kwargs:
            prng = kwargs.pop('prng')
        else:
            prng = self.prng
        generator = src.wraps.generatorq.DataGenerator_Queue_DeltaX_Simplified(len_timeseries=self.args.len_timeseries, prng=prng, **kwargs)
        iterator = generator.__iter__()
        if return_generator:
            return generator, iterator
        else:
            self.generator = generator
            self.iterator = iterator

class Sampler_AE_tfv2_queue_simple(Sampler_AE_tfv2_queue):
    """Used in: train_AE_tfv2_queue_simple.py"""

    def encode(self, samples):
        '''Function generates latent representations of the given observations (samples).
        samples should be of shape: [num_samples, len_timeseries, num_channels=1]'''
        o_latent = self.model_obj.encoder(samples)
        return o_latent

    def build_custom_generator(self, return_generator=False, **kwargs):

        if 'prng' in kwargs:
            prng = kwargs.pop('prng')
        else:
            prng = self.prng
        # generator = wrappers.generatorq.DataGenerator_Queue_Simplified(len_timeseries=self.args.len_timeseries, prng=prng, **kwargs)
        mu_a_min = kwargs.pop('mu_a_min', 0.1)
        generator = src.wraps.generatorq.DataGenerator_Queue_DeltaX_Simplified(len_timeseries=self.args.len_timeseries, mu_a_min=mu_a_min, prng=prng, **kwargs)
        iterator = generator.__iter__()
        if return_generator:
            return generator, iterator
        else:
            self.generator = generator
            self.iterator = iterator

class Sampler_AE_tfv2_NLAR1_BS_BiLSTM(Sampler_AE_tfv2_NLAR1):
    '''Class provides user friendly access to low-dimensional space for summary statistics analysis.
    Used in: train_AE_tfv2_NLAR1_BS_BiLSTM'''

    def encode(self, samples):
        '''Function generates latent representations of the given observations (samples).
        samples should be of shape: [num_samples, len_timeseries, num_channels=1]'''
        o_latent = self.model_obj.encoder(samples)
        return o_latent

    def build_custom_generator(self, return_generator=False, **kwargs):

        if 'prng' in kwargs:
            prng = kwargs.pop('prng')
        else:
            prng = self.prng
        x_0 = kwargs.pop('x_0', 0.25)
        c_lims = kwargs.pop('c_lims', [4.2, 5.8])
        sigma_lims = kwargs.pop('sigma_lims', [0.005, 0.025])
        #############################################
        # fn = lambda c, x_old, sigma, epsilon: c * x_old**2 * (1. - x_old) + sigma * epsilon
        # fn = lambda c, x_old, sigma, epsilon: c * x_old**2 * (np.exp(-x_old)) + sigma * epsilon
        #############################################
        # func = kwargs.pop('fn', fn)
        generator = src.wraps.generators.DataGenerator_NLAR1_Simplified(len_timeseries=self.args.len_timeseries, prng=prng, x_0=x_0, c_lims=c_lims, sigma_lims=sigma_lims, fn=self.user_model, **kwargs)
        iterator = generator.__iter__()
        if return_generator:
            return generator, iterator
        else:
            self.generator = generator
            self.iterator = iterator

#
class Sampler_Regressor_tfv2_NLAR1_FC(Sampler_AE_tfv2_NLAR1_BiLSTM):
    '''Class provides user friendly access to low-dimensional space for summary statistics analysis.
    Used in train_Regressor_tfv2_NLAR1_FC.py, train_Regressor_tfv2_NLAR1_FC_s4.py'''

    def sample(self, num_samples=10, return_noise_vectors=False, params=None):
        '''Function samples #num_samples observed vectors, then returns the mapped representation for each (size: [#num_samples, #stats]).
        if params is not None, existing generator is ignored and a new custom one is initialized.'''
        if params is None: # if none, fallback to default generator of the class instance
            if self.iterator is None:
                raise AssertionError('iterator is not defined. You must provide a parameter dict for the generator.')
            iterator = self.iterator
        else:
            _, iterator = self.build_custom_generator(return_generator=True, **params)

        summary_space = np.zeros((num_samples, self.args.ndims_latent))
        ndarray_noise_timeseries = np.zeros((num_samples, self.args.len_timeseries, self.args.num_noise_channels))
        for i in range(num_samples):
            b = next(iterator) # sample contains a tuple of form (x, params, noise)
            x_i = np.expand_dims(b[0], axis=0).astype(np.float32) #creating minibatch size 1.
            noise_i = np.expand_dims(b[2], axis=0).astype(np.float32) #creating minibatch size 1.
            o_latent = self.model_obj.encoder(x_i, training=False).numpy()
            summary_space[i,...] = o_latent
            ndarray_noise_timeseries[i,...] = noise_i
        if return_noise_vectors:
            return summary_space, ndarray_noise_timeseries
        else:
            return summary_space

    def encode(self, samples):
        '''Function generates latent representations of the given observations (samples).
        samples should be of shape: [num_samples, len_timeseries, num_channels=1]'''
        o_latent = self.model_obj.encoder(samples)
        return o_latent

    def reconstruct(self, num_samples=10, params=None):
        '''Function samples #num_samples p_n vectors, then returns the reconstructions of the from their latent representation for each (size: [#num_samples, len_timeseries]
        if params is not None, existing generator is ignored and a new custom one is initialized.'''
        if params is None: # if none, fallback to default generator of the class instance
            if self.iterator is None:
                raise AssertionError('iterator is not defined. You must provide a parameter dict for the generator.')
            iterator = self.iterator
        else:
            _, iterator = self.build_custom_generator(return_generator=True, **params)
        ndarray_model_parameters = np.zeros((num_samples, self.args.num_model_parameters))
        for i in range(num_samples):
            b = next(iterator) # sample contains a tuple of form (x, params, noise)
            x_i = np.expand_dims(b[0], axis=0).astype(np.float32) #creating minibatch size 1.
            noise_i = np.expand_dims(b[2], axis=0).astype(np.float32) #creating minibatch size 1.
            z_latent = self.model_obj.encoder(x_i, training=False)
            o_reconst = self.model_obj.decoder((z_latent, noise_i), training=False).numpy()
            ndarray_model_parameters[i,...] = np.squeeze(o_reconst)
        return ndarray_model_parameters

    def build_model(self):
        if not hasattr(self.args,'num_input_channels'):
            print(":: Warning: num_input_channels missing. This should be an old training file, or we have a problem.")
            #assert False, ":: Fatal: num_input_channels missing. # We should aim to this sanity check, 23 June 21
            setattr(self.args,'num_input_channels',1)
        self.model_obj = self.arch(ndims_latent=self.args.ndims_latent, len_timeseries=self.args.len_timeseries, num_input_channels=self.args.num_input_channels, num_noise_channels=self.args.num_noise_channels,
        ndims_output=self.args.num_model_parameters)

    def build_custom_generator(self, return_generator=False, **kwargs):

        if 'prng' in kwargs:
            prng = kwargs.pop('prng')
        else:
            prng = self.prng
        x_0 = kwargs.pop('x_0', 0.25)
        c_lims = kwargs.pop('c_lims', [4.2, 5.8])
        sigma_lims = kwargs.pop('sigma_lims', [0.005, 0.025])

        generator = src.wraps.generators.DataGenerator_NLAR1_Simplified(len_timeseries=self.args.len_timeseries, prng=prng, x_0=x_0, c_lims=c_lims, sigma_lims=sigma_lims, **kwargs)
        iterator = generator.__iter__()
        if return_generator:
            return generator, iterator
        else:
            self.generator = generator
            self.iterator = iterator

class Sampler_Regressor2_tfv2_NLAR1_FC(Sampler_Regressor_tfv2_NLAR1_FC):
    '''Class provides user friendly access to low-dimensional space for summary statistics analysis.
    Used in: train_Regressor2_tfv2_NLAR1_FC.py, train_Regressor2_tfv2_NLAR1_FC_noX.py
    (I put this here for consistency as model was specified in main although it was not specified in sampler,
    which was likely an error), train_Regressor1.2_tfv2_NLAR1_FC.py (same as train_Regressor2_tfv2_NLAR1_FC_noX.py)'''

    def build_custom_generator(self, return_generator=False, **kwargs):

        if 'prng' in kwargs:
            prng = kwargs.pop('prng')
        else:
            prng = self.prng
        x_0 = kwargs.pop('x_0', 0.25)
        c_lims = kwargs.pop('c_lims', [4.2, 5.8])
        sigma_lims = kwargs.pop('sigma_lims', [0.005, 0.025])
        generator = src.wraps.generators.DataGenerator_NLAR1_Simplified(len_timeseries=self.args.len_timeseries,
                    prng=prng, x_0=x_0, c_lims=c_lims, sigma_lims=sigma_lims, fn=self.user_model, **kwargs)
        iterator = generator.__iter__()
        if return_generator:
            return generator, iterator
        else:
            self.generator = generator
            self.iterator = iterator

class Sampler_Regressor3_tfv2_NLAR1_FC(Sampler_Regressor_tfv2_NLAR1_FC):
    '''Class provides user friendly access to low-dimensional space for summary statistics analysis.
    Used in: train_Regressor3_tfv2_NLAR1_FC.py'''

    def reconstruct(self, num_samples=10, params=None):
        '''Function samples #num_samples p_n vectors, then returns the reconstructions of the from their latent representation for each (size: [#num_samples, len_timeseries]
        if params is not None, existing generator is ignored and a new custom one is initialized.'''
        if params is None: # if none, fallback to default generator of the class instance
            if self.iterator is None:
                raise AssertionError('iterator is not defined. You must provide a parameter dict for the generator.')
            iterator = self.iterator
        else:
            _, iterator = self.build_custom_generator(return_generator=True, **params)
        ndarray_model_parameters = np.zeros((num_samples, self.args.num_model_parameters))
        for i in range(num_samples):
            b = next(iterator) # sample contains a tuple of form (x, params, noise)
            x_i = np.expand_dims(b[0], axis=0).astype(np.float32) #creating minibatch size 1.
            noise_i = np.expand_dims(b[2], axis=0).astype(np.float32) #creating minibatch size 1.
            z_latent = self.model_obj.encoder(x_i, training=False)
            o_reconst = self.model_obj.decoder(z_latent, training=False).numpy()
            ndarray_model_parameters[i,...] = np.squeeze(o_reconst)
        return ndarray_model_parameters

    def decode(self, tuple_summary_and_noise):
        '''Function reconstruct timeseries for a given tuple of (latent_representations, noise vectors).'''
        o_latent = tuple_summary_and_noise[0]
        noise_i = tuple_summary_and_noise[1]
        o_reconst = self.model_obj.decoder(o_latent, training=False).numpy()
        return o_reconst

    def build_custom_generator(self, return_generator=False, **kwargs):

        if 'prng' in kwargs:
            prng = kwargs.pop('prng')
        else:
            prng = self.prng
        x_0 = kwargs.pop('x_0', 0.25)
        c_lims = kwargs.pop('c_lims', [4.2, 5.8])
        sigma_lims = kwargs.pop('sigma_lims', [0.005, 0.025])
        #############################################
        # fn = lambda c, x_old, sigma, epsilon: c * x_old**2 * (1. - x_old) + sigma * epsilon
        # fn = lambda c, x_old, sigma, epsilon: c * x_old**2 * (np.exp(-x_old)) + sigma * epsilon
        #############################################
        # func = kwargs.pop('fn', fn)
        generator = src.wraps.generators.DataGenerator_NLAR1_Simplified(len_timeseries=self.args.len_timeseries, prng=prng,
                    x_0=x_0, c_lims=c_lims, sigma_lims=sigma_lims, fn=self.user_model, **kwargs)
        iterator = generator.__iter__()
        if return_generator:
            return generator, iterator
        else:
            self.generator = generator
            self.iterator = iterator

class Sampler_Encoder_tfv2_NLAR1_BS_BiLSTM(Sampler_AE_tfv2_NLAR1_BiLSTM):
    '''Class provides user friendly access to low-dimensional space for summary statistics analysis.
    Used in: train_Encoder_tfv2_NLAR1_BS_BiLSTM.py, train_Encoder_tfv2_NLAR1_BiLSTM.py'''

    def sample(self, num_samples=10, return_noise_vectors=False, params=None):
        '''Function samples #num_samples observed vectors, then returns the mapped representation for each (size: [#num_samples, #stats]).
        if params is not None, existing generator is ignored and a new custom one is initialized.'''
        if params is None: # if none, fallback to default generator of the class instance
            if self.iterator is None:
                raise AssertionError('iterator is not defined. You must provide a parameter dict for the generator.')
            iterator = self.iterator
        else:
            _, iterator = self.build_custom_generator(return_generator=True, **params)

        summary_space = np.zeros((num_samples, self.args.ndims_latent))
        ndarray_noise_timeseries = np.zeros((num_samples, self.args.len_timeseries, self.args.num_noise_channels))
        for i in range(num_samples):
            b = next(iterator) # sample contains a tuple of form (x, params, noise)
            x_i = np.expand_dims(b[0], axis=0).astype(np.float32) #creating minibatch size 1.
            noise_i = np.expand_dims(b[2], axis=0).astype(np.float32) #creating minibatch size 1.
            o_latent = self.model_obj.encoder(x_i, training=False).numpy()
            summary_space[i,...] = o_latent
            ndarray_noise_timeseries[i,...] = noise_i
        if return_noise_vectors:
            return summary_space, ndarray_noise_timeseries
        else:
            return summary_space

class Sampler_Encoder_Reg4_tfv2_NLAR1_FC_bs5(Sampler_AE_tfv2_NLAR1_BiLSTM):
    '''Class provides user friendly access to low-dimensional space for summary statistics analysis.
    Used in: train_Encoder_Reg4_tfv2_NLAR1_FC_bs5.py'''

    def sample(self, num_samples=10, return_noise_vectors=False, return_model_parameters=False, return_observations=False, params=None):
        '''Function samples #num_samples observed vectors, then returns the mapped representation for each (size: [#num_samples, #stats]).
        if params is not None, existing generator is ignored and a new custom one is initialized.'''
        if params is None: # if none, fallback to default generator of the class instance
            if self.iterator is None:
                raise AssertionError('iterator is not defined. You must provide a parameter dict for the generator.')
            iterator = self.iterator
        else:
            _, iterator = self.build_custom_generator(return_generator=True, **params)

        summary_space = np.zeros((num_samples, self.args.ndims_latent))
        model_params = np.zeros((num_samples, self.args.num_model_parameters))
        ndarray_noise_timeseries = np.zeros((num_samples, self.args.len_timeseries, self.args.num_noise_channels))
        observations = np.zeros((num_samples, self.args.len_timeseries))
        for i in range(num_samples):
            b = next(iterator) # sample contains a tuple of form (x, params, noise)
            x_i = np.expand_dims(b[0], axis=0).astype(np.float32) #creating minibatch size 1.
            noise_i = np.expand_dims(b[2], axis=0).astype(np.float32) #creating minibatch size 1.
            o_latent = self.model_obj.encoder(x_i, training=False).numpy()
            summary_space[i,...] = o_latent
            ndarray_noise_timeseries[i,...] = noise_i
            model_params[i,...] = b[1]
            observations[i,...] = np.squeeze(b[0])
        elms_return = [summary_space]
        if return_noise_vectors:
            elms_return += [ndarray_noise_timeseries]
        if return_model_parameters:
            elms_return += [model_params]
        if return_observations:
            elms_return += [observations]
        if len(elms_return) == 1:
            return elms_return[0]
        else:
            return elms_return

    def reconstruct(self, num_samples=10, params=None):
    #     '''Function samples #num_samples p_n vectors, then returns the reconstructions of the from their latent representation for each (size: [#num_samples, len_timeseries]
        assert False, ":: Fatal: apparently this should not be called as it was all commented in original version."
    #     if params is not None, existing generator is ignored and a new custom one is initialized.'''
    #     if params is None: # if none, fallback to default generator of the class instance
    #         if self.iterator is None:
    #             raise AssertionError('iterator is not defined. You must provide a parameter dict for the generator.')
    #         iterator = self.iterator
    #     else:
    #         _, iterator = self.build_custom_generator(return_generator=True, **params)
    #     ndarray_model_parameters = np.zeros((self.args.num_model_parameters))
    #     for i in range(num_samples):
    #         b = next(iterator) # sample contains a tuple of form (x, params, noise)
    #         x_i = np.expand_dims(b[0], axis=0).astype(np.float32) #creating minibatch size 1.
    #         noise_i = np.expand_dims(b[2], axis=0).astype(np.float32) #creating minibatch size 1.
    #         z_latent = self.model_obj.encoder(x_i, training=False)
    #         o_reconst = self.model_obj.aggregator(z_latent, training=False).numpy()
    #         ndarray_model_parameters = np.squeeze(o_reconst)
    #     return ndarray_model_parameters

    def decode(self, summary_stats):
        '''Function reconstruct timeseries for a given tuple of (latent_representations, noise vectors).'''
        o_theta = self.model_obj.aggregator(summary_stats, training=False).numpy()
        return o_theta

    def build_model(self):
        if not hasattr(self.args,'num_input_channels'):
            print(":: Warning: num_input_channels missing. This should be an old training file, or we have a problem.")
            #assert False, ":: Fatal: num_input_channels missing. # We should aim to this sanity check, 23 June 21
            setattr(self.args,'num_input_channels',1)
        self.model_obj = self.arch(ndims_latent=self.args.ndims_latent, len_timeseries=self.args.len_timeseries, num_input_channels=self.args.num_input_channels, num_noise_channels=self.args.num_noise_channels, ndims_output=self.args.num_model_parameters, ndims_free_latent_parameters=self.args.ndims_latent-self.args.num_model_parameters)

    def load_model(self, basename='model_best_ckpt'):
        ckpt = tf.train.Checkpoint(encoder=self.model_obj.encoder, aggregator=self.model_obj.aggregator)
        save_manager = tf.train.CheckpointManager(checkpoint=ckpt, directory=self.args.logdir, max_to_keep=3, checkpoint_name=basename)
        # Restore model weights
        ckptname = get_ckptname(logdir=self.args.logdir, id=basename)
        try:
            ckpt.restore(ckptname).assert_existing_objects_matched().expect_partial()
        except:
            raise AssertionError('Model weights with basename %s not found in logdir %s - Quitting.' % (basename, self.args.logdir))
        print('Model weights loaded from %s.' % ckptname)
        # ckpt.restore(save_manager.latest_checkpoint).expect_partial()
        # if save_manager.latest_checkpoint:
        #     print('Model weights loaded from %s.' % save_manager.latest_checkpoint)
        # else:
        #     raise AssertionError('Model weights with basename %s not found in logdir %s. Quitting.' % (basename, self.args.logdir))

class Sampler_AE_tfv2_solarDynamo_BiLSTM(Sampler_Encoder_tfv2_NLAR1_BS_BiLSTM): #based on Encoder_tfv2_NLAR1_BS_BiLSTM
    '''Class provides user friendly access to low-dimensional space for summary statistics analysis.
    Used in: train_AE_tfv2_solarDynamo_BiLSTM.py'''

    def build_custom_generator(self, return_generator=False, **kwargs):
        '''See inside DataGenerator_SolarDynamo_Simplified for the parameters one can modify for custom generator.'''
        if 'prng' in kwargs:
            prng = kwargs.pop('prng')
        else:
            prng = self.prng
        generator = src.wraps.generators.DataGenerator_SolarDynamo_Simplified(len_timeseries=self.args.len_timeseries, prng=prng, **kwargs)
        iterator = generator.__iter__()
        if return_generator:
            return generator, iterator
        else:
            self.generator = generator
            self.iterator = iterator

class Sampler_AE_tfv2_solarDynamo_BiLSTM_s3_bs50(Sampler_Encoder_tfv2_NLAR1_BS_BiLSTM):
    '''Class provides user friendly access to low-dimensional space for summary statistics analysis.
    Used in: train_AE_tfv2_solarDynamo_BiLSTM_s3_bs50.py,
    train_AE_tfv2_solarDynamo_BiLSTM_s4.py (production of solar dynamo with BiLSTM)'''

    def build_custom_generator(self, return_generator=False, **kwargs):
        '''See inside DataGenerator_SolarDynamo_Simplified for the parameters one can modify for custom generator.'''
        if 'prng' in kwargs:
            prng = kwargs.pop('prng')
        else:
            prng = self.prng
        # p0 = 0.62
        alpha1_lims = [0.9, 1.4]
        delta_lims = [0.05, 0.25]
        eps_lims = [0.02, 0.15]
        # epsilon_max = 0.12
        p0 = 1.0
        # alpha1_lims = [0.7, 1.5]
        # delta_lims = [0.01, 0.30]
        # eps_lims = [0.01, 0.25]
        generator = src.wraps.generators.DataGenerator_SolarDynamo_Simplified(len_timeseries=self.args.len_timeseries,
                                                                             prng=prng, p0=p0,
                                                                             alpha1_lims=alpha1_lims,
                                                                             delta_lims=delta_lims,
                                                                             eps_lims=eps_lims, **kwargs)
        # generator = src.wraps.generators.DataGenerator_SolarDynamo_Simplified(len_timeseries=self.args.len_timeseries, prng=prng, **kwargs)
        iterator = generator.__iter__()
        if return_generator:
            return generator, iterator
        else:
            self.generator = generator
            self.iterator = iterator

class Sampler_AE_tfv2_blowfly_BiLSTM(Sampler_Encoder_tfv2_NLAR1_BS_BiLSTM):
    
    def sample(self, num_samples=10, return_noise_vectors=False, return_model_parameters=False, return_observations=False, params=None):
        '''Function samples #num_samples observed vectors, then returns the mapped representation for each (size: [#num_samples, #stats]).
        if params is not None, existing generator is ignored and a new custom one is initialized.'''
        if params is None: # if none, fallback to default generator of the class instance
            if self.iterator is None:
                raise AssertionError('iterator is not defined. You must provide a parameter dict for the generator.')
            iterator = self.iterator
        else:
            _, iterator = self.build_custom_generator(return_generator=True, **params)

        summary_space = np.zeros((num_samples, self.args.ndims_latent))
        ndarray_noise_timeseries = np.zeros((num_samples, self.args.len_timeseries, self.args.num_noise_channels))
        ndarray_model_parameters = np.zeros((num_samples, self.args.num_model_parameters))
        ndarray_observation_timeseries = np.zeros((num_samples, self.args.len_timeseries))
        for i in range(num_samples):
            b = next(iterator) # sample contains a tuple of form (x, params, noise)
            x_i = np.expand_dims(b[0], axis=0).astype(np.float32) #creating minibatch size 1.
            noise_i = np.expand_dims(b[2], axis=0).astype(np.float32) #creating minibatch size 1.
            o_latent = self.model_obj.encoder(x_i, training=False).numpy()
            summary_space[i,...] = o_latent
            ndarray_noise_timeseries[i,...] = noise_i
            ndarray_model_parameters[i,...] = b[1]
            ndarray_observation_timeseries[i,...] = np.squeeze(b[0])
        elms_return = [summary_space]
        if return_noise_vectors:
            elms_return += [ndarray_noise_timeseries]
        if return_model_parameters:
            elms_return += [ndarray_model_parameters]
        if return_observations:
            elms_return += [ndarray_observation_timeseries]
        if len(elms_return) == 1:
            return elms_return[0]
        else:
            return elms_return

    def build_custom_generator(self, return_generator=False, **kwargs):
        '''See inside DataGenerator_Blowfly_Simplified for the parameters one can modify for custom generator.'''
        if 'prng' in kwargs:
            prng = kwargs.pop('prng')
        else:
            prng = self.prng
        #############################################
        ## Popping kwargs to let you know what are the potential kwargs for this stat. model generator. ##
        ## args if generator should sample from normal distribution (mean, var)
        log_P_args = kwargs.pop('log_P_args', [2, 2**2])
        log_n_0_args = kwargs.pop('log_n_0_args', [6, 0.5**2])
        log_sigma_d_args = kwargs.pop('log_sigma_d_args', [-0.75, 1**2])
        log_sigma_p_args = kwargs.pop('log_sigma_p_args', [-0.5, 1**2])
        log_tau_args = kwargs.pop('log_tau_args', [2.7, 0.1**2])
        log_delta_args = kwargs.pop('log_delta_args', [-1.8, 0.4**2])
        ## args if generator should sample from fixed parameters (e.g., true parameters). WARNING: If not None, fixed params override sampling from distributions
        P_fixed = kwargs.pop('P_fixed', None)
        n_0_fixed = kwargs.pop('n_0_fixed', None)
        sigma_d_fixed = kwargs.pop('sigma_d_fixed', None)
        sigma_p_fixed = kwargs.pop('sigma_p_fixed', None)
        tau_fixed = kwargs.pop('tau_fixed', None)
        delta_fixed = kwargs.pop('delta_fixed', None)
        #############################################
        generator = wrappers.generators.DataGenerator_Blowfly_Simplified(len_timeseries=self.args.len_timeseries, prng=prng, log_P_args=log_P_args, log_n_0_args=log_n_0_args, log_sigma_d_args=log_sigma_d_args, log_sigma_p_args=log_sigma_p_args, log_tau_args=log_tau_args, log_delta_args=log_delta_args, P_fixed=P_fixed, n_0_fixed=n_0_fixed, sigma_d_fixed=sigma_d_fixed, sigma_p_fixed=sigma_p_fixed, tau_fixed=tau_fixed, delta_fixed=delta_fixed, **kwargs)
        iterator = generator.__iter__()
        if return_generator:
            return generator, iterator
        else:
            self.generator = generator
            self.iterator = iterator

class Sampler_Regressor4_tfv2_SolarDynamo_FC_multiBatch(Sampler_Encoder_tfv2_NLAR1_BS_BiLSTM):
    '''Class provides user friendly access to low-dimensional space for summary statistics analysis.
    Used in: train_Regressor4_tfv2_SolarDynamo_FC_multiBatch.py'''

    def sample(self, num_samples=10, return_noise_vectors=False, return_model_parameters=False, return_observations=False, params=None):
        '''Function samples #num_samples observed vectors, then returns the mapped representation for each (size: [#num_samples, #stats]).
        if params is not None, existing generator is ignored and a new custom one is initialized.'''
        if params is None: # if none, fallback to default generator of the class instance
            if self.iterator is None:
                raise AssertionError('iterator is not defined. You must provide a parameter dict for the generator.')
            iterator = self.iterator
        else:
            _, iterator = self.build_custom_generator(return_generator=True, **params)

        l_batches = []
        for i in range(num_samples):
            l_batches.append(next(iterator)) # sample contains a tuple of form (x, params, noise)
        observations, model_params, noise = [tf.stack(it, axis=0) for it in list(zip(*l_batches))]
        summary_space = self.model_obj.encoder(observations, training=False).numpy()
        ndarray_noise_timeseries = noise.numpy()
        model_params = model_params.numpy()
        observations = np.squeeze(observations.numpy())
        elms_return = [summary_space]
        if return_noise_vectors:
            elms_return += [ndarray_noise_timeseries]
        if return_model_parameters:
            elms_return += [model_params]
        if return_observations:
            elms_return += [observations]
        if len(elms_return) == 1:
            return elms_return[0]
        else:
            return elms_return

    def encode(self, samples):
        '''Function generates latent representations of the given observations (samples).
        samples should be of shape: [num_samples, len_timeseries, num_channels=1]'''
        o_latent = self.model_obj.encoder(samples, training=False)
        return o_latent

    def reconstruct(self, num_samples=10, params=None):
        assert False, ":: Fatal: not to be used here according to original code."

    def decode(self, summary_stats):
        '''Function reconstruct timeseries for a given tuple of (latent_representations, noise vectors).'''
        o_theta = self.model_obj.aggregator(summary_stats, training=False).numpy()
        return o_theta

    def build_model(self):
        if not hasattr(self.args,'num_input_channels'):
            print(":: Warning: num_input_channels missing. This should be an old training file, or we have a problem.")
            #assert False, ":: Fatal: num_input_channels missing. # We should aim to this sanity check, 23 June 21
            setattr(self.args,'num_input_channels',1)
        self.model_obj = self.arch(mb_of_same_realizations=self.mb_of_same_realizations,
        batch_size=self.batch_size,
        ndims_latent=self.args.ndims_latent,
        len_timeseries=self.args.len_timeseries, num_input_channels=self.args.num_input_channels,
        num_noise_channels=self.args.num_noise_channels, ndims_output=self.args.num_model_parameters,
        ndims_free_latent_parameters=self.args.ndims_latent-self.args.num_model_parameters
        )
        self.old_weights_en = deepcopy(self.model_obj.encoder.weights)
        self.old_weights_de = deepcopy(self.model_obj.aggregator.weights)

    def load_model(self, basename='model_best_ckpt'):
        self.model_obj.encoder.summary()
        self.model_obj.aggregator.summary()
        ckpt = tf.train.Checkpoint(encoder=self.model_obj.encoder, aggregator=self.model_obj.aggregator)
        save_manager = tf.train.CheckpointManager(checkpoint=ckpt, directory=self.args.logdir, max_to_keep=3, checkpoint_name=basename)
        # Restore model weights
        ckptname = get_ckptname(logdir=self.args.logdir, id=basename)
        try:
            ckpt.restore(ckptname).assert_existing_objects_matched().expect_partial()
        except:
            raise AssertionError('Model weights with basename %s not found in logdir %s - Quitting.' % (basename, self.args.logdir))
        print('Model weights loaded from %s.' % ckptname)
        # ckpt.restore(save_manager.latest_checkpoint).expect_partial()
        # if save_manager.latest_checkpoint:
        #     print('Model weights loaded from %s.' % save_manager.latest_checkpoint)
        # else:
        #     raise AssertionError('Model weights with basename %s not found in logdir %s. Quitting.' % (basename, self.args.logdir))
        self.new_weights_en = deepcopy(self.model_obj.encoder.weights)
        self.new_weights_de = deepcopy(self.model_obj.aggregator.weights)
        #for i in range(len(self.new_weights_de)):
        #    print((self.old_weights_de[i] != self.new_weights_de[i]).numpy().any())
        #    #print( self.new_weights_de[i])

    def build_custom_generator(self, return_generator=False, **kwargs):
        '''See inside DataGenerator_SolarDynamo for the parameters one can modify for custom generator.'''
        if 'prng' in kwargs:
            prng = kwargs.pop('prng')
        else:
            prng = self.prng
        p0 = kwargs.pop('p0', 1.0)
        alpha1_lims = kwargs.pop('alpha1_lims', [0.9, 1.4])
        delta_lims = kwargs.pop('delta_lims', [0.05, 0.25])
        # epsilon_max = kwargs.pop('epsilon_max', 0.5)
        eps_lims = kwargs.pop('eps_lims', [0.02, 0.15])
        # #############################################
        batch_size_requested = kwargs.pop('batch_size', None)
        if batch_size_requested is not None:
            if batch_size_requested != self.mb_of_same_realizations:
                print('A separate "batch_size" parameter sent to the data generator. This is expected to match mb_of_same_realizations (N), but it does not, hence it is ignored.')
        generator = wraps.generators.DataGenerator_SolarDynamo_Simplified_BatchSampler(len_timeseries=self.args.len_timeseries, batch_size=self.mb_of_same_realizations, prng=prng, p0=p0, alpha1_lims=alpha1_lims, delta_lims=delta_lims, eps_lims=eps_lims, **kwargs)
        ## Although we can request mb_of_same_realizations custom, it's safer to fix it to initial setup, which will hopefully match the number used in training of the model.
        iterator = generator.__iter__()
        if return_generator:
            return generator, iterator
        else:
            self.generator = generator
            self.iterator = iterator

class Sampler_Regressor4_tfv2_blowfly_FC_multiBatch(Sampler_Regressor4_tfv2_SolarDynamo_FC_multiBatch):

    def build_custom_generator(self, return_generator=False, **kwargs):
        '''See inside DataGenerator_SolarDynamo for the parameters one can modify for custom generator.'''
        if 'prng' in kwargs:
            prng = kwargs.pop('prng')
        else:
            prng = self.prng
        #############################################
        ## Popping kwargs to let you know what are the potential kwargs for this stat. model generator. ##
        ## args if generator should sample from normal distribution (mean, var)
        log_P_args = kwargs.pop('log_P_args', [2, 2**2])
        log_n_0_args = kwargs.pop('log_n_0_args', [6, 0.5**2])
        log_sigma_d_args = kwargs.pop('log_sigma_d_args', [-0.75, 1**2])
        log_sigma_p_args = kwargs.pop('log_sigma_p_args', [-0.5, 1**2])
        log_tau_args = kwargs.pop('log_tau_args', [2.7, 0.1**2])
        log_delta_args = kwargs.pop('log_delta_args', [-1.8, 0.4**2])
        ## args if generator should sample from fixed parameters (e.g., true parameters). WARNING: If not None, fixed params override sampling from distributions
        P_fixed = kwargs.pop('P_fixed', None)
        n_0_fixed = kwargs.pop('n_0_fixed', None)
        sigma_d_fixed = kwargs.pop('sigma_d_fixed', None)
        sigma_p_fixed = kwargs.pop('sigma_p_fixed', None)
        tau_fixed = kwargs.pop('tau_fixed', None)
        delta_fixed = kwargs.pop('delta_fixed', None)
        #############################################
        batch_size_requested = kwargs.pop('batch_size', None)
        if batch_size_requested is not None:
            if batch_size_requested != self.mb_of_same_realizations:
                print('A separate "batch_size" parameter sent to the data generator. This is expected to match mb_of_same_realizations (N), but it does not, hence it is ignored.')
        generator = wrappers.generators.DataGenerator_Blowfly_Simplified_BatchSampler(len_timeseries=self.args.len_timeseries, batch_size=self.mb_of_same_realizations, prng=prng, log_P_args=log_P_args, log_n_0_args=log_n_0_args, log_sigma_d_args=log_sigma_d_args, log_sigma_p_args=log_sigma_p_args, log_tau_args=log_tau_args, log_delta_args=log_delta_args, P_fixed=P_fixed, n_0_fixed=n_0_fixed, sigma_d_fixed=sigma_d_fixed, sigma_p_fixed=sigma_p_fixed, tau_fixed=tau_fixed, delta_fixed=delta_fixed, **kwargs) ## Although we can request mb_of_same_realizations custom, it's safer to fix it to initial setup, which will hopefully match the number used in training of the model.
        iterator = generator.__iter__()
        if return_generator:
            return generator, iterator
        else:
            self.generator = generator
            self.iterator = iterator

class Sampler_Regressor5_tfv2_NLAR1_FC(Sampler_AE_tfv2_NLAR1_BiLSTM):
    '''Class provides user friendly access to low-dimensional space for summary statistics analysis.
    Used in: train_Regressor5_tfv2_NLAR1_FC.py, train_Regressor4_tfv2_NLAR1_FC.py, train_Regressor4_tfv2_NLAR1_FC_bs5.py'''

    def encode(self, samples):
        '''Function generates latent representations of the given observations (samples).
        samples should be of shape: [num_samples, len_timeseries, num_channels=1]'''
        o_latent = self.model_obj.encoder(samples)
        return o_latent

    def reconstruct(self, num_samples=10, params=None):
    #     '''Function samples #num_samples p_n vectors, then returns the reconstructions of the from their latent representation for each (size: [#num_samples, len_timeseries]
        assert False, ":: Fatal: should not be called according to the original code."
    #     if params is not None, existing generator is ignored and a new custom one is initialized.'''
    #     if params is None: # if none, fallback to default generator of the class instance
    #         if self.iterator is None:
    #             raise AssertionError('iterator is not defined. You must provide a parameter dict for the generator.')
    #         iterator = self.iterator
    #     else:
    #         _, iterator = self.build_custom_generator(return_generator=True, **params)
    #     ndarray_model_parameters = np.zeros((self.args.num_model_parameters))
    #     for i in range(num_samples):
    #         b = next(iterator) # sample contains a tuple of form (x, params, noise)
    #         x_i = np.expand_dims(b[0], axis=0).astype(np.float32) #creating minibatch size 1.
    #         noise_i = np.expand_dims(b[2], axis=0).astype(np.float32) #creating minibatch size 1.
    #         z_latent = self.model_obj.encoder(x_i, training=False)
    #         o_reconst = self.model_obj.aggregator(z_latent, training=False).numpy()
    #         ndarray_model_parameters = np.squeeze(o_reconst)
    #     return ndarray_model_parameters

    def decode(self, summary_stats):
        '''Function reconstruct timeseries for a given tuple of (latent_representations, noise vectors).'''
        o_theta = self.model_obj.aggregator(summary_stats, training=False).numpy()
        return o_theta

    def build_model(self):
        if not hasattr(self.args,'num_input_channels'):
            print(":: Warning: num_input_channels missing. This should be an old training file, or we have a problem.")
            #assert False, ":: Fatal: num_input_channels missing. # We should aim to this sanity check, 23 June 21
            setattr(self.args,'num_input_channels',1)
        self.model_obj = self.arch(ndims_latent=self.args.ndims_latent, len_timeseries=self.args.len_timeseries,
        num_input_channels=self.args.num_input_channels, num_noise_channels=self.args.num_noise_channels,
        ndims_output=self.args.num_model_parameters,ndims_free_latent_parameters=self.args.ndims_latent-self.args.num_model_parameters)

    def load_model(self, basename='model_best_ckpt'):
        ckpt = tf.train.Checkpoint(encoder=self.model_obj.encoder, aggregator=self.model_obj.aggregator)
        save_manager = tf.train.CheckpointManager(checkpoint=ckpt, directory=self.args.logdir, max_to_keep=3, checkpoint_name=basename)
        # Restore model weights
        ckptname = get_ckptname(logdir=self.args.logdir, id=basename)
        try:
            ckpt.restore(ckptname).assert_existing_objects_matched().expect_partial()
        except:
            raise AssertionError('Model weights with basename %s not found in logdir %s. Quitting.' % (basename, self.args.logdir))
        print('Model weights loaded from %s.' % ckptname)
        # ckpt.restore(save_manager.latest_checkpoint).expect_partial()
        # if save_manager.latest_checkpoint:
        #     print('Model weights loaded from %s.' % save_manager.latest_checkpoint)
        # else:
        #     raise AssertionError('Model weights with basename %s not found in logdir %s. Quitting.' % (basename, self.args.logdir))

class Sampler_train_Regressor4_tfv2_NLAR1_FC_bs5_OLD(Sampler_Regressor5_tfv2_NLAR1_FC):
    '''Class provides user friendly access to low-dimensional space for summary statistics analysis.
    Used in: train_Regressor4_tfv2_NLAR1_FC_bs5_OLD.py'''

    def sample(self, num_samples=10, return_noise_vectors=False, return_model_parameters=False, params=None):
        '''Function samples #num_samples observed vectors, then returns the mapped representation for each (size: [#num_samples, #stats]).
        if params is not None, existing generator is ignored and a new custom one is initialized.'''
        if params is None: # if none, fallback to default generator of the class instance
            if self.iterator is None:
                raise AssertionError('iterator is not defined. You must provide a parameter dict for the generator.')
            iterator = self.iterator
        else:
            _, iterator = self.build_custom_generator(return_generator=True, **params)

        summary_space = np.zeros((num_samples, self.args.ndims_latent))
        model_params = np.zeros((num_samples, self.args.num_model_parameters))
        ndarray_noise_timeseries = np.zeros((num_samples, self.args.len_timeseries, self.args.num_noise_channels))
        for i in range(num_samples):
            b = next(iterator) # sample contains a tuple of form (x, params, noise)
            x_i = np.expand_dims(b[0], axis=0).astype(np.float32) #creating minibatch size 1.
            noise_i = np.expand_dims(b[2], axis=0).astype(np.float32) #creating minibatch size 1.
            o_latent = self.model_obj.encoder(x_i, training=False).numpy()
            summary_space[i,...] = o_latent
            ndarray_noise_timeseries[i,...] = noise_i
            model_params[i,...] = b[1]
        elms_return = [summary_space]
        if return_noise_vectors:
            elms_return += [ndarray_noise_timeseries]
        if return_model_parameters:
            elms_return += [model_params]
        if len(elms_return) == 1:
            return elms_return[0]
        else:
            return elms_return

class Sampler_Regressor4_tfv2_NLAR1_FC_multiBatch(Sampler_AE_tfv2_NLAR1_BiLSTM):
    '''Class provides user friendly access to low-dimensional space for summary statistics analysis.
    Used in: train_Regressor4_tfv2_NLAR1_FC_multiBatch.py'''

    def sample(self, num_samples=10, return_noise_vectors=False, return_model_parameters=False, return_observations=False, params=None):
        '''Function samples #num_samples observed vectors, then returns the mapped representation for each (size: [#num_samples, #stats]).
        if params is not None, existing generator is ignored and a new custom one is initialized.'''
        if params is None: # if none, fallback to default generator of the class instance
            if self.iterator is None:
                raise AssertionError('iterator is not defined. You must provide a parameter dict for the generator.')
            iterator = self.iterator
        else:
            _, iterator = self.build_custom_generator(return_generator=True, **params)

        l_batches = []
        for i in range(num_samples):
            l_batches.append(next(iterator)) # sample contains a tuple of form (x, params, noise)
        observations, model_params, noise = [tf.stack(it, axis=0) for it in list(zip(*l_batches))]
        summary_space = self.model_obj.encoder(observations, training=False).numpy()
        ndarray_noise_timeseries = noise.numpy()
        model_params = model_params.numpy()
        observations = np.squeeze(observations.numpy())
        elms_return = [summary_space]
        if return_noise_vectors:
            elms_return += [ndarray_noise_timeseries]
        if return_model_parameters:
            elms_return += [model_params]
        if return_observations:
            elms_return += [observations]
        if len(elms_return) == 1:
            return elms_return[0]
        else:
            return elms_return

    def encode(self, samples):
        '''Function generates latent representations of the given observations (samples).
        samples should be of shape: [num_samples, len_timeseries, num_channels=1]'''
        o_latent = self.model_obj.encoder(samples, training=False)
        return o_latent

    def decode(self, summary_stats):
        '''Function reconstruct timeseries for a given tuple of (latent_representations, noise vectors).'''
        o_theta = self.model_obj.aggregator(summary_stats, training=False).numpy()
        return o_theta

    def build_model(self):
        if not hasattr(self.args,'num_input_channels'):
            print(":: Warning: num_input_channels missing. This should be an old training file, or we have a problem.")
            #assert False, ":: Fatal: num_input_channels missing. # We should aim to this sanity check, 23 June 21
            setattr(self.args,'num_input_channels',1)
        self.model_obj = self.arch(mb_of_same_realizations=self.mb_of_same_realizations,ndims_latent=self.args.ndims_latent,
        len_timeseries=self.args.len_timeseries, num_input_channels=self.args.num_input_channels,
        num_noise_channels=self.args.num_noise_channels, ndims_output=self.args.num_model_parameters,
        ndims_free_latent_parameters=self.args.ndims_latent-self.args.num_model_parameters)

    def load_model(self, basename='model_best_ckpt'):
        self.model_obj.encoder.summary()
        self.model_obj.aggregator.summary()
        ckpt = tf.train.Checkpoint(encoder=self.model_obj.encoder, aggregator=self.model_obj.aggregator)
        save_manager = tf.train.CheckpointManager(checkpoint=ckpt, directory=self.args.logdir, max_to_keep=3, checkpoint_name=basename)
        # Restore model weights
        ckptname = get_ckptname(logdir=self.args.logdir, id=basename)
        try:
            ckpt.restore(ckptname).assert_existing_objects_matched().expect_partial()
        except:
            raise AssertionError('Model weights with basename %s not found in logdir %s. Quitting.' % (basename, self.args.logdir))
        print('Model weights loaded from %s.' % ckptname)
        # ckpt.restore(save_manager.latest_checkpoint).expect_partial()
        # if save_manager.latest_checkpoint:
        #     print('Model weights loaded from %s.' % save_manager.latest_checkpoint)
        # else:
        #     raise AssertionError('Model weights with basename %s not found in logdir %s. Quitting.' % (basename, self.args.logdir))

    def build_custom_generator(self, return_generator=False, **kwargs):
        '''See inside DataGenerator_SolarDynamo for the parameters one can modify for custom generator.'''
        if 'prng' in kwargs:
            prng = kwargs.pop('prng')
        else:
            prng = self.prng
        x_0 = kwargs.pop('x_0', 0.25)
        c_lims = kwargs.pop('c_lims', [4.2, 5.8])
        sigma_lims = kwargs.pop('sigma_lims', [0.005, 0.025])
        #############################################
        # fn = lambda c, x_old, sigma, epsilon: c * x_old**2 * (1. - x_old) + sigma * epsilon
        # # fn = lambda c, x_old, sigma, epsilon: c * x_old**2 * (np.exp(-x_old)) + sigma * epsilon
        #############################################
        # func = kwargs.pop('fn', fn)
        batch_size_requested = kwargs.pop('batch_size', None)
        if batch_size_requested is not None:
            if batch_size_requested != self.mb_of_same_realizations:
                print('A separate "batch_size" parameter sent to the data generator. This is expected to match mb_of_same_realizations (N), but it does not, hence it is ignored.')
        generator = wraps.generators.DataGenerator_NLAR1_Simplified_BatchSampler(len_timeseries=self.args.len_timeseries, batch_size=self.mb_of_same_realizations, prng=prng, x_0=x_0, c_lims=c_lims, sigma_lims=sigma_lims, fn=self.user_model, **kwargs)
        #generator = wraps.generators.DataGenerator_NLAR1_Simplified_BatchSampler(len_timeseries=self.args.len_timeseries, batch_size=self.mb_of_same_realizations, prng=prng, x_0=x_0, c_lims=c_lims, sigma_lims=sigma_lims, fn=func, **kwargs) ## Although we can request mb_of_same_realizations custom, it's safer to fix it to initial setup, which will hopefully match the number used in training of the model.
        iterator = generator.__iter__()
        if return_generator:
            return generator, iterator
        else:
            self.generator = generator
            self.iterator = iterator

class Sampler_Regressor4_tfv2_Superflex_FC_multiBatch(Sampler_AE_tfv2_NLAR1_BiLSTM):
    '''you know I mean'''

    def sample(self, num_samples=10, return_noise_vectors=False, return_model_parameters=False, return_observations=False, params=None):

        assert False, ":: Fatal: I am unaware that sample is called for spux and superflex"

    def encode(self, samples):
        '''Function generates latent representations of the given observations (samples).
        samples should be of shape: [num_samples, len_timeseries, num_channels=1]'''
        o_latent = self.model_obj.encoder(samples, training=False)
        return o_latent

    def reconstruct(self, num_samples=10, params=None):
        '''Function samples #num_samples p_n vectors, then returns the reconstructions of them from their latent representation for each (size: [#num_samples, len_timeseries]
        if params is not None, existing generator is ignored and a new custom one is initialized.'''

        assert False, ":: Fatal: I am unaware that reconstruct is called for spux and superflex"

    def decode(self, tuple_summary_and_noise):
        '''Function reconstruct timeseries for a given tuple of (latent_representations, noise vectors).'''

        assert False, ":: Fatal: I am unaware that decode is called for spux and superflex"

    #def check_hyper_params(self):
        # used and inherited

    def build_model(self):
        if not hasattr(self.args,'num_input_channels'):
            print(":: Warning: num_input_channels missing. This should be an old training file, or we have a problem.")
            #assert False, ":: Fatal: num_input_channels missing. # We should aim to this sanity check, 23 June 21
            setattr(self.args,'num_input_channels',1)
        self.model_obj = self.arch(mb_of_same_realizations=self.mb_of_same_realizations,ndims_latent=self.args.ndims_latent,
        len_timeseries=self.args.len_timeseries, num_input_channels=self.args.num_input_channels,
        num_noise_channels=self.args.num_noise_channels, ndims_output=self.args.num_model_parameters,
        ndims_free_latent_parameters=self.args.ndims_latent-self.args.num_model_parameters)

    def load_model(self, basename='model_best_ckpt'):
        ckpt = tf.train.Checkpoint(encoder=self.model_obj.encoder, aggregator=self.model_obj.aggregator)
        save_manager = tf.train.CheckpointManager(checkpoint=ckpt, directory=self.args.logdir, max_to_keep=3, checkpoint_name=basename)
        # Restore model weights
        ckptname = get_ckptname(logdir=self.args.logdir, id=basename)
        try:
            ckpt.restore(ckptname).assert_existing_objects_matched().expect_partial()
        except:
            raise AssertionError('Model weights with basename %s not found in logdir %s. Quitting.' % (basename, self.args.logdir))
        print('Model weights loaded from %s.' % ckptname)

    def build_custom_generator(self, return_generator=False, **kwargs):
        assert False, ":: Fatal: I am unaware that build_custom_generator is called for spux and superflex"

# not working
#class Sampler_Regressor4_tfv2_NLAR1_FC_multiBatch(Sampler_Regressor4_tfv2_SolarDynamo_FC_multiBatch):
#    '''Class provides user friendly access to low-dimensional space for summary statistics analysis.
#    Used in: train_Regressor4_tfv2_NLAR1_FC_multiBatch.py'''
#
#    def build_custom_generator(self, return_generator=False, **kwargs):
#        '''See inside DataGenerator_SolarDynamo for the parameters one can modify for custom generator.'''
#        if 'prng' in kwargs:
#            prng = kwargs.pop('prng')
#        else:
#            prng = self.prng
#        x_0 = kwargs.pop('x_0', 0.25)
#        c_lims = kwargs.pop('c_lims', [4.2, 5.8])
#        sigma_lims = kwargs.pop('sigma_lims', [0.005, 0.025])
#        #############################################
#        # fn = lambda c, x_old, sigma, epsilon: c * x_old**2 * (1. - x_old) + sigma * epsilon
#        # # fn = lambda c, x_old, sigma, epsilon: c * x_old**2 * (np.exp(-x_old)) + sigma * epsilon
#        #############################################
#        # func = kwargs.pop('fn', fn)
#        batch_size_requested = kwargs.pop('batch_size', None)
#        if batch_size_requested is not None:
#            if batch_size_requested != self.mb_of_same_realizations:
#                print('A separate "batch_size" parameter sent to the data generator. This is expected to match mb_of_same_realizations (N), but it does not, hence it is ignored.')
#        #generator = wraps.generators.DataGenerator_NLAR1_Simplified_BatchSampler(len_timeseries=self.args.len_timeseries, batch_size=self.mb_of_same_realizations, prng=prng, x_0=x_0, c_lims=c_lims, sigma_lims=sigma_lims, fn=func, **kwargs) ## Although we can request mb_of_same_realizations custom, it's safer to fix it to initial setup, which will hopefully match the number used in training of the model.
#        generator = wraps.generators.DataGenerator_NLAR1_Simplified_BatchSampler(len_timeseries=self.args.len_timeseries, batch_size=self.mb_of_same_realizations, prng=prng, x_0=x_0, c_lims=c_lims, sigma_lims=sigma_lims, fn=self.user_model, **kwargs)
#        iterator = generator.__iter__()
#        if return_generator:
#            return generator, iterator
#        else:
#            self.generator = generator
#            self.iterator = iterator
#
