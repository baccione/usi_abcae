#Author: Firat Ozdemir, October 2019, firat.ozdemir@datascience.ch
import tensorflow as tf
from tensorflow.python.ops import math_ops

class LearningRateScheduleExponentialDecayWithLinearWarmup(tf.keras.optimizers.schedules.LearningRateSchedule):
    @tf.function
    def __init__(self, steps_warmup, initial_learning_rate, decay_steps, decay_rate, staircase):
        #print(super(LearningRateScheduleExponentialDecayWithLinearWarmup, self).__init__()) #None LearningRateSchedule has no __init__ ...
        super(LearningRateScheduleExponentialDecayWithLinearWarmup, self).__init__()
        self.steps_warmup = steps_warmup
        self.initial_learning_rate = initial_learning_rate
        self.warmup_start_lr = 1e-9
        self.slope = (self.initial_learning_rate - self.warmup_start_lr) / self.steps_warmup
        self.lr_expdecay = tf.keras.optimizers.schedules.ExponentialDecay(initial_learning_rate=initial_learning_rate, decay_steps=decay_steps, decay_rate=decay_rate, staircase=staircase)
    @tf.function
    def linear_warmup(self, step):
        return self.slope * (math_ops.cast(step, tf.float32)+1.) + self.warmup_start_lr
    @tf.function
    def __call__(self, step):
        if step < self.steps_warmup:
            return self.linear_warmup(step)
        else:
            #if (step==self.steps_warmup):
                #tf.print('zio: ',self.linear_warmup(step),self.lr_expdecay(step-self.steps_warmup)) # they are continuous
            return self.lr_expdecay(step-self.steps_warmup)

def convert_timeseries_to_images(gt, pred, y_lim, len_timeseries, name):
    '''Function expects gt and pred to be tensors. It will create a tensorboard friendly image out of timeseries prediction.
    gt shape: [len_timeseries, 1]
    pred shape: [len_timeseries, 1]
    y_lim: #rows allocated for gt and pred separately (higher -> more resolution)
    len_timeseries: #cols
    im_tboard: shape: [y_lim * 2 + 10, len_timeseries], Concatenated image of pred and gt with a dark split region in between
    '''
    y_max = y_lim -1

    im_shp = (y_max + 1, len_timeseries)
    #     im = tf.convert_to_tensor(imtmp)

    # gt_v = tf.Variable(initial_value=tf.ones(im_shp, dtype=tf.uint8) * 255, trainable=False, name='gt', dtype=tf.uint8)
    # pred_v = tf.Variable(initial_value=tf.ones(im_shp, dtype=tf.uint8) * 255, trainable=False, name='pred', dtype=tf.uint8)
    with tf.variable_scope(""):
        gt_v = tf.get_variable(name='gt_'+name, shape=(im_shp), dtype=tf.uint8, trainable=False)
        pred_v = tf.get_variable(name='pred_'+name, shape=(im_shp), dtype=tf.uint8, trainable=False)
        error_v = tf.get_variable(name='MAE_pred_gt_'+name, shape=(im_shp), dtype=tf.uint8, trainable=False)

    val_max = tf.reduce_max([gt, pred])
    val_min = tf.reduce_min([gt, pred])

    gt_s = (gt - val_min) / (val_max - val_min)
    pred_s = (pred - val_min) / (val_max - val_min)

    # after scaling, min val: 0, max val: 1. On image 0-> y_max, 1-> 0
    gt_p = (1 - gt_s) * y_max
    pred_p = (1 - pred_s) * y_max
    error_p = (1 - tf.abs(pred_s - gt_s)) * y_max

    gt_p = tf.reshape(tf.cast(gt_p, dtype=tf.int32), (len_timeseries, 1))
    pred_p = tf.reshape(tf.cast(pred_p, dtype=tf.int32), (len_timeseries, 1))
    error_p = tf.reshape(tf.cast(error_p, dtype=tf.int32), (len_timeseries, 1))

    col_ind = tf.reshape(tf.range(len_timeseries, dtype=tf.int32), (len_timeseries, 1))

    gt_inds = tf.concat([gt_p, col_ind], axis=1)
    pred_inds = tf.concat([pred_p, col_ind], axis=1)
    error_inds = tf.concat([error_p, col_ind], axis=1)

    gt_v = gt_v.assign(255 * tf.ones_like(gt_v))
    pred_v = pred_v.assign(255 * tf.ones_like(pred_v))
    error_v = error_v.assign(255 * tf.ones_like(error_v))

    gt_v = tf.scatter_nd_update(gt_v, gt_inds, tf.zeros((len_timeseries,), dtype=tf.uint8))
    pred_v = tf.scatter_nd_update(pred_v, pred_inds, tf.zeros((len_timeseries,), dtype=tf.uint8))
    error_v = tf.scatter_nd_update(error_v, error_inds, tf.zeros((len_timeseries,), dtype=tf.uint8))

    split = tf.zeros((10, len_timeseries), dtype=tf.uint8)

    im_tboard = tf.concat([pred_v, split, gt_v, split, error_v], axis=0)

    return im_tboard
