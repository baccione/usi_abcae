import numpy as np
from src.wraps.QDC import QDC

def generator_QDC(n, low_s, high_s, mu_a, K):
    noise0 = np.random.exponential(1, n)
    noise1 = np.random.uniform(0.0, 1.0, n)

    a = np.cumsum(noise0 / mu_a)

    s = noise1 * (high_s - low_s) + low_s

    output_QDC = QDC(a, s, K)
    noise_n = np.stack([noise0, noise1], axis = -1)

    output = {
        "p_n": output_QDC["departures"],
        "noise_n": noise_n,
        "p0": 1,
        "alpha1": mu_a,
        "delta": low_s,
        "epsilon_max": high_s,
        "alpha_n": output_QDC["departures"],
        "epsilon_n": output_QDC["departures"],
        "f_n": output_QDC["departures"]
    }

    return output



class DataGenerator_Queue:
    def __init__(self, len_timeseries=20, **kwargs):
        self.prng = kwargs.get('prng', np.random.RandomState(seed=1923))
        self.mu_a_min = kwargs.get('mu_a_min', 0)
        self.mu_a_max = kwargs.get('mu_a_max', 10)
        self.mu_a = kwargs.get('mu_a', None)
        self.low_s = kwargs.get('low_s', None)
        self.high_s = kwargs.get('high_s', None)
        self.K = kwargs.get('K', 1)
        self.len_timeseries = int(len_timeseries)

    def __iter__(self):

        while True:
            if self.mu_a is None:
                mu_a = self.prng.uniform(low=self.mu_a_min, high=self.mu_a_max)
                low_s = self.prng.uniform(low=self.mu_a_min, high=self.mu_a_max)
                high_s = self.prng.uniform(low=self.mu_a_min + low_s, high=self.mu_a_max + low_s)
            else:
                mu_a = self.mu_a
                low_s = self.low_s
                high_s = self.high_s

            output = generator_QDC(self.len_timeseries, low_s, high_s, mu_a, self.K)
            yield output


class DataGenerator_Queue_Simplified:
    '''Copy of DataGenerator_Queue, with minor change for the structure of yielded output, and reshaping observation to explicitly have 1 channel.'''
    def __init__(self, len_timeseries=20, **kwargs):
        self.prng = kwargs.get('prng', np.random.RandomState(seed=1923))
        self.mu_a_min = kwargs.get('mu_a_min', 0)
        self.mu_a_max = kwargs.get('mu_a_max', 10)
        self.mu_a = kwargs.get('mu_a', None)
        self.low_s = kwargs.get('low_s', None)
        self.high_s = kwargs.get('high_s', None)
        self.K = kwargs.get('K', 1)
        self.len_timeseries = int(len_timeseries)

    def __iter__(self):

        while True:
            if self.mu_a is None:
                mu_a = self.prng.uniform(low=self.mu_a_min, high=self.mu_a_max)
                low_s = self.prng.uniform(low=self.mu_a_min, high=self.mu_a_max)
                high_s = self.prng.uniform(low=self.mu_a_min + low_s, high=self.mu_a_max + low_s)
            else:
                mu_a = self.mu_a
                low_s = self.low_s
                high_s = self.high_s

            output = generator_QDC(self.len_timeseries, low_s, high_s, mu_a, self.K)
            x = np.expand_dims(output['p_n'], -1) # observed vector # [len_timeseries, 1]
            params = (output['alpha1'], output['delta'], output['epsilon_max']) # tuple of scalars
            noise = output['noise_n'] # [len_timeseries, N]
            output = (x, params, noise) # in the simplified version, all samples are yielded as a tuple of the form (observations, parameters, concatenated noise vectors)
            yield output

class DataGenerator_Queue_DeltaX_Simplified:
    '''Copy of DataGenerator_Queue, with minor change for the structure of yielded output, and reshaping observation to explicitly have 1 channel.'''
    def __init__(self, len_timeseries=20, **kwargs):
        self.prng = kwargs.get('prng', np.random.RandomState(seed=1923))
        self.mu_a_min = kwargs.get('mu_a_min', 0)
        self.mu_a_max = kwargs.get('mu_a_max', 10)
        self.mu_a = kwargs.get('mu_a', None)
        self.low_s = kwargs.get('low_s', None)
        self.high_s = kwargs.get('high_s', None)
        self.K = kwargs.get('K', 1)
        self.len_timeseries = int(len_timeseries)

    def __iter__(self):

        while True:
            if self.mu_a is None:
                mu_a = self.prng.uniform(low=self.mu_a_min, high=self.mu_a_max)
                low_s = self.prng.uniform(low=self.mu_a_min, high=self.mu_a_max)
                high_s = self.prng.uniform(low=self.mu_a_min + low_s, high=self.mu_a_max + low_s)
            else:
                mu_a = self.mu_a
                low_s = self.low_s
                high_s = self.high_s

            output = generator_QDC(self.len_timeseries, low_s, high_s, mu_a, self.K)
            x = output['p_n']
            dx = x.copy()
            dx[1:] -= dx[:-1].copy() # instead of working with x, work with delta(x)
            x = np.expand_dims(dx, -1) # observed vector # [len_timeseries, 1]
            params = (output['alpha1'], output['delta'], output['epsilon_max']) # tuple of scalars
            noise = output['noise_n'] # [len_timeseries, N]
            output = (x, params, noise) # in the simplified version, all samples are yielded as a tuple of the form (observations, parameters, concatenated noise vectors)
            yield output
