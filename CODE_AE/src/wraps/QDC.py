import numpy as np
import sys

is_sorted = lambda a: np.all(a[:-1] <= a[1:])


def QDC(a, s, K):
    n = a.shape[0]
    if n != s.shape[0]:
        sys.exit("different lengths!")

    a_is_unsorted = not is_sorted(a)
    if a_is_unsorted:
        idx = np.argsort(a)
        a = np.sort(a)
        s = s[idx]

    p = np.zeros(n, dtype=int)
    b = np.zeros(K)
    d = np.zeros(n)

    for i in range(0, n):
        p[i] = np.argmin(b)
        b[p[i]] = max(a[i], b[p[i]]) + s[i]
        d[i] = b[p[i]]

    if a_is_unsorted:
        new_idx = np.argsort(idx)
        d = d[new_idx]
        p = p[new_idx]

    output = {"departures": d, "server_assignments": p}

    return output
