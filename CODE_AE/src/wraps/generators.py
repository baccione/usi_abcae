# Author: Firat Ozdemir, October 2019, firat.ozdemir@datascience.ch

import numpy as np
import src.tools.utils as utils
import src.modls.blowfly as blowfly
#import time, sys


class DataGenerator_SolarDynamo_Simplified:
    def __init__(self, len_timeseries=200, **kwargs):
        self.prng = kwargs.get('prng', np.random.RandomState(seed=1923))
        self.len_timeseries = int(len_timeseries)
        self.p0 = kwargs.get('p0', 1.0)
        # self.p0_std = kwargs.get('p0_std', 0.1)
        self.alpha1_min = kwargs.get('alpha1_min', 1.3)
        self.alpha1_max = kwargs.get('alpha1_max', 1.5)
        self.alpha1_lims = kwargs.get('alpha1_lims', None) # overwrites alpha1_min and alpha1_max
        if self.alpha1_lims is not None:
            self.alpha1_min, self.alpha1_max = self.alpha1_lims[0], self.alpha1_lims[1]
        self.delta_min = kwargs.get('delta_min', 0.0)
        self.delta_max = kwargs.get('delta_max', 0.35)
        self.delta_lims = kwargs.get('delta_lims', None) # overwrites delta_min and delta_max
        if self.delta_lims is not None:
            self.delta_min, self.delta_max = self.delta_lims[0], self.delta_lims[1]
        ###################################################################
        ###################################################################
        # Here is the original version:
        # self.epsilon_max = kwargs.get('epsilon_max', 0.5)
        ###################################################################
        ###################################################################
        # Here is simone modified version:
        self.eps_min = kwargs.get('eps_min', 0.0)
        self.eps_max = kwargs.get('eps_max', 0.35)
        self.eps_lims = kwargs.get('eps_lims', None)  # overwrites delta_min and delta_max
        if self.eps_lims is not None:
            self.eps_min, self.eps_max = self.eps_lims[0], self.eps_lims[1]
        ###################################################################
        ###################################################################
        self.alpha1 = kwargs.get('alpha1', None)
        self.delta = kwargs.get('delta', None)
        if self.alpha1 is not None and self.delta is not None:
            self.overwrite_random_alpha = True
            print('Overwriting alpha1 and delta. They will NOT be sampled from a uniform distribution.')
        else:
            self.overwrite_random_alpha = False

    def __iter__(self):
        while True:
            # sample p0, alpha1,alpha2,epsilon
            p0 = self.p0
            if self.overwrite_random_alpha:
                alpha1 = self.alpha1
                delta = self.delta
                epsilon_max = self.epsilon_max
            else:
                alpha1 = self.prng.uniform(low=self.alpha1_min, high=self.alpha1_max)
                delta = self.prng.uniform(low=self.delta_min, high=self.delta_max)
                ###################################################################
                ###################################################################
                # Here is the original version:
                # epsilon_max = self.prng.uniform(low=0, high=self.epsilon_max)
                ###################################################################
                ###################################################################
                # Here is my modified version:
                epsilon_max = self.prng.uniform(low=self.eps_min, high=self.eps_max)
                ###################################################################
                ###################################################################
            batch = utils.sample_pn_timeseries_v2(p0=p0, alpha_min=alpha1, alpha_max=alpha1+delta, epsilon_max=epsilon_max,
                                                   prng=self.prng, len_timeseries=self.len_timeseries)
            noise_n = np.stack([batch['a'], batch['e']], axis=-1)
            x = np.expand_dims(batch['p'], 1) # [len_timeseries, 1]
            params = (alpha1, delta, epsilon_max) # tuple of scalars
            noise = np.stack([batch['a'], batch['e']], axis=-1) # [timeseries, N]
            d = (x, params, noise)
            yield d

class DataGenerator_SolarDynamo_Simplified_BatchSampler(DataGenerator_SolarDynamo_Simplified):
    def __init__(self, len_timeseries=200, batch_size=None, **kwargs):
        self.batch_size = batch_size
        super().__init__(len_timeseries=len_timeseries, **kwargs)

    def __iter__(self):
        assert self.batch_size is not None # at this point, batch size must have been already defined.
        while True:
            # sample p0, alpha1,alpha2,epsilon
            p0 = self.p0
            if self.overwrite_random_alpha:
                alpha1 = self.alpha1
                delta = self.delta
                epsilon_max = self.epsilon_max
            else:
                alpha1 = self.prng.uniform(low=self.alpha1_min, high=self.alpha1_max)
                delta = self.prng.uniform(low=self.delta_min, high=self.delta_max)
                ###################################################################
                ###################################################################
                # Here is the original version:
                # epsilon_max = self.prng.uniform(low=0, high=self.epsilon_max)
                ###################################################################
                ###################################################################
                # Here is my modified version:
                epsilon_max = self.prng.uniform(low=self.eps_min, high=self.eps_max)
                ###################################################################
                ###################################################################
            l_observation = np.empty((self.batch_size, self.len_timeseries, 1)) # shape [batch_size, timeseries, 1]
            l_noise = np.empty((self.batch_size, self.len_timeseries, 2)) # [batch_size, len_timeseries, 2]
            params = (alpha1, delta, epsilon_max) # tuple of scalars
            for i in range(self.batch_size):
                sample = utils.sample_pn_timeseries_v2(p0=p0, alpha_min=alpha1, alpha_max=alpha1+delta,
                                                       epsilon_max=epsilon_max, prng=self.prng,
                                                       len_timeseries=self.len_timeseries)
                l_observation[i,...] = np.expand_dims(sample['p'], 1)
                l_noise[i,...] = np.stack([sample['a'], sample['e']], axis=-1)
            d = (l_observation, params, l_noise)
            yield d

class DataGenerator_SolarDynamo:
    def __init__(self, len_timeseries=1000, **kwargs):
        self.prng = kwargs.get('prng', np.random.RandomState(seed=1923))
        self.len_timeseries = int(len_timeseries)
        self.p0 = kwargs.get('p0', 1.0)
        # self.p0_std = kwargs.get('p0_std', 0.1)
        self.alpha1_min = kwargs.get('alpha1_min', 1.3)
        self.alpha1_max = kwargs.get('alpha1_max', 1.5)
        self.delta_min = kwargs.get('delta_min', 0.0)
        self.delta_max = kwargs.get('delta_max', 0.35)
        self.epsilon_max = kwargs.get('epsilon_max', 0.5)
        self.alpha1 = kwargs.get('alpha1', None)
        self.delta = kwargs.get('delta', None)
        if self.alpha1 is not None and self.delta is not None:
            self.overwrite_random_alpha = True
            print('Overwriting alpha1 and delta. They will NOT be sampled from a uniform distribution.')
        else:
            self.overwrite_random_alpha = False

    def __iter__(self):
        while True:
            # sample p0, alpha1,alpha2,epsilon
            p0 = self.p0
            if self.overwrite_random_alpha:
                alpha1 = self.alpha1
                delta = self.delta
                epsilon_max = self.epsilon_max
            else:
                alpha1 = self.prng.uniform(low=self.alpha1_min, high=self.alpha1_max)
                delta = self.prng.uniform(low=self.delta_min, high=self.delta_max)
                epsilon_max = self.prng.uniform(low=0, high=self.epsilon_max)
            batch = utils.sample_pn_timeseries_v2(p0=p0, alpha_min=alpha1, alpha_max=alpha1+delta,
                                                  epsilon_max=epsilon_max,prng=self.prng,
                                                  len_timeseries=self.len_timeseries)
            noise_n = np.stack([batch['a'], batch['e']], axis=-1)
            d = {'p0': p0, 'alpha1': alpha1, 'delta': delta, 'epsilon_max': epsilon_max, 'p_n': batch['p'],
                 'noise_n': noise_n, 'alpha_n': batch['a'], 'epsilon_n': batch['e'], 'f_n': batch['f']}
            yield d

class DataGenerator:
    def __init__(self, len_timeseries=1000, **kwargs):
        self.prng = kwargs.get('prng', np.random.RandomState(seed=1923))
        self.len_timeseries = int(len_timeseries)
        self.p0_mean = kwargs.get('p0_mean', 1.0)
        self.p0_std = kwargs.get('p0_std', 0.1)
        self.alpha1_min = kwargs.get('alpha1_min', 1.3)
        self.alpha1_max = kwargs.get('alpha1_max', 1.5)
        # self.alpha2_min = alpha1
        self.alpha2_max = kwargs.get('alpha2_max', 1.65)
        self.epsilon_max = kwargs.get('epsilon_max', 0.5)
        self.alpha1 = kwargs.get('alpha1', None)
        self.alpha2 = kwargs.get('alpha2', None)
        self.timeseries_version = kwargs.get('timeseries_version', None)
        if self.alpha1 is not None and self.alpha2 is not None:
            self.overwrite_random_alpha = True
            print('Warning, alpha1 and alpha2 will NOT be random.')
        else:
            self.overwrite_random_alpha = False

    def __iter__(self):
        while True:
            # sample p0, alpha1,alpha2,epsilon
            p0 = self.prng.normal(loc=self.p0_mean, scale=self.p0_std)
            if self.overwrite_random_alpha:
                alpha1 = self.alpha1
                alpha2 = self.alpha2
                epsilon_max = self.epsilon_max
            else:
                alpha1 = self.prng.uniform(low=self.alpha1_min, high=self.alpha1_max)
                alpha2 = self.prng.uniform(low=alpha1, high=self.alpha2_max)
                epsilon_max = self.prng.uniform(low=0, high=self.epsilon_max)
            if self.timeseries_version == 'old':
                batch = utils.sample_pn_timeseries(p0=p0, alpha_min=alpha1, alpha_max=alpha2, epsilon_max=epsilon_max,
                                                      prng=self.prng, len_timeseries=self.len_timeseries)
            else:
                batch = utils.sample_pn_timeseries_v2(p0=p0, alpha_min=alpha1, alpha_max=alpha2, epsilon_max=epsilon_max,
                                                   prng=self.prng, len_timeseries=self.len_timeseries)
            d = {'p0': p0, 'alpha1': alpha1, 'alpha2': alpha2, 'epsilon': epsilon_max,
                 'p_n': batch['p'], 'alpha_n': batch['a'], 'epsilon_n': batch['e'], 'f_n': batch['f']}
            yield d

class DataGenerator_NLAR1_Simplified:
    def __init__(self, len_timeseries=200, **kwargs):
        self.prng = kwargs.get('prng', np.random.RandomState(seed=1923))
        self.len_timeseries = int(len_timeseries)
        self.x_0 = kwargs.get('x_0', 0.7)
        self.c_lims = kwargs.get('c_lims', [4.2, 5.8])
        self.sigma_lims = kwargs.get('sigma_lims', [0.0, 0.025])
        self.cast_out_dtype = kwargs.get('cast_out_dtype', None)
        self.func = kwargs.get('fn', None)
        if self.func is not None:
            self.fn = self.func
        else:
            self.fn = lambda c, x_old, sigma, epsilon: c * x_old**2 * (1. - x_old) + sigma * epsilon

    def draw_c(self):
        if not (isinstance(self.c_lims, tuple) or isinstance(self.c_lims, list)):
            raise AssertionError('c_lims has to be a list or tuple. Found %s.' % (type(self.c_lims)))
        if len(self.c_lims) != 2:
            raise AssertionError('c_lims has to be a list of 2 components [min_c, max_c]. Found c_lims length: %d' % len(self.c_lims))
        return float(self.prng.uniform(low=self.c_lims[0], high=self.c_lims[1]))

    def draw_sigma(self):
        if not (isinstance(self.sigma_lims, tuple) or isinstance(self.sigma_lims, list)):
            raise AssertionError('sigma_lims has to be a list or tuple. Found %s.' % (type(self.sigma_lims)))
        if len(self.sigma_lims) != 2: #lower & upper bound
            raise AssertionError('sigma_lims has to be a list of 2 components [min_sigma, max_sigma]. Found sigma_lims length: %d' % len(self.sigma_lims))
        return float(self.prng.uniform(low=self.sigma_lims[0], high=self.sigma_lims[1]))

    # def fn(self, c, x_old, sigma, epsilon):
    #    '''This function is defined inside __init__ as a one liner.'''
    #     return c * x_old**2 * (1. - x_old) + sigma * epsilon

    def sample_series(self, x0, c0, sigma0):
        d = {'x_i': np.zeros((self.len_timeseries,)),
             'c_i':np.zeros((self.len_timeseries,)),
             'sigma_i': np.zeros((self.len_timeseries,)),
             'epsilon_i': np.zeros((self.len_timeseries,)),
             'c_0': float(c0),
             'sigma_0': float(sigma0),
             'x_0': float(x0),
             }
        x_old = x0
        c = c0
        sigma = sigma0
        for i in range(self.len_timeseries):
            epsilon = self.prng.normal(loc=0, scale=1)
            # if self.cast_out_dtype is not None:
            #     epsilon = np.array(epsilon, dtype=self.cast_out_dtype)
            d['x_i'][i] = x_old
            d['c_i'][i] = c              # always the same value, a little wasteful or necessary for tf?
            d['sigma_i'][i] = sigma      # always the same value, a little wasteful or necessary for tf?
            d['epsilon_i'][i] = epsilon
            # get the next timeseries item.
            x_new = self.fn(c=c, x_old=x_old, sigma=sigma, epsilon=epsilon)
            # update parameters (when needed)
            x_old = x_new
        return d

    def __iter__(self):
        while True:
            #start = time.time()
            x0 = self.x_0
            c0 = self.draw_c()
            sigma0 = self.draw_sigma()
            sample = self.sample_series(x0=x0, c0=c0, sigma0=sigma0)
            #
            noise = np.expand_dims(sample['epsilon_i'], -1) # shape [timeseries, 1]
            params = (c0, sigma0) # parameters (c, sigma)
            x = np.expand_dims(sample['x_i'], 1) # [len_timeseries, 1]
            if self.cast_out_dtype is not None:
                noise = np.array(noise, dtype=self.cast_out_dtype)
                x = np.array(x, dtype=self.cast_out_dtype)
                params = (np.array(c0, dtype=self.cast_out_dtype), np.array(sigma0, dtype=self.cast_out_dtype))
            d = (x, params, noise)
            #end = time.time()
            #print('exe time __iter__:',end - start)
            #sys.stdout.flush()
            yield d

class DataGenerator_NLAR1_Simplified_BatchSampler(DataGenerator_NLAR1_Simplified):
    def __init__(self, len_timeseries=200, batch_size=None, **kwargs):
        self.batch_size = batch_size
        super().__init__(len_timeseries=len_timeseries, **kwargs)

    def __iter__(self):
        assert self.batch_size is not None # at this point, batch size must have been already defined.
        while True:
            x0 = self.x_0
            c0 = self.draw_c()
            sigma0 = self.draw_sigma()
            l_observation = np.empty((self.batch_size, self.len_timeseries, 1)) # shape [batch_size, timeseries, 1]
            l_noise = np.empty((self.batch_size, self.len_timeseries, 1)) # [batch_size, len_timeseries, 1]
            params = (c0, sigma0) # parameters (c, sigma)
            for i in range(self.batch_size):
                sample = self.sample_series(x0=x0, c0=c0, sigma0=sigma0)
                l_observation[i, ...] = np.expand_dims(sample['x_i'], 1)
                l_noise[i,...] = np.expand_dims(sample['epsilon_i'], -1)
            d = (l_observation, params, l_noise)
            yield d

class DataGenerator_NLAR1_Extended:
    def __init__(self, len_timeseries=200, **kwargs):
        self.prng = kwargs.get('prng', np.random.RandomState(seed=1923))
        self.len_timeseries = int(len_timeseries)
        self.x_0 = kwargs.get('x_0', 0.25)
        self.c_lims = kwargs.get('c_lims', [2.0, 8.0])
        self.sigma_lims = kwargs.get('sigma_lims', [0.005, 0.250])

    def draw_c(self):
        if not (isinstance(self.c_lims, tuple) or isinstance(self.c_lims, list)):
            raise AssertionError('c_lims has to be a list or tuple. Found %s.' % (type(self.c_lims)))
        if len(self.c_lims) != 2:
            raise AssertionError(
                'c_lims has to be a list of 2 components [min_c, max_c]. Found c_lims length: %d' % len(self.c_lims))
        return float(self.prng.uniform(low=self.c_lims[0], high=self.c_lims[1]))

    def draw_sigma(self):
        if not (isinstance(self.sigma_lims, tuple) or isinstance(self.sigma_lims, list)):
            raise AssertionError('sigma_lims has to be a list or tuple. Found %s.' % (type(self.sigma_lims)))
        if len(self.sigma_lims) != 2:  # lower & upper bound
            raise AssertionError(
                'sigma_lims has to be a list of 2 components [min_sigma, max_sigma]. Found sigma_lims length: %d' % len(
                    self.sigma_lims))
        return float(self.prng.uniform(low=self.sigma_lims[0], high=self.sigma_lims[1]))

    def fn(self, c, x_old, sigma, epsilon):
        return c * x_old ** 2 * np.exp(- x_old) + sigma * epsilon

    def sample_series(self, x0, c0, sigma0):
        d = {'x_i': np.zeros((self.len_timeseries,)), 'c_i': np.zeros((self.len_timeseries,)),
             'sigma_i': np.zeros((self.len_timeseries,)), 'epsilon_i': np.zeros((self.len_timeseries,)),
             'c_0': float(c0), 'sigma_0': float(sigma0), 'x_0': float(x0), }
        x_old = x0
        c = c0
        sigma = sigma0
        for i in range(self.len_timeseries):
            epsilon = self.prng.normal(loc=0, scale=1)
            d['x_i'][i] = x_old
            d['c_i'][i] = c
            d['sigma_i'][i] = sigma
            d['epsilon_i'][i] = epsilon
            # get the next timeseries item.
            x_new = self.fn(c=c, x_old=x_old, sigma=sigma, epsilon=epsilon)
            # update parameters (when needed)
            x_old = x_new
        return d

    def __iter__(self):
        while True:
            x0 = self.x_0
            c0 = self.draw_c()
            sigma0 = self.draw_sigma()
            sample = self.sample_series(x0=x0, c0=c0, sigma0=sigma0)
            #
            noise = np.expand_dims(sample['epsilon_i'], -1)  # shape [timeseries, 1]
            params = (c0, sigma0)  # parameters (c, sigma)
            x = np.expand_dims(sample['x_i'], 1)  # [len_timeseries, 1]
            d = (x, params, noise)
            d = (l_observation, params, l_noise)
            yield d


class DataGenerator_AR1:
    def __init__(self, len_timeseries=200, **kwargs):
        self.prng = kwargs.get('prng', np.random.RandomState(seed=1923))
        self.len_timeseries = int(len_timeseries)
        self.x_0 = kwargs.get('x_0', 0.)
        self.c_lims = kwargs.get('c_lims', [0.1, 0.99])
        self.is_c_stationary = kwargs.get('is_c_stationary', True)
        self.sigma_lims = kwargs.get('sigma_lims', [0.1, 1.5])
        self.is_sigma_stationary = kwargs.get('is_sigma_stationary', True)
        self.epsilon_dist_str = kwargs.get('epsilon_dist_str', 'normal')
        if str(self.epsilon_dist_str).lower() == 'normal':
            self.epsilon_dist = lambda: self.prng.normal(loc=0, scale=1)
        elif str(self.epsilon_dist_str).lower() == 'custom': # need to provide the function in this scenario.
            self.epsilon_dist = kwargs.get('epsilon_dist')

    def draw_c(self):
        if isinstance(self.c_lims, tuple) or isinstance(self.c_lims, list):
            if len(self.c_lims) == 2: #lower & upper bound
                return float(self.prng.uniform(low=self.c_lims[0], high=self.c_lims[1]))
            elif len(self.c_lims) == 1: # is a constant, but passed in a list (should not be made).
                return float(self.c_lims[0])
        else: #not a list, scalar value
            return float(self.c_lims)

    def draw_sigma(self):
        if isinstance(self.sigma_lims, tuple) or isinstance(self.sigma_lims, list):
            if len(self.sigma_lims) == 2: #lower & upper bound
                return float(self.prng.uniform(low=self.sigma_lims[0], high=self.sigma_lims[1]))
            elif len(self.sigma_lims) == 1: # is a constant, but passed in a list (should not be made).
                return float(self.sigma_lims[0])
        else: #not a list, scalar value
            return float(self.sigma_lims)

    def fn(self, c, x_old, sigma, epsilon):
        return c * x_old + sigma * epsilon

    def sample_series(self, x0, c0, sigma0):
        d = {'x_i': np.zeros((self.len_timeseries,)),
             'c_i':np.zeros((self.len_timeseries,)),
             'sigma_i': np.zeros((self.len_timeseries,)),
             'epsilon_i': np.zeros((self.len_timeseries,)),
             'c_0': float(c0),
             'sigma_0': float(sigma0),
             'x_0': float(x0),
             }
        x_old = x0
        c = c0
        sigma = sigma0
        for i in range(self.len_timeseries):
            epsilon = self.prng.normal(loc=0, scale=1)
            d['x_i'][i] = x_old
            d['c_i'][i] = c
            d['sigma_i'][i] = sigma
            d['epsilon_i'][i] = epsilon
            # get the next timeseries item.
            x_new = self.fn(c=c, x_old=x_old, sigma=sigma, epsilon=epsilon)
            # update parameters (when needed)
            x_old = x_new
            if not self.is_c_stationary:
                c = self.draw_c()
            if not self.is_sigma_stationary:
                sigma = self.draw_sigma()
        return d

    def __iter__(self):
        while True:
            x0 = self.x_0
            c0 = self.draw_c()
            sigma0 = self.draw_sigma()
            sample = self.sample_series(x0=x0, c0=c0, sigma0=sigma0)
            yield sample # dict with ['x_i', 'c_i', 'sigma_i', 'epsilon_i']
