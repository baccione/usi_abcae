import pandas
dataset = pandas.read_csv ('datasets/dataset.dat', sep=",", index_col=0)

from spux.io import loader
def auxiliary (time):
    return loader.load ('dataset-auxiliary-*.dat' % str(time), directory = 'datasets', verbosity = 0)