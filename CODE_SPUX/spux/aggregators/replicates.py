# # # # # # # # # # # # # # # # # # # # # # # # # #
# Replicates aggregator class for multiple independent datasets
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

import numpy
from copy import deepcopy as copy
import sys # noqa F401

from .aggregator import Aggregator

class Replicates (Aggregator):
    """Replicates aggregator class for multiple independent datasets."""

    # constructor
    def __init__(self, log=1, sort=1):

        self.log = log
        self.sort = sort

        # initially components are not created
        self.components = None

    # assign required components to aggregator
    def assign (self, component, datasets, inputsets=None, timesets=None, errors=None, auxiliarys=None):
        """Assign required components to Replicate aggregator."""

        if not hasattr (component, 'assigned'):
            assert False, ' :: ERROR: component needs to have its components assigned before being assigned to replicates aggregator.'

        self._component = component
        self.datasets = datasets
        self.inputsets = inputsets
        self.timesets = timesets
        self.errors = errors
        self.auxiliarys = auxiliarys

        self.task = self._component
        self.sandboxing = self._component.sandboxing

        self.names = list (datasets.keys ())
        self.components = None

        self.assigned = True

    # configure feedback
    def configure (self, feedback = None):

        self._feedback = feedback

        self.configured = 1

    # feedback
    def feedback (self, info):
        """Feedback method to be executed by the sampler."""

        # compute feedbacks
        if hasattr (self._component, 'feedback') and self.components is not None:
            feedbacks = {name : component.feedback (info ['infos'] [name]) for name, component in self.components.items ()}
            return feedbacks
        else:
            return {name : float ('nan') for name in self.names}

    # evaluate components of the specified parameters for all replicates and combine them
    def __call__ (self, parameters):
        """Evaluate components of the specified parameters for all replicates and combine them."""

        # verbose output
        if self.verbosity:
            print ("Replicates parameters:")
            print (parameters)

        # create components by combining with each dataset in the datasets
        # TODO: to ensure scaling, datasets/inputsets/timesets loading should be delayed
        # to the inside of the actual component (if it is not a DataFrame object already for non-replicates)
        if self.components is None:
            self.components = {}
            for name, dataset in self.datasets.items ():
                self.components [name] = copy (self._component)
                self.components [name] .dataset = dataset ()
                if self.inputsets is not None:
                    self.components [name] .inputset = self.inputsets [name] ()
                if self.timesets is not None:
                    self.components [name] .timeset = self.timesets [name] ()
                if self.errors is not None:
                    self.components [name] .error = self.errors [name]
                if self.auxiliarys is not None:
                    self.components [name] .auxiliary = self.auxiliarys [name]

        # setup components
        for index, (name, component) in enumerate (self.components.items ()):
            label = 'R-%s' % name
            sandbox = self.sandbox.spawn (label) if self.sandboxing else None
            seed = self.seed.spawn (index, name=label)
            component.setup (self.verbosity - 1, self.informative, self.trace)
            component.isolate (sandbox)
            component.plant (seed)
            component.configure (self._feedback [name] if self._feedback is not None else None)

        # sort components according to the length of the associated dataset and the size of the component
        # TODO: perform sorting in the executor.map (...), with the ordering provided as an optional argument?
        if self.sort:
            lengths = [ len (component.dataset) * component.evaluations [0] ['cumulative'] for component in self.components.values () ]
            ordering = numpy.argsort (lengths) [::-1]
            keys = numpy.array ([ key for key in self.components.keys () ]) [ordering]
            components = [ self.components [key] for key in keys ]
            if self.verbosity:
                print ("Using ordered components:")
                print (" -> order: ", keys)
        else:
            keys = self.components.keys ()
            components = self.components.values ()

        # schedule component evaluations
        results, timings = self.executor.map (components, None, [parameters])

        # append executor timing
        timing = self.executor.report()

        # get results
        evaluations = {}
        infos = {}
        for index, name in enumerate (keys):
            evaluations [name], infos [name] = results [index]

        # compute estimated (log-)evaluation as the product of estimates from all replicates
        successful = True
        evaluations_array = numpy.array (list(evaluations.values()))
        nans = numpy.isnan (evaluations_array)
        if any (nans):
            mean = numpy.nanmean (evaluations_array)
            if numpy.isnan (mean):
                successful = False
            if self.verbosity:
                print (" :: WARNING: encountered NaN estimates in the Replicates aggregator:")
                print (evaluations)
                print ("  : -> setting NaN estimates to the mean of the non-NaN estimates: %1.1e" % mean)
            evaluations_array = numpy.where (nans, mean, evaluations_array)
        if self.log:
            if not self._component.log:
                evaluations_array = numpy.log (evaluations_array)
            evaluation = numpy.sum (evaluations_array)
        else:
            if self._component.log:
                evaluations_array = numpy.exp (evaluations_array)
            evaluation = numpy.prod (evaluations_array)

        # clean up sandboxes
        if self.sandboxing and not self.trace:
            for component in self.components.values ():
                component.sandbox.remove ()

        # information
        info = {}
        info ["evaluations"] = evaluations
        info ["infos"] = infos
        info ["successful"] = successful
        if self.informative:
            info ["timing"] = timing
            info ["timings"] = timings

        # return estimated component, its info
        return evaluation, info
