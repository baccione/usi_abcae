# # # # # # # # # # # # # # # # # # # # # # # # # #
# Base aggregator class
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

from ..utils.setup import setup, isolate, plant
from ..utils.seed import Seed
from ..executors.utils import importer
from ..utils import evaluations

class Aggregator (object):
    """Base class for aggregator objects."""

    @property
    def name (self):

        return type(self).__name__

    @property
    def component (self):

        return 'Aggregator'

    @property
    def evaluations (self):

        return evaluations.construct (self, len (self.datasets))

    # attach an executor
    def attach (self, executor = None, workers = None, backend = 'Mpi4py'):
        """Attach an executor."""

        if executor is not None:
            self.executor = executor
        else:
            self.executor = importer ('Pool', backend, workers)
        self.executor.setup (self)
        self.executor.capabilities (['map', 'report'])

    # setup
    def setup (self, verbosity=1, informative=1, trace=1):
        """Setup."""

        setup (self, verbosity, informative, trace)

    # isolate to a 'sandbox'
    def isolate (self, sandbox=None):
        """Isolate to a sandbox."""

        isolate (self, sandbox)

    # plant using specified 'seed'
    def plant (self, seed=Seed()):
        """Plant using specified 'seed'."""

        plant (self, seed)
