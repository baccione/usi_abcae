#
# TODO: I should aim not to duplicate information if it is not there, and to change R-analysis routines by using lists instead of data frames so that amout of data is more flexible
# TODO: can I have obs_err also for posterior case?
# TODO: annotate somehow MAP trajectory
# TODO: propagare prior information to R too, e.g., by computing the value given prior distributions and parameters values
# TODO: refactor so to have functions for all the tasks, possibly avoiding separate functions for predictions and predictions_prior
# TODO: refactor to mandate labels for predictions and observations to be dumped - no less, no more than those
#
# flake8: noqa
#
### imports ####################################
import os
import sys
import pandas
import numpy
import gc
import re
from rpy2 import robjects
from rpy2.robjects import r,pandas2ri
from spux.io import loader
from spux.devstuff.helpers_general import load_from_module
from spux.devstuff.helpers_post_processing import get_basic_infos,read_dumpfl,load_dats,stats2pddf,batch2pddf
################################################
#
class rDump (object):
    """Prototype function to dump SPUX-generated data to R format.

    It is not operative yet.
    """
#
    pandas2ri.activate()
    dumpnms = None                             #list of strings used to control what to read
#
    def __init__(self,dumpfile,datasets,stats,write_proposed,trajs_bef_res,trajs_pos_res,write_sources,write_redraws,write_best,frombatch,uptobatch,loaddir,p2obsdir,obsscr,obsimp,dotrajrecon,prior,p2pridir,priscr,priimp):

        self.dumpfl = dumpfile                 #str or None
        self.datasets = datasets               #bool
        self.stats = stats                     #bool
        #
        #This is now (21/07/20) allowed, but since accepts and successful are not modified in stats2pddf,
        #the Rpp still gives the same answer regardless (but statistics has different information in it)
        #because it discards info until all chains have been accepted at least once (this is a limitation)
        #assert write_proposed is True, ":: Fatal: due to current status of Rpp, write_proposed False should be used only for debug. Hack me if you know what you are doing."
        #
        self.write_proposed = write_proposed   #bool
        self.trajs_bef_res = trajs_bef_res     #bool
        self.trajs_pos_res = trajs_pos_res     #bool
        self.write_sources = write_sources     #bool
        self.write_redraws = write_redraws     #bool
        self.write_best = write_best           #bool
        self.frombatch = frombatch             #int
        self.uptobatch = uptobatch             #int
        self.loaddir = loaddir                 #str
        self.p2obsdir = p2obsdir               #str
        self.obsscr = obsscr                   #str
        self.obsimp = obsimp                   #str
        assert dotrajrecon is False, ":: Fatal: reconstr. of trajs is disabled. We use weights. It is also buggy when it comes to Rpp ML."
        self.dotrajrecon = dotrajrecon         #bool
        self.prior = prior                     #bool - not useful as prior is now dealt with in R direct with reticulate
        self.p2pridir = p2pridir               #str - path to prior dir - not useful as prior is now dealt with in R direct with reticulate
        self.priscr = priscr                   #str - name of prior script (prior.py) - not useful as prior is now dealt with in R direct with reticulate
        self.priimp = priimp                   #str - name(s) of what to import from prior.py (prior) - not useful as prior is now dealt with in R direct with reticulate

    def __call__(self):

        self.sanity_checks()

        # read what quantities to dump, if specified
        if self.dumpfl is not None:
            self.dumpnms = read_dumpfl (self.dumpfl) # set self.dumpnms, which otherwise are None

        # dump prior if required
        if self.prior == True:
            self.dump_prior()

        # dump dataset if required
        if self.datasets == True:
            self.dump_dataset()

        self.infos = None
        if self.stats == True or self.trajs_bef_res == True or self.trajs_pos_res == True:

            self.avail_batches, self.samples, self.infos, self.frombatch, self.uptobatch = load_dats(self.frombatch,self.uptobatch,self.loaddir)
            self.batches_names = [ self.infos[i]['index'] for i in range(0,len(self.infos)) ] #if self.infos is not None ]
            assert len(self.batches_names) == len(self.avail_batches), ":: Fatal: inconsistent information regarding number of batches and their names (1)."
            self.nbatches, self.nchains, self.nreps, self.have_reps, self.have_predictions_prior, self.have_predictions = get_basic_infos(self.infos,self.frombatch)
            assert len(self.batches_names) == self.nbatches, ":: Fatal: inconsistent information regarding number of batches and their names (2)."

            print("\n################################################")
            print("nbatches: {}; nchains: {};, nreps:{}; have_reps: {}; have_predictions_prior: {}; have_predictions: {}".format(
                   self.nbatches,self.nchains,self.nreps,self.have_reps,self.have_predictions_prior,self.have_predictions))
            print("batches' indexes: {}".format(self.batches_names))
            print("################################################")

            if self.stats == True:
                self.dump_statistics()

            if self.trajs_bef_res == True:
                if self.have_predictions_prior == True:
                    self.dump_results('_prior')
                else:
                    print(":: Warning: skipping requested writing of prior-to-resamples trajectories as those are missing from the saved data.")

            if self.trajs_pos_res == True:
                if self.have_predictions == True:
                    self.dump_results('')
                else:
                    assert False, ":: Fatal: self.have_predictions is False. This is a bug."

    def sanity_checks (self):
        """ implement sanity checks"""

        if self.dumpfl is not None:
            assert os.path.isfile(self.dumpfl), ":: Fatal: rdump file {} not found.".format(self.dumpfl)

        if self.prior == True:
            assert os.path.isdir(self.p2pridir),":: Fatal: missing load directory: {}.".format(self.p2pridir)
            assert os.path.isfile(os.path.join(self.p2pridir,self.priscr+".py")),":: Fatal: missing python file for prior: {}.".format(os.path.join(self.p2pridir,self.priscr+".py"))

        if self.datasets == True:
            #if len(self.obsimp)!=1:
            #    print(":: Warning: obsimp too long, only first value {} will be used.".format(self.obsimp[0]))
            #assert not "*" in self.obsimp, ":: Fatal: Inappropriate to try to load * with this application." -> this is true for superflex
            assert os.path.isdir(self.p2obsdir),":: Fatal: missing load directory: {}.".format(self.p2obsdir)
            assert os.path.isfile(os.path.join(self.p2obsdir,self.obsscr+".py")),":: Fatal: missing python file for dataset: {}.".format(os.path.join(self.p2obsdir,self.obsscr+".py"))

        if self.stats == True or self.trajs_bef_res == True or self.trajs_pos_res == True:
            assert os.path.isdir(self.loaddir),":: Fatal: missing load directory: {}.".format(loaddir)
            assert self.frombatch >= 0, ": Fatal:: frombatch is negative."
            if self.frombatch!=0:
                assert self.uptobatch >= 0, ": Fatal:: uptobatch is negative."
                assert self.uptobatch >= self.frombatch, ": Fatal:: uptobatch is > frombatch."
                assert self.uptobatch is not None, ":: Fatal: trying to specify frombatch!=0 with uptobatch==None is not allowed."
            if self.dumpfl is not None and not os.path.isfile(self.dumpfl):
                print(":: Warning: rdump input file not found: {}. All quantities will be written".format(self.dumpfl))
                self.dumpfl = None


    def dump_prior(self):
        """ dump prior.py """

        ldict = load_from_module(self.p2pridir,self.priscr,self.priimp)
        prior = ldict[self.priimp[0]] #assume that prior is specified as first entry of obsimp - this must be in the standard
        print(":: Warning: No prior python object is converted to the corresponding R object as the solution to this is to use reticulate package directly in R.") # 27/05/20

        pass

    def dump_dataset (self):
        """convert observational dataset to R file"""
        ### mfd ##########################################################################################################
        #for i in range(0,len(self.obsimp)):  #coded this way to show the route for the future with more complex scenarios
        #    if i == 0:                  #exec() is very limited, see: https://stackoverflow.com/questions/1463306/how-does-exec-work-with-locals, I must do hand-picking
        #        self.dataset = load_from_module(self.p2obsdir,self.obsscr,self.obsimp[i])
        #    #else:
        #    #    trashbin = load_from_module(self.p2obsdir,self.obsscr,self.obsimp[i]) #used to test that it worked if I loaded also warmup_time from dataset.py 22/Jan/2020
        ##################################################################################################################
        # This also would work in case obsimp is ['*']. In ldict I would then have either everything or what specified in self.obsimp
        ldict = load_from_module(self.p2obsdir,self.obsscr,self.obsimp)
        data = ldict[self.obsimp[0]] #assume that dataset is specified as first entry of obsimp - this must be in the standard

        if isinstance(data, pandas.DataFrame):
            self.nreps = 1 # should become the standard at some point as we should enforce to always have replicates
            self.dataset = {'0': data}
        #    self.have_reps = False
        else:
            assert isinstance(data, dict), ":: Fatal: when using multiple datasets, those must form a dict."
            self.nreps = len(data)
            self.dataset = data
        #    self.have_reps = True

        #if self.have_reps == True:
        for irep, (srep, val) in enumerate(self.dataset.items()): # this is really what I think it is
            self.dataset[srep] = self.dataset[srep].reset_index()
            if irep == 0:
                if self.dumpnms is None:
                    obsnms = ['replicates'] + list(self.dataset[srep].columns) #do not coincide here with what may be annotated in model
                else:
                    whiches = [ strg for strg in self.dumpnms if strg in self.dataset[srep].columns ]
                    obsnms = ['replicates'] + whiches
                    for colnm in self.dataset[srep].columns:
                        if colnm not in obsnms:
                            del self.dataset[srep][colnm]
                observations = pandas.DataFrame(columns=obsnms)
                print("\nThese information will be included in R data set for observations: {}\n".format(obsnms))
            reps = pandas.DataFrame(numpy.repeat(srep,self.dataset[srep].shape[0]))
            block = pandas.concat([reps,self.dataset[srep]],axis=1)
            block.columns = obsnms
            observations = observations.append(block)
#        else:
#            srep = 0 #fake it
#            self.dataset = self.dataset.reset_index()
#            if self.dumpnms is None:
#                obsnms = ['replicates'] + list(self.dataset.columns) #do not coincide here with what may be annotated in model
#            else:
#                whiches = [ strg for strg in self.dumpnms if strg in self.dataset.columns ]
#                obsnms = ['replicates'] + whiches
#                for colnm in self.dataset.columns:
#                    if colnm not in obsnms:
#                        del self.dataset[colnm]
#            print("\nThese information will be included in R data set for observations: {}\n".format(obsnms))
#            observations = pandas.DataFrame(columns=obsnms)
#            reps = pandas.DataFrame(numpy.repeat(srep,self.dataset.shape[0]))
#            block = pandas.concat([reps,self.dataset],axis=1)
#            block.columns = obsnms
#            observations = observations.append(block)
#
        try:
            rdf = pandas2ri.py2rpy_pandasdataframe(observations)
            robjects.r.assign("observations", rdf)
            robjects.r("save(observations, file='{}')".format('observations.RData'))
        except:
            try:
                rdf = pandas2ri.py2rpy_pandasdataframe( observations.applymap(str) )
                robjects.r.assign("observations", rdf)
                robjects.r("save(observations, file='{}')".format('observations.RData'))
            except:
                raise ValueError(":: Fatal: rpy2 failed for observations.")

    def dump_statistics (self):
        """convert sampling statistics to R file - probably not working with replicates"""

        keyb = 'infos'
        colnms = ['batches','chains'] + list(self.samples.columns.values) + ['accepts','priors','likelihoods','posteriors','successful']

        stats = stats2pddf(colnms,self.nbatches,self.batches_names,self.infos,self.nchains,self.write_proposed,self.have_reps,keyb)
        stats = stats.set_index(numpy.arange(0,stats.shape[0]))

        if self.dumpnms is not None:
            for colnm in stats.columns:
                if not colnm in self.dumpnms + ['batches','chains']:
                    del stats[colnm]
        try:
            rdf = pandas2ri.py2rpy_pandasdataframe(stats)
            robjects.r.assign("statistics", rdf)
            robjects.r("save(statistics, file='{}')".format('statistics.RData'))
        except:
            try:
                rdf = pandas2ri.py2rpy_pandasdataframe( stats.applymap(str) )
                robjects.r.assign("statistics", rdf)
                robjects.r("save(statistics, file='{}')".format('statistics.RData'))
            except:
                raise ValueError(":: Fatal: rpy2 failed for statistics.")

    def dump_results (self,case):
        """convert simulation results to R file"""

        keyb = 'infos'
        keyc = 'infos'
        reconstrucnms = ['obs_errors','sources','weights']
        first = True

        for ibatch in range(0, self.nbatches):
            gc.collect()
            #print("###########################################")
            #print( "ibatch: {} batch: {}".format(ibatch,batch) )
            #print("###########################################")
            countsane, self.trjs, self.srcsdf, self.redraws, self.best_trajs, self.dotrajrecon, batch_name, nparticles = batch2pddf(ibatch,self.batches_names,self.nchains,
                                self.infos,self.have_reps,self.dumpnms,case,dotrajrecon=self.dotrajrecon,
                                write_redraws=self.write_redraws,write_best=self.write_best,
                                write_sources=self.write_sources,keyb=keyb,keyc=keyc,reconstrucnms=reconstrucnms)

            if self.trjs is None:
                print("Warning: Skipping batch {} as it's all None...".format(batch_name))
                continue

            if (first == True):
                first = False
                nparticles_old = nparticles
            else:
                assert nparticles == nparticles_old, ":: Fatal: variable number of particles detected."

            if case == '_prior':
                varnamerdw = str('redraws_before_{:05d}'.format(batch_name))             #self.frombatch+ibatch))
                varnamesrc = str('sources_before_{:05d}'.format(batch_name))             #self.frombatch+ibatch))
                varnamebst = str('bests_before_{:05d}'.format(batch_name))             #self.frombatch+ibatch))
                varnametrj = str('trajectories_before_{:05d}'.format(batch_name))        #self.frombatch+ibatch))
                filenametrj = str('trajectories_before_{:05d}.RData'.format(batch_name)) #self.frombatch+ibatch))
            else:
                varnamerdw = str('redraws_post_{:05d}'.format(batch_name))
                varnamesrc = str('sources_post_{:05d}'.format(batch_name))
                varnamebst = str('bests_post_{:05d}'.format(batch_name))
                varnametrj = str('trajectories_post_{:05d}'.format(batch_name))
                filenametrj = str('trajectories_post_{:05d}.RData'.format(batch_name))

            if countsane != 0:
                assert self.trjs is not None, ":: Fatal: countsane is not zero but self.trajs is None?!? This is a bug."

                if self.dumpnms is not None:
                    for colnm in self.trjs.columns:
                        if not colnm in self.dumpnms + ['batches','chains','replicates','particles','times']:
                            #print(":: Warning, removing col: {} from R trajectory.".format(colnm))
                            del self.trjs[colnm]

                print("writing to file: ",filenametrj)
                try:
                    self.redraws.sort_values(by='times',inplace=True)
                    rdfrdw = pandas2ri.py2rpy_pandasdataframe(self.redraws)
                    robjects.r.assign(varnamerdw, rdfrdw)
                    rdfsrc = pandas2ri.py2rpy_pandasdataframe(self.srcsdf)
                    robjects.r.assign(varnamesrc, rdfsrc)
                    rdfbst = pandas2ri.py2rpy_pandasdataframe(self.best_trajs)
                    robjects.r.assign(varnamebst, rdfbst)
                    rdftrj = pandas2ri.py2rpy_pandasdataframe(self.trjs)
                    robjects.r.assign(varnametrj, rdftrj)
                    robjects.r("save(list=c('%s','%s','%s','%s'), file='%s')" %(varnamerdw,varnamesrc,varnamebst,varnametrj,filenametrj))
                except:
                    try:
                        self.redraws.sort_values(by='times',inplace=True)
                        rdfrdw = pandas2ri.py2rpy_pandasdataframe(self.redraws.applymap(str))
                        robjects.r.assign(varnamerdw, rdfrdw)
                        rdfsrc = pandas2ri.py2rpy_pandasdataframe(self.srcsdf.applymap(str))
                        robjects.r.assign(varnamesrc, rdfsrc)
                        rdfbst = pandas2ri.py2rpy_pandasdataframe(self.best_trajs.applymap(str))
                        robjects.r.assign(varnamebst, rdfbst)
                        rdftrj = pandas2ri.py2rpy_pandasdataframe(self.trjs.applymap(str))
                        robjects.r.assign(varnametrj, rdftrj)
                        robjects.r("save(list=c('%s','%s','%s','%s'), file='%s')" %(varnamerdw,varnamesrc,varnamebst,varnametrj,filenametrj))
                    except:
                        raise ValueError(":: Fatal: rpy2 failed for trajectories.")

            else:
                print(":: Warning: skipping completely batch {} cause all is None apparently.".format(batch_name))

        print("\n=== Done with writing trajectories for predictions in {} ===".format(case))
#
    #
### mfd ###
#
#    def get_basic_infos (self):
#        """Serves to asses that the data structure is really as the coder (me) was expecting.
#           Most likely, it will not work for parallel models as I anticipate one extra info layer in infos
#        """
#
#        # === some sanity checks and some assessments
#        assert type(self.infos) == list, ":: Fatal: Wrong type of first level in infos. This is a bug."
#        self.nbatches = len(self.infos)
#
#        assert self.nbatches > 0, ":: Fatal: Not enough batches. This may be a wrong specification for uptobatch or simply a bug."
#        self.names = []
#        self.types = []
#        isfirst = True
#        self.nchains = 0
#        self.nreps = 0
#        for ibatch in range(0, self.nbatches): #always start from 0 regardless of value of frombatch, this is a spux-loading feature, checked on 21/Jan/2020
#            counternones = 0
#            assert type(self.infos[ibatch]) == dict, ":: Fatal: Wrong type of second level in infos. This is a bug."
#            if isfirst == True:
#                self.names.append([key for key in self.infos[ibatch].keys()])
#                self.types.append( [ type(self.infos[ibatch][key]) for key in self.infos[ibatch].keys() ] )
#                for keyb in self.infos[ibatch].keys():
#                    if keyb == 'infos':
#                        try:
#                            self.nchains = len(self.infos[ibatch][keyb])
#                        except:
#                            print(":: Warning: Something is wrong with the detection of nchains at first loaded batch. Let's try again.")
#                            continue
#                        assert self.nchains!=0,":: Fatal: no eligible chains detected. This may be a bug or an unsupported feature."
#                        notdone = True
#                        for ichain in range(0, self.nchains):
#                            if self.infos[ibatch][keyb][ichain] is not None and notdone == True:
#                                self.names.append([key for key in self.infos[ibatch][keyb][ichain].keys()])
#                                self.types.append([ type(self.infos[ibatch][keyb][ichain][key]) for key in self.infos[ibatch][keyb][ichain].keys()])
#                                notdone = False
#                                try:
#                                    self.nreps = len(self.infos[ibatch][keyb][ichain]['infos'])
#                                    assert self.nreps > 0 # there is probably a reason if I am using a try / except constuct rather than an if here: I advise against changing this
#                                    self.have_reps = True
#                                except:
#                                    self.have_reps = False
#                                    self.nreps = 0
#                                    print(":: Warning: No replicas detected.")
#                                if self.have_reps == True:
#                                    for keyc in self.infos[ibatch][keyb][ichain].keys():
#                                        if keyc == 'infos':
#                                            replicas = list(self.infos[ibatch][keyb][ichain][keyc].keys())
#                                            assert len(replicas)==self.nreps, ":: Fatal: got nreps != len(replicas). A bug or an unsupported feature."
#                                            for irep in range(0, self.nreps):
#                                                srep = replicas[irep]
#                                                for keyr in self.infos[ibatch][keyb][ichain][keyc][srep].keys():
#                                                    assert keyr != 'self.infos', ":: Fatal: Too many infos. This is a bug or an unsupported feature."
#                                                if irep == 0:
#                                                    self.names.append([ key for key in self.infos[ibatch][keyb][ichain][keyc][srep].keys() ])
#                                                    self.types.append([ type(self.infos[ibatch][keyb][ichain][keyc][srep][key]) for key in self.infos[ibatch][keyb][ichain][keyc][srep].keys()])
#                                                    assert len(self.names)==3, ":: Fatal: got wrong len of self.names at this point (1). This is a bug or an unsupported feature."
#                                                    if 'predictions_prior' in self.names[2]:
#                                                        print("Found predictions_prior in self.names[2]: {}".format(self.names))
#                                                        self.have_predictions_prior = True
#                                                    else:
#                                                        self.have_predictions_prior = False
#                                                        print(":: Warning: no information before resampling are available.")
#                                                    if 'predictions' in self.names[2]:
#                                                        print("Found predictions in self.names[2]: {}".format(self.names))
#                                                        self.have_predictions = True
#                                                    else:
#                                                        self.have_predictions = False
#                                                        raise ValueError(":: Fatal: Predictions are not there. This is a bug or an unsupported feature.")
#                                                else:
#                                                    pass
#                                        else:
#                                            pass
#                                else:
#                                    assert len(self.names)==2, ":: Fatal: got wrong len of self.names at this point (2). This is a bug or an unsupported feature."
#                                    if 'predictions_prior' in self.names[1]:
#                                        print("Found predictions_prior in self.names[1]: {}".format(self.names))
#                                        self.have_predictions_prior = True
#                                    else:
#                                        self.have_predictions_prior = False
#                                        print(":: Warning: no information before resampling are available.")
#                                    if 'predictions' in self.names[1]:
#                                        print("Found predictions in self.names[1]: {}".format(self.names))
#                                        self.have_predictions = True
#                                    else:
#                                        self.have_predictions = False
#                                        raise ValueError(":: Fatal: Predictions are not there.")
#                            else:
#                                if self.infos[ibatch][keyb][ichain] is None: #data structures from spux run can be very bad, especially if frombatch is not 0
#                                    counternones += 1
#                                    continue
#                                else:
#                                    pass
#                        if counternones > 0:
#                            print(":: Got {} non-accepted or broken chains (infos[][infos][] is None) out of {} total chains for batch {} ".format(counternones,self.nchains,self.frombatch+ibatch))
#                        isfirst = False
#                    else:
#                        continue
#            else: #is not first
#                if ( ibatch < (self.nbatches - 1) or self.frombatch==0 ):
#                    for keyb in self.infos[ibatch].keys():
#                        assert keyb in self.names[0], ":: Fatal: key {} not in self.names. This is a bug.".format(keyb)
#                        assert keyb in self.infos[0].keys(), ":: Fatal: new key {} in batch infos.".format(keyb) #anyway, I saw keys change from first to other batches, but only reducing - but last one!!!
#                keyb = 'infos'
#                assert len(self.infos[ibatch][keyb])==self.nchains,":: Fatal: got inconsistent number of chains accross batches."
#                for ichain in range(0,self.nchains):
#                    if self.infos[ibatch][keyb][ichain] is None:
#                        counternones += 1
#                        continue
#                    if self.have_reps == True:
#                        keyc = 'infos'
#                        replicas = list(self.infos[ibatch][keyb][ichain][keyc].keys()) #it may well be that it is fine that this does not pass the assert...I'll have to keep on eye on this with IBM
#                        assert len(replicas)==self.nreps, ":: Fatal: got nreps != len(replicas). A bug or an unsupported feature."
#                        for irep in range(0, self.nreps):
#                            srep = replicas[irep]
#                            for keyr in self.infos[ibatch][keyb][ichain][keyc][srep].keys():
#                                assert keyr != 'infos', ":: Fatal: Too many infos. This is a bug or an unsupported feature."
#                    else:
#                        pass
#                if counternones > 0:
#                    print(":: Got {} non-accepted or broken chains (infos[][infos][] is None) out of {} total chains for loaded batch {} ".format(counternones,self.nchains,self.frombatch+ibatch))
#
#    def read_dumpfl (self):
#        """ read the rdump_file and returns the name of the quantities that should be converted across all possible pandas"""
#
#        with open(self.dumpfl, 'r') as fl:
#            lines = [line.rstrip() for line in fl]
#
#        lines = list(line for line in lines if line) # Non-blank lines in a list
#
#        self.dumpnms = [ re.split(' +|\\t|\n',line)[0] for line in lines ] #keep only first column
##
#    def load (self):
#        """ load the infos and samples files """
#
#        if self.uptobatch is None: #this implies that self.frombatch == 0, see sanity_checks
#            tmpnms = "samples-*.dat"
#            tmpnmi = "infos-*.dat"
#        else:
#            tmpnms = [ "samples-%05d-*.dat" % i for i in numpy.arange(start=self.frombatch, stop=self.uptobatch+1) ]
#            tmpnmi = [ "infos-%05d-*.dat" % i for i in numpy.arange(start=self.frombatch, stop=self.uptobatch+1) ]
#
#        samples_names, dummyavails = loader.check_interval(tmpnms,directory=os.path.join(self.loaddir))
#        infos_names, avail_batches = loader.check_interval(tmpnmi,directory=os.path.join(self.loaddir))
#        self.avail_batches = avail_batches
#
#        self.samples = pandas.concat (loader.loadall (name=samples_names,directory=os.path.join(self.loaddir), interactive=False), ignore_index=True, sort=True)
#        self.infos = [ info for infos in loader.loadall (name=infos_names,directory=os.path.join(self.loaddir), interactive=False) for info in infos ]
#
#        assert len(self.infos)==len(self.avail_batches),":: Fatal: Error in assessing who is who and which is which. This is a bug."
#        tmplst = [x for x in range(avail_batches[0],avail_batches[-1]+1)]
#        allthere = all(elem in avail_batches for elem in tmplst)
#        if not allthere:
#            print(":: Warning: some batch in between first and last failed completely. Best of luck.")
#
#        if self.frombatch != self.avail_batches[0]:
#            print(":: Warning: changin start batch from {} to {}".format(self.frombatch,self.avail_batches[0]))
#            self.frombatch = self.avail_batches[0]
#        if self.uptobatch != self.avail_batches[-1]:
#            print(":: Warning: changin end batch from {} to {}".format(self.uptobatch,self.avail_batches[-1]))
#            self.uptobacth = self.avail_batches[-1]
#
#        # report
#        print("\n################################################")
#        print (' :: Loaded %d posterior samples from %d chains' % (len (self.samples), len (self.infos [0] ['likelihoods'])))
#        print ('  : -> Indices: %d - %d' % (self.samples.index [0], self.samples.index [-1]))
#        print("################################################")
#
#    def dump_statistics (self):
#        """convert sampling statistics to R file - probably not working with replicates"""
#
#        keyb = 'infos'
#        colnms = ['batches','chains'] + list(self.samples.columns.values) + ['accepts','priors','likelihoods','posteriors','successful']
#        stats = pandas.DataFrame (columns=colnms)
#        for ibatch in range(0,self.nbatches):
#            batch_name = self.avail_batches[ibatch]
#            print('batch: ',batch_name,' on: ',self.avail_batches[-1])
#            block = None
#            try:
#                priors = self.infos[ibatch]['priors']
#            except:
#                assert False, ": Fatal:: this part of the code is outdated and should not be used without checking."
#                priors = self.infos[ibatch]['proposals'] #forecast sampler
#
#            sblock = list()
#            for ichain in range(0,self.nchains):
#                if self.infos[ibatch][keyb] is not None:
#                    if self.infos[ibatch][keyb][ichain] is not None:
#                        val = int(self.infos[ibatch][keyb][ichain]['successful']) # binary logical, 0 or 1
#                    else:
#                        val = 0 #failed or not-accepted
#                else:
#                    val = 0
#                sblock.append(val)
#
#            if self.write_proposed==False:
#                block = pandas.concat(
#                [
#                pandas.DataFrame(numpy.repeat(batch_name,self.nchains)),
#                pandas.DataFrame(numpy.arange(0,self.nchains)),
#                #with this I take currently accepted and valid old values, and forget about proposed but rejected
#                pandas.DataFrame(self.infos[ibatch]['parameters']),
#                #since I always write params that should be kept, I fake that acceptance was always 1
#                pandas.DataFrame(numpy.repeat(1,self.nchains)),
#                pandas.DataFrame(priors),
#                pandas.DataFrame(self.infos[ibatch]['likelihoods']),
#                pandas.DataFrame(self.infos[ibatch]['posteriors']),
#                pandas.DataFrame(sblock),
#                ],axis=1)
#            else:
#                block = pandas.concat(
#                [
#                pandas.DataFrame(numpy.repeat(batch_name,self.nchains)),
#                pandas.DataFrame(numpy.arange(0,self.nchains)),
#                pandas.DataFrame(self.infos[ibatch]['proposes']), #.astype(str)),
#                pandas.DataFrame(self.infos[ibatch]['accepts']),
#                pandas.DataFrame(priors),
#                pandas.DataFrame(self.infos[ibatch]['likelihoods']),
#                pandas.DataFrame(self.infos[ibatch]['posteriors']),
#                pandas.DataFrame(sblock),
#                ],axis=1)
#
#            block.columns = colnms
#            stats = stats.append(block)
#            # this is a check on sanity of accepts info
#            for ii in range(0,len(self.infos[ibatch]['accepts'])):
#                if self.infos[ibatch]['accepts'][ii] == 1:
#                    assert all(self.infos[ibatch]['proposes'].iloc[ii]==self.infos[ibatch]['parameters'].iloc[ii])
#
#        stats = stats.set_index(numpy.arange(0,stats.shape[0]))
#    def resmain_(self,sources,nparticles,predictions,errors,weights,trjdatanms,predictionsnms,batch_name,ichain,srep,i,times,allcolnms,colnms):
#
#        """ part in common with out without replicates """
#
#        if len(sources) < nparticles:
#            if self.dotrajrecon == True:
#                print(":: Warning, cannot reconstruct as sources information is not there. Disabling reconstruction. No further warnings. This may be a bug.")
#                self.dotrajrecon = False
#                self.block1 = pandas.DataFrame(columns=[0]*5)
#        if self.dotrajrecon == True and predictions.shape[0] < nparticles:
#            val = self.docore_(errors,weights,predictions,nparticles,trjdatanms,sources,predictionsnms)
#            #vals = self.docore2_(errors,weights,predictions,nparticles,trjdatanms,sources,predictionsnms)
#            #assert all(val == vals), ":: Fatal: val and vals are different" #checked 14/Feb/2020
#        elif self.dotrajrecon == True:
#            val = pandas.concat([
#            pandas.DataFrame(predictions,index=predictions.index),
#            pandas.DataFrame(errors,index=predictions.index),
#            pandas.DataFrame(sources,index=predictions.index),
#            pandas.DataFrame(weights,index=predictions.index)
#            ],axis=1)
#        else:
#            val = pandas.concat([
#            pandas.DataFrame(predictions,index=predictions.index),
#            pandas.DataFrame(errors,index=predictions.index),
#            pandas.DataFrame(set(sources),index=predictions.index),
#            pandas.DataFrame(weights,index=predictions.index)
#            ],axis=1)
#            tmpbatches = numpy.repeat(batch_name,val.shape[0])
#            tmpchains = [ichain]*val.shape[0]
#            tmpreps = [srep]*val.shape[0]
#            tmptimes = numpy.repeat(times[i],val.shape[0])
#            tmpprtcls = list(set(sources))
#            tmpblck = pandas.concat([
#                      pandas.DataFrame(tmpbatches),
#                      pandas.DataFrame(tmpchains),
#                      pandas.DataFrame(tmpreps),
#                      pandas.DataFrame(tmptimes),
#                      pandas.DataFrame(tmpprtcls),
#                      ],axis=1)
#            self.block1 = self.block1.append(tmpblck) # only in this case I have to touch block1
#        val.columns = trjdatanms
#        if self.write_sources == True:
#            #self.srcsdf = self.srcsdf.append(pandas.DataFrame(sources).transpose())
#            tmpsources = pandas.concat([pandas.DataFrame([batch_name]),pandas.DataFrame([ichain]),pandas.DataFrame([srep]), pandas.DataFrame([times[i]]),pandas.DataFrame(sources).transpose()],axis=1)
#            tmpsources.columns = self.srcsdf.columns
#            self.srcsdf = self.srcsdf.append(tmpsources)
#        self.block2 = self.block2.append(val)
#
#    def docore_(self,errors,weights,predictions,nparticles,trjdatanms,sources,predictionsnms):
#        """ attempt to make it simple, but it also makes slower (~x4) """
#
#        tmperrs = pandas.DataFrame(errors,index=predictions.index)
#        tmpwgts = pandas.DataFrame(weights,index=predictions.index)
#        val = pandas.DataFrame (index=range(0,nparticles),columns=trjdatanms)
#
#        ip = 0
#        for isr in sources:
#            val.iloc[ip] = pandas.DataFrame(predictions.loc[isr]).values.ravel().tolist() + pandas.DataFrame(tmperrs.loc[isr]).values.ravel().tolist() + [ isr ] + pandas.DataFrame(tmpwgts.loc[isr]).values.ravel().tolist()
#            ip += 1
#
#        isdup = val['sources'].duplicated()
#        counter = 0
#        for ip in range(0,nparticles):
#            assert all(predictions.loc[sources[ip]] == val.loc[ip][predictionsnms]), ":: Fatal: predictions and val are mismatched. This is a bug."
#            assert all(tmperrs.loc[sources[ip]] == val.loc[ip]['obs_errors']), ":: Fatal: predictions and val are mismatched. This is a bug."
#            assert all(tmpwgts.loc[sources[ip]] == val.loc[ip]['weights']), ":: Fatal: predictions and val are mismatched. This is a bug."
#            assert sources[ip] == val.loc[ip]['sources'], ":: Fatal: predictions and val are mismatched. This is a bug."
#            if isdup[ip] == True: # is duplicated
#                val.iloc[ip]['weights'] = 0
#                counter += 1
#
#        return val
#
#    def docore2__(self,errors,weights,predictions,nparticles,trjdatanms,sources,predictionsnms):
#        """ attempt to make it faster, but it is actually slower... """
#
#        newindx = numpy.full(nparticles,numpy.nan)
#        for ip in range(0,nparticles):
#            newindx[ip] = predictions.index.get_loc(sources[ip])
#        val = pandas.DataFrame (index=range(0,nparticles),columns=trjdatanms)
#        for ip in range(0,nparticles):
#            val.iloc[ip] = pandas.DataFrame(predictions.loc[sources[ip]]).values.ravel().tolist() + [errors[int(newindx[ip])]] + [sources[ip]] +  [weights[int(newindx[ip])]]
#
#        isdup = val['sources'].duplicated()
#        counter = 0
#        for ip in range(0,nparticles):
#            assert sources[ip] == val.loc[ip]['sources'], ":: Fatal: predictions and val are mismatched. This is a bug."
#            if isdup[ip] == True: # is duplicated
#                val.iloc[ip]['weights'] = 0
#                counter += 1
#
#        return val
#
#def dump_results (self,case):
#    """convert simulation results to R file"""
#
#    assert case in ['_prior',''],":: Fatal : got unknown case. This is a bug"
#    print("\n=== writing trajectories for predictions in {} ===".format(case))
#
#    keyb = 'infos'
#    keyc = 'infos'
#    reconstrucnms = ['obs_errors','sources','weights']
#
#    for ibatch in range(0, self.#s):
#        gc.collect()
#        batch_name = self.avail_batches[ibatch]
#        first = True
#        print('batch: ',batch_name,' on: ',self.avail_batches[-1])
#        countsane = 0
#        nparticles = 0
#        for ichain in range(0, self.nchains):
#            is_sane = True
#            if (self.infos[ibatch][keyb][ichain]) is None:
#                continue
#            if self.have_reps == False:
#                srep = 0 #fake it
#                times = [key for key in sorted(self.infos[ibatch][keyb][ichain]['predictions'+case].keys())]
#                if len(times) == 0:
#                    print(':: Warning: Bonus no time. Best of Luck.')
#                    is_sane = False
#                    continue
#                if is_sane == True:
#                    countsane += 1
#                    if first == True:
#                        trjdatanms = [name for name in self.infos[ibatch][keyb][ichain]['predictions'+case][times[0]].columns]
#                        if self.dumpnms is not None:
#                            for colnm in trjdatanms:
#                                if colnm not in self.dumpnms:
#                                    #print(":: Warning trjdatanms, removing col: {}".format(colnm))
#                                    trjdatanms.remove(colnm)
#                        npredictions = len(trjdatanms)
#                        predictionsnms = trjdatanms
#                        trjdatanms = trjdatanms + reconstrucnms #replaced_by=sources+1
#                        colnms = ['batches','chains','replicates','particles','times'] + trjdatanms
#                        allcolnms = ['batches','chains','replicates','times','particles'] + trjdatanms
#                        nparticles = self.infos[ibatch][keyb][ichain]['particles'] #if the information is not here, we are much better off to kill the job rather than guessing
#                        assert nparticles != 0, ":: Fatal: got nparticles = 0. Unsupported feature or bug."
#                        self.trjs = pandas.DataFrame (columns=colnms)
#                        self.srcsdf = pandas.DataFrame (columns=['batches','chains','replicates','times']+[p for p in range(0,nparticles)])
#                        self.redraws = pandas.DataFrame (columns = ['times', 'redraw'])
#                        self.best_trajs = pandas.DataFrame (columns=['batches','reps','chains','particles','times']+predictionsnms+['best_error'])
#                        first = False
#                    batches = numpy.repeat(batch_name,len(times)*nparticles)
#                    chains = [ichain]*nparticles*len(times)
#                    reps = [srep]*nparticles*len(times)
#                    alltimes = numpy.repeat(numpy.asarray(times),nparticles)
#                    particles = list(range(0,nparticles))*len(times)
#                    if self.dotrajrecon == True:
#                        self.block1 = pandas.concat([
#                        pandas.DataFrame(batches),
#                        pandas.DataFrame(chains),
#                        pandas.DataFrame(reps),
#                        pandas.DataFrame(alltimes),
#                        pandas.DataFrame(particles),
#                        ],axis=1)
#                    else:
#                        self.block1 = pandas.DataFrame(columns=[0]*5) # this 5 is ugly, corresponds to the number of cols of block1 as in the if just above
#                    #
#                    self.block2 = pandas.DataFrame (columns=trjdatanms)
#                    if not 'sources' in self.infos[ibatch][keyb][ichain].keys():
#                        raise ValueError(":: Fatal: got no or corrupted sources information. If you know why, fix me.")
#                    if not 'redraw' in self.infos[ibatch][keyb][ichain].keys():
#                        raise ValueError(":: Fatal: got no or corrupted redraw information. If you know why, fix me.")
#                    if self.write_redraws == True:
#                        # self.redraws = pandas.DataFrame( list( self.infos[ibatch][keyb][ichain]['redraw'].items() ), columns = ['times', 'redraw'] ) # mfd
#                        self.redraws = self.redraws.append(pandas.DataFrame( list( self.infos[ibatch][keyb][ichain]['redraw'].items() ), columns = ['times', 'redraw'] ))
#                    for i in range(0,len(times)):
#                        full_predictions = pandas.DataFrame (columns=predictionsnms)
#                        predictions = self.infos[ibatch][keyb][ichain]['predictions'+case][times[i]]
#                        assert predictions.shape[0] <= nparticles, ":: Fatal: got predictions.shape[0] >= nparticles."
#                        if 'errors'+case in self.infos[ibatch][keyb][ichain].keys():
#                            errors = self.infos[ibatch][keyb][ichain]['errors'+case][times[i]]
#                        if self.dumpnms is not None:
#                            pnms = predictions.columns
#                            for colnm in pnms:
#                                if colnm not in self.dumpnms:
#                                    #print(":: Warning predictions, removing col: {}".format(colnm))
#                                    del predictions[colnm]
#                        if times[i] in self.infos[ibatch][keyb][ichain]['sources'].keys():
#                            sources = self.infos[ibatch][keyb][ichain]['sources'][times[i]]
#                            weights = self.infos[ibatch][keyb][ichain]['weights'][times[i]]
#                            assert sum(weights) == nparticles, ':: Fatal: got sum(weights) != nparticles'
#                            #assert len(sources) == nparticles, ':: Fatal: Since 04/Feb/2020 I changed the code to store sources all. No more tricks please, but I want to provide backward compatibility
#                            for j in sources:
#                                assert j in predictions.index,":: Fatal: Found sources index absent in predictions. This is a bug."
#                        else:
#                            assert False, ":: Fatal: times[i] not in self.infos[ibatch][keyb][ichain]['sources'].keys(). This is a bug."
#                        assert predictions.shape[0]==len(weights), ":: Fatal: predictions.shape[0]!=len(weights)"
#                        assert predictions.shape[0]==len(errors), ":: Fatal: predictions.shape[0]!=len(errors)"
#                    #
#                        #self.resmain_(sources,nparticles,predictions,errors,weights,trjdatanms,predictionsnms,batch_name,ichain,srep,i,times,allcolnms,colnms) #about 5 times slower than tmpdev_: nice code means nothing... 14/Feb/2020
#                        self.tmpdev_(case,sources,nparticles,predictions,errors,weights,npredictions,trjdatanms,full_predictions,predictionsnms,batch_name,ichain,srep,i,times)
#                    #
#                    if self.write_best == True:
#                        self.best_trajs = self.best_trajs.append(pandas.concat([
#                            pandas.DataFrame([batch_name]*len(times),index=times,columns={'batches'}),
#                            pandas.DataFrame([srep]*len(times),index=times,columns={'reps'}),
#                            pandas.DataFrame([ichain]*len(times),index=times,columns={'chains'}),
#                            pandas.DataFrame([float('NaN')]*len(times),index=times,columns={'particles'}),
#                            pandas.DataFrame(times,index=times,columns={'times'}),
#                            pandas.DataFrame(self.infos[ibatch][keyb][ichain]['best']['predictions']).transpose().sort_index(),
#                            pandas.DataFrame([self.infos[ibatch][keyb][ichain]['best']['error']]*len(times),index=times,columns={'best_error'})
#                            ],axis=1),
#                        ignore_index=True)
#                    assert self.block2.shape[0] == self.block1.shape[0], ":: Fatal, wrong shapes of blocks. This is bug."
#                    self.block2 = self.block2.set_index(numpy.arange(0,self.block1.shape[0]))
#                    self.block1 = self.block1.set_index(numpy.arange(0,self.block1.shape[0]))
#                    self.block = pandas.concat([self.block1, self.block2], axis=1)
#                    self.block.columns = allcolnms
#                    self.block = self.block.reindex(columns=colnms)
#                    self.block = self.block.sort_values(['particles', 'times', 'replicates', 'chains'])
#                    self.trjs = self.trjs.append(self.block)
#                    if self.write_redraws == True:
#                        assert max(self.trjs['times']) == max(self.redraws['times']), ":: Fatal. Mismatch between time in redraw and trajectories. This is a bug."
#            else: # we have replicates
#                for irep in range(0, self.nreps):
#                    srep = replicas[irep]
#                    times = [key for key in sorted(self.infos[ibatch][keyb][ichain][keyc][srep]['predictions'+case].keys())]
#                    if len(times) == 0:
#                        print(':: Warning: Bonus no time. Best of Luck.')
#                        is_sane = False
#                        continue
#                    if is_sane == True:
#                        countsane += 1
#                        if first == True:
#                            trjdatanms = [name for name in self.infos[ibatch][keyb][ichain]['predictions'+case][times[0]].columns]
#                            if self.dumpnms is not None:
#                                for colnm in trjdatanms:
#                                    if colnm not in self.dumpnms:
#                                        #print(":: Warning trjdatanms, removing col: {}".format(colnm))
#                                        trjdatanms.remove(colnm)
#                            npredictions = len(trjdatanms)
#                            predictionsnms = trjdatanms
#                            trjdatanms = trjdatanms + reconstrucnms #replaced_by=sources+1
#                            colnms = ['batches','chains','replicates','particles','times'] + trjdatanms
#                            allcolnms = ['batches','chains','replicates','times','particles'] + trjdatanms
#                            nparticles = self.infos[ibatch][keyb][ichain][keyc][srep]['particles'] #if the information is not here, we are much better off to kill the job rather than guessing
#                            assert nparticles != 0, ":: Fatal: got nparticles = 0. Unsupported feature or bug."
#                            self.trjs = pandas.DataFrame (columns=colnms)
#                            self.redraws = pandas.DataFrame (columns = ['times', 'redraw'])
#                            self.best_trajs = pandas.DataFrame (columns=['batches','reps','chains','particles','times']+predictionsnms+['best_error'])
#                            first = False
#                        batches = numpy.repeat(batch_name,len(times)*nparticles)
#                        chains = [ichain]*nparticles*len(times)
#                        reps = [srep]*nparticles*len(times)
#                        alltimes = numpy.repeat(numpy.asarray(times),nparticles)
#                        particles = list(range(0,nparticles))*len(times)
#                        if self.dotrajrecon == True:
#                            self.block1 = pandas.concat([
#                            pandas.DataFrame(batches),
#                            pandas.DataFrame(chains),
#                            pandas.DataFrame(reps),
#                            pandas.DataFrame(alltimes),
#                            pandas.DataFrame(particles),
#                            ],axis=1)
#                        else:
#                            self.block1 = pandas.DataFrame(columns=[0]*5)
#                        #
#                        self.block2 = pandas.DataFrame (columns=trjdatanms)
#                        if not 'sources' in self.infos[ibatch][keyb][ichain][keyc][srep].keys():
#                            raise ValueError(":: Fatal: got no or corrupted sources information. If you know why, fix me.")
#                        if not 'redraw' in self.infos[ibatch][keyb][ichain][keyc][srep].keys():
#                            raise ValueError(":: Fatal: got no or corrupted redraw information. If you know why, fix me.")
#                        if self.write_sources == True:
#                            self.srcsdf = pandas.DataFrame (columns=['batches','chains','replicates','times']+[p for p in range(0,nparticles)])
#                        if self.write_redraws == True:
#                            # self.redraws = pandas.DataFrame( list( self.infos[ibatch][keyb][ichain]['redraw'].items() ), columns = ['times', 'redraw'] ) # mfd
#                            self.redraws = self.redraws.append(pandas.DataFrame( list( self.infos[ibatch][keyb][ichain]['redraw'].items() ), columns = ['times', 'redraw'] ))
#                        for i in range(0,len(times)):
#                            full_predictions = pandas.DataFrame (columns=predictionsnms)
#                            predictions = self.infos[ibatch][keyb][ichain][keyc][srep]['predictions'+case][times[i]]
#                            assert predictions.shape[0] <= nparticles, ":: Fatal: got predictions.shape[0] >= nparticles."
#                            if 'errors'+case in self.infos[ibatch][keyb][ichain].keys():
#                                errors = self.infos[ibatch][keyb][ichain][keyc][srep]['errors'+case][times[i]] #this may be bugged, but I need a trial and error session with reps 04/Feb/2020
#                            if self.dumpnms is not None:
#                                pnms = predictions.columns
#                                for colnm in pnms:
#                                    if colnm not in self.dumpnms:
#                                        #print(":: Warning predictions, removing col: {}".format(colnm))
#                                        del predictions[colnm]
#                            if times[i] in self.infos[ibatch][keyb][ichain][keyc][srep]['sources'].keys():
#                                sources = self.infos[ibatch][keyb][ichain][keyc][srep]['sources'][times[i]]
#                                weights = self.infos[ibatch][keyb][ichain][keyc][srep]['weights'][times[i]]
#                                assert sum(weights) == nparticles, ':: Fatal: got sum(weights) != nparticles'
#                                #assert len(sources) == nparticles, ':: Fatal: Since 04/Feb/2020 I changed the code to store sources all. No more tricks please, but I want to provide backward compatibility
#                                for j in sources:
#                                    assert j in predictions.index,":: Fatal: Found sources index absent in predictions. This is a bug."
#                            else:
#                                assert False, ":: Fatal: times[i] not in self.infos[ibatch][keyb][ichain]['sources'].keys(). This is a bug."
#                            assert predictions.shape[0]==len(weights), ":: Fatal: predictions.shape[0]!=len(weights)"
#                            assert predictions.shape[0]==len(errors), ":: Fatal: predictions.shape[0]!=len(errors)"
#                        #
#                            #self.resmain_(sources,nparticles,predictions,errors,weights,trjdatanms,predictionsnms,batch_name,ichain,srep,i,times,allcolnms,colnms)
#                            self.tmpdev_(case,sources,nparticles,predictions,errors,weights,npredictions,trjdatanms,full_predictions,predictionsnms,batch_name,ichain,srep,i,times)
#                        #
#                        if self.write_best == True:
#                            self.best_trajs = self.best_trajs.append(pandas.concat([
#                                pandas.DataFrame([batch_name]*len(times),index=times,columns={'batches'}),
#                                pandas.DataFrame([srep]*len(times),index=times,columns={'reps'}),
#                                pandas.DataFrame([ichain]*len(times),index=times,columns={'chains'}),
#                                pandas.DataFrame([float('NaN')]*len(times),index=times,columns={'particles'}),
#                                pandas.DataFrame(times,index=times,columns={'times'}),
#                                pandas.DataFrame(self.infos[ibatch][keyb][ichain]['best']['predictions']).transpose().sort_index(),
#                                pandas.DataFrame([self.infos[ibatch][keyb][ichain]['best']['error']]*len(times),index=times,columns={'best_error'})
#                                ],axis=1),
#                            ignore_index=True)
#                        assert self.block2.shape[0] == self.block1.shape[0], ":: Fatal, wrong shapes of blocks. This is bug."
#                        self.block2 = self.block2.set_index(numpy.arange(0,self.block1.shape[0]))
#                        self.block1 = self.block1.set_index(numpy.arange(0,self.block1.shape[0]))
#                        self.block = pandas.concat([self.block1, self.block2], axis=1)
#                        self.block.columns = allcolnms
#                        self.block = self.block.reindex(columns=colnms)
#                        self.block = self.block.sort_values(['particles', 'times', 'replicates', 'chains'])
#                        self.trjs = self.trjs.append(self.block)
#                        if self.write_redraws == True:
#                            assert max(self.trjs['times']) == max(self.redraws['times']), ":: Fatal. Mismatch between time in redraw and trajectories. This is a bug."
#
#        if case == '_prior':
#            varnamerdw = str('redraws_before_{:05d}'.format(batch_name))             #self.frombatch+ibatch))
#            varnamesrc = str('sources_before_{:05d}'.format(batch_name))             #self.frombatch+ibatch))
#            varnamebst = str('bests_before_{:05d}'.format(batch_name))             #self.frombatch+ibatch))
#            varnametrj = str('trajectories_before_{:05d}'.format(batch_name))        #self.frombatch+ibatch))
#            filenametrj = str('trajectories_before_{:05d}.RData'.format(batch_name)) #self.frombatch+ibatch))
#        else:
#            varnamerdw = str('redraws_post_{:05d}'.format(batch_name))
#            varnamesrc = str('sources_post_{:05d}'.format(batch_name))
#            varnamebst = str('bests_post_{:05d}'.format(batch_name))
#            varnametrj = str('trajectories_post_{:05d}'.format(batch_name))
#            filenametrj = str('trajectories_post_{:05d}.RData'.format(batch_name))
#
#        if countsane != 0:
#            assert self.trjs is not None, ":: Fatal: countsane is not zero but self.trajs is None?!? This is a bug."
#
#            if self.dumpnms is not None:
#                for colnm in self.trjs.columns:
#                    if not colnm in self.dumpnms + ['batches','chains','replicates','particles','times']:
#                        #print(":: Warning, removing col: {} from R trajectory.".format(colnm))
#                        del self.trjs[colnm]
#
#            print("writing to file: ",filenametrj)
#            try:
#                self.redraws.sort_values(by='times',inplace=True)
#                rdfrdw = pandas2ri.py2rpy_pandasdataframe(self.redraws)
#                robjects.r.assign(varnamerdw, rdfrdw)
#                rdfsrc = pandas2ri.py2rpy_pandasdataframe(self.srcsdf)
#                robjects.r.assign(varnamesrc, rdfsrc)
#                rdfbst = pandas2ri.py2rpy_pandasdataframe(self.best_trajs)
#                robjects.r.assign(varnamebst, rdfbst)
#                rdftrj = pandas2ri.py2rpy_pandasdataframe(self.trjs)
#                robjects.r.assign(varnametrj, rdftrj)
#                robjects.r("save(list=c('%s','%s','%s','%s'), file='%s')" %(varnamerdw,varnamesrc,varnamebst,varnametrj,filenametrj))
#            except:
#                try:
#                    self.redraws.sort_values(by='times',inplace=True)
#                    rdfrdw = pandas2ri.py2rpy_pandasdataframe(self.redraws.applymap(str))
#                    robjects.r.assign(varnamerdw, rdfrdw)
#                    rdfsrc = pandas2ri.py2rpy_pandasdataframe(self.srcsdf.applymap(str))
#                    robjects.r.assign(varnamesrc, rdfsrc)
#                    rdfbst = pandas2ri.py2rpy_pandasdataframe(self.best_trajs.applymap(str))
#                    robjects.r.assign(varnamebst, rdfbst)
#                    rdftrj = pandas2ri.py2rpy_pandasdataframe(self.trjs.applymap(str))
#                    robjects.r.assign(varnametrj, rdftrj)
#                    robjects.r("save(list=c('%s','%s','%s','%s'), file='%s')" %(varnamerdw,varnamesrc,varnamebst,varnametrj,filenametrj))
#                except:
#                    raise ValueError(":: Fatal: rpy2 failed for trajectories.")
#
#        else:
#            print(":: Warning: skipping completely batch {} cause all is None apparently.".format(batch_name))
#
#    print("\n=== Done with writing trajectories for predictions in {} ===".format(case))
#def tmpdev_(self,case,sources,nparticles,predictions,errors,weights,npredictions,trjdatanms,full_predictions,predictionsnms,batch_name,ichain,srep,i,times):
#        """ old way of doing thigs (get enough information to reconstruct sampling) """
#
#        if len(sources) < nparticles:
#            if self.dotrajrecon == True:
#                print(":: Warning, cannot reconstruct as sources information is not there. Disabling reconstruction. No further warnings. This may be a bug.")
#                self.dotrajrecon = False
#                #self.block1 = pandas.DataFrame(columns=[0]*5) # already set outside, mfd
#        if self.dotrajrecon == True and predictions.shape[0] < nparticles:
#            assert case == '', ":: Fatal: got predictions.shape[0] < nparticles before resampling."
#            newweights = numpy.zeros(nparticles)
#            newsources = numpy.arange(nparticles,dtype='float')
#            newerrors = numpy.full(nparticles,float('NaN'))
#            newindx = numpy.full(nparticles,numpy.nan)
#            tmppred = predictions.copy()
#            for ip in range(0,nparticles):
#                newindx[ip] = predictions.index.get_loc(sources[ip])
#            for ip in range(0, nparticles):
#                predictions.loc[ip] = tmppred.loc[sources[ip]]
#                newsources[ip] = sources[ip]
#                newweights[ip] = weights[int(newindx[ip])]
#                newerrors[ip] = errors[int(newindx[ip])]
#            if len(weights) < nparticles: #overwrite only if necessary
#                weights = newweights
#            if len(sources) < nparticles: #don't want to overwrite sources if I have sources all
#                assert self.dotrajrecon == False, ":: Fatal, wrong combo. This is a bug."
#                sources = newsources
#            if len(errors) < nparticles: #overwrite only if necessary
#                errors = newerrors
#            predictions = predictions.loc[[n for n in range(0,nparticles)]] #re-order come Dio comanda
#            val = predictions
#            full_predictions = full_predictions.append(val,ignore_index=True,sort=False)
#            assert full_predictions.shape[0]==nparticles, ":: Fatal: Got wrong dimensions 0 in full_predictions. This is a bug."
#            assert full_predictions.shape[1]==len(predictionsnms), ":: Fatal: Got wrong dimensions 1 in full_predictions. This is a bug."
#            #
#            isdup = pandas.DataFrame(sources).duplicated()
#            counter = 0
#            for ip in range(0,nparticles):
#                if isdup[ip] == True: # is duplicated
#                    weights[ip] = 0
#                    counter += 1
#            #
#            val = pandas.concat([
#            pandas.DataFrame(full_predictions),
#            pandas.DataFrame(errors),
#            pandas.DataFrame(sources), #cannot make this int (unless pandas >0.24), stackoverflow.com/questions/11548005/numpy-or-pandas-keeping-array-type-as-integer-while-having-a-nan-value - thanks God there's R
#            pandas.DataFrame(weights)
#            ],axis=1)
#        elif self.dotrajrecon == True:
#            val = pandas.concat([
#            pandas.DataFrame(predictions,index=predictions.index),
#            pandas.DataFrame(errors,index=predictions.index),
#            pandas.DataFrame(sources,index=predictions.index),
#            pandas.DataFrame(weights,index=predictions.index)
#            ],axis=1)
#        else:
#            val = pandas.concat([
#            pandas.DataFrame(predictions,index=predictions.index),
#            pandas.DataFrame(errors,index=predictions.index),
#            pandas.DataFrame(set(sources),index=predictions.index),
#            pandas.DataFrame(weights,index=predictions.index)
#            ],axis=1)
#            tmpbatches = numpy.repeat(batch_name,val.shape[0])
#            tmpchains = [ichain]*val.shape[0]
#            tmpreps = [srep]*val.shape[0]
#            tmptimes = numpy.repeat(times[i],val.shape[0])
#            tmpprtcls = list(set(sources))
#            tmpblck = pandas.concat([
#                      pandas.DataFrame(tmpbatches),
#                      pandas.DataFrame(tmpchains),
#                      pandas.DataFrame(tmpreps),
#                      pandas.DataFrame(tmptimes),
#                      pandas.DataFrame(tmpprtcls),
#                      ],axis=1)
#            self.block1 = self.block1.append(tmpblck) # only in this case I have to touch block1
#        val.columns = trjdatanms
#        if self.write_sources == True:
#            #self.srcsdf = self.srcsdf.append(pandas.DataFrame(sources).transpose())
#            tmpsources = pandas.concat([pandas.DataFrame([batch_name]),pandas.DataFrame([ichain]),pandas.DataFrame([srep]), pandas.DataFrame([times[i]]),pandas.DataFrame(sources).transpose()],axis=1)
#            tmpsources.columns = self.srcsdf.columns
#            self.srcsdf = self.srcsdf.append(tmpsources)
#
#        self.block2 = self.block2.append(val)
###### end mfd ###
