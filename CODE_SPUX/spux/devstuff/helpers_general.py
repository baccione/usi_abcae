#
# flake8: noqa
#
### imports ####################################
import os
import sys
import copy
import numpy
################################################
#
#-----------------------------------------------------------------------------------------------------------------------
#
def load_from_module(dir,fl,imp):
    """Import 'imp' from module 'fl' located in directoy 'dir' - load one at a time for simplicity"""

    # There are other options to what is below, see: https://stackoverflow.com/questions/67631/how-to-import-a-module-given-the-full-path
    # But they all require high-enough python3 versions and installing and loading packages, or changing sys.path...
    # I mean, I just want to load a file given an absolute path...why does it have to be such a pain in the neck such a basic functionality?

    assert os.path.isdir(dir),":: Fatal: missing load directory: {}.".format(dir)
    assert os.path.isfile(os.path.join(dir,fl+".py")),":: Fatal: missing python file for dataset: {}.".format(os.path.join(dir,fl+".py"))

    if not dir in sys.path: # to have this here is crucial, checked on 12/Feb/2020, it is not enough to change dir, prolly cause there may be not __init__ there, go figure...
        sys.path = [dir] + [ s for s in sys.path ]

    ### mfd #############################################################################################
    #if obsimp == "*":
    #    exec('from ' + fl + ' import ' + obsimp)
    #else:
        #try:
            #spec = importlib.util.spec_from_file_location("dataset", os.path.join(dir,fl+'.py'))
            #mod = importlib.util.module_from_spec(spec)
            #spec.loader.exec_module(mod)
            #impvar = copy.deepcopy(mod.dataset) #I would have to use the 'dataset' name explicitly
            #
            ### not worthwhile even trying, it just doesn't work as intended... ###
            ##print('Importing ' +  obsimp + ' from ' + os.path.join(dir,fl+'.py'))
            ##ldict = {}
            ##exec('from ' + os.path.join(dir,fl) + ' import ' + obsimp, globals(), ldict) #fails mirably, good thing this is an high-level language...
            ###exec('from ' + fl + ' import ' + obsimp, globals(), ldict) #this works if it is there, but it is inconsistent with the functionality I want to provide...
            #impvar = copy.deepcopy(ldict[obsimp])
            #######################################################################
            #return impvar
        #except:
    #####################################################################################################

    try:
        cwd = os.getcwd()
        os.chdir(dir)
        ldict = {}
        exec('from ' + fl + ' import ' + ', '.join(imp), globals(), ldict)
        os.chdir(cwd)
        sys.path = [ s for s in sys.path[1:] ] # remove mods to sys.path
        return ldict
    except:
        assert False, ":: Fatal: something is wrong in load_from_module in helpers_general.py: {} {} {}".format(dir,fl,imp)
#
#-----------------------------------------------------------------------------------------------------------------------
#
def logistic (lb,rb,x):

    try:
        y = [ (rb - lb) / (1+numpy.exp(-xx)) + lb for xx in x ]
    except:
        y = (rb - lb) / (1+numpy.exp(-x)) + lb

    return y
#
#-----------------------------------------------------------------------------------------------------------------------
#
def inv_logistic (lb,rb,x):

    try:
        y = [ numpy.log( (xx - lb) / (rb - xx ) ) for xx in x ]
    except:
        y = numpy.log( (x - lb) / (rb - x ) )

    return y
#
#-----------------------------------------------------------------------------------------------------------------------
#
