#
# flake8: noqa
#
import os
import re
#
#-----------------------------------------------------------------------------------------------------------------------
#
# global variables (become global in other scripts by importing them, like, module use in fortran, which is nice
# never ever allow a path/filename type of keyword with also a boolean on the same specification, it would bug it
#
# length attribute is quite misleading, standard keyword has 0, but can be longer than 1. Anyway, it is hidden \
# to the user. The value of length is used only to do nothing (=0), duplicate type (=1), or parse fixed types (>1)
#
spux_keys = {
    # one could possibly specify different types if length > 1, otherwise gets recycles same type

    'dodryrun':           { 'val':[True],        'kind':[bool],          'length':0, }, #to print out info like resources, etc.

    # keywords relevant to dorun and possibly to do analysis
    'dorun':              { 'val':[False],          'kind':[bool],          'length':0, },
    'nwc':                { 'val':[1],              'kind':[int],           'length':0, }, #number of workers for chain. Also to activate layers, why not?
    'nwr':                { 'val':[0],              'kind':[int],           'length':0, }, #number of workers for replicates.
    'nwp':                { 'val':[1],              'kind':[int],           'length':0, }, #number of workers for particles.
    'nwm':                { 'val':[0],              'kind':[int],           'length':0, }, #number of workers for models.
    'nparticles':         { 'val':[None],           'kind':[int],           'length':1, }, #it is 1 cause can there be multiple values but must all be int
    'p2obsdir':           { 'val':["./"],           'kind':[str],           'length':0, }, #path to folder that contains dataset(s).py
    'obsscr':             { 'val':["dataset"],      'kind':[str],           'length':0, }, #root name of dataset(s).py script, related to obsimp
    'dopf':               { 'val':[False],          'kind':[bool],          'length':0, }, #do the PF algorithm
    'dosabc':             { 'val':[False],          'kind':[bool],          'length':0, }, #do SABC algorithm
    'p2pridir':           { 'val':["./"],           'kind':[str],           'length':0, }, #path to folder that contains dataset(s).py
    'priscr':             { 'val':["prior"],        'kind':[str],           'length':0, }, #root name of usual prior.py script, related to priimp

    # keywords relevant to doanalysis
    'doanalysis':         { 'val':[False],       'kind':[bool],          'length':0, },
        'frombatch':      { 'val':[0],              'kind':[int],           'length':0, }, #used: rdump,
        'uptobatch':      { 'val':[None],           'kind':[int],           'length':0, }, #used: rdump, should be int if specified
        'loaddir':        { 'val':["./output"],     'kind':[str],           'length':0, }, #used: rdump,
        'obsimp':         { 'val':["dataset"],      'kind':[str],           'length':1, }, #used: rdump, what to import from dataset(s).py - can in priciple be longer than 1 but always strings
        'dotrajrecon':    { 'val':[False],          'kind':[bool],          'length':0, }, #used; rdump, whether to try to reconstruct trajectories or not. Gets overwritten if sources has not enough information
        'priimp':         { 'val':["prior"],        'kind':[str],           'length':1, }, #used: rdump, what to import from prior.py - can in priciple be longer than 1 but always strings

        'dordump':              { 'val':[False],       'kind':[bool],          'length':0, },
            'rdump_labels_file':    { 'val':[None],           'kind':[str],           'length':0, }, #to choose what to dump. If None, all is written to file
            'rdump_datasets':       { 'val':[False],          'kind':[bool],          'length':0, },
            'rdump_stats':          { 'val':[False],          'kind':[bool],          'length':0, },
            'rdump_write_proposed': { 'val':[True],           'kind':[bool],          'length':0, },
            'rdump_trajs_bef_res':  { 'val':[False],          'kind':[bool],          'length':0, },
            'rdump_trajs_pos_res':  { 'val':[False],          'kind':[bool],          'length':0, },
            'rdump_trajs_pos_res':  { 'val':[False],          'kind':[bool],          'length':0, },
            'rdump_write_sources':  { 'val':[False],          'kind':[bool],          'length':0, },
            'rdump_write_redraws':  { 'val':[True],           'kind':[bool],          'length':0, },
            'rdump_write_best':     { 'val':[True],           'kind':[bool],          'length':0, },
            'rdump_prior':          { 'val':[False],          'kind':[bool],          'length':0, }, # whether to dump what in prior file

    # test keywords
    'test_me':            { 'val':[1],              'kind':[int],           'length':1, }, #used to test keywords routines.
    #'test_me':            { 'val':[1,2,False],      'kind':[int,int,bool],  'length':3, }, #length != 1 is useful only if I want to specify on the same line variables with different types
}
#-----------------------------------------------------------------------------------------------------------------------
#
#-----------------------------------------------------------------------------------------------------------------------
def parse_keys(keyfile="key_spux.key"):
    #
    #spux_keys = set_defaults()
    assert os.path.isfile(keyfile), ":: Fatal: keyfile {} does not exist".format(keyfile)
    #
    exec_keys = {}                                   # keys for this specific execution
    unknown_keys = []                                # unkwown keys to be printed out in warning
    with open(keyfile, 'r') as fl:
        lines = [line.rstrip() for line in fl]

    lines = list(line for line in lines if line) # Non-blank lines in a list

    for line in lines:
        str1 = re.split(' +|\\t|\n',line)        # split according to blank(s), tabs, and end-of-line
        str1 = [ s for s in str1 if s]               # remove empty items

        if len(str1)<=1:
            if str1[0] in spux_keys:
                print(":: Warning: possible missing value for keyword: {} - ignoring".format(str1[0]))
                continue
        if not str1[0].lower() in spux_keys:
          str2 = [ s.lower() for s in str1 ]
          #continue                              # do not just 'continue' since we want to be able to store unknown_keys later on
        else: #I decide not to support stringed booleans anymore
            #if bool in spux_keys[str1[0].lower()]['kind']:
            #    assert spux_keys[str1[0].lower()]['length']==0, ":: Fatal: boolean keyword {} should be single-entry keyword.".format(str1[0].lower())
            #    if 'F' in str1[1:]:                      # R style boolean, avoid replacing in keywork though
            #        assert str1[str1[1:].index('F')+1]=='F',":: Fatal: mismatched location of F. This is a bug."
            #        str1[str1[1:].index('F')+1] = '0'    # must use stringed integers
            #    if 'T' in str1[1:]:
            #        assert str1[str1[1:].index('T')+1]=='T',":: Fatal: mismatched location of T. This is a bug."
            #        str1[str1[1:].index('T')+1] = '1'    # must use stringed integers
            #    str2 = [ s.lower() for s in str1 ]
            #    if len(str2) > 1 and 'false' in str2[1:]:
            #        str2[str2[1:].index('false')+1]='0'
            #    if len(str2) > 1 and 'true' in str2[1:]:
            #        str2[str2[1:].index('true')+1]='1'
            #else:
            if len(str1) > 1:
                str2 =  [ str1[0].lower() ]
                for s in str1[1:]:
                    str2.append(s)
            else:
                str2 = [ str1[0].lower() ]

        cmlc = [ x.find("#") for x in str2 ]     # Location of comments -> 0s if present
        if 0 in cmlc:
            cutat = [cmlc.index(0)][0]           # Gives already only the smaller index afaict, but don't ever trust
        else:
            cutat = len(str2)
        str2 = str2[0:cutat]

        if len(str2)>0:
            if not str2[0] in spux_keys:
                unknown_keys.append(str2[0])
            else:
                key = str2[0]                    # name of key
                keyvals = [s for s in str2[1:]]
                exec_keys[key] = keyvals         # values of key
                kinf = spux_keys[key]            # infos of variable (key) from database

                setkey(kinf,exec_keys,key)

    if len(unknown_keys)>0:
        print(":: Warning: these keywords are unknown to SPUX and have been ignored: ",unknown_keys)

    if len(exec_keys)==0:
        assert False, "Fatal: No eligible keworkds found in keyfile: {}.".format(keyfile)

    return exec_keys
#-----------------------------------------------------------------------------------------------------------------------
#
#-----------------------------------------------------------------------------------------------------------------------
def setkey(kinf,keys,key):
    """ work out the keys (type, duplication, some specific sanity checks, etc.)"""

    if kinf['length'] == 0: # not allowed to specify more than one value for keywords with length 0
        assert len(keys[key])==1, ":: Fatal: got too many values: {} for keywords: {}. This keywords accepts only one value. This may be an input file error.".format(keys[key],key)
        i = 0
        if keys[key][i]=='None':
            keys[key][i] = None
        elif kinf['kind'][i]==bool:
            assert keys[key][i] in ['0','1'],":: Fatal: unrecognized value, wrong type? key: {}; value: {}; should be boolean (0,1).".format(key,keys[key][i])
            keys[key][i] = bool(int(keys[key][i]))
        elif kinf['kind'][i]==int:
            keys[key][i] = int(keys[key][i])
        elif kinf['kind'][i]==float:
            keys[key][i] = float(keys[key][i])
        elif kinf['kind'][0]==str:
            keys[key][i] = str(keys[key][i])
        else:
            assert False, ":: Fatal: unrecognized type: {}. This is a bug.".format(kinf['kind'][i])
    elif kinf['length'] == 1:            # recycle same type for all entries
        if kinf['kind'][0]==bool:
            for v in sorted( set( keys[key] ) ):
                assert v in ['0','1'],":: Fatal: unrecognized value, wrong type? key: {}; value: {}; should be boolean (0,1).".format(key,v)
            for i in range(len(keys[key])):
                v = keys[key][i]
                if v != 'None':
                    keys[key][i] = bool(int(v))
                else:
                    keys[key][i] = None
        elif kinf['kind'][0]==int:
            for i in range(len(keys[key])):
                v = keys[key][i]
                if v != 'None':
                    keys[key][i] = int(v)
                else:
                    keys[key][i] = None
        elif kinf['kind'][0]==float:
            for i in range(len(keys[key])):
                v = keys[key][i]
                if v != 'None':
                    keys[key][i] = float(v)
                else:
                    keys[key][i] = None
        elif kinf['kind'][0]==str:
            for i in range(len(keys[key])):
                v = keys[key][i]
                if v != 'None':
                    keys[key][i] = str(v)
                else:
                    keys[key][i] = None
        else:
            assert False, ":: Fatal: unrecognized type: {}. This is a bug.".format(kinf['kind'][0])
    elif kinf['length'] > 1:
        assert len(keys[key])==spux_keys[key]['length'],":: Fatal: mismatched length of key values. May be a bug. key: {}".format(key)
        for i in range(len(keys[key])):
            if keys[key][i]=='None':
                keys[key][i] = None
            elif kinf['kind'][i]==bool:
                assert keys[key][i] in ['0','1'],":: Fatal: unrecognized value, wrong type? key: {}; value: {}; should be boolean (0,1).".format(key,keys[key][i])
                keys[key][i] = bool(int(keys[key][i]))
            elif kinf['kind'][i]==int:
                keys[key][i] = int(keys[key][i])
            elif kinf['kind'][i]==float:
                keys[key][i] = float(keys[key][i])
            elif kinf['kind'][i]==str:
                keys[key][i] = str(keys[key][i])
            else:
                assert False, ":: Fatal: unrecognized type: {}. This is a bug.".format(kinf['kind'][i])
    else:
        assert False, ":: Fatal: unrecognized length: {}. This is a bug.".format(kinf['length'])
#-----------------------------------------------------------------------------------------------------------------------
#
#-----------------------------------------------------------------------------------------------------------------------
def complete_keys(keys):
    """set not specified keywords and returns that set back to the calling instance"""

    defs_keys = {} #just a list to keep track of which keywords were not specified and were taken by default values
    for k in spux_keys:
        if not k in keys:
            keys[k] = spux_keys[k]['val']      # this is copy by value and not by reference, I tested it 20012020
            defs_keys[k] = spux_keys[k]['val']

    return defs_keys

#-----------------------------------------------------------------------------------------------------------------------
#
#-----------------------------------------------------------------------------------------------------------------------
def avail_keys(keys):

    for k, v in keys.items():
            print(k,v)

#-----------------------------------------------------------------------------------------------------------------------
#
