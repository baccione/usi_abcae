#
# flake8: noqa
#
### imports ####################################
#
################################################
#
#-----------------------------------------------------------------------------------------------------------------------
#
def sanity_checks(keys):
    """ checks sanity of keywords and modify what need to be modified """

    assert 'dodryrun' in keys,   ":: Fatal: dodryrun not in keys in sanity_checks. This is a bug."
    assert 'dorun' in keys,      ":: Fatal: dorun not in keys in sanity_checks. This is a bug."
    assert 'doanalysis' in keys, ":: Fatal: doanalysis not in keys in sanity_checks. This is a bug."

    print('#######################################################')
    if keys['doanalysis'][0] == True: #it is gonna be an analysis run
        keys['dodryrun'][0] = False
        keys['dorun'][0] = False
        print("THIS IS AN ANALYSIS RUN")
        #here I should implement the sanity_checks specific to an analysis run
    elif keys['dorun'][0] == True:
        keys['dorun'][0] = False
        keys['dodryrun'][0] = False
        print("THIS IS A RUN")
        #here I should implement the sanity_checks specific to a run (abc, pf, forecast, ...)
    else:
        assert keys['dodryrun'][0] == True, ":: Fatal: dodryrun is false with wrong combo in sanity checks. This is a bug."
        print("THIS IS A DRY RUN")
        #here I should implement the sanity_checks specific to a dry run
    print('#######################################################')
#
#-----------------------------------------------------------------------------------------------------------------------
#
