#
# flake8: noqa
#
#
#-----------------------------------------------------------------------------------------------------------------------
def run_analysis(keys):
    if keys['dordump'][0] == True:
        print(":: Dumping in R format...")
        from spux.devstuff.dump_results_2R import rDump
        rdumper = rDump(
                         keys['rdump_labels_file']     [0],
                         keys['rdump_datasets']        [0],
                         keys['rdump_stats']           [0],
                         keys['rdump_write_proposed']  [0],
                         keys['rdump_trajs_bef_res']   [0],
                         keys['rdump_trajs_pos_res']   [0],
                         keys['rdump_write_sources']   [0],
                         keys['rdump_write_redraws']   [0],
                         keys['rdump_write_best']      [0],
                         keys['frombatch']             [0],
                         keys['uptobatch']             [0],
                         keys['loaddir']               [0],
                         keys['p2obsdir']              [0],
                         keys['obsscr']                [0],
                         keys['obsimp']                [:],
                         keys['dotrajrecon']           [0],
                         keys['rdump_prior']           [0],
                         keys['p2pridir']              [0],
                         keys['priscr']                [0],
                         keys['priimp']                [:],
        )
        print("### For Rdumping these keywords are used:")
        print('rdump_labels_file: ',keys['rdump_labels_file'][0])
        print('rdump_datasets: ',keys['rdump_datasets'][0])
        print('rdump_stats: ',keys['rdump_stats'][0])
        print('rdump_write_proposed: ',keys['rdump_write_proposed'][0])
        print('rdump_trajs_bef_res: ',keys['rdump_trajs_bef_res'][0])
        print('rdump_trajs_pos_res: ',keys['rdump_trajs_pos_res'][0])
        print('rdump_write_sources: ',keys['rdump_write_sources'][0])
        print('rdump_write_redraws: ',keys['rdump_write_redraws'][0])
        print('rdump_write_redraws: ',keys['rdump_write_best'][0])
        print('rdump_write_best: ',keys['frombatch'][0])
        print('uptobatch: ',keys['uptobatch'][0])
        print('loaddir: ',keys['loaddir'][0])
        print('p2obsdir: ',keys['p2obsdir'][0])
        print('obsscr: ',keys['obsscr'][0])
        print('obsimp: ',keys['obsimp'][:])
        print('dotrajrecon: ',keys['dotrajrecon'][0])
        print('rdump_prior: ',keys['rdump_prior'][0])
        print('p2pridir: ',keys['p2pridir'][0])
        print('priscr: ',keys['priscr'][0])
        print('priimp: ',keys['priimp'][:])
        print("#########################################")
        ### just controlled by __call_function, but we could call all methods independently depending on bools
        rdumper()
        ### if rdumper.prior == True: ### we could have all the calls here controlled by booleans for improved readability
        ###    self.dump_prior()
        #################################
        print(":: Done with dumping in R format.")
    else:
        assert False, ":: Fatal: nothing else implemented than R conversion for now, stick to scripts, and check back later."
#-----------------------------------------------------------------------------------------------------------------------
#
