#
# flake8: noqa
#
import os
import sys
import argparse
from spux.devstuff.keywords import avail_keys, parse_keys, complete_keys
from spux.devstuff.helpers_inference import sanity_checks
#
#-----------------------------------------------------------------------------------------------------------------------
if __name__== "__main__":
    #
    parser = argparse.ArgumentParser()
    parser.add_argument("-k", "--keyfile", help="Key File")
    args = parser.parse_args()
    print("Got keyfile: {}".format(args.keyfile))
    #
    exec_keys = parse_keys(args.keyfile)
    print("\nSUMMARY OF SET KEYWORDS AND ASSOCIATED VALUES:   ")
    avail_keys(exec_keys)
    print("#################################################")
    #
    defs_keys = complete_keys(exec_keys)  # set into exec_keys not specified keywords and returns that set into defs_keys
    print("\nSUMMARY OF DEFAULT KEYWORDS AND ASSOCIATED VALUES:")
    avail_keys(defs_keys)
    print("#################################################")
    #
    print(':: Doing sanity checks...')
    sanity_checks(exec_keys)
    print('Done with sanity checks.')
    #
    if exec_keys['dodryrun'][0] == True:
        print("Required number of resources for a run (not an analysis, which is never parallel!) with the specified key file: ",
              1 +
              exec_keys['nwc'][0] +
              exec_keys['nwc'][0]*exec_keys['nwr'][0] +
              exec_keys['nwc'][0]*max(1,exec_keys['nwr'][0])*exec_keys['nwp'][0] +
              exec_keys['nwc'][0]*max(1,exec_keys['nwr'][0])*exec_keys['nwp'][0]*exec_keys['nwm'][0] )
        sys.exit()
    elif exec_keys['dorun'][0] == True:
        assert False,":: Fatal: run mode to be implemented yet, resort to scripts for the moment."
        #import something to do the runs...
    elif exec_keys['doanalysis'][0] == True:
        print("Required number of resources for an analysis run is always 1 for now, ignoring keywords nwc, nwr, nwp, nwm, if specified.")
        ### ok ######################################
        #from pysht import *
        #p2obsdir='/home/mbacci/temp/trash/pythonbug'
        #obsscr='datasets'
        #obsimp='dataset'
        #set_vars(p2obsdir,obsscr,obsimp)
        #############################################
        #from spux_analysis import *
        from spux_analysis import run_analysis # 11/Nov/2020
        run_analysis(exec_keys)
else:
    assert False, ":: Fatal: as far as I know, you should run this script directly, and nothing else."
#-----------------------------------------------------------------------------------------------------------------------
#
