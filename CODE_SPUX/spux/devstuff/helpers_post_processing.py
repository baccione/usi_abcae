#
# flake8: noqa
#
### imports ####################################
import pandas
import os
import numpy
import re
from spux.io import loader
################################################
#
#-----------------------------------------------------------------------------------------------------------------------
#
def NSE (data, predictions, labels=['Q'], write=False, wdir='.', outflnm='NSEs.dat'):
    """Compute NSE"""

    assert isinstance(data, pandas.DataFrame), ":: Fatal: data is not a pandas data frame."
    assert isinstance(predictions, pandas.DataFrame), ":: Fatal: predictions is not a pandas data frame."
    assert isinstance(labels, list), ":: Fatal: labels are not a list."
    assert all ([ x in data.columns for x in labels ]), ":: Fatal: some label is not present in data."
    assert all ([ x in predictions.columns for x in labels ]), ":: Fatal: some label is not present in predictions."

    NSEs = pandas.DataFrame( columns=labels, index=[0] )
    for lbl in labels:
        ave = data[lbl].mean()
        try:
            assert len(predictions[lbl].values) == len(data[lbl].values), ":: Fatal: mismatch on lens."
        except:
            #import pdb; pdb.set_trace() # - if this happens, I should debug it
            assert len(predictions[lbl].values) == len(data[lbl].values), ":: Fatal: mismatch on lens."

        num = numpy.sum((predictions[lbl].values - data[lbl].values)**2)
        den = numpy.sum((data[lbl].values - ave)**2)
        NSEs[lbl][0] = 1 - num / den

    if write == True:
        NSEs.to_csv(os.path.join(wdir,outflnm), encoding='utf-8', sep=' ', index=False)

    return NSEs
#
#-----------------------------------------------------------------------------------------------------------------------
#
def compare_dat_csv(frombatch=None,uptobatch=None,loaddir='./'):
    """Ensure by comparison that .dat info and csv info are in compliance"""

    #sanity_checks
    assert os.path.isdir(loaddir),":: Fatal: missing load directory: {}.".format(loaddir)
    assert frombatch is not None, ": Fatal:: frombatch is None."
    assert frombatch >= 0, ": Fatal:: frombatch is negative."
    assert uptobatch >= frombatch, ": Fatal:: uptobatch is > frombatch."

    tmpdat = [ "samples-%05d-*.dat" % i for i in numpy.arange(start=frombatch, stop=uptobatch+1) ]
    tmpcsv = [ "samples-%05d-*.csv" % i for i in numpy.arange(start=frombatch, stop=uptobatch+1) ]

    for i in range(0,len(tmpdat)):
        dat = loader.load_dat(tmpdat[i],directory=loaddir)
        csv = loader.load_csv(tmpcsv[i],directory=loaddir)
        if dat is not None:
            assert csv is not None, ":: Fatal: dat available while csv is not."
            assert (abs((csv-dat).values)).max() < 1E-06, ":: Fatal: It seems there is quite some mismatch."
#
#-----------------------------------------------------------------------------------------------------------------------
#
def build_params_posterior_from_csvfiles(nchains,nreps,frombatch=None,uptobatch=None,loaddir='./'):
    """Construct parameters posterior from csv files to resample parameters in forecast runs"""

    # sanity checks
    assert os.path.isdir(loaddir),":: Fatal: missing load directory: {}.".format(loaddir)
    assert frombatch is not None, ":: Fatal: frombatch is None."
    assert frombatch >= 0, ": Fatal:: frombatch is negative."
    assert uptobatch >= frombatch, ":: Fatal: uptobatch is > frombatch."
    assert nchains > 0, ":: Fatal: nchains must be > 0."
    assert nreps > 0, ":: Fatal: nreps means the number of dataset in my view, must be > 0."

    batch_length = nreps*nchains # important notion

    tmpnms = [ "samples-%05d-*.csv" % i for i in numpy.arange(start=frombatch, stop=uptobatch+1) ]
    samples_names, avail_batches = loader.check_interval(tmpnms,directory=os.path.join(loaddir)) # back in the day I could not get avail_batches right with csv, there was a stong reason for it
    if frombatch != avail_batches[0]:
        print(":: Warning: changin start batch from {} to {}".format(frombatch,avail_batches[0]))
        frombatch = avail_batches[0]
    if uptobatch != avail_batches[-1]:
        print(":: Warning: changin end batch from {} to {}".format(uptobatch,avail_batches[-1]))
        uptobacth = avail_batches[-1]
    tmpnms = [ "samples-%05d-*.csv" % i for i in numpy.arange(start=frombatch, stop=uptobatch+1) ]

    params = None
    loaded_batches = {}
    for ibatch in range(frombatch, uptobatch+1):
        if params is None:
            params = loader.load_csv(filename=tmpnms[ibatch-frombatch],directory=loaddir)
            if params is not None:
                loaded_batches[ibatch] = params.shape[0]/batch_length
        else:
            tmp = loader.load_csv(filename=tmpnms[ibatch-frombatch],directory=loaddir)
            if tmp is not None:
                loaded_batches[ibatch] = tmp.shape[0]/batch_length
                params = params.append(tmp,sort=False)

    nbatches = int(params.shape[0]/batch_length)
    assert nbatches == len(avail_batches), ":: Fatal: Even with sorceries, this should still not fail!"

    # these are required because loader.check_interval cannot work with csv files as we cannot establish the number of batches from len(infos)
    endbatch = max(avail_batches)
    inibatch = frombatch - (nbatches - (max(avail_batches)-min(avail_batches)) ) + 1

    batches, chains, reps = build_batches_chains_reps(inibatch,endbatch,nbatches,nchains,nreps,None) # build leading columns of pddf - passing None as avail_batches as with csv it is not satisfactory
    assert len(batches)==params.shape[0], ":: Fatal: len(batches)!=shape. This is a bug."
    params = add_batches_chains_reps(batches,chains,reps,params) # add leading columns to pddf

    # report
    print("\n################################################")
    print (' :: Loaded %d posterior parameters samples' % (len (params) ) )
    print ('  : -> Indices: %d - %d' % (params.index [0], params.index [-1]))
    print("################################################")

    return params
#
#-----------------------------------------------------------------------------------------------------------------------
#
def build_params_posterior_from_datfiles(frombatch=None,uptobatch=None,loaddir='./'):
    """Extract params from dat files as dat files limit available information on states"""

    avail_batches, params, infos, frombatch, uptobatch = load_dats(frombatch,uptobatch,loaddir)
    batches_names = [ infos[i]['index'] for i in range(0,len(infos)) ] #if infos is not None ]
    assert len(batches_names) == len(avail_batches), ":: Fatal: inconsistent information regarding number of batches and their names."
    nbatches, nchains, nreps, have_reps, have_predictions_prior, have_predictions = get_basic_infos (infos,frombatch)
    assert len(batches_names) == nbatches, ":: Fatal: inconsistent information regarding number of batches and their names (2)."
    accepts = do_accepts(infos,nbatches,frombatch,uptobatch,nchains,nreps,batches_names)
    assert accepts.shape[0]==params.shape[0], ":: Fatal: mismatch lens between accepts and params. This is a bug."
    assert sum(accepts['accepts'])>=1, ":: Fatal: it seems that no parameter was accepted. Congrats and best of luck!"

    batches, chains, reps = build_batches_chains_reps(frombatch,uptobatch,nbatches,nchains,nreps,batches_names) # build leading columns of pddf
    assert len(batches)==params.shape[0], ":: Fatal: len(batches)!=shape. This is a bug."
    quntnms = params.columns
    params = add_batches_chains_reps(batches,chains,reps,params) # add leading columns to pddf
    params = reset_index_params(batches_names,nchains,nreps,params) # set the index of pddf to the one that it should be from the start

    for i, val in enumerate(accepts['accepts']):
        if val==1 and i >= nchains:
            assert any(params.iloc[1][quntnms] != params.iloc[2][quntnms]), ":: Fatal: params did not change with accept 1 ?"
        elif i >= nchains:
            assert all(params.iloc[i-nchains][quntnms] == params.iloc[i][quntnms]), ":: Fatal: params changed with accept 0 ?"

    return params #, params #, map2params, accepts
#
#-----------------------------------------------------------------------------------------------------------------------
#
def build_states_from_datfiles(frombatch,uptobatch,loaddir,
                                        dumpnms=None,case='',dotrajrecon=False,write_redraws=False,write_best=False,write_sources=False,
                                        keyb='infos',keyc='infos',reconstrucnms=['obs_errors','sources','weights'],load_time=None):
    """ Constructs the state of the model at latest time for all accepted params samples"""

    avail_batches, params, infos, frombatch, uptobatch = load_dats(frombatch,uptobatch,loaddir)
    batches_names = [ infos[i]['index'] for i in range(0,len(infos)) ] #if infos is not None ]
    assert len(batches_names) == len(avail_batches), ":: Fatal: inconsistent information regarding number of batches and their names (1)."
    nbatches, nchains, nreps, have_reps, have_predictions_prior, have_predictions= get_basic_infos (infos,frombatch)
    assert len(batches_names) == nbatches, ":: Fatal: inconsistent information regarding number of batches and their names (2)."
    accepts = do_accepts(infos,nbatches,frombatch,uptobatch,nchains,nreps,batches_names)
    assert accepts.shape[0]==params.shape[0], ":: Fatal: mismatch lens between accepts and params. This is a bug."
    assert sum(accepts['accepts'])>=1, ":: Fatal: it seems that no parameter was accepted. Congrats and best of luck!"

    batches = list(set(accepts['batches']))
    for ibatch in range(0,nbatches):
        countsane, trjs, srcsdf, redraws, best_trajs, dotrajrecon, batch_name, nparticles = batch2pddf(ibatch,batches_names,nchains,
                                infos,have_reps,dumpnms,case,
                                dotrajrecon=dotrajrecon,write_redraws=write_redraws,write_best=write_best,write_sources=write_sources,
                                keyb=keyb,keyc=keyc,reconstrucnms=reconstrucnms)
        if trjs is None:
            continue

        #end = (ibatch+1)*nreps*nchains
        #ini = end - nreps*nchains
        #actl_accepts = [ int(x) for x in accepts['accepts'][ini:end] ]
        actl_accepts = accepts.loc[accepts['batches']==batches[ibatch]]['accepts']

        if dumpnms is None:
            assert "times" in trjs.columns, ":: Fatal: did not find times in labels of trajectory. This is a bug."
            assert "obs_errors" in trjs.columns, ":: Fatal: did not find obs_errors in labels of trajectory. This is a bug."
            colnms = ['batches','chains','replicates','times'] + list(trjs.columns[range( (trjs.columns.get_loc("times")+1), (trjs.columns.get_loc("obs_errors")) )]) + ['weights']
        else:
            colnms = dumpnms

        assert "batches" in trjs.columns, ":: Fatal: did not find batches in labels of trajectory. This is a bug."
        assert "chains" in trjs.columns, ":: Fatal: did not find chains in labels of trajectory. This is a bug."
        assert "replicates" in trjs.columns, ":: Fatal: did not find replicates in labels of trajectory. This is a bug."
        assert "times" in trjs.columns, ":: Fatal: did not find times in labels of trajectory. This is a bug."
        assert "weights" in trjs.columns, ":: Fatal: did not find weights in labels of trajectory. This is a bug."
        assert "batches" in colnms, ":: Fatal: did not find batches in labels of colnms. This is a bug or a lack in dumpfile."
        assert "chains" in colnms, ":: Fatal: did not find chains in labels of colnms. This is a bug or a lack in dumpfile."
        assert "replicates" in colnms, ":: Fatal: did not find replicates in colnms of trajectory. This is a bug or a lack in dumpfile."
        assert "times" in colnms, ":: Fatal: did not find times in colnms of trajectory. This is a bug or a lack in dumpfile."
        assert "weights" in colnms, ":: Fatal: did not find weights in colnms of trajectory. This is a bug or a lack in dumpfile."

        if ibatch == 0:
            states = pandas.DataFrame (columns=colnms)

        if not any([x == 1 for x in actl_accepts]): # no chains were accepted
            continue

        if load_time is None:
            load_time = max(trjs['times'])
        states = states.append(trjs.loc[ ( trjs['times'] == load_time ) ][colnms],sort=False)

    assert list(set(states['batches'])) == batches, ":: Fatal: inconsistent information on batches. This is a bug"

    return states
#
#-----------------------------------------------------------------------------------------------------------------------
#
def build_params_and_states_from_datfiles(frombatch,uptobatch,loaddir,
                                        dumpnms=None,case='',dotrajrecon=False,write_redraws=False,write_best=False,write_sources=False,
                                        keyb='infos',keyc='infos',reconstrucnms=['obs_errors','sources','weights'],load_time=None,
                                        write_trajs=True,return_sources=False):
    """Constructs the parameters and states of the model at latest time for all accepted parameters samples"""

    if return_sources==True and write_sources==False:
        print(":: Warning: Activating write_sources as return_sources is True.")
        write_sources = True

    avail_batches, params, infos, frombatch, uptobatch = load_dats(frombatch,uptobatch,loaddir) # load_dats uses loadall which loads parameters and not proposed, differently from what we have for statistics
    batches_names = [ infos[i]['index'] for i in range(0,len(infos)) ] #if infos is not None ]
    assert len(batches_names) == len(avail_batches), ":: Fatal: inconsistent information regarding number of batches and their names (1)."
    nbatches, nchains, nreps, have_reps, have_predictions_prior, have_predictions = get_basic_infos (infos,frombatch)
    assert len(batches_names) == nbatches, ":: Fatal: inconsistent information regarding number of batches and their names (2)."
    accepts = do_accepts(infos,nbatches,frombatch,uptobatch,nchains,nreps,batches_names)
    check_accepts(accepts)
    assert accepts.shape[0]==params.shape[0], ":: Fatal: mismatch lens between accepts and params. This is a bug."
    assert sum(accepts['accepts'])>=1, ":: Fatal: it seems that no parameter was accepted. Congrats and best of luck!"
    chains_trace = do_chains_trace(accepts,batches_names,nchains)
    trjs = {b: None for b in batches_names}
    srcs = {b: None for b in batches_names}

    ### parameters ###
    batches, chains, reps = build_batches_chains_reps(frombatch,uptobatch,nbatches,nchains,nreps,batches_names) # build leading columns of pddf
    assert len(batches)==params.shape[0], ":: Fatal: len(batches)!=shape. This is a bug."
    assert batches==list(accepts['batches']), ":: Fatal: conflicting information on batches. This is a bug."
    batches_unq = list(set(batches))
    batches_unq.sort() # god bless R
    quntnms = params.columns
    params = add_batches_chains_reps(batches,chains,reps,params) # add leading columns to pddf
    params = reset_index_params(batches_names,nchains,nreps,params) # set the index of pddf to the one that it should be from the start
    # params of dat files is already accounting for accepts/rejects correctly (meaning rejected ones are not stored): just double check
    for i, val in enumerate(accepts['accepts']):
        if val==1 and i >= nchains:
            assert any(params.iloc[1][quntnms] != params.iloc[2][quntnms]), ":: Fatal: params did not change with accept 1 ?"
        elif i >= nchains:
            assert all(params.iloc[i-nchains][quntnms] == params.iloc[i][quntnms]), ":: Fatal: params changed with accept 0 ?"
    ######

    ### states ###
    states = None
    loadstate = None
    samplables = []
    found_chains = []
    nparticles = [0]*nbatches
    for ibatch in range(0,nbatches):
        samplables = samplables + [False]
        countsane, trjs[batches_names[ibatch]], srcs[batches_names[ibatch]], redraws, best_trajs, dotrajrecon, batch_name, nparticles[ibatch] = batch2pddf(ibatch,batches_names,nchains,
                                infos,have_reps,dumpnms,case,
                                dotrajrecon=dotrajrecon,write_redraws=write_redraws,write_best=write_best,write_sources=write_sources,
                                keyb=keyb,keyc=keyc,reconstrucnms=reconstrucnms)
        if trjs[batches_names[ibatch]] is None:
            if loadstate is not None:
                loadstate['batches'] = [batches_names[ibatch]]*len(loadstate['batches']) # update batch ID
                states = states.append(loadstate,sort=False) # replicate previous step
            if ibatch > 0:
                samplables[-1] = samplables[-2] # depends on the previous one if it is samplable
            continue

        oldstate = loadstate

        # If a batch fails completely, then all the sorceries below can happen, and we are in the hands of ...
        #if ibatch > 0:
        #    assert nparticles[ibatch] > 0, ":: Fatal: 0 particles does not sound right."
        #     nparticles[ibatch] == nparticles[ibatch-1], ":: Fatal: variable particles add too much complexity for too little benefits."

        actl_accepts = accepts.loc[accepts['batches']==batches_unq[ibatch]]
        #end = (ibatch+1)*nreps*nchains
        #ini = end - nreps*nchains
        #actl_accepts_old = [ int(x) for x in accepts['accepts'][ini:end] ]
        #assert all(actl_accepts_old == actl_accepts['accepts']), ":: Fatal: not equivalent."
        checkme1 = list(set(actl_accepts.loc[actl_accepts['accepts']==1]['chains'])); checkme1.sort()
        checkme2 = list(set(trjs[batches_names[ibatch]]['chains'])); checkme2.sort()
        assert checkme1==checkme2, ":: Fatal: mismatch on set of chains. This is a bad bug."

        if dumpnms is None:
            assert "times" in trjs[batches_names[ibatch]].columns, ":: Fatal: did not find times in labels of trajectory. This is a bug."
            assert "obs_errors" in trjs[batches_names[ibatch]].columns, ":: Fatal: did not find obs_errors in labels of trajectory. This is a bug."
            colnms = ['batches','chains','replicates','particles','times'] + list(trjs[batches_names[ibatch]].columns[range( (trjs[batches_names[ibatch]].columns.get_loc("times")+1), (trjs[batches_names[ibatch]].columns.get_loc("obs_errors")) )]) + ['weights']
        else:
            colnms = dumpnms

        assert "batches" in trjs[batches_names[ibatch]].columns, ":: Fatal: did not find batches in labels of trajectory. This is a bug."
        assert "chains" in trjs[batches_names[ibatch]].columns, ":: Fatal: did not find chains in labels of trajectory. This is a bug."
        assert "replicates" in trjs[batches_names[ibatch]].columns, ":: Fatal: did not find replicates in labels of trajectory. This is a bug."
        assert "particles" in trjs[batches_names[ibatch]].columns, ":: Fatal: did not find particles in labels of trajectory. This is a bug."
        assert "times" in trjs[batches_names[ibatch]].columns, ":: Fatal: did not find times in labels of trajectory. This is a bug."
        assert "weights" in trjs[batches_names[ibatch]].columns, ":: Fatal: did not find weights in labels of trajectory. This is a bug."
        assert "batches" in colnms, ":: Fatal: did not find batches in labels of colnms. This is a bug or a lack in dumpfile."
        assert "chains" in colnms, ":: Fatal: did not find chains in labels of colnms. This is a bug or a lack in dumpfile."
        assert "particles" in colnms, ":: Fatal: did not find particles in colnms of trajectory. This is a bug or a lack in dumpfile."
        assert "replicates" in colnms, ":: Fatal: did not find replicates in colnms of trajectory. This is a bug or a lack in dumpfile."
        assert "times" in colnms, ":: Fatal: did not find times in colnms of trajectory. This is a bug or a lack in dumpfile."
        assert "weights" in colnms, ":: Fatal: did not find weights in colnms of trajectory. This is a bug or a lack in dumpfile."

        if states is None:
            states = pandas.DataFrame (columns=colnms)
        if load_time is None:
            load_time = max(trjs[batches_names[ibatch]]['times'])

        if oldstate is not None:
            assert load_time in set(oldstate['times']), ":: Fatal: mismatch on state time. This is a bug." # without set() Python fails miserably

        assert load_time in trjs[batches_names[ibatch]]['times'].values, ":: Fatal: this load time was not saved."
        loadstate = trjs[batches_names[ibatch]].loc[ ( trjs[batches_names[ibatch]]['times'] == load_time ) ][colnms]
        actl_batch = list(set(trjs[batches_names[ibatch]]['batches']))
        assert len(actl_batch)==1, ":: Fatal: too many batches (1)."
        actl_batch = int(actl_batch[0])
        actl_chains = list(set(loadstate['chains']))
        actl_chains.sort()
        if not all([ x==1 for x in actl_accepts ]):
            if oldstate is not None:
                for r in range(0,nreps):
                    old = [c for c, a in enumerate(actl_accepts.loc[actl_accepts['replicates']==r]['accepts']) if a != 1] # old chains to be kept
                    #ini = r*nchains
                    #end = ini + nchains #-1 python does not cycle on the last one...
                    #oldold = [c for c, a in enumerate(actl_accepts_old[ini:end]) if a != 1] # old chains the state of which should be kept
                    #assert oldold==old, ":: Fatal: mismatch on management of old and oldold. This is a bug."
                    for c in old:
                        addme = oldstate.loc[(oldstate['chains']==c) & (oldstate['replicates']==r) & (oldstate['times']==load_time) ][colnms]
                        absnt = trjs[batches_names[ibatch]].loc[(trjs[batches_names[ibatch]]['chains']==c) & (trjs[batches_names[ibatch]]['replicates']==r) & (trjs[batches_names[ibatch]]['times']== load_time) ]
                        if addme.shape[0] > 0:
                            assert len(set(loadstate['batches'])) == 1, ":: Fatal: too many batches (2)."
                            addme['batches'] = list(set(loadstate['batches']))*addme.shape[0]
                            loadstate = loadstate.append(addme,ignore_index=True,sort=False)
                        if absnt.shape[0] > 0:
                            assert False, ":: Fatal: horrible bug (1)."
                loadstate = loadstate.sort_values(['batches','chains', 'replicates', 'particles'])
        found_chains = list( set( found_chains + list(set(loadstate['chains'])) ) )
        found_chains.sort()
        found_chains_batch = list ( set(loadstate.loc[loadstate['batches']==actl_batch]['chains']) )
        found_chains_batch.sort()
        #print("######################")
        #print(actl_chains); print(found_chains); print(found_chains_batch)
        assert found_chains == found_chains_batch, ":: Fatal: mismatched on chains (1). This is a bug."
        assert all([x in found_chains for x in actl_chains]), ":: Fatal: accepted chains lost?!?. This is a bug"
        if len(set(loadstate['chains'])) == len(set(chains)):
            samplables[-1] = True
        else:
            assert loadstate.shape[0] < nreps*nchains*nparticles[ibatch], ":: Fatal: horrible bug (2)."
            assert list(set(loadstate['batches']))==[(batches_names[ibatch])], ":: Fatal: I don't know..." #most likely you need just to update batches in loadstate to batches_names[ibatch]."
        states = states.append(loadstate,ignore_index=True,sort=False)
        states = states.sort_values(['batches','chains', 'replicates', 'particles'])
        checkme1 = list(set(states['chains'])); checkme1.sort()
        assert checkme1 == found_chains, ":: Fatal: mismatched on chains (2). This is a bug."
        #states_chains_last_batch = list(set(states.loc[states['batches']==actl_batch]['chains']))
        #states_chains_last_batch.sort()
        #print(states_chains_last_batch)
        #print("######################")
    ######
    assert all(accepts['batches'] == batches), ":: Fatal: inconsistent information on batches. This is a bug"
    assert len(set(params['batches'])) == len(samplables), ":: Fatal: wrong lengths (1)."
    assert len(set(states['batches'])) == len(samplables), ":: Fatal: wrong lengths (2)."
    #assert len(set(nparticles)) == 1 # all equivalent - if a batch fails completely ... one has 0 particles as nothing is saved

    if write_trajs == False:
        return params,states,samplables,accepts,nbatches,nchains,nreps,max(set(nparticles)),frombatch,uptobatch,chains_trace,None

    if have_predictions == False:
        assert False, ":: Fatal: We do not have predictions. I do not know how this has happened."

    first_possible = [i for i,x in enumerate (samplables) if x == True]
    if len(first_possible) > 0:
        first_possible = first_possible[0]
    else:
        assert False,":: Fatal: We do not have samplable cases. Perhaps enrich your results by loading more samples?"

    batches_forecast = numpy.sort(numpy.unique(chains_trace.iloc[first_possible:].values))
    assert any(chains_trace.iloc[ first_possible ].isnull())==False, ":: Fatal: incosistent information between samplables and chains_trace."
    for ibatch in batches_forecast:
        assert trjs[ibatch] is not None, ":: Fatal: None traj selected as forecast ?!? This is a bug."

    if return_sources:
        return params,states,samplables,accepts,nbatches,nchains,nreps,max(set(nparticles)),frombatch,uptobatch,chains_trace,trjs,srcs
    else:
        return params,states,samplables,accepts,nbatches,nchains,nreps,max(set(nparticles)),frombatch,uptobatch,chains_trace,trjs
#
#-----------------------------------------------------------------------------------------------------------------------
#
def dump_trajs(trjs=None,sffx=''):

    if trjs is None:
        print(":: Warning: cannot dump, traj is None")

    flnm = 'trajs'+sffx+'.dat'
    print("dumping trajectories for python in file: {}".format(flnm))

    from spux.io.dumper import dump
    dump(obj=trjs,name=flnm,directory='./')
    print("done")

    print("dumping trajectories for R")
    flnm = 'trajs'+sffx+'.RData'
    from rpy2 import robjects
    from rpy2.robjects import r, pandas2ri
    pandas2ri.activate()
    if isinstance(trjs,dict):
        rlst = robjects.ListVector([(str(k),pandas2ri.py2rpy_pandasdataframe(trjs[k].applymap(str))) for k in trjs.keys()])
        robjects.r.assign("trajset", rlst)
        robjects.r("save(trajset, file='{}')".format(flnm))
    else:
        try:
            rdf = pandas2ri.py2rpy_pandasdataframe(trjs)
            robjects.r.assign("trajset", rdf)
            robjects.r("save(trajset, file='{}')".format(flnm))
        except:
            try:
                rdf = pandas2ri.py2rpy_pandasdataframe( trjs.applymap(str) )
                robjects.r.assign("trajset", rdf)
                robjects.r("save(trajset, file='{}')".format(flnm))
            except:
                raise ValueError(":: Fatal: rpy2 failed for {}".format(flnm))

    print("done")
#
#-----------------------------------------------------------------------------------------------------------------------
#
def dump_srcs(srcs=None,sffx=''):

    if srcs is None:
        print(":: Warning: cannot dump, traj is None")

    flnm = 'sources'+sffx+'.dat'
    print("dumping sources for python in file: {}".format(flnm))

    from spux.io.dumper import dump
    dump(obj=srcs,name=flnm,directory='./')
    print("done")

    print("dumping sources for R")
    flnm = 'sources'+sffx+'.RData'
    from rpy2 import robjects
    from rpy2.robjects import r, pandas2ri
    pandas2ri.activate()
    if isinstance(srcs,dict):
        rlst = robjects.ListVector([(str(k),pandas2ri.py2rpy_pandasdataframe(srcs[k].applymap(str))) for k in srcs.keys()])
        robjects.r.assign("sourceset", rlst)
        robjects.r("save(sourceset, file='{}')".format(flnm))
    else:
        try:
            rdf = pandas2ri.py2rpy_pandasdataframe(srcs)
            robjects.r.assign("sourceset", rdf)
            robjects.r("save(sourceset, file='{}')".format(flnm))
        except:
            try:
                rdf = pandas2ri.py2rpy_pandasdataframe( srcs.applymap(str) )
                robjects.r.assign("sourceset", rdf)
                robjects.r("save(sourceset, file='{}')".format(flnm))
            except:
                raise ValueError(":: Fatal: rpy2 failed for {}".format(flnm))

    print("done")
#
#-----------------------------------------------------------------------------------------------------------------------
#
def rm_params(params,samplables,states=None):
    """simple routine to avoid sampling params for which we do not have full state information"""

    batches = list(set(params['batches']))
    batches.sort()
    assert len(batches)==len(samplables), ":: Fatal: mismatch of lens in rm_params. This is a bug."

    foundit = False

    Ts = [ i for i,s in enumerate(samplables) if s == True ]
    if len(Ts) < 1:
        assert False, ":: Fatal: len(Ts) < 1. No samples available."
    Ts = min(Ts)
    Fs = [ i for i,s in enumerate(samplables) if s == False ]
    if len(Fs) < 1:
        if states is not None: # this is just a check
            for ibatch in batches:
                states.loc[states['batches']==ibatch]['chains']
                schains = list(set(states.loc[states['batches']==ibatch]['chains']))
                schains.sort()
                assert schains == chains, ":: Fatal: mismatch on chains in rm_params. This is a bug."
        return
    Fs = max(Fs)

    assert Fs + 1 == Ts, ":: Fatal: wrong structure in samplables. This is a bug."
    params = params.loc[params['batches'] >= batches[Ts]]
    assert params.shape[0] > 0, ":: Fatal: no parameters left. This is likely due to a too small number of loaded samples. Or a bug."

    batches = list(set(params['batches'])); batches.sort()
    chains = list(set(params['chains'])); chains.sort()

    if states is not None: # this is just a check
        for ibatch in batches:
            states.loc[states['batches']==ibatch]['chains']
            schains = list(set(states.loc[states['batches']==ibatch]['chains']))
            schains.sort()
            assert schains == chains, ":: Fatal: mismatch on chains in rm_params. This is a bug."

    return params
#
#-----------------------------------------------------------------------------------------------------------------------
#
def sample_params_and_states(params, states, nps=None, nparticles=None, seed=0): # nps=number of parameters samples
    """sample from params and states"""

    assert nps is not None, ":: Fatal: number of parameters samples is required."
    assert nparticles is not None, ":: Fatal: number of particles is required."
    assert nparticles > 0, ":: Fatal: number of particles must be positive as no particles means 1 particle (like 1 replica means 1 dataset)."

    if seed is not None:
        sampled_params = params.sample(nps,random_state=seed)
    else:
        sampled_params = params.sample(nps,random_state=seed)

    sampled_states = pandas.DataFrame(columns=states.columns)
    for irow in range(0,nps):
        tmpstates = states.loc[(states['batches']==sampled_params.iloc[irow]['batches']) &
                               (states['chains']==sampled_params.iloc[irow]['chains']) &
                               (states['replicates']==sampled_params.iloc[irow]['replicates'])]
        tmpstates = tmpstates.sample(nparticles,random_state=seed+irow,weights=tmpstates['weights'],replace=True)
        sampled_states = sampled_states.append(tmpstates,ignore_index=True,sort=False)

    return sampled_params, sampled_states
#
#-----------------------------------------------------------------------------------------------------------------------
#
def build_batches_chains_reps(inibatch,endbatch,nbatches,nchains,nreps,batches_names=None):
    """computes batches, chains and reps given their numbers - usually to be used to annotate a data frame"""

    ### 02/07/20: unfortunately it seems that csv files have different indexing than what I can recover from dat files.
    # my intuition tells me that there were batches that completely failed and were not written down as neither csv nor dat
    # files, but their existance is recorded in the indexing of csv files, which can jump by nchains between two adjacet batches

    if batches_names is None: # we should be here only if coming from a csv file reading
        batches = [ int(b) for b in range(inibatch,endbatch+1) for j in range(0,nreps*nchains) ]
        chains = [ int(c) for c in range(0,nchains) ]*nreps*nbatches
        reps = [ int(r) for r in range(0,nreps) for j in range(0,nchains) ]*nbatches
    else:
        assert len(batches_names) == nbatches, ":: Fatal: conflicting information from batches_names and nbatches. This is a bug."
        batches = [ int(b) for b in batches_names for j in range(0,nreps*nchains) ]
        chains = [ int(c) for c in range(0,nchains) ]*nreps*nbatches
        reps = [ int(r) for r in range(0,nreps) for j in range(0,nchains) ]*nbatches

    assert len(batches)==len(reps), ":: Fatal: len(batches)!=len(reps). This is a bug."
    assert len(batches)==len(chains), ":: Fatal: len(batches)!=len(chains). This is a bug."

    return batches, chains, reps
#
#-----------------------------------------------------------------------------------------------------------------------
#
def add_batches_chains_reps(batches,chains,reps,pddf):
    """add batches reps and chains from build_batches_chains_reps to a pandas data frame (pddf)"""

    colnms = ['batches','chains','replicates'] + list(pddf.columns)
    pddf = pandas.concat([
                            pandas.DataFrame(batches).set_index(pddf.index),
                            pandas.DataFrame(chains).set_index(pddf.index),
                            pandas.DataFrame(reps).set_index(pddf.index),
                            pddf,
                            ],axis=1)
    pddf.columns = colnms

    pddf.astype({'batches': 'int64'})
    pddf.astype({'chains': 'int64'})
    pddf.astype({'replicates': 'int64'})

    return pddf
#
#-----------------------------------------------------------------------------------------------------------------------
#
def reset_index_params(batches_names,nchains,nreps,params):
    """when loading dat files, samples pandas df has an index that start from 0, which is inconsistent with csv loading,
    here we fix that problem"""

    start = batches_names[0]*nreps*nchains # (batches_names[0]-1)*nreps*nchains ? - 18/08/20, I don't know
    end = start + len(params.index)
    params.index = [ x for x in range(start,end) ]

    return params
#
#-----------------------------------------------------------------------------------------------------------------------
#
def do_accepts(infos,nbatches,frombatch,uptobatch,nchains,nreps,batches_names):
    """Adapted from statistics of dump_results_2R to:
       (1) check that accepts information is consistent with successful information;
       (2) returns accepts as a single concatenated list.
    """
    assert nreps==1,":: Fatal: not ready yet for multiple data sets, I would have to go to 1-layer deeper in infos."
    if nbatches != uptobatch - frombatch + 1: #I've seen this happen on 07/Aug/2020 due to perhaps completely missing batch in SPUX data
        print(":: Warning: very bad sorcery.") #I hope this is still okay, but likely batch indexing will change irremediably between R and SPUX") - should not anymore

    keyb = 'infos'
    batches, chains, reps = build_batches_chains_reps(frombatch,uptobatch,nbatches,nchains,nreps,batches_names)
    for ibatch in range(0,nbatches):
        if ibatch==0:
            accepts = infos[ibatch]['accepts'].tolist()
        else:
            accepts = accepts + infos[ibatch]['accepts'].tolist()
        for ichain in range(0,nchains):
            if infos[ibatch][keyb] is not None:
                if infos[ibatch][keyb][ichain] is not None:
                    val = int(infos[ibatch][keyb][ichain]['successful']) # binary logical, 0 or 1
                else:
                    val = 0 #failed or not-accepted
            else:
                val = 0
                assert val == accepts[nchains*ibatch+ichain], ":: Fatal: mismatch between accepts and successful. This is a bug as an unsuccessful chain whas accepted?!?"
    assert len(accepts) == len(batches), ":: Fatal: inconsistent length for accepts. This is a bug."
    accepts = pandas.concat([ pandas.DataFrame(batches), pandas.DataFrame(chains), pandas.DataFrame(reps), pandas.DataFrame(accepts)],axis=1)
    accepts.columns = ['batches','chains','replicates','accepts']

    return accepts
#
#-----------------------------------------------------------------------------------------------------------------------
#
def do_chains_trace(accepts,batches_names,nchains):
    """maps chain (of a given batch) to parent batch"""

    chains_trace = pandas.DataFrame(index=batches_names,columns=range(0,nchains))

    for i,ibatch in enumerate(batches_names):
        if ibatch > batches_names[0]:
            chains_trace.loc[ibatch] = chains_trace.loc[batches_names[i-1]]
        chains_trace.loc[ibatch][accepts.loc[ (accepts['batches']==ibatch) & (accepts['accepts']==1) ]['chains']] = ibatch

    return chains_trace
#
#-----------------------------------------------------------------------------------------------------------------------
#
def check_accepts(accepts):
    """check whether there has been at least one accept event per chain or not"""

    chains = list(set(accepts['chains']))
    for ichain in chains:
        if sum(accepts.loc[accepts['chains']==ichain]['accepts']==1)==0:
            print(":: Warning: in the provided set of parameters samples chain {} has not accept events.".format(ichain))
#
#-----------------------------------------------------------------------------------------------------------------------
#
def load_dats(frombatch,uptobatch,loaddir):
        """ load the infos and samples files the way I need """

        # sanity checks
        assert os.path.isdir(loaddir),":: Fatal: missing load directory: {}.".format(loaddir)
        assert frombatch is not None, ": Fatal:: frombatch is None."
        assert frombatch >= 0, ": Fatal:: frombatch is negative."
        assert uptobatch >= 0, ": Fatal:: uptobatch is negative."
        assert uptobatch >= frombatch, ": Fatal:: uptobatch is < frombatch."

        if uptobatch is None: #this implies that frombatch == 0, see sanity_checks
            tmpnms = "samples-*.dat"
            tmpnmi = "infos-*.dat"
        else:
            tmpnms = [ "samples-%05d-*.dat" % i for i in numpy.arange(start=frombatch, stop=uptobatch+1) ]
            tmpnmi = [ "infos-%05d-*.dat" % i for i in numpy.arange(start=frombatch, stop=uptobatch+1) ]

        samples_names, dummyavails = loader.check_interval(tmpnms,directory=os.path.join(loaddir))
        infos_names, avail_batches = loader.check_interval(tmpnmi,directory=os.path.join(loaddir))

        params = pandas.concat (loader.loadall (name=samples_names,directory=os.path.join(loaddir), interactive=False), ignore_index=True, sort=True)
        infos = [ info for infos in loader.loadall (name=infos_names,directory=os.path.join(loaddir), interactive=False) for info in infos ]

        assert len(infos)==len(avail_batches),":: Fatal: Error in assessing who is who and which is which. This is a bug."
        tmplst = [x for x in range(avail_batches[0],avail_batches[-1]+1)]
        allthere = all(elem in avail_batches for elem in tmplst)
        if not allthere:
            print(":: Warning: some batch in between first and last failed completely. Best of luck.")

        if frombatch != avail_batches[0]:
            print(":: Warning: changing start batch from {} to {}".format(frombatch,avail_batches[0]))
            frombatch = avail_batches[0]
        if uptobatch != avail_batches[-1]:
            print(":: Warning: changing end batch from {} to {}".format(uptobatch,avail_batches[-1]))
            uptobatch = avail_batches[-1]

        # report
        print("\n################################################")
        print (' :: Loaded %d posterior params from %d chains' % (len (params), len (infos [0] ['likelihoods'])))
        print ('  : -> Indices: %d - %d' % (params.index [0], params.index [-1]))
        print("################################################")

        return avail_batches, params, infos, frombatch, uptobatch
#
#-----------------------------------------------------------------------------------------------------------------------
#
def get_basic_infos(infos,frombatch):
    """Serves to asses that the data structure is really as the coder (me) was expecting.
       Most likely, it will not work for parallel models as I anticipate one extra info layer in infos
    """

    # === some sanity checks and some assessments
    assert type(infos) == list, ":: Fatal: Wrong type of first level in infos. This is a bug."
    nbatches = len(infos)

    assert nbatches > 0, ":: Fatal: Not enough batches. This may be a wrong specification for uptobatch or simply a bug."
    names = []
    types = []
    isfirst = True
    nchains = 0
    nreps = 1
    warngiven = False
    for ibatch in range(0, nbatches): #always start from 0 regardless of value of frombatch, this is a spux-loading feature, checked on 21/Jan/2020
        counternones = 0
        if infos[ibatch]['index'] != frombatch+ibatch and warngiven == False:
            # this means that there is hole in SPUX batch IDs, like a lost batch. 07/Aug/20
            # To the best of my knowledge this will simply create a mismatch in the ID of batches from R and python, but nothing else
            print(":: Warning: Wrong indexing or some other sorcery...Others warnings of this type omitted.")
            warngiven =  True
        if isfirst == True:
            names.append([key for key in infos[ibatch].keys()])
            types.append( [ type(infos[ibatch][key]) for key in infos[ibatch].keys() ] )
            for keyb in infos[ibatch].keys():
                if keyb == 'infos':
                    try:
                        nchains = len(infos[ibatch][keyb])
                    except:
                        print(":: Warning: Something is wrong with the detection of nchains at first loaded batch. Let's try again.")
                        continue
                    assert nchains!=0,":: Fatal: no eligible chains detected. This may be a bug or an unsupported feature."
                    notdone = True
                    for ichain in range(0, nchains):
                        if infos[ibatch][keyb][ichain] is not None and notdone == True:
                            names.append([key for key in infos[ibatch][keyb][ichain].keys()])
                            types.append([ type(infos[ibatch][keyb][ichain][key]) for key in infos[ibatch][keyb][ichain].keys()])
                            notdone = False
                            try:
                                nreps = len(infos[ibatch][keyb][ichain]['infos'])
                                assert nreps > 0 # there is probably a reason if I am using a try / except constuct rather than an if here: I advise against changing this
                                have_reps = True
                            except:
                                have_reps = False
                                nreps = 1
                                print(":: Warning: No replicas detected.")
                            if have_reps == True:
                                for keyc in infos[ibatch][keyb][ichain].keys():
                                    if keyc == 'infos':
                                        replicas = list(infos[ibatch][keyb][ichain][keyc].keys())
                                        assert len(replicas)==nreps, ":: Fatal: got nreps != len(replicas). A bug or an unsupported feature."
                                        for irep in range(0, nreps):
                                            srep = replicas[irep]
                                            for keyr in infos[ibatch][keyb][ichain][keyc][srep].keys():
                                                assert keyr != 'infos', ":: Fatal: Too many infos. This is a bug or an unsupported feature."
                                            if irep == 0:
                                                names.append([ key for key in infos[ibatch][keyb][ichain][keyc][srep].keys() ])
                                                types.append([ type(infos[ibatch][keyb][ichain][keyc][srep][key]) for key in infos[ibatch][keyb][ichain][keyc][srep].keys()])
                                                assert len(names)==3, ":: Fatal: got wrong len of names at this point (1). This is a bug or an unsupported feature."
                                                if 'predictions_prior' in names[2]:
                                                    print("Found predictions_prior in names[2]: {}".format(names))
                                                    have_predictions_prior = True
                                                else:
                                                    have_predictions_prior = False
                                                    print(":: Warning: no information before resampling are available.")
                                                if 'predictions' in names[2]:
                                                    print("Found predictions in names[2]: {}".format(names))
                                                    have_predictions = True
                                                else:
                                                    have_predictions = False
                                                    raise ValueError(":: Fatal: Predictions are not there. This is a bug or an unsupported feature.")
                                            else:
                                                pass
                                    else:
                                        pass
                            else:
                                assert len(names)==2, ":: Fatal: got wrong len of names at this point (2). This is a bug or an unsupported feature."
                                if 'predictions_prior' in names[1]:
                                    print("Found predictions_prior in names[1]: {}".format(names))
                                    have_predictions_prior = True
                                else:
                                    have_predictions_prior = False
                                    print(":: Warning: no information before resampling are available.")
                                if 'predictions' in names[1]:
                                    print("Found predictions in names[1]: {}".format(names))
                                    have_predictions = True
                                else:
                                    have_predictions = False
                                    raise ValueError(":: Fatal: Predictions are not there.")
                        else:
                            if infos[ibatch][keyb][ichain] is None: #data structures from spux run can be very bad, especially if frombatch is not 0
                                counternones += 1
                                continue
                            else:
                                pass
                    if counternones > 0:
                        print(":: Got {} that failed or were rejected (infos[][infos][] is None) out of {} total chains for batch {} ".format(counternones,nchains,frombatch+ibatch))
                    isfirst = False
                else:
                    continue
        else: #is not first
            if ( ibatch < (nbatches - 1) or frombatch==0 ):
                for keyb in infos[ibatch].keys():
                    assert keyb in names[0], ":: Fatal: key {} not in names. This is a bug.".format(keyb)
                    assert keyb in infos[0].keys(), ":: Fatal: new key {} in batch infos.".format(keyb) #anyway, I saw keys change from first to other batches, but only reducing - but last one!!!
            keyb = 'infos'
            assert len(infos[ibatch][keyb])==nchains,":: Fatal: got inconsistent number of chains accross batches."
            for ichain in range(0,nchains):
                if infos[ibatch][keyb][ichain] is None:
                    counternones += 1
                    continue
                if have_reps == True:
                    keyc = 'infos'
                    replicas = list(infos[ibatch][keyb][ichain][keyc].keys()) #it may well be that it is fine that this does not pass the assert...I'll have to keep on eye on this with IBM
                    assert len(replicas)==nreps, ":: Fatal: got nreps != len(replicas). A bug or an unsupported feature."
                    for irep in range(0, nreps):
                        srep = replicas[irep]
                        for keyr in infos[ibatch][keyb][ichain][keyc][srep].keys():
                            assert keyr != 'infos', ":: Fatal: Too many infos. This is a bug or an unsupported feature."
                else:
                    pass
            if counternones > 0:
                print(":: Got {} chains that failed or were rejected (infos[][infos][] is None) out of {} total chains for batch {} ".format(counternones,nchains,frombatch+ibatch))

    #return nbatches, names, types, nchains, nreps, have_reps, have_predictions_prior, have_predictions
    return nbatches, nchains, nreps, have_reps, have_predictions_prior, have_predictions
#
#-----------------------------------------------------------------------------------------------------------------------
#
def read_dumpfl (dumpfl):
    """ read the rdump_file and returns the name of the quantities that should be converted across all possible pandas"""

    with open(dumpfl, 'r') as fl:
        lines = [line.rstrip() for line in fl]

    lines = list(line for line in lines if line) # Non-blank lines in a list

    dumpnms = [ re.split(' +|\\t|\n',line)[0] for line in lines ] #keep only first column

    return dumpnms
#
#-----------------------------------------------------------------------------------------------------------------------
#
def stats2pddf(colnms,nbatches,batches_names,infos,nchains,write_proposed,have_reps,keyb='infos'):
    """ convert sampling statistics to pandas df - it prolly needs get_basic_info before it can work """

    assert have_reps == False, ":: Fatal: This and all Rpp have not been developed to account for replicates yet. You must do a lot of devs. Best of luck."

    stats = pandas.DataFrame (columns=colnms)
    for ibatch in range(0,nbatches):
        batch_name = batches_names[ibatch]
        print('batch: ',batch_name,' on: ',batches_names[-1])
        block = None
        try:
            priors = infos[ibatch]['priors']
        except:
            assert False, ": Fatal:: this part of the code is outdated and should not be used without checking."
            priors = infos[ibatch]['proposals'] #forecast sampler

        sblock = list()
        for ichain in range(0,nchains):
            if infos[ibatch][keyb] is not None:
                if infos[ibatch][keyb][ichain] is not None:
                    #if write_proposed==False:
                    val = int(infos[ibatch][keyb][ichain]['successful']) # binary logical, 0 or 1
                    #else:
                    #    val = 1 #let's fake it was successful
                else:
                    val = 0 #failed or not-accepted
            else:
                val = 0
            sblock.append(val)

        if write_proposed==False:
            block = pandas.concat(
            [
            pandas.DataFrame(numpy.repeat(batch_name,nchains),columns=['batches']),
            pandas.DataFrame(numpy.arange(0,nchains),columns=['chains']),
            #with this I take currently accepted and valid old values, and forget about proposed but rejected
            pandas.DataFrame(infos[ibatch]['parameters']),
            pandas.DataFrame(infos[ibatch]['accepts'],columns=['accepts']), #if I fake that acceptance is always 1,
                                                        #my Rpp likely fails.With no fake,
                                                        #I just do what I have always been doing: throw away data until
                                                        # all chains have been accepted at least once - 21/07/20
            pandas.DataFrame(priors,columns=['priors']),
            pandas.DataFrame(infos[ibatch]['likelihoods'],columns=['likelihoods']),
            pandas.DataFrame(infos[ibatch]['posteriors'],columns=['posteriors']),
            pandas.DataFrame(sblock,columns=['successful']),
            ],axis=1)
        else:
            block = pandas.concat(
            [
            pandas.DataFrame(numpy.repeat(batch_name,nchains),columns=['batches']),
            pandas.DataFrame(numpy.arange(0,nchains),columns=['chains']),
            pandas.DataFrame(infos[ibatch]['proposes']), #.astype(str)),
            pandas.DataFrame(infos[ibatch]['accepts'],columns=['accepts']),
            pandas.DataFrame(priors,columns=['priors']),
            pandas.DataFrame(infos[ibatch]['likelihoods'],columns=['likelihoods']),
            pandas.DataFrame(infos[ibatch]['posteriors'],columns=['posteriors']),
            pandas.DataFrame(sblock,columns=['successful']),
            ],axis=1)

        assert all([x in colnms for x in block.columns]), ":: Fatal: mismatch naming of block and colnms. This is a bug."
        assert len(block.columns)==len(colnms), ":: Fatal: mismatch on len of block and colnms. This is a bug."
        stats = stats.append(block[colnms],sort=False) # columns can be differently ordered in SPUX...

        # this is a check on sanity of accepts info
        for ii in range(0,len(infos[ibatch]['accepts'])):
            if infos[ibatch]['accepts'][ii] == 1:
                assert all(infos[ibatch]['proposes'].iloc[ii]==infos[ibatch]['parameters'].iloc[ii])

    return stats
#
#-----------------------------------------------------------------------------------------------------------------------
#
def batch2pddf(ibatch,batches_names,nchains,
                infos,have_reps,dumpnms=None,case='',dotrajrecon=False,
                write_redraws=False,write_best=False,
                write_sources=False,keyb='infos',keyc='infos',reconstrucnms=['obs_errors','sources','weights']):
    """ convert sampling results to pandas df - it prolly needs get_basic_info before it can work """

    assert case in ['_prior',''],":: Fatal : got unknown case. This is a bug"
    print("\n=== converting trajectories for predictions ===")
    import time
    start_time = time.time()
    batch_name = batches_names[ibatch]
    first = True
    print('batch: ',batch_name,' on: ',batches_names[-1])
    countsane = 0
    nparticles = 0
    trjs = None
    srcsdf = None
    redraws = None
    best_trajs = None
    for ichain in range(0, nchains):
        #print(ichain,'on',nchains)
        is_sane = True
        if (infos[ibatch][keyb][ichain]) is None:
            continue
        if have_reps == False:
            srep = 0 #fake it
            times = [key for key in sorted(infos[ibatch][keyb][ichain]['predictions'+case].keys())]
            if len(times) == 0:
                print(':: Warning: Bonus no time. Best of Luck.')
                is_sane = False
                continue
            if is_sane == True:
                countsane += 1
                if first == True:
                    trjdatanms = [name for name in infos[ibatch][keyb][ichain]['predictions'+case][times[0]].columns]
                    if dumpnms is not None:
                        for colnm in trjdatanms:
                            if colnm not in dumpnms:
                                #print(":: Warning trjdatanms, removing col: {}".format(colnm))
                                trjdatanms.remove(colnm)
                    npredictions = len(trjdatanms)
                    predictionsnms = trjdatanms
                    trjdatanms = trjdatanms + reconstrucnms #replaced_by=sources+1
                    colnms = ['batches','chains','replicates','particles','times'] + trjdatanms
                    allcolnms = ['batches','chains','replicates','times','particles'] + trjdatanms
                    nparticles = infos[ibatch][keyb][ichain]['particles'] #if the information is not here, we are much better off to kill the job rather than guessing
                    assert nparticles > 0, ":: Fatal: got nparticles = 0. Unsupported feature or bug."
                    trjs = pandas.DataFrame (columns=colnms)
                    srcsdf = pandas.DataFrame (columns=['batches','chains','replicates','times']+[p for p in range(0,nparticles)])
                    redraws = pandas.DataFrame (columns = ['times', 'redraw'])
                    best_trajs = pandas.DataFrame (columns=['batches','chains','reps','particles','times']+predictionsnms+['best_error'])
                    first = False
                batches = numpy.repeat(batch_name,len(times)*nparticles)
                chains = [ichain]*nparticles*len(times)
                reps = [srep]*nparticles*len(times)
                alltimes = numpy.repeat(numpy.asarray(times),nparticles)
                particles = list(range(0,nparticles))*len(times)
                if dotrajrecon == True:
                    block1 = pandas.concat([
                    pandas.DataFrame(batches),
                    pandas.DataFrame(chains),
                    pandas.DataFrame(reps),
                    pandas.DataFrame(alltimes),
                    pandas.DataFrame(particles),
                    ],axis=1)
                else:
                    block1 = pandas.DataFrame(columns=[0]*5) # this 5 is ugly, corresponds to the number of cols of block1 as in the if just above
                #
                block2 = pandas.DataFrame (columns=trjdatanms)
                if not 'sources' in infos[ibatch][keyb][ichain].keys():
                    raise ValueError(":: Fatal: got no or corrupted sources information. If you know why, fix me.")
                if not 'redraw' in infos[ibatch][keyb][ichain].keys():
                    raise ValueError(":: Fatal: got no or corrupted redraw information. If you know why, fix me.")
                if write_redraws == True:
                    # redraws = pandas.DataFrame( list( infos[ibatch][keyb][ichain]['redraw'].items() ), columns = ['times', 'redraw'] ) # mfd
                    redraws = redraws.append(pandas.DataFrame( list( infos[ibatch][keyb][ichain]['redraw'].items() ), columns = ['times', 'redraw'] ))
                for i in range(0,len(times)):
                    full_predictions = pandas.DataFrame (columns=predictionsnms)
                    predictions = infos[ibatch][keyb][ichain]['predictions'+case][times[i]]
                    assert predictions.shape[0] <= nparticles, ":: Fatal: got predictions.shape[0] >= nparticles."
                    if 'errors'+case in infos[ibatch][keyb][ichain].keys():
                        if times[i] in infos[ibatch][keyb][ichain]['errors'+case]:
                            errors = infos[ibatch][keyb][ichain]['errors'+case][times[i]]
                        else:
                            errors = numpy.full(predictions.shape[0], numpy.nan)
                    if dumpnms is not None:
                        pnms = predictions.columns
                        for colnm in pnms:
                            if colnm not in dumpnms:
                                #print(":: Warning predictions, removing col: {}".format(colnm))
                                del predictions[colnm]
                    if times[i] in infos[ibatch][keyb][ichain]['sources'].keys():
                        sources = infos[ibatch][keyb][ichain]['sources'][times[i]]
                        weights = infos[ibatch][keyb][ichain]['weights'][times[i]]
                        assert sum(weights) == nparticles, ':: Fatal: got sum(weights) != nparticles'
                        #assert len(sources) == nparticles, ':: Fatal: Since 04/Feb/2020 I changed the code to store sources all. No more tricks please, but I want to provide backward compatibility
                        if times[i] in infos[ibatch][keyb][ichain]['errors'+case]: # to accommodate for warm-up timesets
                            for j in sources:
                                assert j in predictions.index, ":: Fatal: Found sources index absent in predictions. This is a bug."
                        elif len(sources) > predictions.shape[0]: # someone rewrote the history of particles between two observational points when using timeset but forgot to update sources
                            assert predictions.shape[0] == len(weights), ":: Fatal: mismatch on len of predictions and weights"
                            newsources = [ [x]*weights[i] for i, x in enumerate(predictions.index) ]
                            sources = [ item for sublist in newsources for item in sublist]
                            assert all([ x in predictions.index.tolist() for x in sources ]), ":: Fatal: some sources are not in predictions."
                            # this part would have been the way to go if someone did not delete the history of the particles at timeset point according to future-in-time particles resampling
                            #addthese = []
                            #for mp in range(0,nparticles):
                            #    if mp not in predictions.index:
                            #        predictions.loc [mp] = numpy.nan
                            #        addthese += [mp]
                            #    else:
                            #        assert mp in sources, ":: Fatal not in sources but in index."
                            #tmpweights = numpy.full(nparticles, numpy.nan)
                            #counter = 0
                            #for mp in range(0,nparticles):
                            #    if mp not in addthese:
                            #        tmpweights [mp] = weights [counter]
                            #        counter += 1
                            #    else:
                            #       assert all(numpy.isnan(predictions.loc[mp])), ":: Fatal: preds is not Nan for unknown weight"
                            #assert counter == len(weights), ":: Fatal: not all weights copied."
                            #weights = tmpweights.copy()
                            #assert predictions.shape[0] == nparticles, ":: Fatal: hard to believe."
                            #assert all(numpy.isnan(errors)), ":: Fatal: hard to believe as well"
                            errors = numpy.full(predictions.shape[0], numpy.nan)
                    else:
                        assert False, ":: Fatal: times[i] not in infos[ibatch][keyb][ichain]['sources'].keys(). This is a bug."
                    assert predictions.shape[0]==len(weights), ":: Fatal: predictions.shape[0]!=len(weights)"
                    assert predictions.shape[0]==len(errors), ":: Fatal: predictions.shape[0]!=len(errors)"
                    predictions = predictions.sort_index()
                #   
                    block1, block2, srcsdf, dotrajrecon = rebuild_results_(case,sources,nparticles,predictions,
                                        errors,weights,npredictions,trjdatanms,full_predictions,predictionsnms,batch_name,
                                        ichain,srep,i,times,dotrajrecon,block1,write_sources,srcsdf,block2)
                    assert block1.shape[0]==block2.shape[0], ":: Fatal: mism on shapes of blocks."
                #
                if write_best == True:
                    best_trajs = best_trajs.append(pandas.concat([
                        pandas.DataFrame([batch_name]*len(times),index=times,columns={'batches'}),
                        pandas.DataFrame([srep]*len(times),index=times,columns={'reps'}),
                        pandas.DataFrame([ichain]*len(times),index=times,columns={'chains'}),
                        pandas.DataFrame([float('NaN')]*len(times),index=times,columns={'particles'}),
                        pandas.DataFrame(times,index=times,columns={'times'}),
                        pandas.DataFrame(infos[ibatch][keyb][ichain]['best']['predictions']).transpose().sort_index(),
                        pandas.DataFrame([infos[ibatch][keyb][ichain]['best']['error']]*len(times),index=times,columns={'best_error'})
                        ],axis=1),
                    ignore_index=True,sort=False)
                assert block2.shape[0] == block1.shape[0], ":: Fatal, wrong shapes of blocks. This is bug."
                block2 = block2.set_index(numpy.arange(0,block1.shape[0]))
                block1 = block1.set_index(numpy.arange(0,block1.shape[0]))
                block = pandas.concat([block1, block2], axis=1)
                block.columns = allcolnms
                block = block.reindex(columns=colnms)
                block = block.sort_values(['particles', 'times', 'chains', 'replicates'])
                block = block[block['weights'].notna()] # weights right assigment is fundamental
                trjs = trjs.append(block)
                if write_redraws == True:
                    assert max(trjs['times']) == max(redraws['times']), ":: Fatal. Mismatch between time in redraw and trajectories. This is a bug."
        else: # we have replicates
            assert False, ":: Fatal: too outdated. Forget it."
            for irep in range(0, nreps):
                srep = replicas[irep]
                times = [key for key in sorted(infos[ibatch][keyb][ichain][keyc][srep]['predictions'+case].keys())]
                if len(times) == 0:
                    print(':: Warning: Bonus no time. Best of Luck.')
                    is_sane = False
                    continue
                if is_sane == True:
                    countsane += 1
                    if first == True:
                        trjdatanms = [name for name in infos[ibatch][keyb][ichain]['predictions'+case][times[0]].columns]
                        if dumpnms is not None:
                            for colnm in trjdatanms:
                                if colnm not in dumpnms:
                                    #print(":: Warning trjdatanms, removing col: {}".format(colnm))
                                    trjdatanms.remove(colnm)
                        npredictions = len(trjdatanms)
                        predictionsnms = trjdatanms
                        trjdatanms = trjdatanms + reconstrucnms #replaced_by=sources+1
                        colnms = ['batches','chains','replicates','particles','times'] + trjdatanms
                        allcolnms = ['batches','chains','replicates','times','particles'] + trjdatanms
                        nparticles = infos[ibatch][keyb][ichain][keyc][srep]['particles'] #if the information is not here, we are much better off to kill the job rather than guessing
                        assert nparticles > 0, ":: Fatal: got nparticles = 0. Unsupported feature or bug."
                        trjs = pandas.DataFrame (columns=colnms)
                        redraws = pandas.DataFrame (columns = ['times', 'redraw'])
                        best_trajs = pandas.DataFrame (columns=['batches','chains','reps','particles','times']+predictionsnms+['best_error'])
                        first = False
                    batches = numpy.repeat(batch_name,len(times)*nparticles)
                    chains = [ichain]*nparticles*len(times)
                    reps = [srep]*nparticles*len(times)
                    alltimes = numpy.repeat(numpy.asarray(times),nparticles)
                    particles = list(range(0,nparticles))*len(times)
                    if dotrajrecon == True:
                        block1 = pandas.concat([
                        pandas.DataFrame(batches),
                        pandas.DataFrame(chains),
                        pandas.DataFrame(reps),
                        pandas.DataFrame(alltimes),
                        pandas.DataFrame(particles),
                        ],axis=1)
                    else:
                        block1 = pandas.DataFrame(columns=[0]*5)
                    #
                    block2 = pandas.DataFrame (columns=trjdatanms)
                    if not 'sources' in infos[ibatch][keyb][ichain][keyc][srep].keys():
                        raise ValueError(":: Fatal: got no or corrupted sources information. If you know why, fix me.")
                    if not 'redraw' in infos[ibatch][keyb][ichain][keyc][srep].keys():
                        raise ValueError(":: Fatal: got no or corrupted redraw information. If you know why, fix me.")
                    if write_sources == True:
                        srcsdf = pandas.DataFrame (columns=['batches','chains','replicates','times']+[p for p in range(0,nparticles)])
                    if write_redraws == True:
                        # redraws = pandas.DataFrame( list( infos[ibatch][keyb][ichain]['redraw'].items() ), columns = ['times', 'redraw'] ) # mfd
                        redraws = redraws.append(pandas.DataFrame( list( infos[ibatch][keyb][ichain]['redraw'].items() ), columns = ['times', 'redraw'] ))
                    for i in range(0,len(times)):
                        full_predictions = pandas.DataFrame (columns=predictionsnms)
                        predictions = infos[ibatch][keyb][ichain][keyc][srep]['predictions'+case][times[i]]
                        assert predictions.shape[0] <= nparticles, ":: Fatal: got predictions.shape[0] >= nparticles."
                        if 'errors'+case in infos[ibatch][keyb][ichain].keys():
                            if times[i] in infos[ibatch][keyb][ichain]['errors'+case]:
                                errors = infos[ibatch][keyb][ichain][keyc][srep]['errors'+case][times[i]] #this may be bugged, but I need a trial and error session with reps 04/Feb/2020
                            else:
                                errors = numpy.full(predictions.shape[0], numpy.nan)
                        if dumpnms is not None:
                            pnms = predictions.columns
                            for colnm in pnms:
                                if colnm not in dumpnms:
                                    #print(":: Warning predictions, removing col: {}".format(colnm))
                                    del predictions[colnm]
                        if times[i] in infos[ibatch][keyb][ichain][keyc][srep]['sources'].keys():
                            sources = infos[ibatch][keyb][ichain][keyc][srep]['sources'][times[i]]
                            weights = infos[ibatch][keyb][ichain][keyc][srep]['weights'][times[i]]
                            assert sum(weights) == nparticles, ':: Fatal: got sum(weights) != nparticles'
                            #assert len(sources) == nparticles, ':: Fatal: Since 04/Feb/2020 I changed the code to store sources all. No more tricks please, but I want to provide backward compatibility
                            if times[i] in infos[ibatch][keyb][ichain][keyc][srep]['errors'+case]: # to accoMmodate for warm-up timesets
                                for j in sources:
                                    assert j in predictions.index,":: Fatal: Found sources index absent in predictions. This is a bug."
                            elif len(sources) > predictions.shape[0]: # to fix for PF filtering timeset back from 1st data point
                                predictions = predictions.append(pandas.DataFrame(data=[[numpy.nan]*predictions.shape[1]]*(nparticles-predictions.shape[0]),columns=predictions.columns),ignore_index=True)
                                weights = numpy.append(weights,[numpy.nan]*(nparticles-len(weights)))
                                assert predictions.shape[0] == nparticles, ":: Fatal: hard to believe."
                                assert all(numpy.isnan(errors)), ":: Fatal: hard to believe as well"
                                errors = numpy.full(predictions.shape[0], numpy.nan)
                        else:
                            assert False, ":: Fatal: times[i] not in infos[ibatch][keyb][ichain]['sources'].keys(). This is a bug."
                        assert predictions.shape[0]==len(weights), ":: Fatal: predictions.shape[0]!=len(weights)"
                        assert predictions.shape[0]==len(errors), ":: Fatal: predictions.shape[0]!=len(errors)"
                    #
                        #resmain_(sources,nparticles,predictions,errors,weights,trjdatanms,predictionsnms,batch_name,ichain,srep,i,times,allcolnms,colnms)
                        block1, block2, srcsdf, dotrajrecon = rebuild_results_(case,sources,nparticles,predictions,
                                        errors,weights,npredictions,trjdatanms,full_predictions,predictionsnms,batch_name,
                                        ichain,srep,i,times,dotrajrecon,block1,write_sources,srcsdf,block2)
                    #
                    if write_best == True:
                        best_trajs = best_trajs.append(pandas.concat([
                            pandas.DataFrame([batch_name]*len(times),index=times,columns={'batches'}),
                            pandas.DataFrame([srep]*len(times),index=times,columns={'reps'}),
                            pandas.DataFrame([ichain]*len(times),index=times,columns={'chains'}),
                            pandas.DataFrame([float('NaN')]*len(times),index=times,columns={'particles'}),
                            pandas.DataFrame(times,index=times,columns={'times'}),
                            pandas.DataFrame(infos[ibatch][keyb][ichain]['best']['predictions']).transpose().sort_index(),
                            pandas.DataFrame([infos[ibatch][keyb][ichain]['best']['error']]*len(times),index=times,columns={'best_error'})
                            ],axis=1),
                        ignore_index=True)
                    assert block2.shape[0] == block1.shape[0], ":: Fatal, wrong shapes of blocks. This is bug."
                    block2 = block2.set_index(numpy.arange(0,block1.shape[0]))
                    block1 = block1.set_index(numpy.arange(0,block1.shape[0]))
                    block = pandas.concat([block1, block2], axis=1)
                    block.columns = allcolnms
                    block = block.reindex(columns=colnms)
                    block = block.sort_values(['particles', 'times', 'chains', 'replicates'])
                    trjs = trjs.append(block)
                    if write_redraws == True:
                        assert max(trjs['times']) == max(redraws['times']), ":: Fatal. Mismatch between time in redraw and trajectories. This is a bug."

    print("--- it took %s seconds ---" % (time.time() - start_time))
    return countsane, trjs, srcsdf, redraws, best_trajs, dotrajrecon, batch_name, nparticles

#
#-----------------------------------------------------------------------------------------------------------------------
#
def rebuild_results_(case,sources,nparticles,predictions,errors,weights,npredictions,trjdatanms,
            full_predictions,predictionsnms,batch_name,ichain,srep,i,times,
            dotrajrecon,block1,write_sources,srcsdf,block2):
    """ old way of doing thigs (get enough information to reconstruct sampling) """

    if len(sources) < nparticles:
        if dotrajrecon == True:
            print(":: Warning, cannot reconstruct as sources information is not there. Disabling reconstruction. No further warnings. This may be a bug.")
            dotrajrecon = False
            #block1 = pandas.DataFrame(columns=[0]*5) # already set outside, mfd
    if dotrajrecon == True and predictions.shape[0] < nparticles:
        assert case == '', ":: Fatal: got predictions.shape[0] < nparticles before resampling."
        newweights = numpy.zeros(nparticles)
        newsources = numpy.arange(nparticles,dtype='float')
        newerrors = numpy.full(nparticles,float('NaN'))
        newindx = numpy.full(nparticles,numpy.nan)
        tmppred = predictions.copy()
        for ip in range(0,nparticles):
            newindx[ip] = predictions.index.get_loc(sources[ip])
        for ip in range(0, nparticles):
            predictions.loc[ip] = tmppred.loc[sources[ip]]
            newsources[ip] = sources[ip]
            newweights[ip] = weights[int(newindx[ip])]
            newerrors[ip] = errors[int(newindx[ip])]
        if len(weights) < nparticles: #overwrite only if necessary
            weights = newweights
        if len(sources) < nparticles: #don't want to overwrite sources if I have sources all
            assert dotrajrecon == False, ":: Fatal, wrong combo. This is a bug."
            sources = newsources
        if len(errors) < nparticles: #overwrite only if necessary
            errors = newerrors
        predictions = predictions.loc[[n for n in range(0,nparticles)]] #re-order come Dio comanda
        val = predictions
        full_predictions = full_predictions.append(val,ignore_index=True,sort=False)
        assert full_predictions.shape[0]==nparticles, ":: Fatal: Got wrong dimensions 0 in full_predictions. This is a bug."
        assert full_predictions.shape[1]==len(predictionsnms), ":: Fatal: Got wrong dimensions 1 in full_predictions. This is a bug."
        #
        isdup = pandas.DataFrame(sources).duplicated()
        counter = 0
        for ip in range(0,nparticles):
            if isdup[ip] == True: # is duplicated
                weights[ip] = 0
                counter += 1
        #
        val = pandas.concat([
        pandas.DataFrame(full_predictions),
        pandas.DataFrame(errors),
        pandas.DataFrame(sources), #cannot make this int (unless pandas >0.24), stackoverflow.com/questions/11548005/numpy-or-pandas-keeping-array-type-as-integer-while-having-a-nan-value - thanks God there's R
        pandas.DataFrame(weights)
        ],axis=1)
    elif dotrajrecon == True:
        val = pandas.concat([
        pandas.DataFrame(predictions,index=predictions.index),
        pandas.DataFrame(errors,index=predictions.index),
        pandas.DataFrame(sources,index=predictions.index),
        pandas.DataFrame(weights,index=predictions.index)
        ],axis=1)
    else:
        val = pandas.concat([
        pandas.DataFrame(predictions,index=predictions.index),
        pandas.DataFrame(errors,index=predictions.index),
        pandas.DataFrame(set(sources),index=predictions.index),
        pandas.DataFrame(weights,index=predictions.index)
        ],axis=1)
        tmpbatches = numpy.repeat(batch_name,val.shape[0])
        tmpchains = [ichain]*val.shape[0]
        tmpreps = [srep]*val.shape[0]
        tmptimes = numpy.repeat(times[i],val.shape[0])
        tmpprtcls = list(set(sources))
        tmpblck = pandas.concat([
                  pandas.DataFrame(tmpbatches),
                  pandas.DataFrame(tmpchains),
                  pandas.DataFrame(tmpreps),
                  pandas.DataFrame(tmptimes),
                  pandas.DataFrame(tmpprtcls),
                  ],axis=1)
        block1 = block1.append(tmpblck) # only in this case I have to touch block1
    val.columns = trjdatanms
    if write_sources == True:
        #srcsdf = srcsdf.append(pandas.DataFrame(sources).transpose())
        tmpsources = pandas.concat([pandas.DataFrame([batch_name]),pandas.DataFrame([ichain]),pandas.DataFrame([srep]), pandas.DataFrame([times[i]]),pandas.DataFrame(sources).transpose()],axis=1)
        tmpsources.columns = srcsdf.columns
        srcsdf = srcsdf.append(tmpsources)

    block2 = block2.append(val)

    return block1, block2, srcsdf, dotrajrecon
#
#-----------------------------------------------------------------------------------------------------------------------
#
### mfd ##############################
#def get_basic_infos(infos,frombatch):
#    """Serves to asses that the data structure is really as the coder (me) was expecting.
#       Most likely, it will not work for parallel models as I anticipate one extra info layer in infos
#    """
#
#    # === some sanity checks and some assessments
#    assert type(infos) == list, ":: Fatal: Wrong type of first level in infos. This is a bug."
#    nbatches = len(infos)
#
#    assert nbatches > 0, ":: Fatal: Not enough batches. This may be a wrong specification for uptobatch or simply a bug."
#    names = []
#    types = []
#    isfirst = True
#    nchains = 0
#    nreps = 0
#    for ibatch in range(0, nbatches):
#        counternones = 0
#        assert type(infos[ibatch]) == dict, ":: Fatal: Wrong type of second level in infos. This is a bug."
#        if isfirst == True:
#            names.append([key for key in infos[ibatch].keys()])
#            types.append( [ type(infos[ibatch][key]) for key in infos[ibatch].keys() ] )
#            for keyb in infos[ibatch].keys():
#                if keyb == 'infos':
#                    try:
#                        nchains = len(infos[ibatch][keyb])
#                    except:
#                        print(":: Warning: Something is wrong with the detection of nchains at first loaded batch. Let's try again.")
#                        continue
#                    assert nchains!=0,":: Fatal: no eligible chains detected. This may be a bug or an unsupported feature."
#                    notdone = True
#                    for ichain in range(0, nchains):
#                        if infos[ibatch][keyb][ichain] is not None and notdone == True:
#                            names.append([key for key in infos[ibatch][keyb][ichain].keys()])
#                            types.append([ type(infos[ibatch][keyb][ichain][key]) for key in infos[ibatch][keyb][ichain].keys()])
#                            notdone = False
#                            try:
#                                nreps = len(infos[ibatch][keyb][ichain]['infos'])
#                                assert nreps > 0 # there is probably a reason if I am using a try / except constuct rather than an if here: I advise against changing this
#                                have_reps = True
#                            except:
#                                have_reps = False
#                                nreps = 0
#                                print(":: Warning: No replicas detected.")
#                            if have_reps == True:
#                                for keyc in infos[ibatch][keyb][ichain].keys():
#                                    if keyc == 'infos':
#                                        replicas = list(infos[ibatch][keyb][ichain][keyc].keys())
#                                        assert len(replicas)==nreps, ":: Fatal: got nreps != len(replicas). A bug or an unsupported feature."
#                                        for irep in range(0, nreps):
#                                            srep = replicas[irep]
#                                            for keyr in infos[ibatch][keyb][ichain][keyc][srep].keys():
#                                                assert keyr != 'infos', ":: Fatal: Too many infos. This is a bug or an unsupported feature."
#                                            if irep == 0:
#                                                names.append([ key for key in infos[ibatch][keyb][ichain][keyc][srep].keys() ])
#                                                types.append([ type(infos[ibatch][keyb][ichain][keyc][srep][key]) for key in infos[ibatch][keyb][ichain][keyc][srep].keys()])
#                                                assert len(names)==3, ":: Fatal: got wrong len of names at this point (1). This is a bug or an unsupported feature."
#                                                if 'predictions_prior' in names[2]:
#                                                    print("Found predictions_prior in names[2]: {}".format(names))
#                                                    have_predictions_prior = True
#                                                else:
#                                                    have_predictions_prior = False
#                                                    print(":: Warning: no information before resampling are available.")
#                                                if 'predictions' in names[2]:
#                                                    print("Found predictions in names[2]: {}".format(names))
#                                                    have_predictions = True
#                                                else:
#                                                    have_predictions = False
#                                                    raise ValueError(":: Fatal: Predictions are not there. This is a bug or an unsupported feature.")
#                                            else:
#                                                pass
#                                    else:
#                                        pass
#                            else:
#                                assert len(names)==2, ":: Fatal: got wrong len of names at this point (2). This is a bug or an unsupported feature."
#                                if 'predictions_prior' in names[1]:
#                                    print("Found predictions_prior in names[1]: {}".format(names))
#                                    have_predictions_prior = True
#                                else:
#                                    have_predictions_prior = False
#                                    print(":: Warning: no information before resampling are available.")
#                                if 'predictions' in names[1]:
#                                    print("Found predictions in names[1]: {}".format(names))
#                                    have_predictions = True
#                                else:
#                                    have_predictions = False
#                                    raise ValueError(":: Fatal: Predictions are not there.")
#                        else:
#                            if infos[ibatch][keyb][ichain] is None: #data structures from spux run can be very bad, especially if frombatch is not 0
#                                counternones += 1
#                                continue
#                            else:
#                                pass
#                    if counternones > 0:
#                        print(":: Got {} chains that failed or were rejected (infos[][infos][] is None) out of {} total chains for batch {} ".format(counternones,nchains,frombatch+ibatch))
#                    isfirst = False
#                else:
#                    continue
#        else: #is not first
#            if ( ibatch < (nbatches - 1) or frombatch==0 ):
#                for keyb in infos[ibatch].keys():
#                    assert keyb in names[0], ":: Fatal: key {} not in names. This is a bug.".format(keyb)
#                    assert keyb in infos[0].keys(), ":: Fatal: new key {} in batch infos.".format(keyb) #anyway, I saw keys change from first to other batches, but only reducing - but last one!!!
#            keyb = 'infos'
#            assert len(infos[ibatch][keyb])==nchains,":: Fatal: got inconsistent number of chains accross batches."
#            for ichain in range(0,nchains):
#                if infos[ibatch][keyb][ichain] is None:
#                    counternones += 1
#                    continue
#                if have_reps == True:
#                    keyc = 'infos'
#                    replicas = list(infos[ibatch][keyb][ichain][keyc].keys()) #it may well be that it is fine that this does not pass the assert...I'll have to keep on eye on this with IBM
#                    assert len(replicas)==nreps, ":: Fatal: got nreps != len(replicas). A bug or an unsupported feature."
#                    for irep in range(0, nreps):
#                        srep = replicas[irep]
#                        for keyr in infos[ibatch][keyb][ichain][keyc][srep].keys():
#                            assert keyr != 'infos', ":: Fatal: Too many infos. This is a bug or an unsupported feature."
#                else:
#                    pass
#                print(":: Got {} chains that failed or were rejected (infos[][infos][] is None) out of {} total chains for batch {} ".format(counternones,nchains,frombatch+ibatch))
#
#    return nbatches, nchains, nreps
######################################
