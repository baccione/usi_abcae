# # # # # # # # # # # # # # # # # # # # # # # # # #
# Generate various LaTeX code for tables and figures
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

import glob
import os
import shutil
import numpy
import re

from ..io import dumper
from ..utils import shell

def txt_table (entries, headers=None, title=None, align='l', formatters = {}, widths=None):
    """Generate an ASCII table from the list of dicts with table entries."""

    if headers is None:
        headers = list (entries [0] .keys ())


    if type (align) == list:
        aligns = {header : align [column] for column, header in enumerate (headers)}
    else:
        aligns = {header : align for header in headers}
    for header in headers:
        if aligns [header] == 'c':
            aligns [header] = 'center'
        elif aligns [header] in ['l', 'r']:
            aligns [header] += 'just'
        else:
            aligns [header] = 'ljust'

    entries = [{header : (formatters [header] (entry [header]) if header in formatters else entry [header]) for header in headers} for entry in entries]

    if widths is None:
        widths = [max ([len (str (entry [header])) for entry in entries] + [len (header)]) for header in headers]
    extent = sum (widths) + 3 * (len (entries [0]) - 1)
    if title is not None:
        extent = max (extent, len (title))

    text = ''
    text += '=' * extent + '\n'

    if title is not None:
        text += title + ' ' * (extent - len (title)) + '\n'
        text += '=' * extent + '\n'

    text += ' | '.join ([getattr (header, aligns [header]) (width) for header, width in zip (headers, widths)]) + '\n'
    text += ' + '.join (['-' * width for width in widths]) + '\n'
    for entry in entries:
        text += ' | '.join ([getattr (str (entry [header]), aligns [header]) (width) for header, width in zip (headers, widths)]) + '\n'
    text += '=' * extent
    return text

def tex_escape (text):
    """
        :param text: a plain text message
        :return: the message escaped to appear correctly in LaTeX
    """

    conv = {
        '&': r'\&',
        '%': r'\%',
        '$': r'\$',
        '#': r'\#',
        '_': r'\_',
        '{': r'\{',
        '}': r'\}',
        '~': r'\textapprox',
        '^': r'\^{}',
        '\\': r'\textbackslash{}',
        '<': r'\textless{}',
        '>': r'\textgreater{}',
    }
    regex = re.compile('|'.join(re.escape(str(key)) for key in sorted(conv.keys(), key = lambda item: - len(item))))
    return regex.sub(lambda match: conv[match.group()], text)

def tex_table (entries, headers=None, align='l', formatters={}, math=False, latex=False, columns=None):
    """Generate TeX code with the table from the specified entries and headers."""

    if headers is None:
        headers = list (entries [0] .keys ())

    if columns is not None:
        tables = int (numpy.ceil (len (headers) / columns))
        batches = numpy.array_split (numpy.array (headers), tables)
        return '\n\n'.join ([tex_table (entries, batch, align, formatters, math, latex) for batch in batches])

    if type (align) == list:
        aligns = '|' + '|'.join (align) + '|'
    else:
        aligns = '|' + (align + '|') * len (entries [0])

    text = r'\begin{tabular}{%s}' % aligns + '\n'
    text += r'\hline' + '\n'
    text += (r' & '.join ([r'%s'] * len (headers))) % tuple (headers) + r'\\' + '\n'
    text += r'\hline' + '\n'
    text += r'\hline' + '\n'
    for entry in entries:
        template = r'$%s$' if math else '%s'
        cells = {header : (formatters [header] (entry [header]) if header in formatters else entry [header]) for header in headers}
        text += r' & '.join ([template % (cells [header] if math or latex else tex_escape (str (cells [header]))) for header in headers]) + r'\\' + '\n'
        text += r'\hline' + '\n'

    text += r'\end{tabular}'

    return text

def report (authors=None, title=None, date=None, order=None, reportdir='report', figuredir='fig', latexdir='latex', htmldir='htmldir', scriptsdir='.', figuretype='png'):
    """Generates latex source files and the compiled pdf for the SPUX report."""

    import PIL

    print (' :: Generating LaTeX report in "%s" directory...' % latexdir)
    print ('  : -> HTML files (e.g. for Authorea) are generated in "%s" directory...' % htmldir)

    if authors is None:
        authors = 'Author(s) not specified'

    if title is None:
        title = 'SPUX report for "%s" inference' % dumper.prefixname ()

    if date is None:
        date = r'\today'

    if order is None:

        order = {}

        order ['reports'] = {}
        order ['reports'] ['setup'] = ['config', 'units', 'exact']
        order ['reports'] ['results'] = ['environment', 'resources', 'setup', 'evaluations', 'best', 'infos']
        order ['reports'] ['diagnostics'] = ['metrics']
        order ['reports'] ['performance'] = ['runtimes']

        order ['figures'] = {}
        order ['figures'] ['setup'] = []
        order ['figures'] ['setup'] += ['dataset']
        order ['figures'] ['setup'] += ['errors']
        order ['figures'] ['setup'] += ['distributions-prior']
        order ['figures'] ['setup'] += ['distributions-initial']

        order ['figures'] ['results'] = []
        order ['figures'] ['results'] += ['posteriors']
        order ['figures'] ['results'] += ['posterior2d']
        order ['figures'] ['results'] += ['predictions']

        order ['figures'] ['diagnostics'] = []
        order ['figures'] ['diagnostics'] += ['qq']
        order ['figures'] ['diagnostics'] += ['unsuccessfuls']
        order ['figures'] ['diagnostics'] += ['parameters']
        order ['figures'] ['diagnostics'] += ['acceptances']
        order ['figures'] ['diagnostics'] += ['resets']
        order ['figures'] ['diagnostics'] += ['autocorrelations']
        order ['figures'] ['diagnostics'] += ['likelihoods']
        order ['figures'] ['diagnostics'] += ['distances']
        order ['figures'] ['diagnostics'] += ['fitscores']
        order ['figures'] ['diagnostics'] += ['accuracies']
        order ['figures'] ['diagnostics'] += ['particles']
        order ['figures'] ['diagnostics'] += ['redraw']

        order ['figures'] ['performance'] = []
        order ['figures'] ['performance'] += ['runtime']
        order ['figures'] ['performance'] += ['timestamps']
        order ['figures'] ['performance'] += ['efficienc']
        order ['figures'] ['performance'] += ['traffic']

    directory, filename = os.path.split (os.path.realpath (__file__))
    templatesdir = os.path.join (directory, 'templates')

    dumper.mkdir (latexdir, fresh = True)

    # read table template
    with open (os.path.join (templatesdir, 'table.tex'), 'r') as file:
        tabletex = file.read ()

    # read figure template
    with open (os.path.join (templatesdir, 'figure.tex'), 'r') as file:
        figuretex = file.read ()

    # read frame template
    with open (os.path.join (templatesdir, 'frame.tex'), 'r') as file:
        frametex = file.read ()

    def generate_section (section, template, texs = None, pdfs = None):

        # read section template
        with open (os.path.join (templatesdir, os.path.join ('report', template)), 'r') as file:
            templatetex = file.read ()

        # read section slides template
        with open (os.path.join (templatesdir, os.path.join ('slides', template)), 'r') as file:
            templatetex_slides = file.read ()

        # get all .tex files from the reportdir
        if texs is None:
            texs = glob.glob (os.path.join (reportdir, '*.tex'))
            texs = sorted ([os.path.basename (tex) for tex in texs])
            if len (texs) == 0:
                print (' :: WARNING: No reports found in %s.' % reportdir)
        texs_used = {tex : False for tex in texs}
        reports = []

        # add all .tex files into report
        if len (texs) > 0:
            for name in order ['reports'] [section]:
                for tex in texs:
                    if name in tex:
                        captionfile = os.path.join (reportdir, tex [:-4] + '.cap')
                        if os.path.exists (captionfile):
                            with open (captionfile, 'r') as file:
                                caption = tex_escape (file.read ())
                        else:
                            caption = 'Table caption is not specified.'
                        args = {'table' : tex, 'caption' : caption, 'label' : tex [:-4]}
                        reports += [tabletex % args]
                        texs_used [tex] = True

        # get all .figuretype files from the figuredir
        if pdfs is None:
            pdfs = glob.glob (os.path.join (figuredir, '*.%s' % figuretype))
            pdfs = sorted ([os.path.basename (pdf) for pdf in pdfs])
            if len (pdfs) == 0:
                print (' :: WARNING: No \'%s\' figures found in %s.' % (figuretype, figuredir))
        pdfs_used = {pdf : False for pdf in pdfs}
        figures = []

        # add all .pdf files into report (include .cap captions, if available)
        if len (pdfs) > 0:
            for name in order ['figures'] [section]:
                for pdf in pdfs:
                    if name in pdf:
                        captionfile = os.path.join (figuredir, pdf [:-4] + '.cap')
                        if os.path.exists (captionfile):
                            with open (captionfile, 'r') as file:
                                caption = tex_escape (file.read ())
                        else:
                            caption = 'Figure caption is not specified.'
                        pngfile = os.path.join (figuredir, pdf [:-4] + '.png')
                        width = min (1, max (0.5, PIL.Image.open (pngfile) .size [0] / 4000))
                        args = {'figure' : pdf [:-4], 'width' : width, 'caption' : caption, 'label' : pdf [:-4]}
                        figures += [figuretex % args]
                        pdfs_used [pdf] = True

        # save generated section
        args = {}
        args ['reports'] = '\n\n'.join (reports) if len (reports) > 0 else ''
        args ['figures'] = '\n\n'.join (figures) if len (figures) > 0 else ''
        dumper.text (templatetex % args, template, latexdir, prefix=True)

        # save generated section for slides
        if len (figures) > 0:
            figures_slides = [frametex % {'frameoptions' : 'plain', 'content' : figure} for figure in figures]
        else:
            figures_slides = None
        args = {}
        args ['reports'] = (frametex % {'frameoptions' : 'plain,allowframebreaks', 'content' : '\n\n'.join (reports)}) if len (reports) > 0 else ''
        args ['figures'] = '\n\n'.join (figures_slides) if figures_slides is not None else ''
        dumper.text (templatetex_slides % args, template.replace ('.tex', '_slides.tex'), latexdir, prefix=True)

        # return used tables and figures
        return texs_used, pdfs_used

    # generate setup section
    texs_setup, pdfs_setup = generate_section (section = 'setup', template = 'setup.tex')

    # generate results section
    texs_results, pdfs_results = generate_section (section = 'results', template = 'results.tex')

    # generate diagnostics section
    texs_diagnostics, pdfs_diagnostics = generate_section (section = 'diagnostics', template = 'diagnostics.tex')

    # generate performance section
    texs_performance, pdfs_performance = generate_section (section = 'performance', template = 'performance.tex')

    # filter all remaining (miscellaneous) reports and figures
    texs_misc = [tex for tex in texs_setup.keys () if not (texs_setup [tex] or texs_results [tex] or texs_diagnostics [tex] or texs_performance [tex])]
    pdfs_misc = [pdf for pdf in pdfs_setup.keys () if not (pdfs_setup [pdf] or pdfs_results [pdf] or pdfs_diagnostics [pdf] or pdfs_performance [pdf])]

    # generate miscellaneous section
    order ['reports'] ['miscellaneous'] = texs_misc
    order ['figures'] ['miscellaneous'] = pdfs_misc
    generate_section (section = 'miscellaneous', template = 'miscellaneous.tex', texs = texs_misc, pdfs = pdfs_misc)

    # read listings template
    with open (os.path.join (templatesdir, 'listing.tex'), 'r') as file:
        listingtex = file.read ()

    # read scripts template
    with open (os.path.join (templatesdir, os.path.join ('report', 'scripts.tex')), 'r') as file:
        scriptstex = file.read ()

    # read scripts slides template
    with open (os.path.join (templatesdir, os.path.join ('slides', 'scripts.tex')), 'r') as file:
        scriptstex_slides = file.read ()

    # get all .py files from the scriptsdir
    pys = glob.glob (os.path.join (scriptsdir, '*.py'))
    pys = sorted ([os.path.basename (py) for py in pys])
    if len (pys) == 0:
        print (' :: WARNING: No scripts found in %s.' % scriptsdir)
    scripts = []

    # add all .py files into report
    if len (pys) > 0:
        for py in pys:
            args = {'name' : py}
            scripts += [listingtex % args]

    # generate section with all scripts
    args = {}
    args ['listings'] = '\n\n'.join (scripts) if len (scripts) > 0 else 'No scripts found.'
    dumper.text (scriptstex % args, 'scripts.tex', latexdir, prefix=True)

    # generate section slides with all scripts
    if len (scripts) > 0:
        scripts_slides = [frametex % {'frameoptions' : 'plain,allowframebreaks,squeeze,t', 'content' : '\\vspace*{-10pt}\n' + script} for script in scripts]
    else:
        scripts_slides = [frametex % {'frameoptions' : 'plain,allowframebreaks,squeeze,t', 'content' : 'No scripts found.'}]
    args = {}
    args ['listings'] = '\n\n'.join (scripts_slides)
    dumper.text (scriptstex_slides % args, 'scripts_slides.tex', latexdir, prefix=True)

    # read title template and fill it
    with open (os.path.join (templatesdir, 'title.tex'), 'r') as file:
        titletex = file.read ()
        args = {'title' : title}
        dumper.text (titletex % args, 'title.tex', latexdir, prefix=False)

    # read report template and fill it
    with open (os.path.join (templatesdir, 'report.tex'), 'r') as file:
        reporttex = file.read ()
        args = {'authors' : authors, 'date' : date, 'figuretype' : figuretype}
        args ['setup'] = dumper.prefixname ('setup.tex')
        args ['results'] = dumper.prefixname ('results.tex')
        args ['diagnostics'] = dumper.prefixname ('diagnostics.tex')
        args ['performance'] = dumper.prefixname ('performance.tex')
        args ['miscellaneous'] = dumper.prefixname ('miscellaneous.tex')
        args ['scripts'] = dumper.prefixname ('scripts.tex')
        dumper.text (reporttex % args, 'report.tex', latexdir, prefix=True)

    # read report slides template and fill it
    with open (os.path.join (templatesdir, 'report_slides.tex'), 'r') as file:
        reporttex = file.read ()
        args = {'authors' : authors, 'date' : date, 'figuretype' : figuretype}
        args ['setup'] = dumper.prefixname ('setup_slides.tex')
        args ['results'] = dumper.prefixname ('results_slides.tex')
        args ['diagnostics'] = dumper.prefixname ('diagnostics_slides.tex')
        args ['performance'] = dumper.prefixname ('performance_slides.tex')
        args ['miscellaneous'] = dumper.prefixname ('miscellaneous_slides.tex')
        args ['scripts'] = dumper.prefixname ('scripts_slides.tex')
        dumper.text (reporttex % args, 'report_slides.tex', latexdir, prefix=True)

    # copy remaining needed files (header, logo, etc.)
    shutil.copyfile (os.path.join (templatesdir, 'header.tex'), os.path.join (latexdir, 'header.tex'))
    shutil.copyfile (os.path.join (templatesdir, 'spux-logo.png'), os.path.join (latexdir, 'spux-logo.png'))

    # compile PDF
    if shutil.which ('pdflatex') is None:

        print (' :: SKIPPING: Command "pdflatex" is not available - PDF will not be compiled.')

    else:

        # compile report
        reportfile = dumper.prefixname ('report.pdf')
        name = os.path.join (latexdir, reportfile)
        if os.path.exists (name):
            os.remove (name)
            print ('  : -> Removing previously generated report PDF...')
        command = 'pdflatex -halt-on-error -interaction=nonstopmode %s' % dumper.prefixname ('report.tex')
        shell.execute (command, latexdir)
        if os.path.exists (name):
            print ('  : -> Copying report PDF to the current working directory...')
            shutil.copyfile (name, os.path.join (os.getcwd (), reportfile))

        # compile report slides
        reportfile = dumper.prefixname ('report_slides.pdf')
        name = os.path.join (latexdir, reportfile)
        if os.path.exists (name):
            os.remove (name)
            print ('  : -> Removing previously generated report slides PDF...')
        command = 'pdflatex -halt-on-error -interaction=nonstopmode %s' % dumper.prefixname ('report_slides.tex')
        shell.execute (command, latexdir)
        if os.path.exists (name):
            print ('  : -> Copying report slides PDF to the current working directory...')
            shutil.copyfile (name, os.path.join (os.getcwd (), reportfile))