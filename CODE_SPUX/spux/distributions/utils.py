
import math
import numpy

class Transform (object):
    """A class to transform a distribution by applying a differentiable and invertible transformation."""

    def __init__ (self, distribution, T, Ti, dTi):

        self.distribution = distribution
        self.T = T
        self.Ti = Ti
        self.dTi = dTi
        atomic = distribution.atomic if hasattr (distribution, 'atomic') else []
        self.inverses = {T (x) : x for x in atomic}
        self.atomic = list (self.inverses.keys ())

    def pdf (self, x):

        if x in self.atomic:
            return self.distribution.pdf (self.inverses [x])
        else:
            return self.distribution.pdf (self.Ti (x)) * self.dTi (x)

        #return self.distribution.pdf (self.Ti (x)) * self.dTi (x)

    def logpdf (self, x):

        if x in self.atomic:
            return self.distribution.logpdf (self.inverses [x])
        else:
            return self.distribution.logpdf (self.Ti (x)) + math.log (self.dTi (x))

        #return self.distribution.logpdf (self.Ti (x)) + math.log (self.dTi (x))

    def interval (self, alpha):

        interval = self.distribution.interval (alpha)
        return [self.T (interval [0]), self.T (interval [1])]

    def rvs (self, random_state = None):

        draw = self.distribution.rvs (random_state = random_state)
        return self.T (draw)

class Concentrate (object):
    """A class to concentrate tail(s) of a distribution to an atomic part at the desired edge location(s)."""

    def __init__ (self, distribution, left = None, right = None):

        self.distribution = distribution
        self.left = left
        self.right = right

        self.atomic = []
        if left is not None:
            self.atomic.append (left)
        if right is not None:
            self.atomic.append (right)

    def pdf (self, x):
        try:
            if self.left is not None and x < self.left:
                return 0

            if self.right is not None and x > self.right:
                return 0

            return self.distribution.pdf (x)

        except:

            res = self.distribution.pdf (x)

            if self.left is not None:
                indx = numpy.where(x < self.left)
                try:
                    res [indx] = 0.0
                    indx = numpy.where(x==self.left)
                    if len(indx)>0:
                        res [indx] = self.distribution.cdf (self.left)
                except:
                    res = 0.0

            if self.right is not None:
                indx = numpy.where(x > self.right)
                try:
                    res [indx] = 0.0
                    indx = numpy.where(x==self.right)
                    if len(indx)>0:
                        res [indx] = self.distribution.cdf (self.right)
                except:
                    res = 0.0

            return res

    def logpdf (self, x):

        if self.left is not None:
            if x < self.left:
                return float ('-inf')
            elif x == self.left:
                return self.distribution.logcdf (self.left)

        if self.right is not None:
            if x > self.right:
                return float ('-inf')
            elif x == self.right:
                return self.distribution.logsf (self.right)

        return self.distribution.logpdf (x)

    def interval (self, alpha):

        interval = list (self.distribution.interval (alpha))
        if self.left is not None:
            interval [0] = max (self.left, interval [0])
        if self.right is not None:
            interval [1] = min (self.right, interval [1])
        return interval

    def rvs (self, random_state = None):

        draw = self.distribution.rvs (random_state = random_state)
        if self.left is not None and draw < self.left:
            return self.left
        if self.right is not None and draw > self.right:
            return self.right
        return draw

    def kwds (self):
        res = self.distribution.kwds
        return res

    def mean (self):
        res = self.distribution.mean()
        return res

class Truncate (object):
    """A class to truncate tail(s) of a distribution at the desired edge location(s)."""

    def __init__ (self, distribution, left = None, right = None):

        self.distribution = distribution
        self.left = left
        self.right = right

        mass = 1
        if left is not None:
            mass -= distribution.cdf (left)
        if right is not None:
            mass -= distribution.sf (right)
        self.scale = 1 / mass
        self.logscale = - math.log (mass)

    def pdf (self, x):
        try:
            if self.left is not None and x < self.left:
                return 0

            if self.right is not None and x > self.right:
                return 0

            return self.scale * self.distribution.pdf (x)

        except:

            res = self.scale * self.distribution.pdf (x)

            if self.left is not None:
                indx = numpy.where(x < self.left)
                try:
                  res [indx] = 0.0
                except:
                    res = 0.0

            if self.right is not None:
                indx = numpy.where(x > self.right)
                try:
                    res [indx] = 0.0
                except:
                    res = 0.0

            return res

    def logpdf (self, x):

        try:
            if self.left is not None and x < self.left:
                return float ('-inf')

            if self.right is not None and x > self.right:
                return float ('-inf')

            return self.logscale + self.distribution.logpdf (x)

        except:

            if self.left is not None:
                indx = numpy.where(x < self.left)
                try:
                    res [indx] = float ('-inf')
                except:
                    res = float ('-inf')

            if self.right is not None:
                indx = numpy.where(x > self.right)
                try:
                    res [indx] = float ('-inf')
                except:
                    rex = float ('-inf')

            return res #self.logscale + self.distribution.logpdf (x)

    def interval (self, alpha):

        interval = list (self.distribution.interval (alpha)) # / self.scale))
        if self.left is not None:
            interval [0] = max (self.left, interval [0])
        if self.right is not None:
            interval [1] = min (self.right, interval [1])
        return interval

    def rvs (self, random_state = None):

        draw = None
        while draw is None:
            proposal = self.distribution.rvs (random_state = random_state)
            if self.left is not None and proposal < self.left:
                continue
            if self.right is not None and proposal > self.right:
                continue
            draw = proposal
        return draw

def logmeanstd (logm, logs):
    '''Return the needed quantities to construct stats.lognorm with a specified mean (logm) and standard deviation (logs).

    According to documentation at:
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.lognorm.html
    '''

    factor = 1 + (logs / logm) ** 2
    expm = logm / numpy.sqrt (factor)
    s = numpy.sqrt (numpy.log (factor))

    return { 's' : s, 'scale' : expm }
