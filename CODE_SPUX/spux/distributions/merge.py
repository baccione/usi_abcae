# # # # # # # # # # # # # # # # # # # # # # # # # #
# Wrapper class for merging a standard SPUX distribution with a SPUX distribution for auxiliary observations
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

from .distribution import Distribution

class Merge (Distribution):

    def __init__ (self, distval, distaux):

        self.distval = distval
        self.distaux = distaux

    # evaluate a joint PDF of the tensorized random variables
    # 'parameters' is assumed to be of a pandas.Series type
    def pdf (self, parameters):
        """Evaluate the (joint) prob. distr. function of the tensorized, i.e. assuming independence, random variables 'parameters'."""

        return self.distval.pdf (parameters ['values']) * self.distaux.pdf (parameters ['auxiliary'])

    # evaluate a joint log-PDF of the tensorized random variables
    # 'parameters' is assumed to be of a pandas.Series type
    def logpdf (self, parameters):
        """Evaluate the logarithm of the (joint) prob. distr. function of the tensorized, i.e. assuming independence, random variables 'parameters'."""

        return self.distval.logpdf (parameters ['values']) + self.distaux.logpdf (parameters ['auxiliary'])

    # return marginal PDF for the specified parameter
    def mpdf (self, label, parameter):
        """Return marginal PDF for the specified parameter."""

        return self.distval.mpdf (label, parameter)

    # return marginal log-PDF for the specified parameter
    def logmpdf (self, label, parameter):
        """Return marginal log-PDF for the specified parameter."""

        return self.distval.logmpdf (label, parameter)

    # return intervals for the specified centered probability mass
    def intervals (self, alpha=0.99):
        """Return intervals for the specified centered probability mass."""

        return self.distval.intervals (alpha)

    # draw a random vector using the provided random state 'rng'
    def draw (self, rng):
        """Draw a random vector using the provided random state 'rng'."""

        return {'values' : self.distval.draw (rng), 'auxiliary' : self.distaux.draw (rng)}
