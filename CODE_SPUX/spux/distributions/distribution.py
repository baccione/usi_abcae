# # # # # # # # # # # # # # # # # # # # # # # # # #
# Base class for distributions
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

import pandas

from spux.utils.transforms import rounding

class Distribution (object):

    def configure (self, units = None, types = None):
        """Configure distribution units and variable types."""

        self.units = units
        self.types = {}

        for key in self.distributions:
            if types is not None:
                if key not in types:
                    raise ValueError("Fatal. Specified parameter is not in the list.")
                self.types [key] = types [key]
                if types[key][0:3].lower() == 'int' or types[key].lower() == 'binary':
                    self.distributions [key].pdf = rounding (self.distributions [key].pmf)
                    self.distributions [key].logpdf = rounding (self.distributions [key].logpmf)
            else:
                self.types [key] = 'float'

    def setup (self, units = None, types = None):
        """temporary wrapper, legacy after cherry-picking changes from dev_jonas"""

        self.configure (units, types)

    # return the list of values where the distribution is atomic
    def atomic (self, label):

        distribution = self.distributions [label]
        if hasattr (distribution, 'atomic') and distribution.atomic is not None:
            return distribution.atomic
        else:
            return []

    # evaluate a joint PDF of the distribution
    # 'parameters' is assumed to be of a pandas.DataFrame type
    def pdf (self, parameters):
        """Base method to be overloaded to evaluate the (joint) prob. distr. function of parameters.

        'parameters' are assumed to be of a pandas.DataFrame type
        """

        return float ('nan')

    # evaluate a joint log-PDF of the distribution
    # 'parameters' is assumed to be of a pandas.DataFrame type
    def logpdf (self, parameters):
        """Base method to be overloaded to evaluate the logarithm of the
        (joint) prob. distr. function of parameters.

        'parameters' are assumed to be of a pandas.DataFrame type
        """

        return float ('nan')

    # return marginal PDF for the specified parameter
    def mpdf (self, label, parameter):
        """Return marginal PDF for the specified parameter."""

        return float ('nan')

    # return marginal log-PDF for the specified parameter
    def logmpdf (self, label, parameter):
        """Return marginal log-PDF for the specified parameter."""

        return float ('nan')

    # return intervals (for each parameter) for the specified centered probability mass
    def intervals (self, alpha=0.99):
        """Return intervals for the specified centered probability mass.

        Intervals are returned for each parameter.
        """

        return { 'parameter' : [float ('nan'), float ('nan')] }

    # draw a random vector using the provided random state 'rng'
    def draw (self, rng):
        """
        Draw a random vector using the provided random state 'rng'.
        """

        parameters = { 'parameter' : float ('nan') }
        return pandas.Series (parameters)
