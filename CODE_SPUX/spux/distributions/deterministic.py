# # # # # # # # # # # # # # # # # # # # # # # # # #
# Wrapper class for a deterministic distribution
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

class Deterministic (object):

    def __init__ (self, value):

        self.value = value
        self.atomic = [value]

    def pdf (self, x):

        return 1 if x == self.value else 0

    def logpdf (self, x):

        return 0 if x == self.value else float ('-inf')

    def interval (self, alpha):

        return [self.value, self.value]

    def rvs (self, random_state = None):

        return self.value
