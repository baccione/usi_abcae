# # # # # # # # # # # # # # # # # # # # # # # # # #
# Randomwalk Model class
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

from scipy import stats
import math
import numpy

from spux.models.model import Model
from spux.utils.annotate import annotate
#from mpi4py import MPI

class Randomwalk (Model):
    """Class for Randomwalk model."""

    # construct model
    def __init__ (self, dt = 0.1, sandboxing = False):

        # set the specified time step size
        self.dt = dt

        # set sandboxing
        self.sandboxing = sandboxing

    # initialize model using specified 'inputset' and 'parameters'
    def init (self, inputset, parameters):
        """Initialize model using specified 'inputset' and 'parameters'."""

        # OPTIONAL: base class 'init (...)' method
        Model.init (self, inputset, parameters)

        # store parameters required by the model
        self.drift = parameters ["drift"]
        self.volatility = parameters ["volatility"]

        # use inputset to set the start time of the walk
        self.time = inputset ['time']

        # use inputset to draw a start position of the walk
        self.position = inputset ['initial'] .draw (self.rng) ['position']

        # write initial position to a file in the model sandbox
        if self.sandboxing:
            path = self.sandbox ('positions-%05.1f.dat' % self.time)
            numpy.savetxt (path, [self.position])

        #comm = MPI.COMM_WORLD
        #rnk = comm.Get_rank()
        #noise = self.rng.standard_normal(100)
        #file_object = open('noise_'+str(rnk)+'.txt', 'a')
        #file_object.write("\n"+str(noise[0:10]))
        #file_object.close()


    # run model up to specified 'time' and return the annotated prediction
    def run (self, time):
        """Run model up to specified 'time' and return the annotated prediction."""

        # OPTIONAL: base class 'run (...)' method
        Model.run (self, time)

        # update position (e.g., perform walk)
        while self.time < time:

            # compute the next time step size
            # taking into account restriction by the specidied time
            dt = min (time - self.time, self.dt)

            # generate standard normal random variable
            # using the provided "random state" as the source of randomness
            N = stats.norm.rvs (random_state = self.rng)

            # compute a random step using the generated random variable
            # and taking into account drift and volatility
            step = self.dt * self.drift + math.sqrt (self.dt) * self.volatility * N

            # update position with the generated random step
            self.position += step

            # update time
            self.time += dt

            # write updated position to the file in the model sandbox
            if self.sandboxing:
                path = self.sandbox ('positions-%05.1f.dat' % self.time)
                numpy.savetxt (path, [self.position])

        # return results
        return annotate ([self.position], ['position'], time)
