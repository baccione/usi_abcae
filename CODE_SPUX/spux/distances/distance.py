
import pandas

from ..utils.setup import setup, isolate, plant
from ..utils.seed import Seed
from ..utils import evaluations
from ..executors.utils import importer

class Distance (object):
    """A distance measure between an observed dataset and a model simulation."""

    @property
    def name (self):
        return type(self).__name__

    @property
    def component (self):
        return 'Distance'

    @property
    def evaluations (self):
        return evaluations.construct (self, 1)

    # assign required components to distance
    def assign (self, model, statistic=None, observation=None, dataset=None, inputset=None, timeset=None, auxiliary=None, error=None):
        """Assign required components to distance."""

        self.model = model
        self.observation = observation
        if dataset is not None:
            self.dataset = dataset
        else:
            self.dataset = observation
        self.statistic = statistic
        self.inputset = inputset
        self.auxiliary = auxiliary
        self.error = error
        self.timeset = timeset

        self.sandboxing = self.model.sandboxing
        self.task = self.model

        self.observed = statistic (self.observation) if self.observation is not None else None #pandas.DataFrame().append([statistic(dataset.loc[i]) for i in dataset.index])

        self.assigned = True

    # attach an executor
    def attach (self, executor = None, workers = None, backend = 'Mpi4py'):
        """Attach an executor to distance."""

        if executor is not None:
            self.executor = executor
        else:
            #self.executor = importer ('Ensemble', backend, workers)
            self.executor = importer ('Pool', backend, workers)
        self.executor.setup (self)
        #self.executor.capabilities (['connect', 'call', 'disconnect', 'report'])

    # setup
    def setup (self, verbosity=1, informative=1, trace=1):
        """Setup."""

        setup (self, verbosity, informative, trace)

    # isolate to a 'sandbox'
    def isolate (self, sandbox=None):
        """Isolate to a sandbox."""

        isolate (self, sandbox)

    # plant using specified 'seed'
    def plant (self, seed=Seed()):
        """Plant using specified 'seed'."""

        plant (self, seed)

    @staticmethod
    def diagnose (time, prediction, parameters, rng, verbosity, snapshots, error):
        """A helper function to compute observations of the model predictions at the specified time."""

        if snapshots [time] or time is None:

            # successful
            diagnostics = {'successful' : True}

            if error is not None:
                if verbosity >= 2:
                    print ("Taking observational error into account...")
                diagnostics ['observations'] = error (prediction, parameters).draw (rng)
            else:
                diagnostics ['observations'] = None

            return diagnostics

        # if this is not a snapshot, return None
        else:

            return None
