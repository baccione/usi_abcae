import pandas as pd
import numpy as np

from .distance import Distance
from ..ensembles.particles import Particles
from ..utils.timing import Timing

class Euclidean (Distance):
    """A Euclidean distance measure."""

    def __init__ (self, alpha = 2):
        self.alpha = alpha

    def __call__ (self, parameters):
        """Evaluate Euclidean distance to the specified parameters."""

        if self.verbosity >= 2:
            print("Distance computation for parameters:")
            print(parameters)

        timing = Timing (self.informative >= 2)
        timing.start('evaluate')

        # ensemble consists only of a single model
        ensemble = Particles (self.model, self.inputset, parameters, self.statistic)
        ensemble.setup (self.sandbox, self.seed, self.verbosity - 1, self.informative, self.trace)
        self.executor.connect(ensemble, indices=[0])

        predictions = {}

        # process self.timeset and self.dataset.index
        if self.timeset is not None:
            times = np.concatenate ((self.timeset, self.dataset.index))
            times = np.unique (times)
            times.sort (kind = 'mergesort')
        else:
            times = list (self.dataset.index)

        # iterate over all times
        for time in times:

            if self.verbosity:
                print("Time", time)

            if self.verbosity >= 2:
                print("Running %s model..." % self.task.name)
            predictions [time] = self.executor.call ('run', args = [time]) [0]

            if self.error is not None:
                if self.verbosity >= 2:
                    print("Adding observational error...")
                if self.auxiliary is None:
                    predictions [time] += self.error.distribution (predictions [time], parameters).draw (self.rng)
                else:
                    error = self.error.distribution (predictions [time], parameters).draw (self.rng)
                    predictions [time] ['values'] += error ['values']
                    predictions [time] ['auxiliary'] += error ['auxiliary']

            self.executor.call ('advance', results=0)

        if self.auxiliary is not None:
            statistics = pd.DataFrame().append([self.statistic(predictions[i]['values']) for i in self.dataset.index])
        else:
            statistics = pd.DataFrame().append([self.statistic(predictions[i]) for i in self.dataset.index])
        distance = np.sum((np.abs(statistics - self.observed)**self.alpha).to_numpy()) / len (self.dataset.index)
        if self.auxiliary is not None:
            aux_distance = 0
            for time in self.dataset.index:
                statistic = self.statistic (predictions[time]['auxiliary'])
                aux_distance += np.sum((np.abs(statistic - self.auxiliary (time))**self.alpha).to_numpy()) / len (statistic)
            distance += aux_distance / len (self.dataset.index)
        distance = distance ** (1/self.alpha)

        timings = self.executor.disconnect()
        timing += self.executor.report()
        timing.time('evaluate')

        if self.verbosity:
            print("Distance: {}".format(distance))

        info = {}
        info["predictions"] = predictions
        info["successful"] = True
        if self.informative:
            info ["timing"] = timing
            info ['timings'] = timings

        return distance, info
