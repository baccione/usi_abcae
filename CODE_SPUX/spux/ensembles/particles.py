# # # # # # # # # # # # # # # # # # # # # # # # # #
# Ensemble class for processing of multiple models (particles) in likelihoods
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

from ..io.formatter import compactify
from .ensemble import Ensemble

class Particles (Ensemble):
    """Class of type ensemble operates set of particles (user models)."""

    # constructor requires dynamical system 'model' including its 'inputset' and 'parameters', and the 'error' distribution for prediction validation
    def __init__(self, model, inputset, parameters, error, log=1):

        self.model = model
        self.inputset = inputset
        self.parameters = parameters
        self.error = error
        self.log = log

        self.sandboxing = model.sandboxing

        # label for sandboxes
        self.label = "P%04d"

        # seeds for iterations
        self.seeds = {}

        self.task = self.model

    # run all particles in ensemble up to the specified time
    def run (self, time):
        """Run all particles in ensemble up to the specified time."""

        self.time = time

        if self.verbosity:
            print("Ensemble run with root", compactify (self.root))

        self.predictions = {}
        values = {}
        for index, particle in self.members.items ():
            self.plant (index)
            self.predictions [index] = particle.run (time)
            if isinstance (self.predictions [index], dict):
                values [index] = self.predictions [index] ['values']
            else:
                values [index] = self.predictions [index]

        return values

    # compute errors for all particles in ensemble
    def errors (self, dataset, auxiliary):
        """Compute errors for all particles in ensemble."""

        if self.verbosity:
            print("Ensemble errors with root", compactify (self.root))

        if hasattr (self.error, 'transform') and self.verbosity:
            print (' -> Model predictions and dataset will be transformed before error computations.')

        # transform dataset if error requires so
        if hasattr (self.error, 'transform'):
            dataset = self.error.transform (dataset, self.parameters)

        errors = {}
        self._peaks = {}
        for index, particle in self.members.items():
            prediction = self.predictions [index]
            if hasattr (self.error, 'transform'):
                prediction = self.error.transform (prediction, self.parameters)
            distribution = self.error.distribution (prediction, self.parameters)
            if self.verbosity >= 2:
                print (' -> Prediction (particle %05d): ' % index, prediction)
                print (' -> Dataset (particle %05d): ' % index, dataset)
            pdfcall = 'logpdf' if self.log else 'pdf'
            if auxiliary is not None:
                errors [index] = getattr (distribution, pdfcall) ({'values' : dataset, 'auxiliary' : auxiliary})
            else:
                errors [index] = getattr (distribution, pdfcall) (dataset)
            self._peaks [index] = getattr (distribution, pdfcall) (prediction)

        return errors

    def peaks (self):
        """Return peaks computed in earlier in self.errors (...)."""

        return self._peaks
