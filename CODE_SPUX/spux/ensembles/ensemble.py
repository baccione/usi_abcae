# # # # # # # # # # # # # # # # # # # # # # # # # #
# Ensemble base class for dynamic processing of multiple components using ensemble executors
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

from copy import deepcopy as copy

from ..utils.setup import setup, isolate, plant
from ..utils.seed import Seed
from ..io.formatter import compactify
#from mpi4py import MPI

class Ensemble (object):
    """Bsae class for an ensemble."""

    @property
    def name (self):

        #cwrank = MPI.COMM_WORLD.Get_rank ()
        #print("within name of ensemble/ensemble.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()
        result = type(self).__name__
        #print("done with name of ensemble/ensemble.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        return result

    # setup ensemble
    def setup (self, sandbox=None, seed=Seed(), verbosity=1, informative=0, trace=0):
        """Do a standardized setup."""

        #cwrank = MPI.COMM_WORLD.Get_rank ()
        #print("within setup of ensemble/ensemble.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        # standardized setup, isolate and plant
        setup (self, verbosity, informative, trace)
        isolate (self, sandbox)
        plant (self, seed)

        #print("done with setup ensemble/ensemble.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

    # isolate the 'index' member
    def isolate (self, index):
        """Isolate member: set 'sandbox'."""

        #cwrank = MPI.COMM_WORLD.Get_rank ()
        #print("within isolate of ensemble/ensemble.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        if not self.sandboxing:
            sandbox = None
        else:
            label = self.label % index
            sandbox = self.sandbox.spawn (label)
        self.members [index] .setup (self.verbosity - 2, self.informative, self.trace)
        self.members [index] .isolate (sandbox)

        #print("done with isolate ensemble/ensemble.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

    # plant the 'index' member
    def plant (self, index):
        """Plant member: set 'seed'."""

        #cwrank = MPI.COMM_WORLD.Get_rank ()
        #print("within plant of ensemble/ensemble.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        seed = self.seeds [self.iteration] .spawn (index, name='Ensemble index')
        self.members [index] .plant (seed)

        #print("done with plant ensemble/ensemble.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

    # stash 'index' member
    def stash (self, index, suffix='-stash'):
        """ Stash (move) member key and sandbox by appending the specified suffix."""

        #cwrank = MPI.COMM_WORLD.Get_rank ()
        #print("within stash of ensemble/ensemble.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        if self.sandboxing:
            self.members [index] .sandbox.stash ()

        key = str (index) + suffix

        self.members [key] = self.members [index]
        del self.members [index]

        #print("done with stash ensemble/ensemble.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        return key

    # fetch member from 'key' stash and move it to a specified index
    def fetch (self, key, index, suffix='-stash'):
        """ Fetch (move) member key and sandbox by removing the specified suffix and moving to a specified index."""

        #cwrank = MPI.COMM_WORLD.Get_rank ()
        #print("within fetch of ensemble/ensemble.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        self.members [index] = self.members [key]
        del self.members [key]

        if self.sandboxing:
            label = self.label % index
            self.members [index] .sandbox.fetch (tail = label)

        #print("done with fetch ensemble/ensemble.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

    # remove 'index' member (including its sandbox, if trace is not enabled)
    def remove (self, index, trace=0):
        """Remove member for the specified index."""

        #cwrank = MPI.COMM_WORLD.Get_rank ()
        #print("within remove of ensemble/ensemble.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        self.members [index] .exit ()
        if self.sandboxing and not trace:
            self.members [index] .sandbox.remove ()
        del self.members [index]

        #print("done with removing ensemble/ensemble.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

    # initialize ensemble
    def init (self, indices):
        """Initialize ensemble."""

        #cwrank = MPI.COMM_WORLD.Get_rank ()
        #print("within init of ensemble/ensemble.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        if self.verbosity:
            print("Ensemble init with root", compactify (self.root))

        # set iteration
        self.iteration = 0

        # set iteration seed
        self.seeds [self.iteration] = self.seed.spawn (self.iteration, name='Ensemble iteration')

        # construct members and sandboxes
        self.members = {}
        for index in indices:
            self.members [index] = copy (self.model)
            self.isolate (index)
            self.plant (index)

        # initialize members with specified parameters
        for index, member in self.members.items ():
            member.init (self.inputset, self.parameters)

        #print("done with init ensemble/ensemble.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

    # advance ensemble state to next iteration
    def advance (self):
        """Advance ensemble state to next iteration."""

        #cwrank = MPI.COMM_WORLD.Get_rank ()
        #print("within advance of ensemble/ensemble.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        if self.verbosity:
            print("Ensemble advance with root", compactify (self.root))

        # set iteration
        self.iteration += 1

        # set iteration seed
        self.seeds [self.iteration] = self.seed.spawn (self.iteration, name='Ensemble iteration')

        #print("done with advance ensemble/ensemble.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

    # cleanup
    def exit (self):
        """Cleanup Ensemble object."""

        #cwrank = MPI.COMM_WORLD.Get_rank ()
        #print("within exit of ensemble/ensemble.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        if self.verbosity:
            print("Ensemble exit with root", compactify (self.root))

        for index in list (self.members.keys ()):
            self.remove (index, self.trace)

        # take out trash
        self.members = None

        #print("done with exit ensemble/ensemble.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()
