# # # # # # # # # # # # # # # # # # # # # # # # # #
# Cloudpickle-based loader class
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

import cloudpickle
import os
import glob
import numpy
import pandas
import re
import psutil
#import itertools

def tail (samples = None, infos = None, batch = 'last'):
    """Return only the tail of the samples and infos after the batch cutoff."""

    if batch == 'last':
        batch = len (infos) - 1

    if infos is not None and samples is not None:
        batchsize = len (samples) // len (infos)
    else:
        batchsize = 1

    if samples is not None and len (samples) > batch * batchsize:
        samples = samples.loc [batch * batchsize:]
    if infos is not None and len (infos) > batch:
        infos = infos [batch:]

    return samples, infos

def load (name="dump.dat", directory="report", verbosity=1):
    """Load the specified binary file."""

    path = name
    process = psutil.Process(os.getpid())

    if directory is not None:
        path = os.path.join (directory, path)
    with open (path, "rb") as f:
        obj = cloudpickle.load (f)
    if verbosity:
        print ('  : -> Loaded %s with size: %.1f GB' % (path, os.path.getsize (path) / (1024 ** 3)))
        print('mem usage in GB after loading of ',path,' :',process.memory_info().rss/ (1024 ** 3) ) # in GB
    return obj

def check_interval (name=None, directory='output'):
    """ Adjust intervals to feasible ones """

    assert (name is not None),":: Fatal: Please, specify a valid interval of batches. Got None."
    assert isinstance(name,list),":: Fatal: Please, specify a valid interval of batches. Did not get a list."
    assert all([ inm.split('-')[0] in ['samples','infos'] for inm in name ]), ":: Fatal: unknown rootname in check_interval. This is a bug."
    assert all([ inm.split('-')[2] in ['*.dat','*.csv'] for inm in name ]), ":: Fatal: unknown extention in check_interval. This is a bug."
    assert all([ len(inm.split('-'))==3 for inm in name ]), ":: Fatal: unknown length after split in check_interval. This is a bug."

    isinfo = False
    if name[0].split('-')[0] in ['infos']:
        isinfo = True

    avail_batches = list()
    avail_names = list()
    for inm in name:
        tmpstr = ''.join(glob.glob(os.path.join (directory, inm))) # intuitive as usual
        if tmpstr == '':
            print("batch {} not found".format(inm))
            continue
        if isinfo == True:
            actlb = [int(re.search('-(.+?)-', inm).group(1))]
            result = load (tmpstr, directory=None, verbosity=0)
            initb = [ actlb[0] - l + 1 for l in range(len(result),1,-1) ]
            actlb = initb + [int(re.search('-(.+?)-', inm).group(1))]
        else:
            actlb = [int(re.search('-(.+?)-', inm).group(1))]
        #print("actlb: {}".format(actlb))
        avail_batches = avail_batches + actlb  #.append(int(re.search('-(.+?)-', inm).group(1))) # very intuitive as always
        avail_names.append(inm)

    if tmpstr == '': # just don't guess anymore
         print(":: Warning: I could not include the last required batch: {}. This may be a bug or a wrong specification.".format(inm))
         assert len(avail_batches)>0, ":: Fatal. No eligible batches were found. This may be a bug or a wrong specification."

    avail_batches = sorted (avail_batches)
    avail_names = sorted (avail_names)

    ### mfd ######################################
    #if avail_names[0].split('-')[0] in ['infos']:
    #    tmpstr = ''.join(glob.glob(os.path.join (directory, avail_names[0])))
    #    result = load (tmpstr, directory=None, verbosity=0)
    #    trueini = avail_batches[0] - len(result) + 1
    #elif avail_names[0].split('-')[0] in ['samples']:
    #    trueini = None
    #else:
    #    assert False, ":: Fatal: wrong name. This is bug."
    ##############################################

    return avail_names, avail_batches


def loadall (name="samples-*.dat", directory="output", verbosity=1, interactive=True, burnin=0, legacy=False, chains=None, last=False):
    """Load SPUX binary output files as generator."""

    if isinstance(name,str) == True:
        assert name.split('-')[0] in ['samples','infos'], ":: Fatal: unknown rootname in loadall. This is a bug."
        assert name.split('-')[1] in ['*.dat'], ":: Fatal: unknown extention in loadall. This is a bug."
    else:
        assert all([ inm.split('-')[0] in ['samples','infos'] for inm in name ]), ":: Fatal: unknown rootname in loadall. This is a bug."
        assert all([ inm.split('-')[2] in ['*.dat'] for inm in name ]), ":: Fatal: unknown extention in loadall. This is a bug."
        assert all([ len(inm.split('-'))==3 for inm in name ]), ":: Fatal: unknown length after split in loadall. This is a bug."

    if isinstance(name,str):
        path = os.path.join (directory, name)
        paths = sorted (glob.glob (path))
    elif isinstance(name,list):
        print("Going for interval of files:")
        path = list()
        for inm in name:
            tmpstr = ''.join(glob.glob(os.path.join (directory, inm))) # intuitive as usual
            if tmpstr == '': # should fatal now thanks to check_intervals
                assert False, ":: Fatal: got non-existing name in loadall. This is a bug."
                #continue
            path.append(tmpstr)
        paths = sorted (path)
    else:
        assert False, ":: Fatal: wrong type of name in loadall. This is likely a bug."

    previous = -1
    for checkpoint, p in enumerate (paths):
        lastindex = int (p.split ('-') [-2])
        count = lastindex - previous
        previous = lastindex
        if lastindex >= burnin:
            if burnin > 0:
                paths = paths [checkpoint:]
            break
    if last:
        paths = [paths [-1]]

    sizes = [ os.path.getsize (p) / (1024 ** 3) for p in paths ]
    size = numpy.sum (sizes)
    print ('  : -> Size of all \'%s\' files to be loaded (minimum requirement for RAM): %.1f GB' % (path, size))

    if size > 4 and interactive:
        print ('  : -> Proceed? [press ENTER if yes, and enter \'n\' if not]')
        reply = input ('  : -> ')
        if reply == 'n':
            print ('  : -> Canceling.' % size)
            yield None
        else:
            print ('  : -> Proceeding.' % size)

    previous = -1

    for checkpoint, path in enumerate (paths):
        lastindex = int (p.split ('-') [-2])
        count = lastindex - previous
        previous = lastindex
        result = load (path, directory=None, verbosity=verbosity)

        if chains is None:
            chains = len (result) // count
        #if verbosity:
        #    print ('  : -> Loaded %s with length %d%s and size: %.1f GB' % (path, len (result), (' (%d chains)' % chains) if burnin > 0 else '', sizes [checkpoint]))
        if burnin > 0 and checkpoint == 0:
            tail = - 1 - (lastindex - burnin)
            if hasattr (result, 'iloc'):
                result = result.iloc [tail * chains:]
            else:
                result = result [tail:]
        if legacy and burnin > 0 and checkpoint == 0:
            result = result.reindex (index = range (burnin * chains, burnin * chains + len (result)))
        yield result

def reconstruct (samplesfiles='samples-*.dat', infosfiles='infos-*.dat', timingsfiles='timings-*.dat', directory='output', verbosity=1, interactive=True, burnin=0, legacy=False, chains=None, last=False):
    """Recostruct samples and infos from the specified checkpoint files, taking into account a possible burnin cutoff."""

    if samplesfiles is not None:
        if not last:
            samples = pandas.concat (loadall (samplesfiles, directory, verbosity, interactive, burnin, legacy, chains, last), sort=False, ignore_index=legacy)
        else:
            samples = loadall (samplesfiles, directory, verbosity, interactive, burnin, legacy, chains, last)
    else:
        samples = None

    if infosfiles is not None:
        infos = [info for infos in loadall (infosfiles, directory, verbosity, interactive, burnin, last = last) for info in infos]
        #infos = [info for info in itertools.chain (loadall (infosfiles, directory, verbosity, interactive, burnin, last = last))]
    else:
        infos = None

    if timingsfiles is not None:
        timings = [timing for timings in loadall (timingsfiles, directory, verbosity, interactive, burnin, last = last) for timing in timings]
    else:
        timings = None

    if timings is None:
        if infos is None:
            return samples
        else:
            return samples, infos
    else:
        if samples is None and infos is None:
            return timings
        else:
            return samples, infos, timings

def last (name="infos-*.dat", directory="output", verbosity=1, interactive=True):
    """Load the last sample batch from the SPUX binary output files."""

    return [info for info in loadall (name, directory, verbosity, interactive, last=1)] [0] [-1]

def pickup (name="pickup-*.dat", directory="output"):
    """Loads the most recent pickup file."""

    path = os.path.join (directory, name)
    paths = sorted (glob.glob (path))
    assert os.path.isfile(paths [-1]), ":: Fatal: pick-up file {} not found. This may be a bug. Hack me if you know what you are doing.".format(paths[-1])
    if len (paths) >= 1:
        return load (os.path.basename (paths [-1]), directory)
    else:
        return None

def read_types (infl=None):
    """Read file with keys and types"""

    if infl is None or not os.path.exists (infl):
        return None

    if infl is not None:
        types = {}
        with open(infl) as fl:
            for line in fl:
                (key, val) = line.split()
                types[key] = val

    return types

def load_dict (filename = 'datasets/exact.py'):
    """Read text file into a dictionary."""

    if os.path.exists (filename):
        return pandas.read_csv (filename, sep="\s+", index_col=0, header=None, squeeze=True)
    else:
        return None

def load_auxiliary (filenames, timeformat=float, verbosity = 0):
    """Load auxiliary dataset as a dictionary."""

    files = glob.glob (filenames)
    if len (files) == 0:
        return None

    dataset = {}
    for file in files:
        time = timeformat (file.split ('-') [-1] .split ('.') [0])
        dataset [time] = load (file, verbosity = verbosity)

    return dataset

def load_csv (filename, verbosity = 0, directory='./output'):
    """Load csv file to a pandas.DataFrame."""

    if directory is not None:
        path = os.path.join (directory, filename)
    else:
        path = filename

    if os.path.exists ( path  ):
        return pandas.read_csv (path, sep=",", index_col=0)
    else:
        path = os.popen( 'ls '+ os.path.join (directory, filename) ).read().rstrip()
        if ( os.path.isfile(path) ):
            return pandas.read_csv (path, sep=",", index_col=0)
        else:
            return None

def load_dat(filename, directory='./output'):
    """ independent load of dat file more similar to load_csv """

    if directory is not None:
        path = os.path.join (directory, filename)
    else:
        path = filename

    if os.path.exists ( path ):
        with open (path, "rb") as f:
            obj = cloudpickle.load (f)
            return obj
    else:
        path = os.popen( 'ls '+ os.path.join (directory, filename) ).read().rstrip()
        if ( os.path.isfile(path) ):
            with open (path, "rb") as f:
                obj = cloudpickle.load (f)
                return obj
        else:
            return
