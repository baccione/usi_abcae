# # # # # # # # # # # # # # # # # # # # # # # # # #
# Base sampler class
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

import pandas
import os
import sys
import gc
import numpy
import argparse
from pydoc import locate
from copy import deepcopy as copy

from ..utils.timer import Timer
from ..utils.progress import Progress
from ..utils import evaluations
from ..io import dumper
from ..io import loader
from ..io import formatter
from ..utils.transforms import cast
from ..utils.setup import setup, isolate, plant
from ..utils.seed import Seed
from ..io.checkpointer import Checkpointer
from ..executors.utils import importer

class Sampler (object):
    """Base class for sampler (of parameter space)."""

    @property
    def name (self):

        return type(self).__name__

    @property
    def component (self):

        return 'Sampler'

    def evaluations (self, size):

        return evaluations.construct (self, size)

    # assign required components to sampler
    def assign (self, likelihood, prior=None):
        """Assign required components to sampler."""

        if not hasattr (likelihood, 'assigned'):
            assert False, ' :: ERROR: likelihood needs to have components assigned before being assigned to sampler.'

        self.likelihood = likelihood
        self.prior = prior

        self.types = prior.types if prior is not None else None

        self.replicates = (self.likelihood.name == 'Replicates')

        self.sandboxing = self.likelihood.sandboxing
        self.task = self.likelihood

        self.assigned = True

    # attach an executor
    def attach (self, executor = None, workers = None, backend = 'Mpi4py'):
        """Attach an executor."""

        if executor is not None:
            self.executor = executor
        else:
            self.executor = importer ('Pool', backend, workers)
        self.executor.setup (self)
        self.executor.capabilities (['map', 'report'])

    # setup
    def setup (self, verbosity=1, informative=1, trace=1):
        """Setup."""

        setup (self, verbosity, informative, trace)

    # isolate to a 'sandbox'
    def isolate (self, sandbox=None):
        """Isolate to a sandbox."""

        isolate (self, sandbox)

    # plant using specified 'seed'
    def plant (self, seed=Seed()):
        """Plant using specified 'seed'."""

        plant (self, seed)

    # configure sampler
    def configure (self, lock=None, thin=1):
        """Configure sampler."""

        # set lock
        self.lock = lock

        # set thin period
        self.thin = thin

        # report sampler setup: thin, lock
        arglist = {}
        arglist ['thin'] = self.thin
        arglist ['lock (batch)'] = self.lock
        # arglist ['lock (sample)'] = self.lock * self.chains if self.lock is not None else None
        title = 'Sampler setup'
        headers = ['thin', 'lock (batch)']
        # headers = ['thin', 'lock (batch)', 'lock (sample)']
        dumper.report (self.reportdir, 'setup-sampler', arglist, title, [arglist], headers, align = 'c')

        self.configured = 1

    # read pickup if requested
    def read_pickup (self):

        self.index = 0
        self.best_sample = None
        self.best_fit = float ('-inf')
        self._feedback = None
        self._pickup = None

        parser = argparse.ArgumentParser ()
        parser.add_argument ("--continue", dest = "cont", help = "continue sampling", action = "store_true")
        args, unknown = parser.parse_known_args ()
        if args.cont:
            pickup = loader.pickup ('pickup-*.dat', self.outputdir)
            if pickup is not None:
                print (' :: CONTINUE: Loaded index and feedback from the pickup file.')
                self.index = pickup ['index'] + 1
                self.best_fit = pickup ['best_fit']
                self.best_sample = pickup ['best_sample']
                self._feedback = pickup ['feedback']
            else:
                print (' :: ERROR: No pickup file found in {} -> can not continue sampling as requested.'.format(self.outputdir))
                self.executor.abort ()
        else:
            pickup = None

        # report index
        if self.verbosity and self.index > 0:
            print ("Sampler index:", self.index)

        return pickup ['init'] if pickup is not None else None

    # return fitness measure for the specified chain (higher is better)
    def fit (self, info, chain):

        return info ['posteriors'] [chain]

    # update best sample and fit
    def update_best (self, parameters, info):

        # look through each chain for a better fit than the current 'self.best_fit'
        best_chain = None
        for chain in range (self.chains):

            # check if chain 'infos' is not None
            if info ['infos'] [chain] is None:
                continue

            # check if evaluation was successful
            if 'successful' in info ['infos'] [chain] and not info ['infos'] [chain] ['successful']:
                continue

            # for replicates, also check if all aggregated evaluations were successful
            if self.replicates:
                successful = True
                for name in info ['infos'] [chain] ['infos'] .keys ():
                    if 'successful' in info ['infos'] [chain] ['infos'] [name] and not info ['infos'] [chain] ['infos'] [name] ['successful']:
                        successful = False
                        break
                if not successful:
                    continue

            # check if the new fit is better than the current fit
            fit = self.fit (info, chain)
            if fit > self.best_fit:
                self.best_fit = fit
                best_chain = chain
            else:
                continue

        # if a better fit was found, update 'self.best_sample' accordingly
        if best_chain is not None:

            self.best_sample = {}
            self.best_sample ['batch'] = self.index
            self.best_sample ['chain'] = best_chain
            if hasattr (self, 'types'):
                self.best_sample ['parameters'] = cast (parameters.loc [best_chain], self.types)
            else:
                self.best_sample ['parameters'] = parameters.loc [best_chain]
            self.best_sample ['fit'] = self.best_fit
            self.best_sample ['prior'] = info ['priors'] [best_chain]

            # no replicates - all straight forward
            if not self.replicates:

                # get the best predictions and fit, if available
                if 'best' in info ['infos'] [best_chain]:
                    if info ['infos'] [best_chain] ['best'] is not None:
                        predictions = info ['infos'] [best_chain] ['best'] ['predictions']
                    else:
                        predictions = None
                else:
                    predictions = info ['infos'] [best_chain] ['predictions']

            # replicates - need predictions for each dataset
            else:

                predictions = {}

                for name in info ['infos'] [best_chain] ['infos'] .keys ():

                    if 'best' in info ['infos'] [best_chain] ['infos'] [name]:
                        if info ['infos'] [best_chain] ['infos'] [name] ['best'] is not None:
                            predictions [name] = info ['infos'] [best_chain] ['infos'] [name] ['best'] ['predictions']
                        else:
                            predictions [name] = None
                    else:
                        predictions [name] = info ['infos'] [best_chain] ['infos'] [name] ['predictions']

            self.best_sample ['predictions'] = predictions

    # online sampling (Python generator) with progress tracking and periodic saving
    # iteratively yielding results for each sample (or ensemble of samples)
    def generator (self, size=1, batches=None):
        """Generate samples iteratively."""

        # if batches is specified use them instead of 'size'
        if batches is not None:
            steps = batches
        else:
            steps = size

        # initialize progress bar
        if self.verbosity == 1:
            progress = Progress (prefix="Sampling: ", steps=steps, length=40)
            progress.init ()
        elif self.verbosity >= 2:
            if batches is not None:
                print ("Drawing %d batches..." % batches)
            else:
                print ("Drawing %d samples..." % size)

        # initialize sample count
        count = 0

        # store informative flag separately
        if self.informative is None:
            informative = None
        else:
            informative = self.informative

        # flag for the last yield
        last = False

        # initialize termination flag
        terminate = 0

        # initialize batch
        batch = 0

        # sample iteratively
        while not terminate:

            # use informative flag only for the first and the last samples
            if informative is None:
                if self.index == 0 or last:
                    self.informative = 2
                else:
                    self.informative = 1

            # set label for current index
            label = "S%05d" % self.index

            # spawn a sandbox for current index
            sandbox = self.sandbox.spawn (label) if self.sandboxing else None

            # spawn a seed for current index
            seed = self.seed.spawn (self.index, name=label)

            # draw a random sample from posterior
            parameters, info = self.draw (sandbox, seed, self._feedback)

            # increment count
            count += len (parameters)

            # increment batch
            batch += 1

            # process feedback
            if hasattr (self, 'feedback'):
                self.feedback (info)
            feedback = (' (feedback: %s)' % self._feedback) if self._feedback is not None else ''

            # update progress
            if self.verbosity == 1:
                if batches is not None:
                    progress.update (batch)
                else:
                    progress.update (count)
            elif self.verbosity >= 2:
                if batches is not None:
                    print("Batches(s) %d/%d%s" % (batch, batches, feedback))
                else:
                    print("Sample(s) %d/%d%s" % (count, size, feedback))

            # cleanup sample-specific sandbox
            if self.sandboxing and not self.trace:
                sandbox.remove ()

            # update best sample and best fit
            self.update_best (parameters, info)

            # yield results as a generator for additional external processing
            if self.index % self.thin == 0:
                yield parameters, info

            # increment index
            self.index += 1

            # return after processing the last yield
            if last:
                return

            # check termination
            if batches is not None:
                terminate = (batch == batches)
            else:
                terminate = (count >= size)

            # will the next yield be the last?
            if batches is not None:
                last = (batch + self.thin == batches and self.index % self.thin == 0)
            else:
                last = (count + self.thin * len (parameters) >= size and self.index % self.thin == 0)

        if self.verbosity == 1:
            progress.finalize ()

    # save samples and infos and free the memory
    def flush (self, samples, infos, time):
        """Save samples and infos and free the memory."""

        # initialize timer
        timer = Timer ()
        timer.start ()

        # format time to a timestamp
        timestamp = formatter.timestamp (time)

        # format suffix
        suffix = '%05d-%s' % (self.index, timestamp)

        # CSV export of samples
        samples.to_csv (os.path.join (self.outputdir, 'samples-%s.csv' % suffix))

        # binary export of samples
        dumper.dump (samples, name='samples-%s.dat' % suffix, directory=self.outputdir)

        # binary export of infos
        # garbage collector is temprorarily disabled to increase performance
        gc.disable ()
        size = dumper.dump (infos, name='infos-%s.dat' % suffix, directory=self.outputdir)
        gc.enable ()

        # report best fit parameters
        if self.best_sample is not None:
            headers = list (self.best_sample ['parameters'] .index)
            entry_unformatted = dict (self.best_sample ['parameters'])
            entry = {}
            dels = []
            for key, value in entry_unformatted.items ():
                if key not in self.prior.types: # for simplicity and portability, not all might be there
                    dels = dels + [key]
                    continue
                if self.prior.types [key] in ['float', 'double']:
                    entry [key] = '%.2e' % value
                else:
                    datatype = locate (self.prior.types [key])
                    if datatype is not None:
                        entry [key] = str (datatype (value))
                    else:
                        entry [key] = str (value)
                entry [key] = '$' + entry [key] + '$'
            if self.units is not None:
                for key in entry.keys ():
                    entry [key] += ' %s' % self.units ['parameters'] [key]
            for key in dels:
                print(":: Warning: remove absent key (likely due to a lenient parameters.types file): ",key)
                if key in entry_unformatted:
                    del entry_unformatted [key]
                if key in entry:
                    del entry [key]
                if key in headers:
                    headers.remove(key) # what a fckng pain
            assert len(entry) == len(headers), ":: Fatal: mismatch between headers and entry. This is a bug."
            for key in entry:
                assert key in headers, ":: Fatal: key in entry not in headers. This is a bug"
            title = 'Best model parameters'
            locations = (self.best_sample ['batch'], self.best_sample ['chain'], self.best_sample ['batch'] * self.chains + self.best_sample ['chain'])
            title += ' (batch:%d, chain:%d, sample:%d, fit (unnormalized):%.2e)' % (locations + (self.best_sample ['fit'],))
            dumper.report (self.reportdir, 'best', self.best_sample, title, [entry], headers, align = 'c', latex = True, columns = 8)

        # dump current index, feedback, and sampler state for later continuation
        pickup = {'index' : self.index, 'best_fit' : self.best_fit, 'best_sample' : self.best_sample, 'feedback' : self._feedback}
        pickup ['init'] = self.pickup ()
        dumper.dump (pickup, name='pickup-%s.dat' % suffix, directory=self.outputdir)

        # report timer
        if self.verbosity >= 2:
            print ('  : -> Checkpointer took:', formatter.timestamp (timer.current (), precise = True))
            print ('  : -> Infos size:', size)

        # reset samples and infos
        timer.start ()
        samples.drop (samples.index, inplace=True)
        infos.clear ()
        gc.collect ()
        if self.verbosity >= 2:
            print ('  : -> Garbage collector took:', formatter.timestamp (timer.current (), precise = True))

        # flush any pending stdout output
        sys.stdout.flush ()

    def __call__ (self, size=1, batches=None, checkpointer=Checkpointer(600), trim=False):
        """Performs posterior sampling and returns pandas dataframe containing results (either all [default] or the latest batch only [trim=True])."""

        # report the total number of foreseen model evaluations
        evaluations = self.evaluations (size if batches is None else batches * self.chains)
        title = 'Number of model evaluations'
        entries = evaluations [::-1]
        headers = ['Component', 'Class', 'tasks', 'sizes', 'cumulative']
        align = ['l', 'l', 'r', 'r', 'r']
        formatters = {'tasks' : formatter.intf, 'sizes' : formatter.intf, 'cumulative' : formatter.intf}
        dumper.report (self.reportdir, 'evaluations', evaluations, title, entries, headers, align, formatters)

        # initialize checkpointer
        if checkpointer is not None:
            checkpointer.init (self.verbosity >= 2)

        # initialize samples and infos
        samples = pandas.DataFrame ()
        infos = []

        # process all generated parameters
        for (parameters, info) in self.generator (size, batches):

            # if only the latest batch of samples is needed, overwrite samples and infos (no copy)
            if trim:
                samples = parameters
                infos = [info]

            # if all samples are needed, append a copy of samples and infos
            else:
                if len (samples) == 0:
                    samples = pandas.DataFrame (columns = parameters.columns.values)
                indices = numpy.arange (self.index * len (parameters), (self.index + 1) * len (parameters))
                for i, index in enumerate (indices):
                    samples.loc [index] = parameters.iloc [i]
                infos += [copy (info)]

            # save samples and infos at periodical checkpoints
            if checkpointer is not None:
                time = checkpointer.check ()
                if time is not None:
                    self.flush (samples, infos, time)


        isthe = True
        extms = []
        if isthe:
            for i in range(len(infos)):
                for j in range(len(infos[i]['infos'])):
                    if (infos[i]['infos'][j]):
                        try:
                            extms = extms + [infos[i]['infos'][j]['extime']]
                        except:
                            isthe = False
        try:
            print('average(ex. time): {:.3f} +- {:.3f}'.format(numpy.average(extms),numpy.quantile(extms,0.975)-numpy.quantile(extms,0.025)))
        except:
            pass

        # final checkpoint
        if checkpointer is not None and len (samples) > 0 and len (infos) > 0:
            time = checkpointer.check (force=1)
            self.flush (samples, infos, time)

        if checkpointer is None:
            return samples, infos
