# # # # # # # # # # # # # # # # # # # # # # # # # #
# SABC sampler class
#
# TODO: add reference
#
# The core contents of the 'draw (...)' method were implemented by Alex Madsen.
# Feedback from Anthony Ebert and Simone Ulzega influenced the user interface design.
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

import math
import numpy as np
import pandas as pd
import sys # noqa F401
from scipy.optimize import brentq

from ..utils.timing import Timing
from .sampler import Sampler
from ..utils.transforms import cast

np.seterr(invalid='ignore')

class DistanceTransformation:

    def __init__(self, sampled_distances):
        self.rho = np.concatenate(([0], np.sort(sampled_distances)))
        self.G = np.arange(len(self.rho)) / len(self.rho)

    def __call__(self, rho):
        return np.interp(rho, self.rho, self.G)

def optimize_temperature(U, v):

    #v here is actually sqrt(2v/gamma) in eq (32) in the SABC paper
    return brentq(lambda e: v*e**(3/2) + e**2 - U**2, 0, U)

class SABC (Sampler):
    """Simulated Annealing ABC sampler for non-informative priors."""


    def __init__ (self, batchsize, chains, epsilon_init, v=1, beta=0.8, update_jump=True, s=0.001):
        """Constructor for the SABC sampler.

        Options:
        batchsize - Number of samples that will be drawn for each batch (last batch contains posterior samples)
        chains - Number of particles (samples) to be updated for each batch (i.e. between temperature evaluations)
        epsion_init - Initial value of epsilon used to create the ensemble
        v - Tuning parameter controlling the annealing speed (corresponds to sqrt(2v/gamma) in the original SABC publication)
        beta - Scaling of the empirical covariance used to construct the jump distribution
        update_jump - Recompute the jump distribution after each iteration
        s - Scaling of an additional diagonal term added to the jump distribution to keep it from degenrating

        NOTE: the total number of model runs (for a single 'draw (...)') is 'chains', and not 'batchsize'.
        """
        self.batchsize = batchsize
        self.chains = chains
        self.epsilon_init = epsilon_init
        self.v = v
        self.beta = beta
        self.update_jump = update_jump
        self.s = s
        self.iter = 0
        self.infos = [None for index in range (self.batchsize)]

    def assign (self, distance, prior=None):
        """Assign required components to sampler."""

        if not hasattr (distance, 'assigned'):
            assert False, ' :: ERROR: distance needs to have components assigned before being assigned to sampler.'

        self.distance = distance
        self.prior = prior

        self.types = prior.types if prior is not None else None

        self.replicates = (self.distance.name == 'Replicates')

        self.sandboxing = self.distance.sandboxing
        self.task = self.distance

        self.assigned = True

    def init (self):

        # reset status
        self.initialized = 0

        # for continuation sampling, attempt to load the most recent pickup file
        pickup = self.read_pickup ()

        if pickup is not None:
            self.G = pickup ['G']
            self.E = pickup ['E']
            self.epsilon = pickup ['epsilon']
            self.K = pickup ['K']
            self.labels = pickup ['labels']
            self.dimensions = pickup ['dimensions']
            self.distances = pickup ['distances']
            self.priors = pickup ['priors']
            self.initialized = 1

    def pickup (self):
        """Return sampler pickup information as a dictionary."""

        state = {'G' : self.G, 'E' : self.E, 'epsilon' : self.epsilon, 'K' : self.K}
        state ['labels'] = self.labels
        state ['dimensions'] = self.dimensions
        state ['distances'] = self.distances
        state ['priors'] = self.priors
        return state

    def evaluate (self, qs):
        """Evaluate priors and distances of the proposed parameters."""

        # assign right datatypes to parameters
        qsc = cast (qs, self.prior.types)

        ps = np.array([self.prior.logpdf(parameters) if self.prior else 0.0 for index, parameters in qsc.iterrows()])

        # skip distance evaluation for parameters with zero prior
        keep = np.where(ps != -math.inf)[0]
        #keep = np.arange(len(ps)) # keep the whole thing for statistic # not good, you need not to go out of prior for model

        # evaluate the respective distances (according to the specified indices)
        ds = np.full (len(qs), math.nan)
        parameters = [ parameters for index, parameters in qsc.iloc[keep].iterrows () ]
        args = []
        sandboxes = [ self.sandboxes [index] for index in keep ] if self.sandboxing else None
        seeds = [ self.seeds [index] for index in keep ]
        #print(self.executor.map)
        results, timings = self.executor.map (self.distance, parameters, args, sandboxes, seeds)

        # extract estimates and infos
        predictions = [ None ] * len(qs)
        infos = [ None ] * len(qs)
        #profiles = [ None ] * len(qs)
        #print(len(results))
        for i, result in enumerate (results):
            #print(result[1]['statistic'])
            ds [keep[i]], infos [keep[i]] = result
            #ds [keep[i]], predictions [keep[i]], infos [keep[i]], profiles [keep[i]] = result
            #ds [keep[i]], predictions [keep[i]], infos [keep[i]] = result

        #for i in range(len(infos)):
        #    assert infos[i] is not None, ":: Fatal: This should not be None anymore (1)"
        #    print(infos[i].keys())
        #    print(infos[i]['statistic'])

        # get executor timing
        timing = self.executor.report ()

        return ps, ds, infos, timing, timings

    def results (self):

        # select needed part
        E = self.E.loc[:,self.labels]

        # assign right datatypes to parameters and proposes
        E = cast (E, self.prior.types)
        proposes = cast (self.proposes, self.prior.types)

        # sampler info
        info = {}
        info ["index"] = self.index
        info ["parameters"] = E
        info ["particle_ids"] = self.particle_ids
        info ["proposes"] = proposes
        info ["priors"] = self.priors
        info ["distances"] = self.distances
        info ["epsilon"] = self.epsilon
        info ["U"] = self.U
        info ["trace(K)"] = np.trace (self.K) if len (self.K.shape) > 0 else self.K
        #info ["trace(K)"] = np.trace (self.K)
        info ["accepts"] = self.accepts
        #for i in range(len(self.infos)):
        #    print(type(self.infos[i]))
        info ["infos"] = self.infos

        if self.informative:
            info ["timing"] = self.timing
            info ["timings"] = self.timings

        return self.E.loc[:,self.labels], info

    # return fitness measure for the specified chain (higher is better)
    # TODO: what is better to be used here?
    def fit (self, info, chain):

        return 1.0 / info ['distances'] [chain]

    def optimize_jump (self, ensemble_parameters):

        Sigma = np.cov(ensemble_parameters.T)
        if len (Sigma.shape) == 0:
            return self.beta * Sigma + self.s * Sigma
        else:
            return self.beta*Sigma + self.s*np.trace(Sigma)*np.identity(Sigma.shape[0])

    def draw (self, sandbox, seed, feedback):
        """Draw samples from posterior distribution."""

        # setup distance
        self.distance.setup (self.verbosity - 2, self.informative, self.trace)

        # construct sandboxes and seeds
        labels = ['C%05d' % chain for chain in range (self.chains)]
        if self.sandboxing:
            self.sandboxes = [sandbox.spawn (label) for label in labels]
        else:
            self.distance.isolate (sandbox = None)
        self.seeds = [seed.spawn (chain, name=label) for chain, label in enumerate (labels)]

        #if not hasattr(self,'infos'):
        #    assert False, ":: Fatal: self.infos not available?!?"

        self.accepts = np.full (self.batchsize, False)

        # treat 'initial' parameters as the first sample
        if not self.initialized:

            if self.verbosity >= 3:
                print ('SABC: draw (initialize)')

            self.particle_ids = np.arange (self.batchsize)

            # persistent distances and priors (always include the latest valid values)
            self.distances = np.empty (self.batchsize)
            self.priors = np.empty (self.batchsize)

            # store keys for later wrapping
            self.labels = self.prior.labels

            # store parameter dimensions - how many paramters to be inferred
            self.dimensions = len (self.labels)

            #These will grow and mutate dynamically so they are initially kept as lists of DataFrames
            E = [] #Ensemble of particles
            P = [] #Ensemble of all initial draws (including rejected)
            count = 0
            self.timing = Timing (self.informative >= 2)
            self.timings = [Timing (self.informative >= 2) for worker in range (self.executor.workers)]
            while count < self.batchsize:
                p = [self.prior.draw(rng=self.rng) for _ in range(self.chains)]
                df = pd.DataFrame().append(p)
                self.proposes = df
                ps, rho, infos, timing, timings = self.evaluate(df)
                df['log_prior'] = ps
                df['rho'] = rho
                accepted = self.rng.uniform(size=self.chains) < np.exp( -df['rho'] / self.epsilon_init)
                accepts = np.array (accepted)
                P.append(df)
                E.append(df[accepted])
                infos = [info for index, info in enumerate (infos) ] #if accepts [index]]
                priors = [ps [index] for index in range (self.chains) ] # if accepts [index]]
                distances = np.array (rho) #[accepts]
                for index, info in enumerate (infos):
                    if count + index < self.batchsize:
                        self.infos [count + index] = info
                        self.priors [count + index] = priors [index]
                        self.distances [count + index] = distances [index]
                        self.accepts [count + index] = True
                count += np.sum(accepted)
                self.timing += timing
                for worker, timing in enumerate (self.timings):
                    timing += timings [worker]
                if self.verbosity >= 2:
                    print("SABC: initial samples: {}/{} draws".format(count, self.batchsize))

            #for ii in range(len(self.infos)):
            #    print(self.infos[ii]['statistic'])

            # QUESTION: P might be larger than self.batchsize - is this OK?
            P = pd.DataFrame().append(P, ignore_index=True)
            self.E = pd.DataFrame().append(E, ignore_index=True)
            if self.verbosity >= 2:
                print("SABC: initial sample: {} draws, {} accepted".format(len(P.index), len(self.E.index)))
            self.E = self.E.iloc [:self.batchsize]

            self.G = DistanceTransformation(P['rho'])
            self.E['u'] = self.G(self.E['rho'])
            self.U = np.mean(self.E['u'])
            self.epsilon = optimize_temperature(self.U, self.v)
            self.K = self.optimize_jump(self.E.loc[:, self.labels].to_numpy())

            self.initialized = 1

            return self.results ()

        if self.verbosity >= 3:
            print ('SABC: draw (update)')

        self.particle_ids = self.rng.choice(range(len(self.E)), size=self.chains, replace=False)

        current = self.E.loc[self.particle_ids]
        proposal = pd.DataFrame().reindex_like(current)

        if len (self.K.shape) == 0:
            for i in proposal.index:
                proposal.loc[i, self.labels] = np.sqrt(self.K) * self.rng.randn() + current.loc[i, self.labels]
        else:
            # there is no vectorized version of multivariate_normal
            for i in proposal.index:
                proposal.loc[i, self.labels] = self.rng.multivariate_normal(current.loc[i, self.labels], self.K)

        self.proposes = proposal.loc[:, self.labels]

        ps, rho, infos, self.timing, self.timings = self.evaluate(proposal.loc[:, self.labels])
        #for ii in range(len(infos)):
        #    print(type(infos[ii])) #['statistic'])
        proposal.loc[:,'log_prior'] = ps
        proposal.loc[:,'rho'] = rho
        proposal.loc[:,'u'] = self.G(rho)

        accept_prob = np.exp(proposal.loc[:, 'log_prior'] - current.loc[:, 'log_prior'] + (current.loc[:, 'u'] - proposal.loc[:, 'u']) / self.epsilon)
        accepted = pd.Series(index = proposal.index, dtype=bool)
        accepted[accept_prob >= 1] = True
        accepted[accept_prob < 1] = self.rng.uniform(size = np.sum(accept_prob < 1)) < accept_prob[accept_prob < 1]
        accepted[accept_prob.isna()] = proposal.loc[:, 'u'] < current.loc[:, 'u']
        accepted[proposal.loc[:, 'log_prior'] == -math.inf] = False #Never wander outside of the prior

        accepts = accepted.to_numpy ()

        self.accepts [self.particle_ids] = accepts
        #for ii in range(len(infos)):
        #    print(type(infos[ii])) #['statistic'])
        #print(len(self.particle_ids))
        #print(len(infos))
        for index, info in enumerate (infos):
            #assert info is not None, ":: Fatal: this should not be None anymore (2)"
            if info is None: # happens for out of prior stuff as keep must be kept
                continue
            ### added 15 July 21 to try to keep more statistic in a crazy design ###
            self.infos [self.particle_ids [index]] = info
            if self.informative <= 2:
                if not accepts [index] and self.infos [self.particle_ids [index]] is not None:
                    self.infos [self.particle_ids [index]]['observations'] = None
                    self.infos [self.particle_ids [index]]['predictions'] = None
            ###
            ### before 15 July 21 ###
            #if accepts [index]:
                #self.infos [self.particle_ids [index]] = info
            #else:
            #    self.infos [self.particle_ids [index]] = info
            ###
            if accepts [index]:
                self.distances [self.particle_ids [index]] = rho [index]
                self.priors [self.particle_ids [index]] = ps [index]

        #for ii in range(len(self.infos)):
        #    print(type(self.infos[ii])) #['statistic'])

        self.E.update(proposal.loc[accepted])

        self.U = np.mean(self.E['u'])
        self.epsilon = optimize_temperature (self.U, self.v)
        if self.update_jump:
            self.K = self.optimize_jump(self.E.loc[:, self.labels].to_numpy())

        #TODO: Add an optional final resampling step

        return self.results ()
