# # # # # # # # # # # # # # # # # # # # # # # # # #
# Markov chain Monte Carlo (affine-invariant ensemble) sampler class
# Foreman-Mackey, Hogg, Lang & Goodman
# "emcee: The MCMC Hammer"
# PASP, 2012.
#
# Code in 'stretch ()' and 'propose ()' methods was adapted from
# https://github.com/dfm/emcee (dated in 2018)
# and extended according to the needs of the SPUX framework.
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

import numpy
import pandas

from .sampler import Sampler
from ..utils.timing import Timing
from ..utils.transforms import cast

numpy.seterr (invalid='ignore')

class EMCEE (Sampler):
    """Markov chain Monte Carlo (affine-invariant ensemble) sampler class."""

    def __init__(self, chains, a=2.0, attempts=100, reset=10):
        """
        Constructor: *reset* is the number of chain updates after which likelihoods of stuck chains are re-evaluated.
        """

        self.chains = chains
        self.a = a
        self.attempts = attempts
        self.reset = reset

    # init chain
    def init (self, initial=None, reinit=0, posteriors=None):
        """Init chain.

        Initial values and associated posteriors can be provided to continue the sampling process instead of starting from the beginning.
        """

        # additional routines for the first init
        if not reinit:

            # issue a warning if there are fewer chains than twice the number of workers
            if self.chains < 2 * self.executor.workers:
                print (' :: WARNING: in EMCEE sampler, number of chains is less than twice the number of workers:')
                print ('  : -> %d < 2 * %d.' % (self.chains, self.executor.workers))

            # for continuation sampling, attempt to load the most recent pickup file
            pickup = self.read_pickup ()
            if pickup is not None:
                initial = pickup ['parameters']
                posteriors = pickup ['posteriors']
                self.stuck = pickup ['stuck']
            else:
                self.stuck = numpy.zeros (self.chains, dtype=int)

        # reset status
        self.initialized = 0

        # if initial parameters are not specified, draw them from the prior distribution
        if initial is None:
            self.parameters = pandas.DataFrame (index=range(0), columns=self.prior.labels)
            for chain in range (self.chains):
                parameters = self.prior.draw (rng=self.rng)
                self.parameters = self.parameters.append (parameters, ignore_index=1, sort=True)

        # else, if only one set of parameters is specified, replicate it
        elif not isinstance (initial, pandas.DataFrame):
            self.parameters = pandas.DataFrame (initial, index=range(1))
            self.parameters = pandas.DataFrame (numpy.repeat (self.parameters.values, self.chains, axis=0), index=range(self.chains), columns=self.parameters.keys())

        # else, if all parameteres are specified, use them
        else:
            self.parameters = initial
            if posteriors is not None:
                self.Lps = posteriors
                self.initialized = 1
            else:
                print("EMCEE: Got this parameters from initial")
                print(self.parameters)
                print("#######################################")

        # store keys for later wrapping
        self.labels = self.parameters.keys()

        # store parameter dimensions - how many paramters to be inferred
        self.dimensions = len (self.labels)

        if self.verbosity >= 3:
            print ("Initial samples:")
            print (self.parameters)

        if self.initialized and self.verbosity >= 3:
            print ("Initial posteriors:")
            print (self.Lps)

        if self.verbosity >= 3:
            print ("Initial stuck counters:")
            print (self.stuck)

        #print(self.executor)
        #assert False

    def pickup (self):
        """Return sampler pickup information as a dictionary."""

        return {'parameters' : self.parameters, 'posteriors' : self.Lps, 'stuck' : self.stuck}

    # process feedback
    def feedback (self, info):

        if not hasattr (self.likelihood, 'feedback'):
            return

        # for replicates likelihoods, return a dictionary with associated likelihood estimates across chains
        if self.likelihood.name == 'Replicates':
            feedbacks = [self.likelihood.feedback (info ["infos"] [chain]) for chain in range (self.chains) if info ["infos"] [chain] is not None]
            feedback = {name : numpy.nanmedian ([feedback [name] for feedback in feedbacks]) for name in self.likelihood.names}
            info ["feedback"] = feedback
            if self.informative:
                info ['feedbacks'] = feedbacks
            if (self.lock is None or self.index < self.lock):
                if self._feedback is None:
                    self._feedback = {name: (feedback [name] if not numpy.isnan (feedback [name]) else None) for name in self.likelihood.names}
                else:
                    for name in self.likelihood.names:
                        if not numpy.isnan (feedback [name]):
                            self._feedback [name] = feedback [name]

        # for all other likelihoods, simply return all likelihood estimates across chains
        else:
            feedbacks = numpy.array ([self.likelihood.feedback (info ["infos"] [chain]) for chain in range (self.chains) if info ["infos"] [chain] is not None])
            feedback = numpy.nanmedian (feedbacks)
            info ["feedback"] = feedback
            if self.informative:
                info ['feedbacks'] = feedbacks
            if (self.lock is None or self.index < self.lock) and not numpy.isnan (feedback):
                self._feedback = feedback

    # evaluate likelihoods and priors of the proposed parameters
    def evaluate (self, qs):
        """Evaluate likelihoods and priors of the proposed parameters."""

        # assign right datatypes to parameters
        qsc = cast (qs, self.prior.types)

        # get indices to be evaluated
        indices = qs.index.values

        # evaluate priors
        ps = numpy.array ([ self.prior.logpdf (parameters) if self.prior else 0.0 for index, parameters in qsc.iterrows () ])

        # skip likelihood evaluation for parameters with zero prior
        keep = numpy.where (ps != float ('-inf')) [0] #numpy.array(numpy.where (ps != float('-inf')))
        skip = numpy.where (ps == float ('-inf')) [0] #numpy.array(numpy.where (ps == float('-inf')))

        # evaluate the respective likelihoods (according to the specified indices)
        Ls = numpy.full (len(qs), float ('-inf'))
        parameters = [ parameters for index, parameters in qsc.iloc[keep].iterrows () ]
        args = []
        sandboxes = [ self.sandboxes [ indices [index] ] for index in keep ] if self.sandboxing else None
        seeds = [ self.seeds [ indices [index] ] for index in keep ]
        results, timings = self.executor.map (self.likelihood, parameters, args, sandboxes, seeds)

        # extract estimates and infos
        infos = [ None ] * len(qs)
        for i, result in enumerate (results):
            Ls [keep[i]], infos [keep[i]] = result

        # get executor timing
        timing = self.executor.report ()

        # compute Ls + ps
        Lps = Ls + ps

        # set skipped likelihoods to 'NaN's (after computing Lps)
        Ls [skip] = float ('nan')

        return Lps, ps, Ls, infos, timing, timings

    # returned packed results
    def results (self):
        """Returned packed results."""

        # assign right datatypes to parameters and proposes
        parameters = cast (self.parameters, self.prior.types)
        proposes = cast (self.proposes, self.prior.types)

        # filter out rejected infos
        if self.informative <= 2:
            for index, accept in enumerate (self.accepts):
                if not accept:
                    self.infos [index] = None

        # sampler info
        info = {}
        info ["index"] = self.index
        info ["parameters"] = parameters
        info ["proposes"] = proposes
        info ["priors"] = self.ps
        info ["likelihoods"] = self.Ls
        info ["posteriors"] = self.Lps
        info ["accepts"] = self.accepts
        info ["resets"] = self.resets
        info ["infos"] = self.infos

        # auxiliary informative fields
        if self.informative:
            info ["timing"] = self.timing
            info ["timings"] = self.timings

        return self.parameters, info

    # routine adapted from EMCEE
    def stretch (self, p0, p1, lnprob0):
        """Routine adapted from EMCEE."""

        s = numpy.atleast_2d(p0.values)
        Ns = len(s)
        c = numpy.atleast_2d(p1.values)
        Nc = len(c)

        zz = ((self.a - 1.) * self.rng.rand(Ns) + 1) ** 2. / self.a
        rint = self.rng.randint(Nc, size=(Ns,))
        proposes = pandas.DataFrame (c[rint] - zz[:, numpy.newaxis] * (c[rint] - s), columns=self.labels, index=p0.index)

        # check if some chains are stuck and re-evaluate their posteriors
        stuck = {index : loc for index, loc in enumerate (proposes.index) if self.stuck [loc] == self.reset}
        for index, loc in stuck.items ():
            proposes.loc [loc] = self.parameters.loc [loc]

        Lps, ps, Ls, infos, timing, timings = self.evaluate (proposes)
        try:
            lnpdiff = (self.dimensions - 1.) * numpy.log(zz) + Lps - lnprob0
        except FloatingPointError as e:
            print(":: WARNING: reject proposed parameter as two specular chains are at -inf, ",e)
            #assert numpy.where(lnprob0 == -float('inf'))[0] in numpy.where(Lps == -float('inf') )[0], ":: Fatal: -inf are not located where expected. This is a bug."
            #print(lnprob0)
            #print(self.parameters)
            lnprob0[numpy.isinf(lnprob0)] = 0
            lnpdiff = (self.dimensions - 1.) * numpy.log(zz) + Lps - lnprob0
        except:
            raise ValueError(":: Fatal: wrong computation of lnpdiff in EMCEE. This is a bug.")

        accepts = (lnpdiff > numpy.log(self.rng.rand(len(lnpdiff))))

        # accept all proposes that were stuck and their posteriors were re-evaluated
        for index, loc in stuck.items ():
            accepts [index] = True
        self.resets += len (stuck)

        return proposes, Lps, ps, Ls, infos, accepts, timing, timings

    # routine adapted from EMCEE
    def propose (self):
        """Propose new parameters."""

        self.timing = Timing (self.informative >= 2)
        self.timings = [Timing (self.informative >= 2) for worker in range (self.executor.workers)]
        self.proposes = pandas.DataFrame (index=range(self.chains), columns=self.labels)
        self.ps = numpy.zeros (self.chains)
        self.Ls = numpy.zeros (self.chains)
        self.infos = [None for chain in range(self.chains)]
        self.accepts = numpy.zeros (self.chains)
        half = self.chains // 2
        first, second = numpy.arange (0, half), numpy.arange (half, self.chains)
        for S0, S1 in [(first, second), (second, first)]:
            proposes, Lps, ps, Ls, infos, accepts, timing, timings = self.stretch (self.parameters.iloc [S0], self.parameters.iloc [S1], self.Lps [S0])
            self.proposes.iloc[S0] = proposes
            self.ps[S0] = ps
            self.Ls[S0] = Ls
            for index, info in enumerate (infos):
                self.infos [S0[index]] = info
            self.accepts[S0] = accepts
            self.stuck [S0[accepts]] = 0
            self.stuck [S0[numpy.logical_not (accepts)]] += 1
            self.Lps[S0[accepts]] = Lps [accepts]
            self.parameters.values[S0[accepts]] = proposes.values [accepts]
            self.timing += timing
            for worker, timing in enumerate (self.timings):
                timing += timings [worker]

    # draw samples from posterior distribution
    def draw (self, sandbox, seed, feedback):
        """Draw samples from posterior distribution."""

        # setup and configure likelihood
        self.likelihood.setup (self.verbosity - 2, self.informative, self.trace)
        self.likelihood.configure (feedback)

        # construct sandboxes and seeds
        labels = ['C%05d' % chain for chain in range (self.chains)]
        if self.sandboxing:
            self.sandboxes = [sandbox.spawn (label) for label in labels]
        else:
            self.likelihood.isolate (sandbox = None)
        self.seeds = [seed.spawn (chain, name=label) for chain, label in enumerate (labels)]

        # initialize resets counter
        self.resets = 0

        # treat 'initial' parameters as the first sample
        if not self.initialized:

            if self.verbosity >= 3:
                print ('EMCEE: draw (initialize)')

            # attempt to evaluate likelihood of the initial parameters, with a redraw if needed
            for attempt in range (self.attempts):

                # all initial samples are proposed
                self.proposes = self.parameters

                # evaluate likelihood and prior of the initial parameters
                self.Lps, self.ps, self.Ls, self.infos, self.timing, self.timings = self.evaluate (self.proposes)

                # check if at least one likelihood is valid
                if not all (self.Lps == float ("-inf")):
                    break

                # otherwise, attempt to redraw prior samples
                else:

                    # if this is the final attempt, crash
                    if attempt == self.attempts - 1:
                        print (" :: Fatal: Unable to find initial parameters with non-zero likelihoods.")
                        print ("  : -> You may try to change seed and/or give explicit initial parameters.")
                        self.executor.abort ()

                    # otherwise, attempt to redraw prior samples
                    if self.verbosity >= 0:
                        print (" :: Warning: The likelihoods of all initial parameters are zero.")
                        print ("  : -> Re-drawing initial parameters from prior (attempt: %d/%d)" % (attempt, self.attempts))
                    self.init (initial=None, reinit=1)

            # all initial samples are accepted
            self.accepts = numpy.ones ((self.chains,))

            # all initial parameters are accepted
            self.parameters = self.proposes

            # update status
            self.initialized = 1

            return self.results ()

        if self.verbosity >= 3:
            print ('EMCEE: draw (sample)')

        # get new parameters
        self.propose ()

        return self.results ()
