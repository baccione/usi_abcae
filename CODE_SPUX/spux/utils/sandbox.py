# # # # # # # # # # # # # # # # # # # # # # # # # #
# Sandbox class
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

import os
import sys # noqa: F401
import shutil
import glob
import argparse

from ..io.formatter import plain

class Sandbox (object):
    """Class to construct isolated environments (sandboxes) where to run independent instances of user models."""

    # constructor
    def __init__ (self, path='sandbox', template=None, statefiles=None, cache=True):

        #cwrank = MPI.COMM_WORLD.Get_rank ()
        #print("within __init__ of sandbox") #, cwrank: ",cwrank)
        #sys.stdout.flush()
        self.parser = argparse.ArgumentParser ()
        self.parser.add_argument ("--continue", action = "store_true")
        self.parser.add_argument ("--unsafe", action = "store_true")
        self.args, unknown = self.parser.parse_known_args ()
        self.template = template
        self.statefiles = statefiles

        if path is None:
            assert False, " :: ERROR: For sandbox, please specify a path (or a name) to a scratch directory."

        # if template directory does not exist, issue a warning
        if template is not None and not os.path.exists (self.template):
            print (' :: WARNING: Template directory not found:')
            print ('  : -> %s' % self.template)

        self.path = path
        if not os.path.exists (self.path):
            os.makedirs (self.path, exist_ok = True)
        else:
            # This is all hopeless because of spux bad design - I should redesign the whole command line pipeline, lol
            #if not vars(self.args)['continue'] or not vars(self.args)['unsafe']:
            #    print(not vars(self.args)['unsafe'])
            #    assert False, ":: Fatal: sandbox {} already exists. Please, remove it by hand.".format(self.path)
            assert False, ":: Fatal: sandbox {} already exists. Remove it by hand or hack me.".format(self.path)

        # set cache directory to be in the root of the parent sandbox directory
        # notice, that this might mean no caching in the root sandbox
        if cache:
            try:
                parentpath, = os.path.split (self.path)
                self.cachepath = os.path.join (parentpath, '.cache')
            except:
                self.cachepath = None
        else:
            self.cachepath = None

    # get sandbox directory (and create it, if necessary)
    def __call__ (self, path = None):
        """Get sandbox directory (and create it, if necessary). Join specified path, if provided."""

        if not os.path.exists (self.path):
            os.makedirs (self.path, exist_ok = True)

        if path is not None:
            return os.path.join (self.path, path)
        else:
            return self.path

    # get sandbox description
    def __str__ (self):
        """Get sandbox description."""

        #cwrank = MPI.COMM_WORLD.Get_rank ()
        #print("within __str__ of sandbox") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        result = self.path

        #print("done with __str__ of sandbox") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        return result

    # spawn new sandbox
    def spawn (self, name):
        """Create new sandbox."""

        #cwrank = MPI.COMM_WORLD.Get_rank ()
        #print("within __spawn__ of sandbox") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        path = os.path.join (self.path, name)
        result = Sandbox (path, template = self.template, statefiles = self.statefiles)

        #print("done with __spawn__ of sandbox") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        return result

    # copy all files from a given template directory to the sandbox
    def copyin (self):
        """Copy all files from a given template directory to the sandbox."""

        # if template is not specified, return
        if self.template is None:
            return

        # if template directory does not exist, fatal
        if not os.path.exists (self.template):
            assert False, ":: Fatal: Template directory {} not found.".format(self.template)

        # cache template directory, if needed
        if self.cachepath is not None:
            sourcepath = self.cachepath + '-' + plain (self.template)
            if not os.path.exists (sourcepath):
                shutil.copytree (self.template, sourcepath)
        else:
            sourcepath = self.template

        # remove directory
        counter = 1
        notdone = True
        while counter < 1E04 and notdone is True:
            try:
                shutil.rmtree (self.path)
                notdone = False
            except Exception as e: # noqa: F841
                counter = counter + 1

        if notdone:
            assert(counter >= 1E04),":: Fatal: counter and notdone mismatch (1)."
            assert False, ":: Fatal: could not remove this: {}".format(self.path)

        # copy all files
        counter = 1
        notdone = True
        while counter < 1E04 and notdone is True:
            try:
                shutil.copytree (sourcepath, self.path)
                notdone = False
            except FileExistsError as exc: # noqa: F841
                notdone = False
            except shutil.Error as exc: # noqa: F841
                counter = counter + 1

        if notdone:
            assert(counter >= 1E04),":: Fatal: counter and notdone mismatch (2)."
            assert False, ":: Fatal (2): could not copy this: {} into: {}".format(sourcepath,self.path)

    # remove
    def remove (self):
        """Remove entire sandbox."""

        # remove entire sandbox
        if os.path.exists (self.path):
            try:
              shutil.rmtree (self.path)
            except:
              print(":: WARNING: Failed to remove path: {}".format(self.path))

        #print("done with remove of sandbox") #, cwrank: ",cwrank)
        #sys.stdout.flush()

    # stash
    def stash (self, suffix='-stash'):
        """Stash (move) sandbox by appending the specified suffix."""

        path = self.path + suffix

        if os.path.exists (path):
            shutil.rmtree (path)

        if os.path.exists (self.path):
            os.rename (self.path, path)

        self.path = path

    # fetch
    def fetch (self, tail, suffix='-stash'):
        """Fetch (move) sandbox by removing the specified suffix and changing path end by a specified tail."""

        path = self.path [ : - (len (suffix) + len (tail))] + tail

        if os.path.exists (path):
            shutil.rmtree (path)

        if os.path.exists (self.path):
            os.rename (self.path, path)

        self.path = path

    def save (self, files = None):
        """Save all sandbox files specified in a list to a binary state."""

        if files is None:
            if self.statefiles is None:
                return None
            else:
                files = self.statefiles

        contents = {}
        for file in files:
            # TODO: check if it is a directory?
            paths = glob.glob (self (file))
            for path in paths:
                with open (path, 'rb') as f:
                    contents [file] = f.read ()

        #print("done with save of sandbox") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        return contents

    def load (self, contents):
        """Load all files from the binary state to the sandbox."""

        if contents is None:
            return

        #cwrank = MPI.COMM_WORLD.Get_rank ()
        #print("within load of sandbox") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        for file, content in contents.items ():
            # TODO: mkdirs if needed?
            with open (self (file), 'wb') as f:
                f.write (content)

        #print("done with load of sandbox") #, cwrank: ",cwrank)
        #sys.stdout.flush()
