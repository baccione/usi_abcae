
import numpy
import pandas
from copy import deepcopy as copy

def flatten (dictlist):
    """Flatten a list of dictionaries into a dictionary."""

    #if all (dictlist):
    results = {key : value for result in dictlist for key, value in result.items ()}

    return results

# convert dict with integer keys to numpy array - assume user plays nice
def numpify (dictionary):
    """Convert dict with integer keys to numpy array - assume user plays nice."""

    values = [dictionary [key] for key in range (len (dictionary))]
    return numpy.array (values)

# convert dict with integer keys and pandas DataFrame rows to pandas DataFrame - assume user plays nice
def pandify (dictionary):
    """Convert dict with integer keys and pandas DataFrame rows to pandas DataFrame - assume user plays nice."""

    if len (dictionary) == 0:
        return None

    auxiliary = isinstance (list (dictionary.values ()) [0], dict)
    if auxiliary:
        columns = list (list (dictionary.values ()) [0] ['series'] .index)
        series = {key : value ['series'] for key, value in dictionary.items ()}
    else:
        columns = list (list (dictionary.values ()) [0] .index)
        series = dictionary
    dataframe = pandas.DataFrame.from_dict (series, orient = 'index', columns = columns).sort_index ()
    if auxiliary:
        return {'series' : dataframe, 'auxiliary' : dictionary ['auxiliary']}
    else:
        return dataframe

def rounding (method):
    '''A decorator to map method arguments from float to integer by rounding.'''

    def mapped (value):
        #print("value {} in rounding that will become: {}".format( value, (numpy.round (value)).astype(int)) ) #ok
        return method ( (numpy.round (value)).astype(int) )
    return mapped

def logmeanstd (logm, logs):
    '''Return the needed quantities to construct stats.lognorm with a specified mean (logm) and standard deviation (logs).

    According to documentation at:
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.lognorm.html
    '''

    factor = 1 + (logs / logm) ** 2
    expm = logm / numpy.sqrt (factor)
    s = numpy.sqrt (numpy.log (factor))

    return { 's' : s, 'scale' : expm }

def cast (data, types):
    """Cast all entries in the copy of the dataframe or series accordingly.
    If all types match no casting and no copy is made - original data instance is returned instead."""

    matching = [datatype in str (data [label] .dtype if hasattr (data [label], 'dtype') else type (data[label])) for label, datatype in types.items ()]
    if not all (matching):
        data = copy (data)
        for label, datatype in types.items ():
            data [label] = data [label] .astype (datatype)
    return data

def update_times (times, additional):
    """Update times array with additional entries (used internally below)."""

    if times is None:
        updated = additional
    else:
        updated = numpy.concatenate ((times, additional))
        updated = numpy.unique (updated)
        updated.sort (kind = 'mergesort')
    return updated

def times (dataset = None, timeset = None, auxset = None, snapshots = None):
    """Construct a list of times from the dataset index (or explicitly provided snaphots), timeset, and auxset. Also return snapshot flags."""

    if snapshots is None and (type (dataset) is pandas.DataFrame or type (dataset) is pandas.Series):
        snapshots = list (dataset.index)

    if snapshots is None and timeset is None:
        return [None], {None : True}

    if auxset is not None and auxset ['snapshots'] is not None:
        snapshots = update_times (snapshots, auxset ['snapshots'])

    times = None
    if snapshots is not None:
        times = update_times (times, snapshots)
    if timeset is not None:
        if type (timeset) == int:
            if snapshots is not None and len (snapshots) >= 2:
                # try:
                intervals = zip (snapshots [:-1], snapshots [1:])
                intermediate = [time for left, right in intervals for time in intermediate_times (left, right, timeset)]
                times = update_times (times, intermediate)
                # except:
                #     print (' :: ERROR: Integer "timeset" unsupported for this configuration:', timeset)
                #     print ('  : -> Consider setting "timeset" to "None" or to an explicit array of values.')
                #     sys.exit (1)
            else:
                timeset = None
        elif type (timeset) == str:
            intermediate = pandas.date_range (start = snapshots [0], end = snapshots [-1], freq = timeset)
            timeset = update_times (times, intermediate.to_list ())
        else:
            times = update_times (times, timeset)

    # construct snapshot flags
    flags = {}
    upcoming = 0
    for time in times:
        if upcoming < len (snapshots) and time == snapshots [upcoming]:
            flags [time] = True
            upcoming += 1
        else:
            flags [time] = False

    return times, flags

def append (info, time, dictionary):
    """Append value of *dictionary* to the *info* dictionary (according to keys) at the specified *time*."""

    if dictionary is not None:
        for diagnostic, value in dictionary.items ():
            if diagnostic == 'successful':
                if 'successful' not in info.keys ():
                    info ['successful'] = diagnostic
                else:
                    info ['successful'] = info ['successful'] and diagnostic
            else:
                if diagnostic not in info.keys ():
                    info [diagnostic] = {}
                info [diagnostic] [time] = value
