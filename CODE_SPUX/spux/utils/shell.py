import subprocess
import os, sys
import shutil
import argparse

from ..io import dumper

def execute (command, directory = None, verbosity = 1, executable = None):
    """
    Execute an application command (including any arguments) in a command line shell.

    Parameter `verbosity`:
      0 - no output to the console
      1 - print only error messages (default)
      2 - prints the command being executed and the result

    Returns (process returncode, standard output, error output)
    """

    PIPE = subprocess.PIPE
    if verbosity:
        print ('  : -> Executing: %s' % command)
    process = subprocess.Popen ([command], cwd = directory, shell = True, stdout = PIPE, stderr = PIPE, env = os.environ.copy (), executable = executable)
    output, error = process.communicate ()

    if verbosity:
        if process.returncode == 0:
            print ('  : -> Shell command done.')
        else:
            print ('  : -> Shell command returned a non-zero exit code.')
            print ('  : -> The standard output and the standard error are printed below.')
            print ('  : -> STDOUT:')
            print (output.decode ('ascii'))
            print ('  : -> STDERR:')
            print (error.decode ('ascii'))

    return (process.returncode, output, error)

def importer (script):
    """Execute python script, tracing its execution for reproducibility."""

    command = '%s %s' % (sys.executable, script)
    directory = os.getcwd ()
    returncode = None
    if shutil.which ('reprozip') is not None:
        parser = argparse.ArgumentParser ()
        help = "do not generate reproducible package"
        parser.add_argument ("--no-repro", dest = "norepro", help = help, action = "store_true")
        args, unknown = parser.parse_known_args ()
        if args.norepro:
            print (' :: WARNING: Reproducible package disabled - *.rpz will not be generated.')
            returncode, output, error = execute (command, directory)
        else:
            print (' :: INFO: Reproducible package *.rpz will be generated - use "--no-repro" to disable.')
            execute ('reprozip usage_report --disable', directory)
            returncode, output, error = execute ('reprozip trace --overwrite ' + command, directory)
            if returncode == 0:
                name = dumper.prefixname ('reprozip.rpz')
                if os.path.exists (name):
                    os.remove (name)
                execute ('reprozip pack %s' % name, directory)
            else:
                print (' :: WARNING: Command "reprozip trace" failed - *.rpz will not be generated.')
                returncode, output, error = execute (command, directory)
    else:
        print (' :: WARNING: Command "reprozip" is not available - *.rpz will not be generated.')
        returncode, output, error = execute (command, directory)
    if returncode != 0:
        assert False, ' :: ERROR: importing %s failed.' % script
