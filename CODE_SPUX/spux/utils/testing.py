
import copy

from .sandbox import Sandbox

def clone (model, parameters, time_clone, time_compare, inputset=None):
    """
    Test model cloning correctness (i.e., the save/load methods).

    1. Running the specified model to the .
    2. Save model state using save.
    3. Run the model further to the specified *time_compare*
    4. Make a clone of a model from the saved model state at the specified *time_clone*.
    5. Run the cloned model to the specified *time_compare*.
    6. Compare the results from both models.
    """

    # store framework seed and sandbox
    seed = model.seed
    sandbox = model.sandbox

    # clone the first model to the second model before any other actions
    model2 = copy.deepcopy (model)

    if model.sandboxing and sandbox is None:
        print ('  : -> WARNING: No \'sandbox\' specified, but sandboxing is enabled for %s.' % model.name)
        print ('  : -> Setting sandbox to a default \'sandbox = Sandbox ()\'.')
        sandbox = Sandbox ()

    # set seed for the initial run until the clone time
    seed_clone = seed.spawn (0, name = 'clone')

    # run the first model up to the clone time and save its state
    print (' :: Running the model up to the specified clone time...')
    model1 = copy.deepcopy (model)
    sandbox1 = sandbox.spawn ('sandbox1') if sandbox is not None else None
    model1.isolate (sandbox1)
    model1.plant (seed_clone)
    model1.init (inputset, parameters)
    prediction = model1.run (time_clone)
    print ('  : -> Obtained prediction at the clone time:')
    print (prediction)
    print (' :: Saving the state of the original model...')
    state = model1.save ()

    # plant and run the first model up to the specified compare time
    print (' :: Running the original model up to the specified compare time...')
    seed_compare = seed.spawn (1, name = 'compare')
    model1.plant (seed_compare)
    prediction1 = model1.run (time_compare)

    # clone the first model to the second model by loading its state
    sandbox2 = sandbox.spawn ('sandbox2') if sandbox is not None else None
    model2.isolate (sandbox2)
    model2.load (state)

    # plant and the cloned model up to the specified compare time
    print (' :: Running the cloned model up to the specified compare time...')
    seed_compare = seed.spawn (1, name = 'compare')
    model2.plant (seed_compare)
    prediction2 = model2.run (time_compare)

    # compare predictions
    print ('  : -> Prediction of the original model (model1 in sandbox1):')
    print (prediction1)
    print ('  : -> Prediction of the cloned model (model2 in sandbox2):')
    print (prediction2)
    if type (prediction1) == dict:
        print ('  : -> Prediction of the original model (model1 in sandbox1):')
        print (prediction1 ['values'])
        print ('  : -> Prediction of the cloned model (model2 in sandbox2):')
        print (prediction2 ['values'])
        if all (prediction1 ['values'] == prediction2 ['values']):
            print (' :: SUCCESS: predictions of both models are identical.')
        else:
            print (' :: ERROR: predictions of both models are NOT identical.')
        print (' :: WARNING: auxiliary predictions were NOT checked.')
    else:
        print ('  : -> Prediction of the original model (model1 in sandbox1):')
        print (prediction1)
        print ('  : -> Prediction of the cloned model (model2 in sandbox2):')
        print (prediction2)
        if all (prediction1 == prediction2):
            print (' :: SUCCESS: predictions of both models are identical.')
        else:
            print (' :: ERROR: predictions of both models are NOT identical.')
