
import numpy
import pandas
import os

from ..io import dumper
from .sandbox import Sandbox

def generate (model, parameters, snapshots, error=None, inputset=None, timeset=None, replicates=None, errors=None, inputsets=None):
    """
    Generate a synthetic dataset (or multiple datasets if replicates is set) for the specified model and parameters.

    Two sets of files are generated:
     - predictions*.dat: exact model predictions (at *timeset*) to be used as a reference for comparison with the maximum a posteriori (MAP) estimate,
     - dataset*.dat: model predictions (at *snapshots*) perturbed by the specified error model (only if the error model is specified).
    If *timeset* is not specified, then *timeset* is set to *snapshots*.

    If *replicates* is specified (non-negative integer), multiple stochastic realizations are generated.
    """

    # store framework seed, sandbox, and outputdir
    seed = model.seed
    sandbox = model.sandbox
    outputdir = model.outputdir

    if model.sandboxing and sandbox is None:
        print ('  : -> WARNING: No \'sandbox\' specified, but sandboxing is enabled for %s.' % model.name)
        print ('  : -> Setting sandbox to a default \'sandbox = Sandbox ()\'.')
        sandbox = Sandbox ()

    rng = numpy.random.RandomState (seed ())

    if error is None and errors is None:
        print (" ::WARNING: Error model is not specified, dataset*.dat will be identical to predictions*.dat.")

    dumper.mkdir (outputdir, fresh = True)

    def acquire (prediction, error):
        if error is not None:
            dataset = error.distribution (prediction, parameters) .draw (rng)
            if isinstance (dataset, dict):
                if isinstance (prediction, dict):
                    dataset ['values'] .name = prediction ['values'] .name
                else:
                    dataset ['values'] .name = prediction.name
            else:
                dataset.name = prediction.name
        else:
            dataset = prediction
        return dataset

    # process timeset and snapshots
    if timeset is not None:
        times = numpy.concatenate ((timeset, snapshots))
        times = numpy.unique (times)
        times.sort (kind = 'mergesort')
    else:
        times = snapshots

    for replicate in range (0, replicates if replicates is not None else 1):

        suffix = ('_%s' % replicate) if replicates is not None else ''

        sandbox_replicate = Sandbox.spawn ('-%d' % replicate) if replicates else sandbox
        model.isolate (sandbox_replicate)
        seed_replicate = seed.spawn (replicate)
        model.plant (seed_replicate)
        inputset = inputsets [replicate] if inputsets is not None else inputset
        model.init (inputset, parameters)

        predictions = None
        dataset = None
        error = errors [replicate] if errors is not None else error

        # upcoming snapshot
        upcoming = 0

        # iterate over all times
        for iteration, time in enumerate (times):

            model.plant (seed_replicate.spawn (iteration))

            prediction = model.run (time)
            if isinstance (prediction, dict):
                if predictions is None:
                    predictions = pandas.DataFrame (prediction ['values']).T
                else:
                    predictions.loc [time] = prediction ['values']
                dumper.dump (prediction ['auxiliary'], name = 'predictions%s-auxiliary-%s.dat' % (suffix, str(time)), directory = outputdir)
            else:
                if predictions is None:
                    predictions = pandas.DataFrame (prediction).T
                else:
                    predictions.loc [time] = prediction

            # check if time is the upcoming snapshot in the dataset
            if upcoming < len (snapshots) and time == snapshots [upcoming]:

                print("This is a snapshot.")
                upcoming += 1

                data = acquire (prediction, error)
                if isinstance (data, dict):
                    if dataset is None:
                        dataset = pandas.DataFrame (data ['values']).T
                    else:
                        dataset.loc [time] = data ['values']
                    dumper.dump (data ['auxiliary'], name = 'dataset%s-auxiliary-%s.dat' % (suffix, str(time)), directory = outputdir)
                else:
                    if dataset is None:
                        dataset = pandas.DataFrame (data).T
                    else:
                        dataset.loc [time] = data

        predictions.index.name = 'time'
        dataset.index.name = 'time'
        predictions.to_csv (os.path.join (outputdir, 'predictions%s.dat' % suffix))
        dataset.to_csv (os.path.join (outputdir, 'dataset%s.dat' % suffix))