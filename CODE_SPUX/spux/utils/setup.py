# process setup
import numpy

from .sandbox import Sandbox
from ..io.report import report

def isolate (instance, sandbox):
    """Isolate an instance to a sandbox."""

    # standartized report
    report (instance, 'isolate')

    # make a default sandbox if sandboxing is required
    if instance.sandboxing and sandbox is None:
        if instance.verbosity:
            print ('  : -> WARNING: No \'sandbox\' specified for %s, but sandboxing is enabled.' % instance.name)
            print ('  : -> Setting sandbox to a default \'sandbox = Sandbox ()\' for %s.', instance.name)
        sandbox = Sandbox ()

    # set sandbox verbosity
    if sandbox is not None:
        sandbox.verbosity = instance.verbosity

    # set sandbox
    instance.sandbox = sandbox
    if instance.sandboxing and hasattr (instance, 'executor'):
        instance.executor.sandbox = sandbox

    # report
    if instance.verbosity:
        if hasattr (instance, 'sandboxing') and instance.sandboxing:
            print ("  : -> %s sandbox: %s" % (instance.name, instance.sandbox))

def plant (instance, seed):
    """Plant instance using specified 'seed'."""

    # standartized report
    report (instance, 'plant')

    # set seed and rng
    if seed is not None:
        instance.seed = seed
        # print('in plant with seed: ',instance.seed(),instance.seed.cumulative(),flush=True)
        instance.rng = numpy.random.RandomState (instance.seed ())

    # report
    if instance.verbosity:
        if hasattr (instance, 'seed') and instance.seed is not None:
            print ("  : -> %s seed: %s" % (instance.name, instance.seed))

def setup (instance, verbosity, informative, trace):
    """Do standardized setup on 'instance'."""

    # standartized report
    report (instance, 'setup')

    # set verbosity
    instance.verbosity = verbosity if verbosity >= 0 else 0

    # set informativity
    instance.informative = informative

    # set trace
    instance.trace = trace

    # report
    if instance.verbosity:
        print ("  : -> %s verbosity: %s" % (instance.name, instance.verbosity))
        if hasattr (instance, 'informative'):
            print ("  : -> %s informative: %s" % (instance.name, instance.informative))
        if hasattr (instance, 'trace') and instance.trace is not None:
            print ("  : -> %s trace: %s" % (instance.name, instance.trace))

    # # isolate instance to a sandbox
    # isolate (instance, sandbox, verbosity, trace)

    # # plant instance using the specified seed
    # plant (instance, seed, informative)
