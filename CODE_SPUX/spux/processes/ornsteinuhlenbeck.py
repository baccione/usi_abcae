
import numpy
numpy.seterr(under='warn')

import numba
OrnsteinUhlenbeck_numba_spec = [
    ('tau', numba.float64),
    ('t', numba.float64),
    ('xi', numba.float64),
]

class OrnsteinUhlenbeck (object):
    """Class for Ornstein-Uhlenbeck process."""

    def __init__ (self, tau):

        self.tau = tau

    def init (self, t, xi):

        self.t = t
        self.xi = xi

    def evaluate (self, t, rng):
        """Evaluate Ornstein-Uhlenbeck process at time 't'."""

        if t < self.t:
            assert t >= self.t, ' :: ERROR: OU process assumes evaluation of increasing time sequence, and not %f < %f' % (t, self.t)
        if t == self.t:
            return self.xi
        dt = t - self.t
        try:
            val = numpy.exp (-2 * dt / self.tau)
            V = 1.0 - val
        except FloatingPointError as e:
            try:
                print(":: WARNING: Floating Error in OU process ({}). Setting V to 1 {}".format(val,e))
            except:
                print(":: WARNING: Floating Error in OU process. Setting V to exactly 1.",e)
            V = 1.0
        E = self.xi * numpy.exp (-dt / self.tau)
        self.xi = rng.normal (loc=E, scale=numpy.sqrt (V))
        self.t = t
        return numpy.float64 (self.xi)

    def disass (self, t, rng):
        """return the ingredients to compute it separately by starting from bare noise"""

        if t < self.t:
            assert t >= self.t, ' :: ERROR: OU process assumes evaluation of increasing time sequence, and not %f < %f' % (t, self.t)
        if t == self.t:
            return self.xi
        dt = t - self.t
        try:
            val = numpy.exp (-2 * dt / self.tau)
            V = 1.0 - val
        except FloatingPointError as e:
            try:
                print(":: WARNING: Floating Error in OU process ({}). Setting V to 1 {}".format(val,e))
            except:
                print(":: WARNING: Floating Error in OU process. Setting V to exactly 1.",e)
            V = 1.0
        E = self.xi * numpy.exp (-dt / self.tau)
        self.xi = rng.normal (loc=E, scale=numpy.sqrt (V))
        self.t = t
        return E, numpy.sqrt(V)

