# # # # # # # # # # # # # # # # # # # # # # # # # #
# Utils for MatPlotLib plotting class
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

import matplotlib

# does not need $DISPLAY - must be called before import pylab
matplotlib.use ('Agg')

# figure configuration
matplotlib.rcParams ["figure.max_open_warning"] = 100
matplotlib.rcParams ["savefig.dpi"] = 300

# font configuration
matplotlib.rcParams ["font.size"] = 16
matplotlib.rcParams ["legend.fontsize"] = 14

# === helper routines

import os
import argparse

# setup outputdir
parser = argparse.ArgumentParser ()
parser.add_argument ("--raise", dest = "raise_exceptions", help = "raise exceptions in plotting", action = "store_true")
args, unknown = parser.parse_known_args ()
raise_exceptions = args.raise_exceptions

# create a new solid color which is slighly brighter
def brighten (color, factor=0.7):
    """Create a new solid color which is slighly brighter."""

    if color is None:
        return None
    rgb = list (matplotlib.colors.ColorConverter().to_rgb (color))
    brighter = rgb
    for channel, value in enumerate (rgb):
        brighter [channel] += factor * (1.0 - value)
    return tuple (brighter)

# generate figure name using the format 'figpath/pwd_suffix.extension'
def figname (save, figpath="fig", suffix="", extension="pdf"):
    """Generate figure name using the format 'figpath/pwd_suffix.extension'."""

    if save is not None:
        return save

    if not os.path.exists (figpath):
        os.mkdir (figpath)
    runpath, rundir = os.path.split (os.getcwd ())
    if suffix == "":
        return os.path.join (figpath, rundir + "_" + "." + extension)
    else:
        return os.path.join (figpath, rundir + "_" + suffix + "." + extension)

passed = []
failed = []

def skip_on_error (method):

    def wrapper (*args, **kwargs):

        try:
            method (*args, **kwargs)
            passed.append (method.__name__)
        except Exception as exception:
            if raise_exceptions:
                raise exception
            else:
                print (' :: CAUGHT exception in %s with message:' % method.__name__)
                print ('  : -> %s' % exception)
                print ('  : -> Rerun with "--raise" command line option to raise exceptions.')
                failed.append (method.__name__)

    return wrapper