// # # # # # # # # # # # # # # # # # # # # # # # # #
// Source code for an executable that creates a pool for MPI DPM
// Usage: mpiexec -n <ranks> ./pool <hours>
//
// Jonas Sukys
// Eawag, Switzerland
// jonas.sukys@eawag.ch
// All rights reserved.
// # # # # # # # # # # # # # # # # # # # # # # # # #

#include <mpi.h>
#include <stdlib.h>

int main (int argc, char** argv) {

    // pool timeout in seconds
    int timeout;
    if (argc == 2)
        timeout = atoi (argv[1]) * 60 * 60;
    else
        timeout = 24 * 60 * 60;

    MPI_Init (NULL, NULL);
    MPIX_Comm_rankpool (MPI_COMM_WORLD, NULL, timeout);
    MPI_Finalize ();

    exit (0);