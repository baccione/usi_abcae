# # # # # # # # # # # # # # # # # # # # # # # # # #
# Mpi4pyModel executor class using mpi4py bindings and MPI backend for parallel models
#
# Marco Bacci
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

from .baseclass import Mpi4pyExecutor

from .connectors import utils

import os

class Mpi4pyModel (Mpi4pyExecutor):
    """Class to execute tasks in compliance with legacy MPI implementations."""

    def __init__ (self, workers, connector = None):

        self.workers = workers
        if connector is None:
            connector = utils.select ('auto')
        self.connector = connector

    def bootup (self, peers):
        """Return means of inter-communication along a possible hierarchy of processes."""

        # no bootup for model executor, as it supports only non-persistent worker processes
        port = None
        return port

    def shutdown (self):
        """Finalize executor."""

        pass

    def execute (self, command, directory = None):
        """Request execution of the specified command in parallel."""

        contract = None
        task = None
        dynamic = False
        sandbox = self.sandbox() if self.sandbox is not None else None
        if sandbox is not None and directory is not None:
            sandbox = os.path.join (sandbox, directory)
        if self.verbosity and sandbox is not None:
            print ('Executing application in: %s' % sandbox)
        errorcodes = self.connector.bootup (contract, task, self.resources () [0], self.root, self.verbosity, command, sandbox, dynamic)
        return errorcodes

    def connect (self, command, directory = None):
        """Establish inter-connection with the parallel model to be executed using the specified command."""

        contract = None
        task = None
        dynamic = True
        sandbox = self.sandbox() if self.sandbox is not None else None
        if sandbox is not None and directory is not None:
            sandbox = os.path.join (sandbox, directory)
        if self.verbosity and sandbox is not None:
            print ('Connecting to application in: %s' % sandbox)
        self.port = self.connector.bootup (contract, task, self.resources () [0], self.root, self.verbosity, command, sandbox, dynamic)
        self.workers_comm = self.connector.accept (self.port, self.verbosity)
        return self.workers_comm

    def disconnect (self):
        """Disconnect from the parallel model."""

        self.connector.disconnect (self.workers_comm, self.verbosity)
        self.workers_comm = None
        self.connector.shutdown (self.port, self.verbosity)
