# # # # # # # # # # # # # # # # # # # # # # # # # #
# Base executor class for Mpi4py executors using mpi4py bindings and MPI backend for distributed memory paralellization
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

from mpi4py import MPI
import cloudpickle
MPI.pickle.__init__ (cloudpickle.dumps, cloudpickle.loads)

from ..executor import Executor

class Mpi4pyExecutor (Executor):
    "Base class for Mpi4py executors."

    manager = 1

    @staticmethod
    def address (peers):
        """Return rank in peers communicator"""

        return peers.Get_rank ()

    @staticmethod
    def universe_address ():
        """Return rank in MPI COMM_WORLD"""

        address = MPI.COMM_WORLD.Get_rank ()
        return address

    def info (self):

        thread = MPI.Query_thread()
        if thread == MPI.THREAD_MULTIPLE:
            return 'multiple threads support'
        elif thread == MPI.THREAD_SERIALIZED:
            return 'multiple threads support (serialized)'
        else:
            return 'NO support for multiple threads'

    # abort
    def abort (self):
        """Exit from a parallel execution."""

        if hasattr (self, 'peers'):
            self.peers.Abort ()
        else:
            try:
                MPI.COMM_WORLD.Abort ()
            except:
                assert False, "Program is being terminated."
