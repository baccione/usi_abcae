# # # # # # # # # # # # # # # # # # # # # # # # # #
# Base executor class for serial executors (no parallelization)
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

from ..executor import Executor

class SerialExecutor (Executor):
    """Executor class for serial execution of tasks."""

    manager = 0
    workers = 1

    @staticmethod
    def address (peers):
        """Set rank of serial executor to 0 fictitiously."""

        address = 0
        return address

    @staticmethod
    def universe_address ():
        """Set address of serial executor to None as there is no hierarchy."""

        address = None
        return address

    def bootup (self, peers):
        """Return means of inter-communication along a possible hierarchy of processes."""

        # bind task
        if self.task is not None:
            self.taskroot = self.task.rootcall (peers)
        else:
            self.taskroot = None

        # init task executor
        if hasattr (self.task, 'executor'):
            self.taskport = self.task.executor.init (peers=peers, internal=1)
        else:
            self.taskport = None

        # serial executor simply forwards taskport onwards to other executors
        return self.taskport

    def shutdown (self):
        """Finalize executor."""

        if hasattr (self.task, 'executor'):
            self.task.executor.exit (internal=1)

    # abort - likely to hang if there are others MPI-aware executors
    def abort (self):
        """Exit from a serial execution."""

        import sys
        sys.exit ()
