# # # # # # # # # # # # # # # # # # # # # # # # # #
# SerialModel executor class using serial execution (no parallelization)
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

from .baseclass import SerialExecutor
from ...utils import shell

import os

class SerialModel (SerialExecutor):
    """SerialModel executor class for serial execution of tasks."""

    def execute (self, command, directory = None):

        sandbox = self.sandbox() if self.sandbox is not None else None
        if sandbox is not None and directory is not None:
            sandbox = os.path.join (sandbox, directory)
        if self.verbosity and sandbox is not None:
            print ('Executing application in: %s' % sandbox)
        errcode = shell.execute (command, sandbox, self.verbosity)
        return [errcode]

    def connect (self, command, directory = None):

        return self.execute (command, directory)

    def disconnect (self):

        pass