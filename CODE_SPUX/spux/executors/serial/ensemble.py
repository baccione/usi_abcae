# # # # # # # # # # # # # # # # # # # # # # # # # #
# SerialEnsemble executor class using serial execution (no parallelization)
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

import copy
import numpy

from .baseclass import SerialExecutor
from ...utils.timing import Timing

class SerialEnsemble (SerialExecutor):
    """SerialEnsemble executor class for serial execution of tasks."""

    def connect (self, ensemble, indices):
        """Establish inter-connection with the lower level along the possible hierarchy of Executors."""

        self.timing = Timing (self.informative >= 2)
        self.worker_timing = Timing (self.informative >= 2)
        self.ensemble = ensemble

        # prepare ensemble
        self.prepare (self.ensemble)

        # bind ensemble
        self.ensemble.root = self.taskroot

        # bind ensemble task
        self.ensemble.task.root = self.taskroot

        # bind ensemble task executor
        if hasattr (self.ensemble.task, 'executor'):
           self.ensemble.task.executor.bind (self.taskroot, self.taskport)

        # init ensemble
        self.worker_timing.start ('init')
        self.ensemble.init (indices)
        self.worker_timing.time ('init')

    # disconnect task ensemble
    def disconnect (self):
        """Disconnect taks ensemble."""

        self.ensemble.exit ()
        return [self.worker_timing]

    # report performance
    def report (self):
        """Return execution timings."""

        return self.timing

    # execute ensemble method with specified args and return results
    def call (self, method, args=[], results=1):
        """Execute ensemble method with specified args and return results."""

        self.worker_timing.start (method)
        call = getattr (self.ensemble, method)
        results = call (*args)
        self.worker_timing.time (method)
        if results:
            return results
        else:
            return

    # resample (delete and clone) tasks according to the specified indices
    def resample (self, indices):
        """Init, clone and kill (resample) tasks according to specified 'indices'."""

        # 1. remove all extinct particles (not to be kept: 'kill')
        # 2. stash remaining particles
        # 3. clone particles (to be kept: 'keep'):
        #   3.1. fetch stashed particles according to 'keep_counters'
        #   3.2. copy particles according to 'keep_counters'

        self.worker_timing.start ('resample')

        # container for traffic information
        traffic = {}

        # track sources
        sources = numpy.empty (len (indices), dtype=int)

        # count keep particles for each index
        keep_counters = {}
        for reindex, index in enumerate (indices):
            if index not in keep_counters:
                keep_counters [index] = [reindex]
            else:
                keep_counters [index] .append (reindex)
            sources [reindex] = index

        # 1. remove all extinct particles according to the 'keep_counters'
        self.worker_timing.start ('kill')
        kill = set (self.ensemble.members.keys ()) - set (keep_counters.keys ())
        for index in kill:
            self.ensemble.remove (index)
        traffic ["kill"] = len (kill) / len (indices)
        self.worker_timing.time ('kill')

        self.worker_timing.start ('copy')

        # 2. stash particles that are to be kept
        self.worker_timing.start ('stash')
        stashes = {}
        for index in keep_counters.keys ():
            stashes [index] = self.ensemble.stash (index)
        self.worker_timing.time ('stash')

        # 3. clone particles according to 'keep_counters':

        # 3.1. fetch (rename) stashed particles according to 'keep_counters'
        self.worker_timing.start ('fetch')
        for index, reindices in keep_counters.items ():
            reindex = reindices [0]
            self.ensemble.fetch (stashes [index], reindex)
            del stashes [index]
        self.worker_timing.time ('fetch')

        # 3.2. copy particles according to 'keep_counters'
        copys = 0
        for index, reindices in keep_counters.items ():
            if len (reindices) > 1:
                reindex = reindices [0]
                state = self.ensemble.members [reindex] .save ()
                for reindex in reindices [1:]:
                    self.ensemble.members [reindex] = copy.deepcopy (self.ensemble.task)
                    self.ensemble.isolate (reindex)
                    self.ensemble.members [reindex] .load (state)
                    copys += 1
        traffic ["copy"] = copys / len (indices)

        self.worker_timing.time ('copy')

        self.worker_timing.time ('resample')

        return traffic, sources
