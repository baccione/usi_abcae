# # # # # # # # # # # # # # # # # # # # # # # # # #
# SerialPool executor class using serial execution (no parallelization)
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

from collections.abc import Iterable

from .baseclass import SerialExecutor
from ...utils.timing import Timing

class SerialPool (SerialExecutor):
    """SerialPool executor class for serial execution of tasks."""

    def map (self, functions, parameters=None, args=[], sandboxes=None, seeds=None):
        """Request execution of tasks following a task-dependent logic, and receive results."""

        # prepare timings
        self.timing = Timing (self.informative >= 2)
        timing = Timing (self.informative >= 2)

        # determine operational mode
        if parameters is not None:
            if isinstance (functions, Iterable):
                assert len (parameters) == len (functions)
                mode = 'MFMP' # multiple functions with multiple parameters
            else:
                mode = 'SFMP' # single function with multiple parameters
        else:
            mode = 'MFNP' # multiple functions with no parameters (should be specified in 'args')

        # prepare tasks according to the operational mode
        if mode == 'SFMP':
            function = functions
            self.prepare (function)
            function.root = self.taskroot
            if hasattr (function, 'executor'):
                function.executor.bind (self.taskroot, self.taskport)
            timing.start ('task')
            results = [ None ] * len (parameters)
            for index, ps in enumerate (parameters):
                if sandboxes is not None:
                    function.isolate (sandboxes [index])
                if seeds is not None:
                    function.plant (seeds [index])
                results [index] = function (ps, *args)
            timing.time ('task')
        if mode == 'MFNP':
            for function in functions:
                self.prepare (function)
                function.root = self.taskroot
                if hasattr (function, 'executor'):
                    function.executor.bind (self.taskroot, self.taskport)
            timing.start ('task')
            results = [ None ] * len (functions)
            for index, function in enumerate (functions):
                if sandboxes is not None:
                    function.isolate (sandboxes [index])
                if seeds is not None:
                    function.plant (seeds [index])
                results [index] = function (*args)
            timing.time ('task')
        if mode == 'MFMP':
            for function in functions:
                self.prepare (function)
                function.root = self.taskroot
                if hasattr (function, 'executor'):
                    function.executor.bind (self.taskroot, self.taskport)
            timing.start ('task')
            results = [ None ] * len (parameters)
            for index, function in enumerate (functions):
                if sandboxes is not None:
                    function.isolate (sandboxes [index])
                if seeds is not None:
                    function.plant (seeds [index])
                results [index] = function (parameters [index], *args)
            timing.time ('task')

        timings = [timing]
        return results, timings

    # report performance
    def report (self):
        """Return execution timings."""

        return self.timing