import importlib

def importer (type, backend, workers):
    """Construct the executor of the specified type, backend, and workers."""

    if not workers:
        backend = 'Serial'
    name = '..%s.%s' % (backend.lower(), type.lower())
    module = importlib.import_module (name, __name__)
    Executor = getattr (module, backend + type)
    if not workers:
        executor = Executor ()
    else:
        executor = Executor (workers = workers)
    return executor
