#!/bin/bash

if [ "$#" -ne 1 ]; then
  echo "wrong usage - need one input string."
  exit 1
fi

grep -nr $1 . | grep -v "Binary" | grep -v "tox" | grep -v "html" #| grep ".py"

