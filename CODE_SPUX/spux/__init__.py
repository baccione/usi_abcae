"""
SPUX: Scalable Package for Uncertainty Quantification in X
"""

__version__ = '0.4.0'

import cProfile
import os
import sys
import argparse

from .utils.timer import Timer
from .io import dumper
from .io import formatter

class Framework (object):
    """Computational SPUX environment."""

    connector = None

    def barrier (self, connector = None):
        """Barrier for the SPUX framework (optional for serial run and parallel runs with DPM)."""

        if connector is None:
            from .executors.mpi4py.connectors import utils
            connector = utils.select ('auto')
        self.connector = connector

    def assign (self, component, units=None, reportdir='report'):
        """Assign the main component to the SPUX computational environment."""

        self.component = component
        self.component.reportdir = reportdir

        self.component.units = units

        dumper.config (self.component)

        if units is not None:
            dumper.units (units, verbose = False, directory = reportdir)

    def setup (self, sandbox=None, seed=None, verbosity=2, informative=None, trace=0, outputdir='output', stdoutfile = None):
        """Setup the main component - for more setup functionality, refer to the component's setup (...) method."""

        if stdoutfile is not None:
            print (' :: INFO: Standard output is redirected to:', stdoutfile)
            self.stdout = sys.stdout
            self.stdoutopenfile = open (dumper.prefixname (stdoutfile), 'w')
            sys.stdout = self.stdoutopenfile
        else:
            self.stdoutopenfile = None

        if seed is None:
            from .utils.seed import Seed
            seed = Seed ()

        # report framework setup arguments
        arglist = {}
        arglist ['seed'] = seed ()
        arglist ['informative'] = informative
        arglist ['trace'] = trace
        title = 'Framework setup argument list'
        headers = ['seed', 'informative', 'trace']
        dumper.report (self.component.reportdir, 'setup', arglist, title, [arglist], headers, align = 'c')

        # setup outputdir
        parser = argparse.ArgumentParser ()
        parser.add_argument ("--continue", dest = "cont", help = "continue sampling", action = "store_true")
        args, unknown = parser.parse_known_args ()
        fresh = (not args.cont)
        dumper.mkdir (outputdir, fresh)

        self.component.setup (verbosity, informative, trace)
        self.component.isolate (sandbox)
        self.component.plant (seed)
        self.component.outputdir = outputdir

        if hasattr (self.component, 'configure') and not hasattr (self.component, 'configured'):
            self.component.configure ()

        # setup profile
        parser = argparse.ArgumentParser ()
        parser.add_argument ("--profile", dest = "prof", help = "profile code", action = "store_true")
        args, unknown = parser.parse_known_args ()
        self.profile = args.prof

    def init (self):
        """Init the SPUX computational environment."""

        print (' :: SPUX framwork init.')

        # attach default executors to each component, if needed
        task = self.component
        while task is not None:

            if not hasattr (task, 'executor'):
                task.attach ()

            if hasattr (task, 'task'):
                task = task.task
            else:
                task = None

        # set connectors for each component, if needed
        if self.connector is not None:

            task = self.component
            while task is not None:

                task.executor.connector = self.connector

                if hasattr (task, 'task'):
                    task = task.task
                else:
                    task = None

        # initialize the main executor
        self.component.executor.init (connector = self.connector)

        # initialize profile
        if self.profile:
            self.profile = cProfile.Profile ()
            self.profile.enable ()

        # initialize timer and runtime
        self.timer = Timer ()
        self.timer.start ()

    def exit (self):
        """Finalize the executor on destruction."""

        # report runtimes
        runtime = self.timer.current ()
        runtimes = {}
        runtimes ['total'] = runtime
        runtimes ['serial'] =  runtime #runtime * self.component.executor.resources () [0] ['cumulative'] # EMCEE has no executor bug Oct 18 2021, no idea
        descriptions = {'total' : 'Total runtime', 'serial' : 'Equivalent serial runtime'}
        order = ['total', 'serial']
        headers = ['Timer', 'Value']
        entries = [{'Timer' : descriptions [key], 'Value' : formatter.timestamp (runtimes [key], precise=True, expand=True)} for key in order]
        title = 'Runtimes'
        dumper.report (self.component.reportdir, 'runtimes', runtimes, title, entries, headers)

        # dump profile
        if self.profile:
            timer = Timer ()
            timer.start ()
            self.profile.disable ()
            self.profile.create_stats ()
            self.profile.dump_stats (os.path.join (self.component.outputdir, 'profile.pstats'))
            if self.component.verbosity >= 2:
                print (' :: INFO: Profile dump took: ', formatter.timestamp (timer.current (format=1)))

        try:
            self.component.executor.exit ()
        except:
            assert False, "We finished, maybe even correctly, but you never know..."

        if self.stdoutopenfile is not None:
            sys.stdout = self.stdout
            self.stdoutopenfile.close ()

        print (' :: SPUX framwork exit.')

framework = Framework ()
