# # # # # # # # # # # # # # # # # # # # # # # # # #
# Base likelihood class
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

from ..utils.setup import setup, isolate, plant
from ..utils.seed import Seed
from ..executors.utils import importer

class Likelihood (object):
    """Base class for objects that can be classified as likelihoods in a mathematical perspective."""

    @property
    def name (self):

        return type(self).__name__

    @property
    def component (self):

        return 'Likelihood'

    # assign required components to likelihood
    def assign (self, model, error=None, dataset=None, inputset=None, timeset=None, auxiliary=None):
        """Assign required components to likelihood."""

        self.model = model
        self.error = error
        self.dataset = dataset
        self.inputset = inputset
        self.timeset = timeset
        self.auxiliary = auxiliary

        self.sandboxing = self.model.sandboxing
        self.task = self.model

        self.assigned = True

    # attach an executor
    def attach (self, executor = None, workers = None, backend = 'Mpi4py'):
        """Attach an executor to likelihood."""

        if executor is not None:
            self.executor = executor
        else:
            self.executor = importer ('Ensemble', backend, workers)
        self.executor.setup (self)
        self.executor.capabilities (['connect', 'call', 'resample', 'disconnect', 'report'])

    # setup
    def setup (self, verbosity=1, informative=1, trace=1):
        """Setup."""

        setup (self, verbosity, informative, trace)

    # isolate to a 'sandbox'
    def isolate (self, sandbox=None):
        """Isolate to a sandbox."""

        isolate (self, sandbox)

    # plant using specified 'seed'
    def plant (self, seed=Seed()):
        """Plant using specified 'seed'."""

        plant (self, seed)

    # configure feedback
    def configure (self, feedback = None):

        self._feedback = feedback

        self.configured = 1