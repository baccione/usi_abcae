# # # # # # # # # # # # # # # # # # # # # # # # # #
# Particle Filter likelihood class for a stochastic model
# Particle filtering based on
# Kattwinkel & Reichert, EMS 2017.
# Implementation described in
# Sukys & Kattwinkel, Proceedings of ParCo 2017,
# Advanced Parallel Computing, IOS Press, 2018.
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

import sys # noqa: F401
import scipy
import numpy
import functools

from ..utils.timing import Timing
from ..utils import transforms
from ..utils import evaluations
from .likelihood import Likelihood
from ..ensembles.particles import Particles

numpy.seterr(divide='ignore')

import warnings #what happens in PF, stays is PF
warnings.filterwarnings ('ignore', r'All-NaN (slice|axis) encountered')
warnings.filterwarnings ('ignore', 'Mean of empty slice')
warnings.filterwarnings ('ignore', 'underflow encountered in exp')

class PF (Likelihood):
    """Particle Filter likelihood class for a stochastic model.

    Operates ensemble of particles.
    """

    # constructor
    def __init__ (self, particles=10, threshold=-2, accuracy=0.15, margin=0.05, stride=1):

        #cwrank = MPI.COMM_WORLD.Get_rank ()
        #print("within __init__ of likelihoods/pf.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        self.particles = particles
        try:
            self.particles_min = particles [0]
            self.particles_max = particles [1]
            self.adaptive = True
        except:
            self.particles_min = particles
            self.particles_max = particles
            self.adaptive = False
        self.particles_set = self.particles_min
        self.threshold = threshold
        self.accuracy = accuracy
        self.margin = margin
        self.stride = stride
        self.factor = 2
        self.log = 1

        # initialize variance to NaN
        self.variance = float ('nan')

        #print("done with __init__ of likelihoods/pf.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

    @property
    def evaluations (self):

        #cwrank = MPI.COMM_WORLD.Get_rank ()
        #print("within evaluations of likelihoods/pf.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        result = evaluations.construct (self, self.particles_max)

        #print("done with evaluations of likelihoods/pf.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        return result

    # attach an executor
    def attach (self, executor = None, workers = None, backend = 'Mpi4py'):
        """Attach an executor."""

        #cwrank = MPI.COMM_WORLD.Get_rank ()
        #print("within attach of likelihoods/pf.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        Likelihood.attach (self, executor, workers, backend)

        if self.particles_min < self.executor.workers:
            print (' :: WARNING: The minimum number of particles should NOT be smaller than the number of workers in the attached executor.')
            print (' :-> Correcting the value for the minimum number of particles.')
            self.particles_min = self.executor.workers
        if self.particles_max < self.executor.workers:
            print (' :: WARNING: The maximum number of particles should NOT be smaller than the number of workers in the attached executor.')
            print (' :-> Correcting the value for the maximum number of particles.')
            self.particles_max = self.executor.workers

        #print("done with attach of likelihoods/pf.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

    # feedback
    def feedback (self, info):
        """Feedback method to be executed by the sampler (self.* includes only constructor fields)."""

        #cwrank = MPI.COMM_WORLD.Get_rank ()
        #print("within feedback of likelihoods/pf.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        particles = float ('nan')
        if info is not None and not numpy.isnan (info ['avg_deviation']):
            if info ['fitscore'] > self.threshold:
                particles = info ['particles']
                if info ['avg_deviation'] > min (numpy.log (1 + self.accuracy + self.margin), - numpy.log (1 - self.accuracy - self.margin)):
                    particles = info ['particles'] * self.factor
                if info ['avg_deviation'] < min (numpy.log (1 + self.accuracy - self.margin), - numpy.log (1 - self.accuracy + self.margin)):
                    particles = max (2, int ( numpy.ceil (info ['particles'] / self.factor)))

        #print("done with feedback of likelihoods/pf.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        return particles

    # redraw particles based on errors
    def redraw (self, logerrors, logscaling, particles):
        """Redraw particles based on errors."""

        #cwrank = MPI.COMM_WORLD.Get_rank ()
        #print("within redraw of likelihoods/pf.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        # compute indices
        indices = numpy.arange (particles)

        # compute probabilities for discrete distribution
        nans = numpy.isnan (logerrors)
        if logscaling == float ('-inf'):
            if self.verbosity:
                print(" :: WARNING: The sum of all particle errors is 0 (i.e., exp (-inf)).")
                print("  : -> This issue should have been mitigated already earlier.")
                print("  : -> Assigning equal probabilities to all particles with non-NaN error.")
            probabilities = numpy.where (nans, 0, 1.0 / numpy.sum (~nans))
        else:
            probabilities = numpy.where (nans, 0, numpy.exp (logerrors - logscaling))

        # sample from discrete distribution
        choice = self.rng.choice (indices, size=particles, p=probabilities)

        # compute redraw rate
        redraw = len(set(choice)) / float(len(indices))

        #print("done with redraw of likelihoods/pf.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        return choice, redraw

    # evaluate/approximate likelihood of the specified parameters
    def __call__ (self, parameters):
        """Evaluate estimate of the likelihood for the specified parameters."""

        #cwrank = MPI.COMM_WORLD.Get_rank ()
        #print("within __call__ of likelihoods/pf.py") #, cwrank: ",cwrank)
        #sys.stdout.flush()

        # verbose output
        if self.verbosity >= 2:
            print ("PF likelihood parameters:")
            print (parameters)
            print ("PF likelihood feedback:")
            print (self._feedback)

        # start global timer
        timing = Timing (self.informative >= 2)
        timing.start ('evaluate')

        # adaptive number of particles
        if not self.adaptive or self._feedback is None or numpy.isnan (self._feedback):
            particles = self.particles_min
        else:
            particles = int (self._feedback)
            if particles > self.particles_max:
                particles = self.particles_max
            if particles < self.particles_min:
                particles = self.particles_min
        if self.verbosity >= 2:
            print ("PF likelihood particles:")
            print (particles)

        # construct ensemble task
        ensemble = Particles (self.model, self.inputset, parameters, self.error)

        # setup ensemble
        ensemble.setup (self.sandbox, self.seed, self.verbosity - 2, self.informative, self.trace)

        # initialize task ensemble in executor
        self.executor.connect (ensemble, indices = numpy.arange (particles))

        # initialize indices container
        indices = { 'prior' : {}, 'posterior' : {} }

        # initialize predictions container
        predictions = { 'prior' : {}, 'posterior' : {}, 'unique' : {} }

        # initialize weights container
        weights = {}

        # initialize errors containter
        errors = { 'prior' : {}, 'posterior' : {}, 'unique' : {} }

        # initialize peaks container
        peaks = {}

        # initialize fitscores container
        fitscores = {}
        observation_dimensions = self.error.dimensions if hasattr (self.error, 'dimensions') else len (self.dataset.columns)

        # initialize estimates container
        estimates = {}

        # initialize quality control container
        variances = {}

        # initialize container for source indices
        sources = { 'all' : {}, 'unique' : {} }

        # initialize traffic measurements container
        traffic = {}

        # initialize particle redraw rate measurements container
        redraw = {}

        # process self.timeset and self.dataset.index
        if self.timeset is not None:
            times = numpy.concatenate ((self.timeset, self.dataset.index))
            times = numpy.unique (times)
            times.sort (kind = 'mergesort')
        else:
            times = list (self.dataset.index)

        # container for pending snaphots
        snapshots = []

        # container for pending times
        pending = []

        # upcoming snapshot
        upcoming = 0

        # iterate over all times
        for time in times:

            if self.verbosity:
                print("Time: ", time)

            # check if time is the upcoming snapshot in the dataset
            if upcoming < len (self.dataset.index) and time == self.dataset.index [upcoming]:
                snapshot = time
                snapshots.append (time)
                upcoming += 1
                if self.verbosity:
                    print("This is a snapshot in the dataset")
            else:
                snapshot = None

            # set resampling flag
            resampling = (len (snapshots) == self.stride or snapshot == self.dataset.index [-1])
            if not resampling:
                pending.append (time)
            if self.verbosity:
                print("Resampling: %s" % resampling)

            # run particles (models)
            if self.verbosity >= 2:
                print("Running particles (%s models)..." % self.task.name)

            # print('in pf, commanding model to run at time: ',time)
            predictions ['prior'] [time] = transforms.pandify (self.executor.call ('run', args = [time]))
            if self.verbosity >= 2:
                print ("Prior (non-resampled) predictions:")
                print (predictions ['prior'] [time])

            if snapshot is not None:

                # load auxiliary dataset entry
                if self.verbosity >= 2 and self.auxiliary is not None:
                    print("Loading auxiliary dataset entry...")
                auxiliary = self.auxiliary (snapshot) if self.auxiliary is not None else None

                # compute errors
                if self.verbosity >= 2:
                    print("Computing errors...")
                errors ['prior'] [snapshot] = transforms.numpify (self.executor.call ('errors', args = [self.dataset.loc [snapshot], auxiliary]))
                if self.verbosity >= 2:
                    print("Prior (non-resampled) errors", errors ['prior'] [snapshot])

                # retrieve peaks of errors
                peaks [snapshot] = transforms.numpify (self.executor.call ('peaks'))

                # if all errors are NaNs - no further filtering is possible
                if all (numpy.isnan (errors ['prior'] [snapshot])):
                    estimates [snapshot] = float ('nan')
                    variances [snapshot] = float ('nan')
                    if self.verbosity:
                        print (" :: WARNING: NaN estimate in the PF likelihood.")
                        print ("  : -> All error model distribution densities are NaN.")
                        print ("  : -> Stopping here and returning the infos as they are.")
                    successful = False
                    break

                # if all errors are '-inf's - no further filtering is possible
                if all (errors ['prior'] [snapshot] == float ('-inf')):
                    estimates [snapshot] = float ('-inf') if self.log else 0
                    variances [snapshot] = float ('nan')
                    if self.verbosity:
                        print (" :: WARNING: '-inf' (i.e., log (0)) estimate in the PF likelihood.")
                        print ("  : -> All error model distribution densities are '-inf' (i.e., log (0)).")
                        print ("  : -> Stopping here and returning the infos as they are.")
                    successful = False
                    break

                # compute (log-)error estimates for the current snapshot
                nonnan = errors ['prior'] [snapshot] [~ numpy.isnan (errors ['prior'] [snapshot])]
                p = len (nonnan)
                logmean = scipy.special.logsumexp (nonnan) - numpy.log (p)
                mean = numpy.exp (logmean)
                estimates [snapshot] = logmean if self.log else mean
                if self.verbosity:
                    print ("Estimated %slikelihood at snapshot %s: %1.1e" % ('log-' if self.log else '', str (snapshot), estimates [snapshot]))

                # if estimate is '-inf' (or 0 for self.log = 0) - no further filtering is possible
                if (self.log and estimates [snapshot] == float ('-inf')) or (not self.log and estimates [snapshot] == 0):
                    variances [snapshot] = float ('nan')
                    if self.verbosity:
                        print (" :: WARNING: '-inf' (i.e., log (0)) estimate in the PF likelihood.")
                        print ("  : -> Stopping here and returning the infos as they are.")
                    successful = False
                    break

                # estimate variance of the log-likelihood estimate for the current snapshot,
                # using 1st order Taylor approximation for the log-likelihood case:
                # https://stats.stackexchange.com/questions/57715/expected-value-and-variance-of-loga#57766
                if p == 1 or mean == 0:
                    variances [snapshot] = float ('nan')
                else:
                    logscaling = numpy.max (nonnan)
                    if not numpy.isfinite (logscaling):
                        logscaling = 0
                    variance = numpy.var (numpy.exp (nonnan - logscaling), ddof=1)
                    if variance != 0:
                        deviation = numpy.exp (0.5 * (numpy.log (variance) + 2 * logscaling - numpy.log (p)))
                    else:
                        deviation = 0
                    variances [snapshot] = (deviation / mean) ** 2
                if self.verbosity:
                    print ("Estimated variance of log-likelihood at snapshot %s: %1.1e" % (str (snapshot), variances [snapshot]))

                if resampling:
                    # compute errors (cumulative across pending snapshots for stride > 1)
                    cum_errors = functools.reduce (numpy.add, [errors ['prior'] [s] for s in snapshots])
                    logscaling = scipy.special.logsumexp (cum_errors [~ numpy.isnan (cum_errors)])
                    if self.verbosity >= 2 and self.stride > 1:
                        print ("Prior cumulative errors:")
                        print (cum_errors)

                    # if all errors are NaNs - no further filtering is possible
                    if all (numpy.isnan (cum_errors)):
                        if self.verbosity:
                            print (" :: WARNING: NaN estimate in the PF likelihood.")
                            print ("  : -> All cumulative error model distribution densities are NaN.")
                            print ("  : -> Stopping here and returning the infos as they are.")
                        successful = False
                        break

                    # if all errors are '-inf's - no further filtering is possible
                    if all (cum_errors == float ('-inf')):
                        if self.verbosity:
                            print (" :: WARNING: '-inf' (i.e., log (0)) estimate in the PF likelihood.")
                            print ("  : -> All cumulative error model distribution densities are '-inf' (i.e., log (0)).")
                            print ("  : -> Stopping here and returning the infos as they are.")
                        successful = False
                        break

                    # redraw particles based on errors (cumulative across pending snapshots for stride > 1)
                    indices ['posterior'] [snapshot], redraw [snapshot] = self.redraw (cum_errors, logscaling, particles)
                    if self.verbosity >= 2:
                        print ("Posterior (redrawn) indices (arbitrary order, particles not yet resampled):")
                        print (indices ['posterior'] [snapshot])
                        print("Cumulative errors: ",cum_errors)

            # advance ensemble state to next iteration (do not gather results)
            self.executor.call ('advance', results=0)

            if resampling:
                # resample (delete and clone) particles and balance ensembles in the executor and record resulting traffic
                traffic [snapshot], sources ['all'] [snapshot] = self.executor.resample (indices ['posterior'] [snapshot])
                # set identity sources for all other pending times
                for t in pending:
                    sources ['all'] [t] = numpy.arange (particles)
                if self.verbosity >= 2:
                    print ("Posterior (resampled) particles sources:")
                    print (sources ['all'] [snapshot])
                    print ("Traffic:")
                    print (traffic [snapshot])

                # construct posterior predictions indexed according to sources
                predictions ['posterior'] [snapshot] = predictions ['prior'] [snapshot] .loc [sources ['all'] [snapshot]]
                # # also do this for all other snapshots since last resampling
                # for s in snapshots [:-1]:
                #     predictions ['posterior'] [s] = predictions ['prior'] [s] .loc [sources ['all'] [s]]
                # also do this for all pending times (including other snapshots since last resampling)
                for t in pending:
                    ### this is to save history with overwriting - tested with debug in parallel
                    predictions ['posterior'] [t] = predictions ['prior'] [t] .loc [sources ['all'] [snapshot]]
                    ### this is to save history without overwriting
                    #predictions ['posterior'] [t] = predictions ['prior'] [t] .loc [sources ['all'] [t]] #baccione

                # unique indices and counts for posterior predictions and sources (compression to reduce 'info' size)
                # TODO: compute isunique from sources instead, and use posterior indexing for posterior samples?
                # Otherwise sources are not really needed, since they are in predictions indices - however,
                # this is not sufficient for tracking trajectories only from 'unique' since posterior indices are lost.
                isunique = ~(predictions ['posterior'] [snapshot] .index.duplicated (keep = 'first'))
                predictions ['unique'] [snapshot] = predictions ['posterior'] [snapshot] .loc [isunique]
                counts = [numpy.sum (predictions ['posterior'] [snapshot] .index == index) for index in predictions ['unique'] [snapshot] .index]
                weights [snapshot] = numpy.array (counts)
                # # also do this for all other snapshots since last resampling
                # for s in snapshots [:-1]:
                #     predictions ['unique'] [s] = predictions ['posterior'] [s] .loc [isunique]
                sources ['unique'] [snapshot] = sources ['all'] [snapshot] [isunique]
                errors ['unique'] [snapshot] = errors ['prior'] [snapshot] [isunique] # prior == posterior for isunique

                # also do this for all pending times (including other snapshots since last resampling)
                for t in pending:
                    predictions ['unique'] [t] = predictions ['posterior'] [t] .loc [isunique]
                    weights [t] = weights [snapshot]
                    if errors is not None:
                        if 'unique' in errors:
                            if t in errors ['unique']:
                                errors ['unique'] [t] = errors ['prior'] [t] .loc [isunique] # prior == posterior for isunique
                                # sources ['unique'] [t] = sources ['all'] [t] .loc [isunique] - not needed - all are unique?

                if self.verbosity >= 2:
                    print ("Posterior (resampled) predictions:")
                    print (predictions ['posterior'] [snapshot])
                    print ("Posterior (resampled) unique predictions:")
                    print (predictions ['unique'] [snapshot])
                    print ("Posterior (resampled) unique predictions weights:")
                    print (weights [snapshot])

                # construct posterior errors indexed according to sources
                for s in snapshots:
                    errors ['posterior'] [s] = errors ['prior'] [s] [sources ['all'] [snapshot]]
                if self.verbosity >= 2:
                    print("Posterior (resampled) errors", errors ['posterior'] [snapshot])

                # compute fitscore: the log of the average (over particles)
                # relative (normalized with respect to maximum pdf and the dimensions of the observations)
                # posterior errors
                for s in snapshots:
                    try:
                        relative_errors = errors ['posterior'] [s] - peaks [s] [sources ['all'] [snapshot]]
                    except: #for some pythonic reasons -inf - -inf does not seem to work
                        print("peaks [s] [sources ['all'] [snapshot]]: ",peaks [s] [sources ['all'] [snapshot]])
                        assert False, ":: Fatal: This should not happen anymore. Buggone."
                        whrerr = numpy.where(numpy.isinf(errors ['posterior'] [s]))
                        whrpks = numpy.where(numpy.isinf(peaks [s] [sources ['all'] [snapshot]] ))
                        assert whrerr in whrpks, ":: Fatal: Unkown case. This is a bug."
                        #
                        curl = len( errors ['posterior'] [s] )
                        assert curl == len( peaks [s] [sources ['all'] [snapshot]] ), ":: Fatal: wrong lengths. This is a bug"
                        relative_errors = numpy.zeros(curl)
                        for i in range(0,curl):
                            if i in whrerr and i in whrpks:
                                relative_errors [i] = numpy.nan
                            else:
                                relative_errors [i] = errors ['posterior'] [s] [i] - peaks [s] [sources ['all'] [snapshot]] [i]
                        #
                    # TODO: use scipy.special.logsumexp () to average in the linear domain?
                    fitscores [s] = numpy.nanmean (relative_errors / observation_dimensions)

                # reset snapshots and pending lists
                snapshots = []
                pending = []

            # all steps were successful
            successful = True

        # finalize task ensemble in executor
        timings = self.executor.disconnect ()

        # append executor timing
        timing += self.executor.report ()

        # compute estimated (log-)likelihood as the product of estimates from all snapshots
        if self.log:
            estimate = numpy.sum (list (estimates.values()))
        else:
            estimate = numpy.prod (list (estimates.values()))

        # compute estimated variance of the estimated log-likelihood
        variance = numpy.sum (list (variances.values()))

        # compute estimated average std. deviation across estimated log-errors
        avg_deviation = numpy.mean (numpy.sqrt (list (variances.values())))

        # compute fitscore: the log of the average (over snapshots and particles)
        # relative (normalized with respect to maximum pdf and the dimensions of the observations)
        # posterior errors
        # TODO: use scipy.special.logsumexp () to average in the linear domain?
        fitscore = numpy.nanmean (list (fitscores.values ()))

        if self.verbosity:
            print ("Estimated %slikelihood: %1.1e" % ('log-' if self.log else '', estimate))
            print ("Estimated variance of log-likelihood: %1.1e" % variance)

        # TODO: also perform a cleanup of predictions and sources
        # that survived intermediate resamplings
        # but their ancestors did not make it all the way to the end of the dataset

        MAP = None
        # compute MAP estimate
        if not successful:
            MAP = None
        else:
            cumulative = errors ['posterior'] [self.dataset.index [0]] [:]
            for snapshot in self.dataset.index [1:]:
                if self.verbosity >= 2:
                    print ('Cumulative posterior errors (tracked by sources) for snapshot %s:' % str (snapshot))
                    print (cumulative [sources ['all'] [snapshot]])
                cumulative = errors ['posterior'] [snapshot] + cumulative [sources ['all'] [snapshot]]
            MAP_indices = {}
            MAP_predictions = {}
            last = self.dataset.index [-1]
            MAP_indices [last] = sources ['all'] [last] [numpy.argmax (cumulative)]
            MAP_predictions [last] = predictions ['unique'] [last] .loc [MAP_indices [last]]
            # for snapshot in reversed (self.dataset.index [:-1]):
            #     MAP_indices [snapshot] = sources ['all'] [snapshot] [MAP_indices [last]]
            #     MAP_predictions [snapshot] = predictions ['unique'] [snapshot] .loc [MAP_indices [snapshot]]
            #     last = snapshot
            snapshots = list (self.dataset.index [:-1])
            for time in reversed (times [:-1]):
                if len (snapshots) > 0 and time == snapshots [-1]:
                    snapshot = snapshots [-1]
                    MAP_indices [snapshot] = sources ['all'] [snapshot] [MAP_indices [last]]
                    MAP_predictions [snapshot] = predictions ['unique'] [snapshot] .loc [MAP_indices [snapshot]]
                    last = snapshot
                    del snapshots [-1]
                else:
                    MAP_indices [time] = MAP_indices [last]
                    MAP_predictions [time] = predictions ['unique'] [time] .loc [MAP_indices [time]]
            MAP = { 'indices' : MAP_indices, 'predictions' : MAP_predictions, 'error' : numpy.max (cumulative) }

        # measure evaluation runtime and timestamp
        timing.time ('evaluate')

        # information includes predictions, errors, estimates and their variances, MAP trajectory
        # resampling sources, redraw rate, used up communication traffic, and timings
        info = {}
        info ["particles"] = particles
        info ["predictions"] = predictions ['unique'] # I should keep this here for my Rpp
        info ["weights"] = weights
        info ["best"] = MAP
        info ["variance"] = variance
        info ["fitscore"] = fitscore
        info ["avg_deviation"] = avg_deviation
        info ["redraw"] = redraw
        info ["successful"] = successful
        #info ["sources"] = sources ['unique'] #useless
        info ["sources"] = sources ['all']
        info ["errors"] = errors ['unique']
        if self.informative >= 1:
            info ["traffic"] = traffic
        if self.informative >= 2:
            #info ["sources"] = sources ['all']
            info ["predictions_prior"] = predictions ['prior']
            info ["errors_prior"] = errors ['prior']
            info ["timings"] = timings #this will be split by jonas
            info ["timing"] = timing   #this will be split by jonas
        if self.informative >= 3:
            info ["estimates"] = estimates
            info ["variances"] = variances
            info ["indices"] = indices ['posterior']
            info ["predictions_posterior"] = predictions ['posterior']
            info ["errors_posterior"] = errors ['posterior']

        # return estimated likelihood and its info
        return estimate, info #this will be changed by jonas, I write this comment to generate a conflict
