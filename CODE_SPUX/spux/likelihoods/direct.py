# # # # # # # # # # # # # # # # # # # # # # # # # #
# Direct likelihood class for a determistic model
#
# Jonas Sukys
# Eawag, Switzerland
# jonas.sukys@eawag.ch
# All rights reserved.
# # # # # # # # # # # # # # # # # # # # # # # # # #

import numpy

from ..utils.timing import Timing
from ..utils import evaluations
from ..utils import transforms
from .likelihood import Likelihood
from ..ensembles.particles import Particles

class Direct (Likelihood):
    """Likelihood class for a deterministic model, where likelihood is computed directly from the specified error model."""

    # constructor
    def __init__ (self):

        self.log = 1

    @property
    def evaluations (self):

        return evaluations.construct (self, 1)

    # evaluate likelihood of the specified parameters
    def __call__ (self, parameters):
        """Evaluate likelihood for the specified parameters."""

        # verbose output
        if self.verbosity >= 2:
            print ("Direct likelihood parameters:")
            print (parameters)

        # start global timer
        timing = Timing (self.informative >= 2)
        timing.start ('evaluate')

        # ensemble consists only of a single model
        ensemble = Particles (self.model, self.inputset, parameters, self.error)
        ensemble.setup (self.sandbox, self.seed, self.verbosity - 1, self.informative, self.trace)

        # initialize task ensemble in executor
        self.executor.connect (ensemble, indices=[0])

        # initialize predictions container
        predictions = {}

        # initialize peaks container
        peaks = {}

        # initialize fitscores container
        fitscores = {}
        observation_dimensions = self.error.dimensions if hasattr (self.error, 'dimensions') else len (self.dataset.columns)

        # initialize estimates container
        estimates = {}

        # initial estimate is assumed to be successful
        successful = True

        # process self.timeset and self.dataset.index
        if self.timeset is not None:
            times = numpy.concatenate ((self.timeset, self.dataset.index))
            times = numpy.unique (times)
            times.sort (kind = 'mergesort')
        else:
            times = list (self.dataset.index)

        # upcoming snapshot
        upcoming = 0

        # iterate over all times
        for time in times:

            if self.verbosity:
                print("Time", time)

            # check if time is the upcoming snapshot in the dataset
            if upcoming < len (self.dataset.index) and time == self.dataset.index [upcoming]:
                snapshot = time
                upcoming += 1
                if self.verbosity:
                    print("This is a snapshot in the dataset")
            else:
                snapshot = None

            # model
            if self.verbosity >= 2:
                print("Running %s model..." % self.task.name)
            predictions [time] = self.executor.call ('run', args = [time]) [0]

            if snapshot is not None:

                # load auxiliary dataset entry
                if self.verbosity >= 2:
                    print("Loading auxiliary dataset entry...")
                auxiliary = self.auxiliary (snapshot) if self.auxiliary is not None else None

                # compute estimate
                if self.verbosity >= 2:
                    print("Computing estimate...")
                estimates [snapshot] = self.executor.call ('errors', args = [self.dataset.loc [snapshot], auxiliary]) [0]

                # retrieve peaks of errors
                peaks [snapshot] = transforms.numpify (self.executor.call ('peaks'))

                if self.verbosity:
                    print ("Estimated %slikelihood: %1.1e" % ('log-' if self.log else '', estimates [snapshot]))

                # if estimator failed - no further simulation makes sense
                if numpy.isnan (estimates [snapshot]):
                    successful = False
                    if self.verbosity:
                        print (" :: WARNING: NaN estimate in the Direct likelihood.")
                        print ("  : -> stopping here and returning the infos as they are.")
                    break

                # if estimator failed - no further simulation makes sense
                if estimates [snapshot] == float ('-inf'):
                    successful = False
                    if self.verbosity:
                        print (" :: WARNING: '-inf' (i.e., log (0)) estimate in the Direct likelihood.")
                        print ("  : -> stopping here and returning the infos as they are.")
                    break

                # compute fitscore: the log of the estimates [snapshot],
                # normalized with respect to maximum pdf and the dimensions of the observations
                # TODO: use scipy.special.logsumexp () to average in the linear domain?
                fitscores [snapshot] = (estimates [snapshot] - peaks [snapshot]) / observation_dimensions

            # advance ensemble state to next iteration (do not gather results)
            self.executor.call ('advance', results=0)

        # finalize task ensemble in executor
        timings = self.executor.disconnect ()

        # append executor timing
        timing += self.executor.report ()

        # compute estimated (log-)likelihood as the product of estimates from all snapshots
        if self.log:
            estimate = numpy.sum (list (estimates.values()))
        else:
            estimate = numpy.prod (list (estimates.values()))

        # compute fitscore: the log of the average (over snapshots) estimates,
        # normalized with respect to maximum pdf and the dimensions of the observations
        # TODO: use scipy.special.logsumexp () to average in the linear domain?
        fitscore = numpy.nanmean (list (fitscores.values ()))

        if self.verbosity:
            print ("Estimated %slikelihood: %1.1e" % ('log-' if self.log else '', estimate))

        # measure evaluation runtime and timestamp
        timing.time ('evaluate')

        # information includes predictions, errors, estimates, and timings
        info = {}
        info ["predictions"] = predictions
        info ["fitscore"] = fitscore
        info ["successful"] = successful
        if self.informative >= 1:
            info ["timing"] = timing
            info ['timings'] = timings
        if self.informative >= 3:
            info ["estimates"] = estimates

        # return estimated likelihood and its info
        return estimate, info
